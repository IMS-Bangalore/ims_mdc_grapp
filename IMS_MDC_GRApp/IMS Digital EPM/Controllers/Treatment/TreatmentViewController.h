//
//  TreatmentViewController.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 10/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ComponentGroup.h"
#import "CheckBox.h"
#import "PredictiveTextField.h"
#import "RadioButtonGroup.h"
#import "CustomPickerView.h"

#import "CustomTextArea.h"
#import "CustomTextField.h"
#import "Treatment.h"
#import "CustomLabel.h"
#import "TherapyTypeChangeViewController.h"
#import "SpecialitySelectionViewController.h"
#import "RadioButton.h"
#import "CustomSegmentedControl.h"
#import "ChangesControlProtocol.h"
#import "ModalControllerProtocol.h"
#import "MultipleChoiceViewController.h"

@class TreatmentViewController;
@protocol TreatmentViewControllerDelegate <ModalControllerProtocol>
- (void)treatmentViewController:(TreatmentViewController*)controller willPresentPopover:(UIPopoverController*)popoverController;
- (void)treatmentViewController:(TreatmentViewController*)controller isReady:(BOOL)ready;

@end

@interface TreatmentViewController : UIViewController <PredictiveTextFieldDelegate, RadioButtonGroupDelegate, CheckBoxDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UITextViewDelegate, UIPopoverControllerDelegate, SpecialitySelectionViewControllerDelegate, TherapyTypeChangeViewControllerDelegate>

// Delegate
@property (nonatomic, assign) id<TreatmentViewControllerDelegate> delegate;
@property (nonatomic, retain) id<ChangesControlProtocol> changesControlProtocol;

// Entity injection
@property (nonatomic, retain) Treatment *treatment;
@property (nonatomic, assign, getter=isEnabled) BOOL enabled;

// Medicament
@property (retain, nonatomic) IBOutlet ComponentGroup *medicamentGroup;
@property (retain, nonatomic) IBOutlet UISegmentedControl *medicamentFilterSegmentControl;
@property (retain, nonatomic) IBOutlet CheckBox *medicamentFrequentCheckBox;
@property (retain, nonatomic) IBOutlet PredictiveTextField *medicamentTextField;
// Presentation
@property (retain, nonatomic) IBOutlet ComponentGroup *presentationGroup;
@property (retain, nonatomic) IBOutlet PredictiveTextField *presentationTextField;
@property (retain, nonatomic) IBOutlet UITableView *presentationTableView;
// Desired effect
@property (retain, nonatomic) IBOutlet ComponentGroup *effectGroup;
@property (retain, nonatomic) IBOutlet PredictiveTextField *effectTextField;
// Units
@property (retain, nonatomic) IBOutlet ComponentGroup *unitsGroup;
@property (retain, nonatomic) IBOutlet CheckBox *onDemandCheckBox;
@property (retain, nonatomic) IBOutlet CustomLabel *unitsLabel;
@property (retain, nonatomic) IBOutlet CustomPickerView *unitsPickerView;
@property (retain, nonatomic) IBOutlet CustomPickerView *frequencyPickerView;
@property (retain, nonatomic) IBOutlet CheckBox *uniqueDoseCheckBox;
// Treatment length
@property (retain, nonatomic) IBOutlet ComponentGroup *treatmentLengthGroup;
@property (retain, nonatomic) IBOutlet CheckBox *longTermCheckBox;
@property (retain, nonatomic) IBOutlet CustomPickerView *treatmentLengthPickerView;
// Dosage
@property (retain, nonatomic) IBOutlet ComponentGroup *dosageGroup;
@property (retain, nonatomic) IBOutlet CustomTextField *dosageCountTextField;
@property (retain, nonatomic) IBOutlet CustomTextArea *dosageCommentTextArea;
// Therapy choice
@property (retain, nonatomic) IBOutlet ComponentGroup *therapyChoiceGroup;
@property (retain, nonatomic) IBOutlet RadioButtonGroup *therapyChoiceButtonGroup;
@property (retain, nonatomic) IBOutlet CustomLabel *therapyChoiceSpecialistLabel;
@property (retain, nonatomic) IBOutlet RadioButton *therapyChoiceSpecialistButton;
// Therapy type
@property (retain, nonatomic) IBOutlet ComponentGroup *therapyTypeGroup;
@property (retain, nonatomic) IBOutlet RadioButtonGroup *therapyTypeButtonGroup;
@property (retain, nonatomic) IBOutlet CustomLabel *therapyTypeChangeLabel;
@property (retain, nonatomic) IBOutlet RadioButton *therapyTypeChangeButton;

@property (retain, nonatomic) MultipleChoiceViewController *choiceController;

//Ravi_Bukka: Added for localization
@property (retain, nonatomic) IBOutlet CustomLabel *medicationPrescribedLabel;
@property (retain, nonatomic) IBOutlet CustomLabel *formPresentationLabel;
@property (retain, nonatomic) IBOutlet CustomLabel *desiredActionLabel;
@property (retain, nonatomic) IBOutlet CustomLabel *frequenciaLabel;
@property (retain, nonatomic) IBOutlet CustomLabel *durationLabel;
@property (retain, nonatomic) IBOutlet CustomLabel *prescribedLabel;
@property (retain, nonatomic) IBOutlet CustomLabel *choiceTherapyLabel;
@property (retain, nonatomic) IBOutlet CustomLabel *typeTherapyLabel;

@property (retain, nonatomic) IBOutlet RadioButton *personalChoiceButton;
@property (retain, nonatomic) IBOutlet RadioButton *newButton;
@property (retain, nonatomic) IBOutlet RadioButton *continueButton;

//Kanchan Nair: Added label for Polish design
@property (retain, nonatomic) IBOutlet CustomLabel *productBrandLabel;

@property (retain, nonatomic) IBOutlet PredictiveTextField *therapyTextField;
@property (retain, nonatomic) IBOutlet PredictiveTextField *kindOfTherapyTextField;

@property (retain, nonatomic) IBOutlet ComponentGroup *patientInsuranceGroup;
@property (retain, nonatomic) IBOutlet PredictiveTextField *patientInsuranceTextField;
@property (retain, nonatomic) IBOutlet CustomLabel *patientInsuranceLabel;


@property (retain, nonatomic) IBOutlet ComponentGroup *drugreimbursementgroup;
@property (retain, nonatomic) IBOutlet PredictiveTextField *drugreimbursementTextField;
@property (retain, nonatomic) IBOutlet CustomLabel *drugreimbursementLabel;

// Events
- (IBAction)medicamentTypeFilterChanged:(UISegmentedControl*)sender;

#pragma mark - Public methods
- (BOOL)requestDismiss;
//Deepak_Carpenter : CR#11 Added for seperator in picker view
@property (retain, nonatomic) IBOutlet UILabel *lengthPickerLabel;
@property (retain, nonatomic) IBOutlet UILabel *unitsPickerLabel;

@end
