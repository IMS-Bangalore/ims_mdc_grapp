//
//  TreatmentViewController.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 10/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "TreatmentViewController.h"
#import "EntityFactory.h"
#import "Presentation.h"
#import "Medicament.h"
#import "Effect.h"
#import "Frequency.h"
#import "DurationType.h"
#import "TherapyReplacementReason.h"
#import "TherapyType.h"
#import "TherapyElection.h"
#import "Dosage.h"
#import "Diagnosis.h"
#import "Patient.h"
#import "OtherSpecialty.h"
#import "UnitType.h"
#import "ConfirmationMessageController.h"
#import "RecentMedicament.h"
#import "MultipleChoiceViewController.h"
#import "Dosage+Complete.h"

#import "DrugReimbursement.h"
#import "PatientInsurance.h"

#define kFormConstantsPList @"formConstants"
#define kPresentationKey    @"presentation"
#define kUnitValuesKey      @"unitValues"
#define kUnitsKey           @"units"
#define kFrequencyKey       @"frequency"
#define kTreatmentLengthKey @"treatmentLength"
#define kMinTimesKey        @"minTimes"
#define kMaxTimesKey        @"maxTimes"
#define kMinLengthKey       @"minLength"
#define kMaxLengthKey       @"maxLength"

static NSString* const kIdentifierKey = @"identifier";
static NSString* const kDescriptionKey = @"name";

static NSString* const kProductTypeIndexActivePrinciple = @"B";
static NSString* const kProductTypeIndexBrand = @"G";
static NSString* const kProductTypeIndexGeneric = @"P";

static const NSInteger kProductTypeSegmentActivePrinciple = 0;
static const NSInteger kProductTypeSegmentBrand = 1;
static const NSInteger kProductTypeSegmentGeneric = 2;

const NSInteger kRecipeCountWarning = 10;


typedef enum {
    kSelectorTypeNone,
    kSelectorTypeTherapyChange,
    kSelectorTypeTherapyChoiceSpecialist,
    kSelectorTypeRecipeCountWarning
} SelectorType;

typedef enum {
    kMedicamentResetSection,
    kPresentationResetSection,
    kEffectResetSection,
    kDosageResetSection,
    kTherapyResetSection
} ResetSection;

@interface TreatmentViewController()

@property (nonatomic, retain) NSArray *dosageQuantityArray;
@property (nonatomic, retain) NSArray *frequencyArray;
@property (nonatomic, retain) NSArray *dosageTreatmentLengthArray;
@property (nonatomic, retain) NSArray *dosageCountArray;
@property (nonatomic, retain) NSArray *dosageDurationArray;
@property (nonatomic, retain) NSArray *productTypeArray;

@property (nonatomic, retain) UIPopoverController *popoverController;
@property (nonatomic, assign) SelectorType selectorType;
@property (nonatomic, retain) SpecialitySelectionViewController *specialitySelector;
@property (nonatomic, retain) TherapyTypeChangeViewController *therapyChangeSelector;

@property (nonatomic, retain) NSDictionary *formConstants;
@property (nonatomic, retain) id<CancelableOperation> fetchTextFieldOperation;

@property (readonly, getter = isRefreshing) BOOL refreshing;

-(void) updateUI;
-(void) refresh;
-(void) refreshPickerViews;
- (void)showSpecialityController:(SelectorType)selectorType inView:(UIView*)sourceView;
- (ProductType*) selectedProductTypeFilter;
- (void) loadAvailablePresentations;
- (void) autoselectProductTypeFilter;
- (void) resetSection:(ResetSection)section;


@end

@implementation TreatmentViewController

@synthesize delegate = _delegate;
@synthesize changesControlProtocol = _changesControlProtocol;
@synthesize treatment = _treatment;
@synthesize enabled = _enabled;
@synthesize therapyChoiceGroup = _therapyChoiceGroup;
@synthesize unitsGroup = _unitsGroup;
@synthesize onDemandCheckBox = _onDemandCheckBox;
@synthesize unitsLabel = _unitsLabel;
@synthesize unitsPickerView = _unitsPickerView;
@synthesize frequencyPickerView = _frequencyPickerView;
@synthesize treatmentLengthGroup = _treatmentLengthGroup;
@synthesize longTermCheckBox = _longTermCheckBox;
@synthesize treatmentLengthPickerView = _treatmentLengthPickerView;
@synthesize dosageGroup = _dosageGroup;
@synthesize dosageCountTextField = _dosageCountTextField;
@synthesize dosageCommentTextArea = _dosageCommentTextArea;
@synthesize therapyTypeGroup = _therapyTypeGroup;
@synthesize therapyChoiceButtonGroup = _therapyChoiceButtonGroup;
@synthesize therapyChoiceSpecialistLabel = _therapyChoiceSpecialistLabel;
@synthesize therapyTypeButtonGroup = _therapyTypeButtonGroup;
@synthesize therapyTypeChangeLabel = _therapyTypeChangeLabel;
@synthesize effectGroup = _effectGroup;
@synthesize effectTextField = _effectTextField;
@synthesize presentationGroup = _presentationGroup;
@synthesize presentationTextField = _presentationTextField;
@synthesize presentationTableView = _presentationTableView;
// Medicament
@synthesize medicamentGroup = _medicamentGroup;
@synthesize medicamentFilterSegmentControl = _medicamentFilterSegmentControl;
@synthesize medicamentFrequentCheckBox = _medicamentFrequentCheckBox;
@synthesize medicamentTextField = _medicamentTextField;
//Private properties
@synthesize dosageQuantityArray = _dosageQuantityArray;
@synthesize frequencyArray = _frequencyArray;
@synthesize dosageTreatmentLengthArray = _dosageTreatmentLengthArray;
@synthesize dosageCountArray = _dosageCountArray;
@synthesize dosageDurationArray = _dosageDurationArray;
@synthesize popoverController = _popover;
@synthesize selectorType = _selectorType;
@synthesize specialitySelector = _specialitySelector;
@synthesize therapyChangeSelector = _therapyChangeSelector;
@synthesize productTypeArray = _productTypeArray;
@synthesize refreshing = _refreshing;
@synthesize fetchTextFieldOperation = _fetchTextFieldOperation;
@synthesize formConstants = _formConstants;
@synthesize choiceController = _choiceController;

//Kanchan Nair
@synthesize drugreimbursementTextField = _drugreimbursementTextField;
@synthesize therapyTextField = _therapyTextField;
@synthesize kindOfTherapyTextField = _kindOfTherapyTextField;
@synthesize patientInsuranceTextField = _patientInsuranceTextField;
@synthesize drugreimbursementgroup=_drugreimbursementgroup;

//Deepak_Carpenter : Added for CR#11
@synthesize lengthPickerLabel = _lengthPickerLabel;
@synthesize unitsPickerLabel = _unitsPickerLabel;


-(void) initialize {
    _enabled = YES;
    _formConstants = [[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:kFormConstantsPList ofType:@"plist"]] retain];
}

- (id)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self initialize];
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Deepak_Carpenter CR#11 : Segment Control appearance for iOS 8 +
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{
                                                              NSForegroundColorAttributeName : [UIColor whiteColor]
                                                              } forState:UIControlStateNormal];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{
                                                              NSForegroundColorAttributeName : [UIColor whiteColor]
                                                              } forState:UIControlStateSelected];
   //Deepak_Carpenter : CR#11 Added for picker view seperator appearance in iOS 8
        
        _unitsPickerLabel = [[UILabel alloc]initWithFrame:CGRectMake(109, 0, 1, 115)];
    _unitsPickerLabel.backgroundColor = [UIColor darkGrayColor];
    [_unitsPickerView addSubview:_unitsPickerLabel];
    
    _lengthPickerLabel = [[UILabel alloc]initWithFrame:CGRectMake(109, 0, 1, 115)];
    _lengthPickerLabel.backgroundColor = [UIColor darkGrayColor];
    [_treatmentLengthPickerView addSubview:_lengthPickerLabel];
    
    // Configure predictive textfields
    _medicamentTextField.descriptionField = kDescriptionKey;
    _medicamentTextField.allowUserValues = YES;
    _presentationTextField.descriptionField = kDescriptionKey;
    _presentationTextField.allowUserValues = YES;
    _effectTextField.descriptionField = kDescriptionKey;
    _effectTextField.popoverArrowDirections = UIPopoverArrowDirectionDown;
    _effectTextField.allowUserValues = YES;
    
    //Kanchan Nair
    _kindOfTherapyTextField.popoverArrowDirections=UIPopoverArrowDirectionDown;
    _drugreimbursementTextField.descriptionField = kDescriptionKey;
    _drugreimbursementTextField.allowUserValues = YES;
    
    _patientInsuranceTextField.descriptionField = kDescriptionKey;
    _patientInsuranceTextField.allowUserValues = YES;
    
    _patientInsuranceTextField.popoverArrowDirections=UIPopoverArrowDirectionDown;

      //  [_frequencyPickerView setOverlappedTextFont:[UIFont systemFontOfSize:10.0]];

    //
   
    // Get form constants from property list
    NSDictionary* formConstants = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:kFormConstantsPList ofType:@"plist"]];
    _dosageQuantityArray = [[formConstants objectForKey:kUnitValuesKey] retain];
    _frequencyArray =  [[[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([Frequency class])  sortByIdentifier:YES] retain];
    _dosageTreatmentLengthArray = [[[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([DurationType class]) sortByIdentifier:YES] retain];
    _productTypeArray = [[[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([ProductType class])  sortByIdentifier:NO] retain];
    
    int minTimes = [[formConstants objectForKey:kMinTimesKey] intValue];
    int maxTimes = [[formConstants objectForKey:kMaxTimesKey] intValue];
    NSMutableArray* tmpArray = [[NSMutableArray alloc] init];
    for (int i=minTimes; i<= maxTimes; i++) {
        [tmpArray addObject:[NSString stringWithFormat:@"%d", i]];
    }
    _dosageCountArray = [tmpArray retain];
    [tmpArray release];
    
    minTimes = [[formConstants objectForKey:kMinLengthKey] intValue];
    maxTimes = [[formConstants objectForKey:kMaxLengthKey] intValue];
    tmpArray = [[NSMutableArray alloc] init];
    for (int i=minTimes; i<= maxTimes; i++) {
        [tmpArray addObject:[NSString stringWithFormat:@"%d", i]];
    }
    _dosageDurationArray = [tmpArray retain];
    [tmpArray release];
    //Deepak
  
    
    
    
    // Set placeholder to comments text area
    _dosageCommentTextArea.placeholder = NSLocalizedString(@"COMMENTS", nil);
    
    // Load Radio Button groups
    //Deepak_Carpenter: Added for Therapy Election Button Group
     NSArray* therapyElectionArray = [[[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([TherapyElection class]) filteringByName:@""] retain];
    
    NSArray* therapyTypeArray = [[[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([TherapyType class]) filteringByName:@""] retain];
    _therapyTypeButtonGroup.tagIdentifierField = kIdentifierKey;
    _therapyTypeButtonGroup.values = therapyElectionArray;
    _therapyChoiceButtonGroup.tagIdentifierField = kIdentifierKey;
    _therapyChoiceButtonGroup.values = therapyTypeArray;
    [therapyTypeArray release];
    [therapyElectionArray release];
    [self updateUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.popoverController = nil;
    

//Ravi_Bukka: Added for localization
    
     if ([[[[NSBundle mainBundle]preferredLocalizations]objectAtIndex:0] isEqualToString:@"el"]){
         
         [_medicamentFilterSegmentControl setTitle:NSLocalizedString(@"TREATMENT_SEGMENT1_BRAND", nil) forSegmentAtIndex:0];
         [_medicamentFilterSegmentControl setTitle:NSLocalizedString(@"TREATMENT_SEGMENT0_ACTIVE", nil) forSegmentAtIndex:1];
     }
     else {
    [_medicamentFilterSegmentControl setTitle:NSLocalizedString(@"TREATMENT_SEGMENT0_ACTIVE", nil) forSegmentAtIndex:0];
    [_medicamentFilterSegmentControl setTitle:NSLocalizedString(@"TREATMENT_SEGMENT1_BRAND", nil) forSegmentAtIndex:1];
    [_medicamentFilterSegmentControl setTitle:NSLocalizedString(@"TREATMENT_SEGMENT2_GENERIC", nil) forSegmentAtIndex:2];
     }
    if ([[[[NSBundle mainBundle]preferredLocalizations]objectAtIndex:0] isEqualToString:@"pl"]){
        
        _medicationPrescribedLabel.text = NSLocalizedString(@"TREATMENT_MEDICATION_PRESCRIBED", nil);
        _formPresentationLabel.text = NSLocalizedString(@"TREATMENT_FORM_OF_PRESENTATION", nil);
        _desiredActionLabel.text = NSLocalizedString(@"TREATMENT_DESIRED_ACTION", nil);
        _frequenciaLabel.text = NSLocalizedString(@"TREATMENT_FREQUENCY", nil);
        _durationLabel.text = NSLocalizedString(@"TREATMENT_DURATION", nil);
        _prescribedLabel.text = NSLocalizedString(@"TREATMENT_NO_PRESCRIBED", nil);
        _choiceTherapyLabel.text = NSLocalizedString(@"TREATMENT_CHOICE_THERAPY", nil);
        _typeTherapyLabel.text = NSLocalizedString(@"TREATMENT_TYPE_OF_THERAPY", nil);
        _unitsLabel.text = NSLocalizedString(@"TREATMENT_UNITS", nil);
        _drugreimbursementLabel.text = NSLocalizedString(@"DRUG_REIMBURSEMENT", nil);
        _patientInsuranceLabel.text=NSLocalizedString(@"PATIENT_INSURANCE", nil);
        _productBrandLabel.text=NSLocalizedString(@"PRODUCT_PRESCRIBED", nil);
        
    }
else
    _medicationPrescribedLabel.text = NSLocalizedString(@"TREATMENT_MEDICATION_PRESCRIBED", nil);
    _formPresentationLabel.text = NSLocalizedString(@"TREATMENT_FORM_OF_PRESENTATION", nil);
    _desiredActionLabel.text = NSLocalizedString(@"TREATMENT_DESIRED_ACTION", nil);
    _frequenciaLabel.text = NSLocalizedString(@"TREATMENT_FREQUENCY", nil);
    _durationLabel.text = NSLocalizedString(@"TREATMENT_DURATION", nil);
    _prescribedLabel.text = NSLocalizedString(@"TREATMENT_NO_PRESCRIBED", nil);
    _choiceTherapyLabel.text = NSLocalizedString(@"TREATMENT_CHOICE_THERAPY", nil);
    _typeTherapyLabel.text = NSLocalizedString(@"TREATMENT_TYPE_OF_THERAPY", nil);
    _unitsLabel.text = NSLocalizedString(@"TREATMENT_UNITS", nil);
    

    
    [_medicamentFrequentCheckBox setTitle:NSLocalizedString(@"TREATMENT_FREQUENTLY", nil) forState:UIControlStateNormal];
    [_medicamentFrequentCheckBox setTitle:NSLocalizedString(@"TREATMENT_FREQUENTLY", nil) forState:UIControlStateHighlighted];
    
    [_onDemandCheckBox setTitle:NSLocalizedString(@"TREATMENT_ON_REQUEST", nil) forState:UIControlStateNormal];
    [_onDemandCheckBox setTitle:NSLocalizedString(@"TREATMENT_ON_REQUEST", nil) forState:UIControlStateHighlighted];
    
    [_uniqueDoseCheckBox setTitle:NSLocalizedString(@"TREATMENT_SINGLE_DOSE", nil) forState:UIControlStateNormal];
    [_uniqueDoseCheckBox setTitle:NSLocalizedString(@"TREATMENT_SINGLE_DOSE", nil) forState:UIControlStateHighlighted];
    
    if ([[[[NSBundle mainBundle]preferredLocalizations]objectAtIndex:0] isEqualToString:@"el"]){

        [_longTermCheckBox setTitle:NSLocalizedString(@"TREATMENT_TLD", nil) forState:UIControlStateNormal];
        [_longTermCheckBox setTitle:NSLocalizedString(@"TREATMENT_TLD", nil) forState:UIControlStateHighlighted];
        
        _onDemandCheckBox.titleLabel.font = [UIFont systemFontOfSize:12.0];
        _uniqueDoseCheckBox.titleLabel.font = [UIFont systemFontOfSize:12.0];
         _prescribedLabel.font = [UIFont systemFontOfSize:12.0];
        
    }
    else {
        
    }
  
    
    [_therapyTypeChangeButton setTitle:NSLocalizedString(@"TREATMENT_CHANGE", nil) forState:UIControlStateNormal];
    [_therapyTypeChangeButton setTitle:NSLocalizedString(@"TREATMENT_CHANGE", nil) forState:UIControlStateHighlighted];
    [_therapyChoiceSpecialistButton setTitle:NSLocalizedString(@"TREATMENT_STARTED_BY_ANOTHER_SPL", nil) forState:UIControlStateNormal];
    [_therapyChoiceSpecialistButton setTitle:NSLocalizedString(@"TREATMENT_STARTED_BY_ANOTHER_SPL", nil) forState:UIControlStateHighlighted];
    [_personalChoiceButton setTitle:NSLocalizedString(@"TREATMENT_PERSONAL_DECISION", nil) forState:UIControlStateNormal];
    [_personalChoiceButton setTitle:NSLocalizedString(@"TREATMENT_PERSONAL_DECISION", nil) forState:UIControlStateHighlighted];
    [_newButton setTitle:NSLocalizedString(@"TREATMENT_NEW", nil) forState:UIControlStateNormal];
    [_newButton setTitle:NSLocalizedString(@"TREATMENT_NEW", nil) forState:UIControlStateHighlighted];
    [_continueButton setTitle:NSLocalizedString(@"TREATMENT_CONTINUATION", nil) forState:UIControlStateNormal];
    [_continueButton setTitle:NSLocalizedString(@"TREATMENT_CONTINUATION", nil) forState:UIControlStateHighlighted];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    if (_choiceController != nil) {
        [_delegate dismissModalController:_choiceController];
        _choiceController = nil;
    }
    if (self.popoverController != nil) {
        [self.popoverController dismissPopoverAnimated:NO];
    }
    [super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
    
    //Kanchan Nair
    self.therapyTextField=nil;
    self.kindOfTherapyTextField=nil;
    self.patientInsuranceTextField=nil;
    self.drugreimbursementTextField=nil;
    self.drugreimbursementgroup=nil;
    
    self.medicamentGroup = nil;
    self.medicamentFilterSegmentControl = nil;
    self.medicamentFrequentCheckBox = nil;
    self.medicamentTextField = nil;
    self.presentationGroup = nil;
    self.presentationTextField = nil;
    self.presentationTableView = nil;
    self.effectGroup = nil;
    self.effectTextField = nil;
    [self setEffectTextField:nil];
    [self setTherapyChoiceButtonGroup:nil];
    [self setTherapyTypeButtonGroup:nil];
    [self setUnitsGroup:nil];
    [self setOnDemandCheckBox:nil];
    [self setUnitsPickerView:nil];
    [self setFrequencyPickerView:nil];
    [self setTreatmentLengthGroup:nil];
    [self setLongTermCheckBox:nil];
    [self setTreatmentLengthPickerView:nil];
    [self setDosageGroup:nil];
    [self setDosageCountTextField:nil];
    [self setDosageCommentTextArea:nil];
    [self setTherapyChoiceGroup:nil];
    [self setTherapyTypeGroup:nil];
    [self setTherapyTypeChangeLabel:nil];
    [self setTherapyChoiceSpecialistLabel:nil];
    [self setUnitsLabel:nil];
    [self setTherapyChoiceSpecialistButton:nil];
    [self setTherapyTypeChangeButton:nil];
    [self setUniqueDoseCheckBox:nil];
    [super viewDidUnload];
}

- (void)dealloc {
    [_drugreimbursementTextField release];
    [_therapyTextField release];
    [_kindOfTherapyTextField release];
    [_patientInsuranceTextField release];
    [_drugreimbursementgroup release];
    
    [_medicamentGroup release];
    [_medicamentFilterSegmentControl release];
    [_medicamentFrequentCheckBox release];
    [_medicamentTextField release];
    [_presentationGroup release];
    [_presentationTextField release];
    [_presentationTableView release];
    [_effectGroup release];
    [_effectTextField release];
    [_therapyChoiceButtonGroup release];
    [_therapyTypeButtonGroup release];
    [_unitsGroup release];
    [_onDemandCheckBox release];
    [_unitsPickerView release];
    [_frequencyPickerView release];
    [_treatmentLengthGroup release];
    [_longTermCheckBox release];
    [_treatmentLengthPickerView release];
    [_dosageGroup release];
    [_dosageCountTextField release];
    [_dosageCommentTextArea release];
    [_therapyChoiceGroup release];
    [_therapyTypeGroup release];
    [_dosageQuantityArray release];
    [_frequencyArray release];
    [_dosageTreatmentLengthArray release];
    [_dosageCountArray release];
    [_dosageDurationArray release];
    [_therapyTypeChangeLabel release];
    [_therapyChoiceSpecialistLabel release];
    [_popover release];
    [_specialitySelector release];
    [_therapyChangeSelector release];
    [_productTypeArray release];
    _delegate = nil;
    [_unitsLabel release];
    [_fetchTextFieldOperation release];
    [_formConstants release];
    [_therapyChoiceSpecialistButton release];
    [_therapyTypeChangeButton release];
    [_uniqueDoseCheckBox release];
    [super dealloc];
}

-(void) updateUI {
    [self refresh];
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {

    BOOL enableMedicamentSelector = self.enabled;
    _medicamentGroup.enabled = enableMedicamentSelector;
    
    BOOL enabledrugreimburesement= enableMedicamentSelector && _treatment.medicament!=nil;
        NSLog(@"MEDICAMENT Check 6:: %@",_treatment.medicament);

    _drugreimbursementgroup.enabled=enabledrugreimburesement;
    
    BOOL enablePresentation = _drugreimbursementgroup.enabled && _drugreimbursementTextField.text!=nil && _drugreimbursementgroup !=nil && _treatment.drugReimbursement !=nil;
    _presentationGroup.enabled = enablePresentation;
    
    BOOL enableinsurance=enablePresentation && (_treatment.presentation != nil || [_treatment.userPresentation stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0);;
    _patientInsuranceGroup.enabled=enableinsurance;
    
    BOOL enableUnits = enableinsurance && (_treatment.patientInsurance != nil || [_treatment.userPatientInsurance stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0);
    _unitsGroup.enabled = enableUnits;

    BOOL onDemand = _treatment.dosage.onDemand.boolValue;
    BOOL uniqueDose = _treatment.dosage.uniqueDose.boolValue;
    
    _unitsPickerView.enabled = enableUnits;
    // Add overlapped text to units picker view
    _frequencyPickerView.enabled = enableUnits && !onDemand && !uniqueDose;
    
    _uniqueDoseCheckBox.enabled = enableUnits && !onDemand;
    _onDemandCheckBox.enabled = enableUnits && !uniqueDose;
    
    if (uniqueDose) {
        _unitsPickerView.overlappedText = NSLocalizedString(@"UNIQUE_DOSE", nil);
    }
    else if (onDemand) {
        _unitsPickerView.overlappedText = _treatment.presentation.unitType.name;
    }
        
        
    else {
        _unitsPickerView.overlappedText = NSLocalizedString(@"UNITS_COUNT", nil);
    }
    
    BOOL selectedUnits = enableUnits && (onDemand ||
                                         ((_treatment.dosage.quantity != nil &&
                                           _treatment.dosage.units != nil &&
                                           _treatment.dosage.frequency != nil)) || uniqueDose);
    BOOL enableLength = selectedUnits && !onDemand && !uniqueDose;
    _treatmentLengthGroup.enabled = enableLength;
    BOOL longTermTreatment = _treatment.dosage.longDurationTreatment.boolValue;
    _treatmentLengthPickerView.enabled = enableLength && !longTermTreatment;
    BOOL enableDosage = selectedUnits && ((onDemand && _treatment.dosage.quantity != nil) || longTermTreatment ||
                                          (_treatment.dosage.duration != nil &&
                                           _treatment.dosage.durationType != nil) || (uniqueDose && _treatment.dosage.quantity != nil));
    _dosageGroup.enabled = enableDosage;
    BOOL enableTherapyChoice = enableDosage && _treatment.dosage.recipeCount.integerValue != 0 && _treatment.dosage.recipeCount != nil;
    _therapyChoiceGroup.enabled = enableTherapyChoice;
    BOOL enableTherapyType = enableTherapyChoice && _treatment.therapyChoiceReason != nil;
    _therapyTypeGroup.enabled = enableTherapyType;
        
    //Deepak_Carpenter: Added for therapy election radio button
        BOOL isReady = enableTherapyType && _treatment.therapyElection!=nil;
        //_treatment.therapyType != nil;
    [_delegate treatmentViewController:self isReady:isReady];
    }

else
{
    BOOL enableSegmentOnManual;
    if (_medicamentFilterSegmentControl.selectedSegmentIndex==-1) {
        enableSegmentOnManual=YES;
    }
    else
         enableSegmentOnManual=NO;
    
    BOOL enableMedicamentSelector = self.enabled;
    _medicamentGroup.enabled = enableMedicamentSelector;
    NSLog(@"Segment Value %d",_medicamentFilterSegmentControl.selectedSegmentIndex);
    BOOL enableMedicament = enableMedicamentSelector && (_medicamentFilterSegmentControl.selectedSegmentIndex != UISegmentedControlNoSegment || enableSegmentOnManual);
    _medicamentTextField.enabled = enableMedicament;
    BOOL enablePresentation = enableMedicament && (_treatment.medicament != nil || [_treatment.userMedicament stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0);
    _presentationGroup.enabled = enablePresentation;
    BOOL enableEffect = enablePresentation && (_treatment.presentation != nil || [_treatment.userPresentation stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0);
    _effectGroup.enabled = enableEffect;
    BOOL enableUnits = enableEffect && (_treatment.desiredEffect != nil || [_treatment.userDesiredEffect stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0);
    _unitsGroup.enabled = enableUnits;
    BOOL onDemand = _treatment.dosage.onDemand.boolValue;
    BOOL uniqueDose = _treatment.dosage.uniqueDose.boolValue;
    
    _unitsPickerView.enabled = enableUnits;
    // Add overlapped text to units picker view
    _frequencyPickerView.enabled = enableUnits && !onDemand && !uniqueDose;
    
    _uniqueDoseCheckBox.enabled = enableUnits && !onDemand;
    _onDemandCheckBox.enabled = enableUnits && !uniqueDose;

    if (uniqueDose) {
        _unitsPickerView.overlappedText = NSLocalizedString(@"UNIQUE_DOSE", nil);
    }
    else if (onDemand) {
        _unitsPickerView.overlappedText = _treatment.presentation.unitType.name;
    }
    else {
        _unitsPickerView.overlappedText = NSLocalizedString(@"UNITS_COUNT", nil);
    }

    BOOL selectedUnits = enableUnits && (onDemand ||
                                         ((_treatment.dosage.quantity != nil &&
                                          _treatment.dosage.units != nil &&
                                          _treatment.dosage.frequency != nil)) || uniqueDose);
    BOOL enableLength = selectedUnits && !onDemand && !uniqueDose;
    _treatmentLengthGroup.enabled = enableLength;
    BOOL longTermTreatment = _treatment.dosage.longDurationTreatment.boolValue;
    _treatmentLengthPickerView.enabled = enableLength && !longTermTreatment;
    BOOL enableDosage = selectedUnits && ((onDemand && _treatment.dosage.quantity != nil) || longTermTreatment ||
                                          (_treatment.dosage.duration != nil &&
                                           _treatment.dosage.durationType != nil) || (uniqueDose && _treatment.dosage.quantity != nil));
    _dosageGroup.enabled = enableDosage;
    BOOL enableTherapyChoice = enableDosage && _treatment.dosage.recipeCount.integerValue != 0 && _treatment.dosage.recipeCount != nil;
    _therapyChoiceGroup.enabled = enableTherapyChoice;
    BOOL enableTherapyType = enableTherapyChoice && _treatment.therapyChoiceReason != nil;
    _therapyTypeGroup.enabled = enableTherapyType;
    //Deepak_Carpenter: Added for therapy election radio button
    BOOL isReady = enableTherapyType && _treatment.therapyElection!=nil;
    //_treatment.therapyType != nil;
    [_delegate treatmentViewController:self isReady:isReady];

}
}
-(void) refresh {
    _refreshing = YES;
    
    
if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"])
{

        _medicamentTextField.selectedElement = _treatment.medicament;
        if (_treatment.medicament == nil && _treatment.userMedicament.length > 0) {
            _medicamentTextField.text = _treatment.userMedicament;
        }
        
        [self autoselectProductTypeFilter];
        
        // Presentation
        _presentationTextField.selectedElement = _treatment.presentation;
        if (_treatment.presentation == nil && _treatment.userPresentation.length > 0) {
            _presentationTextField.text = _treatment.userPresentation;
            //@kanchan Nair: Clearing Text fields //
            
            
        }
        //Patient Insurance
        _patientInsuranceTextField.selectedElement = _treatment.patientInsurance;
        if (_treatment.patientInsurance == nil && _treatment.userPatientInsurance.length > 0) {
            _patientInsuranceTextField.text = _treatment.userPatientInsurance;
        }
        _unitsLabel.text = _treatment.presentation.unitType != nil ? NSLocalizedString(_treatment.presentation.unitType.name,nil) : @"-";
        
        //Kanchan Nar : Drug Reimbursement
        _drugreimbursementTextField.selectedElement = _treatment.drugReimbursement;
        if (_treatment.drugReimbursement == nil && _treatment.userDrugReimbursement.length > 0) {
            _drugreimbursementTextField.text = _treatment.userDrugReimbursement;
        }
        
        // Effect
        _effectTextField.selectedElement = _treatment.desiredEffect;
        if (_treatment.desiredEffect == nil && _treatment.userDesiredEffect.length > 0) {
            _effectTextField.text = _treatment.userDesiredEffect;
        }
        
        // Therapy choice
        _therapyChoiceButtonGroup.selectedValue = _treatment.therapyChoiceReason;
        
        //Ravi_Bukka: Added for localization
        _therapyChoiceSpecialistLabel.text = NSLocalizedString(_treatment.recommendationSpecialist.name,nil);
        _therapyChoiceSpecialistLabel.hidden = _treatment.recommendationSpecialist == nil;
        // Therapy type
    //Deepak_Carpenter: Added for therapy election radio button
    _therapyTypeButtonGroup.selectedValue = _treatment.therapyElection;
    //_treatment.therapyType;
        if (_treatment.replacingMedicament != nil) {
            
            //Ravi_Bukka: Added for localization
            _therapyTypeChangeLabel.text = [NSString stringWithFormat:NSLocalizedString(@"THERAPY_CHANGE_REASON", nil), NSLocalizedString(_treatment.replacingMedicament.name, nil), NSLocalizedString(_treatment.replaceReason.name.lowercaseString,nil)];
            NSLog(@"_treatment.replacingMedicament.name -- %@",_treatment.replacingMedicament.name);
        } else {
            _therapyTypeChangeLabel.text = [NSString stringWithFormat:NSLocalizedString(@"THERAPY_CHANGE_REASON", nil), NSLocalizedString(_treatment.userReplacingMedicament,nil), NSLocalizedString(_treatment.replaceReason.name.lowercaseString,nil)];
        
            
        }
        
        _therapyTypeChangeLabel.hidden = (_treatment.replacingMedicament == nil && _treatment.userReplacingMedicament.length == 0) || _treatment.replaceReason == nil;
        
        // Dosage
        _onDemandCheckBox.selected = [_treatment.dosage.onDemand boolValue];
        _uniqueDoseCheckBox.selected = [_treatment.dosage.uniqueDose boolValue];
        _longTermCheckBox.selected = [_treatment.dosage.longDurationTreatment boolValue];
        NSNumber* recipeCount = _treatment.dosage.recipeCount;
        _dosageCountTextField.text = recipeCount.intValue != 0 ? [NSString stringWithFormat:@"%d", recipeCount.intValue] : @"";
        _dosageCommentTextArea.text = _treatment.dosage.comments;

        _refreshing = NO;
}
    
    else{
    // Medicament
    _medicamentTextField.selectedElement = _treatment.medicament;
         NSLog(@"medicament ide %@", _treatment.medicament);
        NSLog(@"MEDICAMENT Check 7:: %@",_treatment.medicament);

    if (_treatment.medicament == nil && _treatment.userMedicament.length > 0) {
        _medicamentTextField.text = _treatment.userMedicament;
        NSLog(@"medicament ide %@",_treatment.userMedicament);

    }
    [self autoselectProductTypeFilter];
    // Presentation
    _presentationTextField.selectedElement = _treatment.presentation;
        NSLog(@"Presentation ide %@ slectedElement %@",_treatment.presentation,_presentationTextField.selectedElement);
    if (_treatment.presentation == nil && _treatment.userPresentation.length > 0) {
        _presentationTextField.text = _treatment.userPresentation;
       
    }
    _unitsLabel.text = _treatment.presentation.unitType != nil ? NSLocalizedString(_treatment.presentation.unitType.name,nil) : @"-";
    // Effect
    _effectTextField.selectedElement = _treatment.desiredEffect;
    if (_treatment.desiredEffect == nil && _treatment.userDesiredEffect.length > 0) {
        _effectTextField.text = _treatment.userDesiredEffect;
    }
    // Therapy choice
    _therapyChoiceButtonGroup.selectedValue = _treatment.therapyChoiceReason;
    
//Ravi_Bukka: Added for localization
    _therapyChoiceSpecialistLabel.text = NSLocalizedString(_treatment.recommendationSpecialist.name,nil);
    _therapyChoiceSpecialistLabel.hidden = _treatment.recommendationSpecialist == nil;
    // Therapy type
        //Deepak_Carpenter: Added for therapy election radio button
        _therapyTypeButtonGroup.selectedValue = _treatment.therapyElection;
        NSLog(@"therapy election value %@",_treatment.therapyElection.identifier);
        //_treatment.therapyType;
    if (_treatment.replacingMedicament != nil) {
    
//Ravi_Bukka: Added for localization        
        _therapyTypeChangeLabel.text = [NSString stringWithFormat:NSLocalizedString(@"THERAPY_CHANGE_REASON", nil), NSLocalizedString(_treatment.replacingMedicament.name, nil), NSLocalizedString(_treatment.replaceReason.name.lowercaseString,nil)];
        
    } else {
        _therapyTypeChangeLabel.text = [NSString stringWithFormat:NSLocalizedString(@"THERAPY_CHANGE_REASON", nil), NSLocalizedString(_treatment.userReplacingMedicament,nil), NSLocalizedString(_treatment.replaceReason.name.lowercaseString,nil)];
        
    }
    
    _therapyTypeChangeLabel.hidden = (_treatment.replacingMedicament == nil && _treatment.userReplacingMedicament.length == 0) || _treatment.replaceReason == nil;
    // Dosage
    _onDemandCheckBox.selected = [_treatment.dosage.onDemand boolValue];
    _uniqueDoseCheckBox.selected = [_treatment.dosage.uniqueDose boolValue];
    _longTermCheckBox.selected = [_treatment.dosage.longDurationTreatment boolValue];
    NSNumber* recipeCount = _treatment.dosage.recipeCount;
    _dosageCountTextField.text = recipeCount.intValue != 0 ? [NSString stringWithFormat:@"%d", recipeCount.intValue] : @"";
    _dosageCommentTextArea.text = _treatment.dosage.comments;
    _refreshing = NO;
    }
    [self refreshPickerViews];
}

-(void) refreshPickerViews {
    _refreshing = YES;
    // Dosage quantity
    NSInteger unitsRow = 0;
    if (_treatment != nil && _treatment.dosage.quantity != nil) {
        NSNumberFormatter* numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
        [numberFormatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"US"] autorelease]];
        [numberFormatter setPositiveFormat:@"#,##0.#"];
        NSNumber* quantityNumber = _treatment.dosage.quantity;
        if (quantityNumber != nil) {
            NSString* numberString = [numberFormatter stringFromNumber:quantityNumber];
            unitsRow = [_dosageQuantityArray indexOfObject:numberString];
            if (unitsRow != NSNotFound) {
                unitsRow = unitsRow + 1;
            }
            else {
                unitsRow = 0;
            }
        }
        else {
            unitsRow = 0;
        }
    }
    if (unitsRow != NSNotFound) {
    
            [_unitsPickerView.pickerView selectRow:unitsRow inComponent:0 animated:YES];
            
    }
    
    // Dosage count
    NSInteger dosageRow = 0;
    if (_treatment.dosage.units != nil) {
        dosageRow = [_treatment.dosage.units intValue];
    }
    BOOL onDemand = [_treatment.dosage.onDemand boolValue];
    BOOL uniqueDose = [_treatment.dosage.uniqueDose boolValue];
    [_unitsPickerView.pickerView reloadAllComponents];
    if (!onDemand && !uniqueDose) {
            [_unitsPickerView.pickerView selectRow:dosageRow inComponent:1 animated:YES];
    }

    // Frequency
    NSInteger frequencyRow = 0;
    if (_treatment.dosage.frequency != nil) {
        frequencyRow = [_frequencyArray indexOfObject:_treatment.dosage.frequency] + 1;
    }
    [_frequencyPickerView.pickerView selectRow:frequencyRow inComponent:0 animated:YES];
    // Dosage duration
    NSInteger durationRow = 0;
    if (_treatment.dosage.duration != nil) {
        durationRow = [_treatment.dosage.duration intValue];
    }
    [_treatmentLengthPickerView.pickerView selectRow:durationRow inComponent:0 animated:YES];
    // Duration type
    NSInteger durationTypeRow = 0;
    if (_treatment.dosage.durationType != nil) {
        durationTypeRow = [_dosageTreatmentLengthArray indexOfObject:_treatment.dosage.durationType] + 1;
    }
    [_treatmentLengthPickerView.pickerView selectRow:durationTypeRow inComponent:1 animated:YES];
    _refreshing = NO;
}

-(void)setEnabled:(BOOL)enabled {
    _enabled = enabled;
    [self updateUI];
}

- (void)showSpecialityController:(SelectorType)selectorType inView:(UIView*)sourceView{
    if (self.popoverController != nil) {
        return;
    }
    
    self.selectorType = selectorType;
    UIViewController* viewController = nil;
    // Select view controller to display in function of selector type
    if (selectorType == kSelectorTypeTherapyChange) {
        if (_therapyChangeSelector == nil) {
            _therapyChangeSelector = [[TherapyTypeChangeViewController alloc] init];
            _therapyChangeSelector.delegate = self;
        }
        [_therapyChangeSelector clear];
        viewController = _therapyChangeSelector;
    }
    else {
        if (_specialitySelector == nil) {
            _specialitySelector = [[SpecialitySelectionViewController alloc] init];
            _specialitySelector.values = [[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([OtherSpecialty class]) filteringByName:nil];
            _specialitySelector.delegate = self;
            _specialitySelector.windowTitle = NSLocalizedString(@"THERAPY_CHANGE_SPECIALITY_POPUP_TITLE", @"");
        }
        _specialitySelector.disabled = nil;
        viewController = _specialitySelector;
    }
    [self.popoverController dismissPopoverAnimated:NO];
    self.popoverController = [[[UIPopoverController alloc] initWithContentViewController:viewController] autorelease];
    self.popoverController.popoverContentSize = viewController.view.frame.size;
    self.popoverController.delegate = self;
    
    // Notify delegate before showing the popover
    [_delegate treatmentViewController:self willPresentPopover:self.popoverController];
    
    // Queue the popover for the next cycle of the Main thread to let the scroll update
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self.popoverController presentPopoverFromRect:sourceView.frame inView:sourceView.superview permittedArrowDirections:UIPopoverArrowDirectionDown | UIPopoverArrowDirectionUp animated:YES];
    }];
}

- (void)setTreatment:(Treatment *)aTreatment {
    [aTreatment retain];
    [_treatment release];
    _treatment = aTreatment;
    // Create dosage if nil
    if (_treatment.dosage == nil) {
        _treatment.dosage = [[EntityFactory sharedEntityFactory] insertNewWithEntityName:NSStringFromClass([Dosage class])];
        // Default recipe count
        _treatment.dosage.recipeCount = [NSNumber numberWithInt:1];
    }
    _refreshing = YES;
    // Reset product type filter
    _medicamentFilterSegmentControl.selectedSegmentIndex = UISegmentedControlNoSegment;
    // Reset medicament selection
    _medicamentTextField.selectedElement = nil;
    _medicamentFrequentCheckBox.selected = NO;
    _refreshing = NO;
    [self updateUI];
    [self refreshPickerViews];
    [self loadAvailablePresentations];
}

- (void)obtainOrCreateRecentMedicament:(Medicament*)medicament {
    NSSet* recentMedicaments = self.treatment.diagnosis.patient.doctorInfo.recentMedicaments;
    
    RecentMedicament* recentMedicament = nil;
    for (RecentMedicament* recent in recentMedicaments) {
        if (recent.medicament == medicament) {
            recentMedicament = recent;
            break;
        }
    }
    
    if (recentMedicament == nil) {
        recentMedicament = [[EntityFactory sharedEntityFactory] insertNewWithEntityName:NSStringFromClass([RecentMedicament class])];
        recentMedicament.doctorInfo = _treatment.diagnosis.patient.doctorInfo;
        recentMedicament.medicament = medicament;
    }
    
    recentMedicament.count = [NSNumber numberWithInteger:recentMedicament.count.integerValue + 1];
    recentMedicament.lastUse = [NSDate date];
}

#pragma mark - Private methods
-(ProductType *)selectedProductTypeFilter {
    ProductType* productTypeFilter = nil;
    NSString* productTypeIndex = nil;
    switch (_medicamentFilterSegmentControl.selectedSegmentIndex) {
        case kProductTypeSegmentBrand:
            productTypeIndex = kProductTypeIndexBrand;
            break;
        case kProductTypeSegmentActivePrinciple:
            productTypeIndex = kProductTypeIndexActivePrinciple;
            break;
        case kProductTypeSegmentGeneric:
            productTypeIndex = kProductTypeIndexGeneric;
            break;
    }
    // Locate product type associated with selected index
    if (productTypeIndex != nil) {
        for (ProductType* productType in _productTypeArray) {
            if ([productType.identifier isEqualToString:productTypeIndex]) {
                productTypeFilter = productType;
                break;
            }
        }
    }
    return productTypeFilter;
}

- (void) autoselectProductTypeFilter {
    Medicament* selectedMedicament = _medicamentTextField.selectedElement;
    if (selectedMedicament != nil) {
        NSInteger selectedSegment = UISegmentedControlNoSegment;
        if (selectedMedicament != nil) {
            NSString* selectedProductTypeIndex = selectedMedicament.productType.identifier;
            if ([selectedProductTypeIndex isEqualToString:kProductTypeIndexBrand]) {
                selectedSegment = kProductTypeSegmentBrand;
            }
            else if ([selectedProductTypeIndex isEqualToString:kProductTypeIndexActivePrinciple]) {
                selectedSegment = kProductTypeSegmentActivePrinciple;
            }
            else if ([selectedProductTypeIndex isEqualToString:kProductTypeIndexGeneric]) {
                selectedSegment = kProductTypeSegmentGeneric;
            }
        }
        _medicamentFilterSegmentControl.selectedSegmentIndex = selectedSegment;
    }
}

-(void) loadAvailablePresentations {
    NSMutableSet* unorderedPresentations = [NSMutableSet set];
    for (Presentation* presentation in _treatment.medicament.presentations) {
        NSLog(@"MEDICAMENT Check :: %@",_treatment.medicament);
        if (presentation.deleted != [NSNumber numberWithBool:YES]) {
            [unorderedPresentations addObject:presentation];
        }
    }
    NSArray* sortedPresentations = nil;
    if (unorderedPresentations != nil) {
        sortedPresentations = [unorderedPresentations sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:kDescriptionKey ascending:YES selector:@selector(caseInsensitiveCompare:)]]];
    } else {
        sortedPresentations = [NSArray array];
    }
    
    NSMutableArray* filteredArray = [NSMutableArray arrayWithCapacity:sortedPresentations.count];
    for (Presentation* presentation in sortedPresentations) {
        BOOL isUnique = YES;
        for (Treatment* treatment in presentation.treatments) {
            if (treatment != _treatment && treatment.diagnosis == self.treatment.diagnosis) {
                isUnique = NO;
                break;
            }
        }
        if (isUnique) {
            [filteredArray addObject:presentation];
        }
    }
    _presentationTextField.staticElements = filteredArray;
}

- (void)hideKeyboard {
    [self.medicamentTextField resignFirstResponder];
    [self.presentationTextField resignFirstResponder];
    [self.effectTextField resignFirstResponder];
    [self.dosageCountTextField resignFirstResponder];
    [self.dosageCommentTextArea resignFirstResponder];
    
    //Kanchan Nair
    [self.drugreimbursementTextField resignFirstResponder];
    [self.therapyTextField resignFirstResponder];
    [self.kindOfTherapyTextField resignFirstResponder];
    [self.patientInsuranceTextField resignFirstResponder];
}

- (void)resetSection:(ResetSection)section {
    switch (section) {
        case kMedicamentResetSection: {
            _treatment.medicament = nil;
            _treatment.userMedicament = nil;
        }
        case kEffectResetSection: {
            _treatment.desiredEffect = nil;
            _treatment.userDesiredEffect = nil;
        }
        case kTherapyResetSection: {
            _treatment.therapyChoiceReason = nil;
            //Deepak_Carpenter: Added for therapy election radio button
            _treatment.therapyElection=nil;
            //_treatment.therapyType = nil;
            _treatment.therapyElection=nil;
            _treatment.replaceReason = nil;
            _treatment.replacingMedicament = nil;
            _treatment.recommendationSpecialist = nil;
        }
        case kPresentationResetSection: {
            _treatment.presentation = nil;
            _treatment.userPresentation = nil;
        }
        case kDosageResetSection: {
            _treatment.dosage.longDurationTreatment = nil;
            _treatment.dosage.units = nil;
            _treatment.dosage.onDemand = nil;
            _treatment.dosage.uniqueDose = nil;
            _treatment.dosage.frequency = nil;
            _treatment.dosage.durationType = nil;
            _treatment.dosage.duration = nil;
            _treatment.dosage.comments = nil;
            _treatment.dosage.quantity = nil;
            _treatment.dosage.recipeCount = [NSNumber numberWithInt:1];
            [self refreshPickerViews];
        }   
        default:
            break;
    }
}

- (BOOL)isEqualRemovingWhitespace:(NSString*)string toString:(NSString*)toString {
    NSString* normalizedString = [[string componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString: @""];
    NSString* normalizedToString = [[toString componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString: @""];
    
    return [normalizedString compare:normalizedToString options:NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch] == NSOrderedSame;
}

- (BOOL)checkDuplicatedUserPresentations {
    // Comprobamos que no este duplicado y si es asi mostramos el popup
    BOOL isUnique = YES;
    for (Treatment* treatment in _treatment.diagnosis.treatments) {
        
        BOOL isDuplicatedMedicament = NO;
        BOOL isDuplicatedUserMedicament = NO;
        BOOL isDuplicatedPresentation = NO;
        BOOL isDuplicatedUserPresentation = NO;
        
        if (![self.medicamentTextField.text isEqualToString:@""]) {
            if ([treatment.medicament name] != nil) {
                isDuplicatedMedicament = [self isEqualRemovingWhitespace:[treatment.medicament name] toString:self.medicamentTextField.text];
            }
            if (treatment.userMedicament != nil) {
                isDuplicatedUserMedicament = [self isEqualRemovingWhitespace:treatment.userMedicament toString:self.medicamentTextField.text];
            }
            
        }
        if (![self.presentationTextField.text isEqualToString:@""]) {
            if ([treatment.presentation name] != nil) {
                isDuplicatedPresentation = [self isEqualRemovingWhitespace:[treatment.presentation name] toString:self.presentationTextField.text];
            }
            if (treatment.userPresentation != nil) {
                isDuplicatedUserPresentation = [self isEqualRemovingWhitespace:treatment.userPresentation toString:self.presentationTextField.text];
            }
        }
        
        if (treatment != _treatment && (isDuplicatedMedicament || isDuplicatedUserMedicament) && (isDuplicatedPresentation || isDuplicatedUserPresentation)) {
            isUnique = NO;
            break;
        }
    }
    return isUnique;
}

- (void)showDuplicatedUserPresentationMultipleChoice {
    // Message indicating that there are duplicated userPresentation
    // Ask the user to confirm editing before continue
    self.choiceController = [[MultipleChoiceViewController new] autorelease];
    _choiceController.message = NSLocalizedString(@"PATIENT_DUPLICATED_USERPRESENTATION_MESSAGE", @"");
    _choiceController.acceptButtonTitle = nil;
    _choiceController.cancelButtonTitle = NSLocalizedString(@"PATIENT_DUPLICATED_USERPRESENTATION_CANCEL", @"");
    _choiceController.otherButtonTitle = nil;
    _choiceController.eventBlock = ^(MultipleChoiceButton button){
        // Dismiss and release the controller
        [_delegate dismissModalController:_choiceController];
        self.choiceController = nil;
    };
    
    // Present modal controller
    [_delegate presentModalController:_choiceController];
}

- (void)assignSelectedTherapyChoice {
    TherapyType* selectedChoice = self.therapyChoiceButtonGroup.selectedValue;
    // Assign value to model
    _treatment.therapyChoiceReason = selectedChoice;
    DLog(@"Set therapy choice: %@", selectedChoice.name);
    
    [self updateUI];
}

- (void)assignSelectedTherapyType {
    //Deepak_Carpenter: Added for therapy election
    TherapyElection* selectedType = self.therapyTypeButtonGroup.selectedValue;
      DLog(@"Set therapy type: %@--- %@", selectedType.identifier, selectedType.name);
    // TherapyType* selectedType = self.therapyTypeButtonGroup.selectedValue;

    // Assign value to model
    _treatment.therapyElection=selectedType;
 //   _treatment.therapyType = selectedType;
    _treatment.replacingMedicament = nil;
    _treatment.replaceReason = nil;
    DLog(@"Set therapy type: %@", selectedType.name);
    
    [self updateUI];
}

- (void)selectTherapyReplacementReason:(TherapyReplacementReason*)replaceReason withMedicament:(Medicament*)medicament userMedicament:(NSString*)userMedicament {
    
   
    [self assignSelectedTherapyType];
    
    self.selectorType = kSelectorTypeNone;
    _treatment.replacingMedicament = medicament;
    _treatment.userReplacingMedicament = userMedicament;
    _treatment.replaceReason = replaceReason;
    [self.popoverController dismissPopoverAnimated:YES];
    [self updateUI];
}

#pragma mark PredictiveTextFieldDelegate

- (void)predictiveTextField:(PredictiveTextField*)textField elementsForInputString:(NSString*)inputString {
  
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"])
    {
        if (textField == _medicamentTextField) {
            // Check if the recent medicament checkbox is selected
            if (_medicamentFrequentCheckBox.selected) {
                EntityFactory *entityFactory = [EntityFactory sharedEntityFactory];
                NSArray *recentMedicaments = [entityFactory fetchRecentMedicamentsforDoctorInfo:self. treatment.diagnosis.patient.doctorInfo andProductType:[self selectedProductTypeFilter]];
                
                NSMutableArray *medicaments = [[NSMutableArray alloc] initWithCapacity:[recentMedicaments count]];
                for (RecentMedicament *recentMedicament in recentMedicaments) {
                    if ([recentMedicament medicament] != nil) {
                        [medicaments addObject:[recentMedicament medicament]];
                    }
                }
                [textField fetchedElements:medicaments forInputString:inputString];
                [medicaments release];
                
                
            }
            else {
                [self.fetchTextFieldOperation cancel];
                self.fetchTextFieldOperation = [[EntityFactory sharedEntityFactory] fecthMedicamentsInBackgroundWithFilter:inputString ofType:[self selectedProductTypeFilter] onComplete:^(NSArray* resultArray){
                    [textField fetchedElements:resultArray forInputString:inputString];
                    
                    
                }];
            }
            
        }
        else if (textField == _effectTextField) {
            [self.fetchTextFieldOperation cancel];
            self.fetchTextFieldOperation = [[EntityFactory sharedEntityFactory] fetchRemovableEntitiesInBackground:NSStringFromClass([Effect class]) filteringByName:inputString sortResults:YES onComplete:^(NSArray *results) {
                [textField fetchedElements:results forInputString:inputString];
            }];
        }
        else if (textField == _drugreimbursementTextField) {
            
            [self.fetchTextFieldOperation cancel];
            self.fetchTextFieldOperation = [[EntityFactory sharedEntityFactory] fetchRemovableEntitiesInBackground:NSStringFromClass([DrugReimbursement class]) filteringByName:inputString sortResults:YES onComplete:^(NSArray *results) {
                [textField fetchedElements:results forInputString:inputString];
            }];
        }
        else if (textField == _patientInsuranceTextField) {
            
            [self.fetchTextFieldOperation cancel];
            self.fetchTextFieldOperation = [[EntityFactory sharedEntityFactory] fetchRemovableEntitiesInBackground:NSStringFromClass([PatientInsurance class]) filteringByName:inputString sortResults:YES onComplete:^(NSArray *results) {
                [textField fetchedElements:results forInputString:inputString];
            }];
        }
    }
    
  else
  {
      if (textField == _medicamentTextField) {
           // Check if the recent medicament checkbox is selected
           if (_medicamentFrequentCheckBox.selected) {
           EntityFactory *entityFactory = [EntityFactory sharedEntityFactory];
           NSArray *recentMedicaments = [entityFactory fetchRecentMedicamentsforDoctorInfo:self.treatment.diagnosis.patient.doctorInfo andProductType:[self selectedProductTypeFilter]];
           
           NSMutableArray *medicaments = [[NSMutableArray alloc] initWithCapacity:[recentMedicaments count]];
           for (RecentMedicament *recentMedicament in recentMedicaments) {
           if ([recentMedicament medicament] != nil) {
           [medicaments addObject:[recentMedicament medicament]];
           }
           }
           [textField fetchedElements:medicaments forInputString:inputString];
           [medicaments release];
           }
           else {
           [self.fetchTextFieldOperation cancel];
           self.fetchTextFieldOperation = [[EntityFactory sharedEntityFactory] fecthMedicamentsInBackgroundWithFilter:inputString ofType:[self selectedProductTypeFilter] onComplete:^(NSArray* resultArray){
           [textField fetchedElements:resultArray forInputString:inputString];
           }];
           }
        

      }
    else if (textField == _effectTextField) {
        [self.fetchTextFieldOperation cancel];
        self.fetchTextFieldOperation = [[EntityFactory sharedEntityFactory] fetchRemovableEntitiesInBackground:NSStringFromClass([Effect class]) filteringByName:inputString sortResults:YES onComplete:^(NSArray *results) {
            [textField fetchedElements:results forInputString:inputString];
        }];
    }
    else {
        DLog(@"Unhandled elements for textfield %@", textField);
        NSArray* elements = [NSArray array];
        [textField fetchedElements:elements forInputString:inputString];
    }
}
}
- (void)predictiveTextFieldValueChanged:(PredictiveTextField*)textField {
    // If change is made due to user interaction, update model
    if (!self.refreshing) {
        if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"])

        {
            if (textField == _medicamentTextField) {
                self.presentationTextField.text = @"";
                Medicament* selectedMedicament = textField.selectedElement;
                if (_treatment.medicament != selectedMedicament || (selectedMedicament == nil && textField.text != _treatment.userMedicament)) {
                    NSLog(@"MEDICAMENT Check 1:: %@",_treatment.medicament);

                    [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
                        if (accepted) {
                            [self resetSection:kMedicamentResetSection];
                            _treatment.medicament = selectedMedicament;
                            NSLog(@"MEDICAMENT Check 2:: %@",_treatment.medicament);

                            _treatment.userMedicament = nil;
                            if (_treatment.medicament == nil && textField.text > 0) {
                                _treatment.userMedicament = textField.text;
                            }
                            [self loadAvailablePresentations];
                            [self autoselectProductTypeFilter];
                            
                            // Add it to the recent medicament list
                            [self obtainOrCreateRecentMedicament:_treatment.medicament];
                          
                        }
                        [self updateUI];
                    }];
                } else {
                    DLog(@"Entered medicament is same as registered. Do nothing.");
                    
                    
                }
                
            }
            else if (textField == _presentationTextField) {
                if (_treatment.presentation != textField.selectedElement || (textField.selectedElement == nil && textField.text != _treatment.userPresentation)) {
                    [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
                        if (accepted) {
                            [self resetSection:kPresentationResetSection];
                            Presentation* selectedPresentation = _presentationTextField.selectedElement;
                            _treatment.presentation = selectedPresentation;
                            _treatment.userPresentation = nil;
                            if (_treatment.presentation == nil && textField.text > 0) {
                                if ([self checkDuplicatedUserPresentations]) {
                                    _treatment.userPresentation = textField.text;
                                } else {
                                    [self showDuplicatedUserPresentationMultipleChoice];
                                }
                            }
                            DLog(@"Set presentation: %@", selectedPresentation.name);
                        }
                        // [self setunit]; // Kanchan
                        
                        [self updateUI];
                    }];
                } else {
                    DLog(@"Entered presentation is same as registered. Do nothing.");
                }
            }
            
            
            else if (textField == _effectTextField) {
                if (_treatment.desiredEffect != textField.selectedElement || (textField.selectedElement == nil && textField.text != _treatment.userDesiredEffect)) {
                    [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
                        if (accepted) {
                            Effect* selectedEffect = _effectTextField.selectedElement;
                            _treatment.desiredEffect = selectedEffect;
                            _treatment.userDesiredEffect = nil;
                            if (_treatment.desiredEffect == nil && textField.text > 0) {
                                _treatment.userDesiredEffect = textField.text;
                            }
                            DLog(@"Set effect: %@", selectedEffect.name);
                        }
                        [self updateUI];
                    }];
                } else {
                    DLog(@"Entered desired effect is same as registered. Do nothing.");
                }
            }
            
            else if (textField == _drugreimbursementTextField) {
                if (_treatment.drugReimbursement != textField.selectedElement || (textField.selectedElement == nil && textField.text != _treatment.userDrugReimbursement)) {
                    [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
                        if (accepted) {
                            DrugReimbursement* selectedDrug = _drugreimbursementTextField.selectedElement;
                            _treatment.drugReimbursement = selectedDrug;
                            _treatment.userDrugReimbursement = nil;
                            if (_treatment.drugReimbursement == nil && textField.text > 0) {
                                _treatment.userDrugReimbursement = textField.text;
                            }
                            DLog(@"Set Drug Reimbursement: %@", selectedDrug.name);
                        }
                        [self updateUI];
                    }];
                } else {
                    DLog(@"Entered Drug Reimbursement is same as registered. Do nothing.");
                }
            }
            else if (textField == _patientInsuranceTextField) {
                if (_treatment.patientInsurance != textField.selectedElement || (textField.selectedElement == nil && textField.text != _treatment.userPatientInsurance)) {
                    [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
                        if (accepted) {
                            PatientInsurance* selectedPatientInsurance = _patientInsuranceTextField.selectedElement;
                            _treatment.patientInsurance = selectedPatientInsurance;
                            _treatment.userPatientInsurance = nil;
                            if (_treatment.patientInsurance == nil && textField.text > 0) {
                                _treatment.userPatientInsurance = textField.text;
                            }
                            DLog(@"Set Patient Insurance: %@", selectedPatientInsurance.name);
                        }
                        [self updateUI];
                    }];
                } else {
                    DLog(@"Entered Patient Insurance is same as registered. Do nothing.");
                }
            }

        
        }
        else{
            
        if (textField == _medicamentTextField) {
            
            self.presentationTextField.text = @"";
            
            Medicament* selectedMedicament = textField.selectedElement;
             
            if (_treatment.medicament != selectedMedicament || (selectedMedicament == nil && textField.text != _treatment.userMedicament)) {
                NSLog(@"MEDICAMENT Check 3:: %@",_treatment.medicament);

                [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
                    if (accepted) {
                        [self resetSection:kMedicamentResetSection];
                        _treatment.medicament = selectedMedicament;
                        _treatment.userMedicament = nil;
                        if (_treatment.medicament == nil && textField.text > 0) {
                            _treatment.userMedicament = textField.text;
                        }
                        [self loadAvailablePresentations];
                        [self autoselectProductTypeFilter];
                        
                        // Add it to the recent medicament list
                        [self obtainOrCreateRecentMedicament:_treatment.medicament];
                        NSLog(@"MEDICAMENT Check 4:: %@",_treatment.medicament);

                        
                    }
                  
                    [self updateUI];
                   
                }];
            } else {
                DLog(@"Entered medicament is same as registered. Do nothing.");
                
            }
        }
        else if (textField == _presentationTextField) {
            if (_treatment.presentation != textField.selectedElement || (textField.selectedElement == nil && textField.text != _treatment.userPresentation)) {
                [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
                    if (accepted) {
                        [self resetSection:kPresentationResetSection];
                        ///
                        Medicament* selectedMedicament = _medicamentTextField.selectedElement;
                        _treatment.medicament=selectedMedicament;
                        NSLog(@"MEDICAMENT Check 5:: %@",_treatment.medicament);

                        NSLog(@"Medicament value is %@",_treatment.medicament);
                        ///
                        Presentation* selectedPresentation = _presentationTextField.selectedElement;
                        _treatment.presentation = selectedPresentation;
                        _treatment.userPresentation = nil;
                        if (_treatment.presentation == nil && textField.text > 0) {
                            if ([self checkDuplicatedUserPresentations]) {
                                _treatment.userPresentation = textField.text;
                            } else {
                                [self showDuplicatedUserPresentationMultipleChoice];
                            }
                        }
                        DLog(@"Set presentation: %@ --- %@", selectedPresentation.name,selectedMedicament.name);
                    }
                    [self updateUI];
                }];
            } else {
                DLog(@"Entered presentation is same as registered. Do nothing.");
            }
        }
        else if (textField == _effectTextField) {
            if (_treatment.desiredEffect != textField.selectedElement || (textField.selectedElement == nil && textField.text != _treatment.userDesiredEffect)) {
                [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
                    if (accepted) {
                        Effect* selectedEffect = _effectTextField.selectedElement;
                        _treatment.desiredEffect = selectedEffect;
                        _treatment.userDesiredEffect = nil;
                        if (_treatment.desiredEffect == nil && textField.text > 0) {
                            _treatment.userDesiredEffect = textField.text;
                        }
                        DLog(@"Set effect: %@", selectedEffect.name);
                    }
                    [self updateUI];
                }];
            } else {
                DLog(@"Entered desired effect is same as registered. Do nothing.");
            }
        }
    }
    }

}

#pragma mark UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    NSInteger rows = 0;
    if (pickerView == _unitsPickerView.pickerView) {
        if ([_treatment.dosage.onDemand boolValue] || [_treatment.dosage.uniqueDose boolValue]) {
            rows = 1;
        } else {
            rows = 2;
        }
    }
    else if (pickerView == _frequencyPickerView.pickerView) { 
        rows = 1;
    }
    else if (pickerView == _treatmentLengthPickerView.pickerView) {
        rows = 2;
    }
    return rows;
}

- (NSArray*)arrayForPickerView:(UIPickerView *)pickerView component:(NSInteger)component {
    NSArray* array = nil;
    
    //Deepak Carpenter Changes Picker View for Polish
    if (pickerView == _unitsPickerView.pickerView) {
        if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {
        if (component==0) {
            array = _dosageCountArray;
        } else {
            array = _dosageQuantityArray;
        }
        }else
            if (component==0) {
                array = _dosageQuantityArray;
            } else {
                array = _dosageCountArray;
            }
    
    } else if (pickerView == _frequencyPickerView.pickerView) {
        array = _frequencyArray;
    } else if (pickerView == _treatmentLengthPickerView.pickerView) {
        if (component==0) {
            array = _dosageDurationArray;
        } else {
            array = _dosageTreatmentLengthArray;
        }
    }
    return array;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
   UILabel* tView = (UILabel*)view;
    if (pickerView==_frequencyPickerView.pickerView) {
    if (!tView){
        tView = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
        [tView setFont:[UIFont fontWithName:@"verdana" size:8]];
        tView.adjustsFontSizeToFitWidth=YES;
       
    }
    // Fill the label text here
    }
        return tView;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self arrayForPickerView:pickerView component:component].count + 1;
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString* title = nil;
    if (row == 0) {
        title = @"-";
    } else {
        id element = [[self arrayForPickerView:pickerView component:component] objectAtIndex:row-1];
        title = [element respondsToSelector:@selector(name)] ? [element name] : element;
    }
    return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    // If change is made due to user interaction, update model
    if (!self.refreshing) {
        [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
            if (accepted) {
                // Decrement in 1 due to '-' values in picker row 0
                NSInteger indexComponent0 = [pickerView selectedRowInComponent:0] - 1;
                NSInteger indexComponent1 = pickerView.numberOfComponents > 1 ? [pickerView selectedRowInComponent:1] - 1 : 0;
                if (pickerView == _unitsPickerView.pickerView) {
                    // Set both component values to not ignore simultaneus changes
                    // Component 0
                    NSNumber* quantity = nil;
                    if (index >= 0 && indexComponent0 < _dosageQuantityArray.count) {
                        id value = [_dosageQuantityArray objectAtIndex:indexComponent0];
                        quantity = [NSNumber numberWithFloat:[value floatValue]];
                    }
                    _treatment.dosage.quantity = quantity;
                    DLog(@"Set dosage quantity: %@", quantity);
                    // Component 1
                    NSNumber* units = nil;
                    if (index >= 0 && indexComponent1 < _dosageCountArray.count) {
                        id value = [_dosageCountArray objectAtIndex:indexComponent1];
                        units = [NSNumber numberWithInt:[value intValue]];
                    }
                    _treatment.dosage.units = units;
                    DLog(@"Set units count: %@", units);
                } else if (pickerView == _frequencyPickerView.pickerView) {
                    Frequency* frequency = nil;
                    if (index >= 0 && indexComponent0 < _frequencyArray.count) {
                        frequency = [_frequencyArray objectAtIndex:indexComponent0];
                    }
                    _treatment.dosage.frequency = frequency;
                    DLog(@"Set frequency: %@", frequency.name);
                } else if (pickerView == _treatmentLengthPickerView.pickerView) {
                    // Set both component values to not ignore simultaneus changes
                    // Component 0
                    NSNumber* duration = nil;
                    if (index >= 0 && indexComponent0 < _dosageDurationArray.count) {
                        id value = [_dosageDurationArray objectAtIndex:indexComponent0];
                        duration = [NSNumber numberWithInt:[value intValue]];
                    }
                    _treatment.dosage.duration = duration;
                    DLog(@"Set duration to: %@", duration);
                    // Component 1
                    DurationType* durationType = nil;
                    if (index >= 0 && indexComponent1 < _dosageTreatmentLengthArray.count) {
                        durationType = [_dosageTreatmentLengthArray objectAtIndex:indexComponent1];
                    }
                    _treatment.dosage.durationType = durationType;
                    DLog(@"Set duration type: %@", durationType.name);
                }
            } else {
                [self refreshPickerViews];
            }
        
            [self hideKeyboard];
            [self updateUI];
        }];
    }
}

#pragma mark RadioButtonDelegate
-(void)radioButtonGroupValueChanged:(RadioButtonGroup*)buttonGroup {
    [self hideKeyboard];
    // If change is made due to user interaction, update model
    if (!self.refreshing) {
        [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
            if (accepted) {
                if (buttonGroup == _therapyChoiceButtonGroup) {
                    _treatment.recommendationSpecialist = nil;
                    
                    TherapyType* selectedChoice = self.therapyChoiceButtonGroup.selectedValue;
                    if (selectedChoice.identifier.integerValue == _therapyChoiceSpecialistButton.tag) {
                        _treatment.therapyChoiceReason = nil;
                        [self showSpecialityController:kSelectorTypeTherapyChoiceSpecialist inView:_therapyChoiceSpecialistButton];
                    }
                    else {
                        [self assignSelectedTherapyChoice];
                    }
                }
                 else if (buttonGroup == _therapyTypeButtonGroup) {
                    TherapyElection* selectedType = self.therapyTypeButtonGroup.selectedValue;
                    NSLog(@"Value %@",selectedType);
                    if (selectedType.identifier.integerValue == _therapyTypeChangeButton.tag) {
                        [self showSpecialityController:kSelectorTypeTherapyChange inView:_therapyTypeChangeButton];
                    }
                    else {
                        [self assignSelectedTherapyType];
                    }
                }
            }
        }];
    }
}

#pragma mark UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == _dosageCountTextField) {
        [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
            if (accepted) {
                NSString *inputText = [textField.text stringByReplacingCharactersInRange:range withString:string];
                // Clear all non numeric characters
                NSString *pureNumbers = [[inputText componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
                if (pureNumbers.length <= 2) {
                    _dosageCountTextField.text = pureNumbers;
                }
            }
        }];
        return NO;
    } else {
        return YES;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (!self.refreshing) {
        [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
            if (accepted) {
                if (textField == _dosageCountTextField) {
                    NSNumber* recipeCount = nil;
                    if (_dosageCountTextField.text.length > 0) {
                        recipeCount = [NSNumber numberWithInt:[_dosageCountTextField.text intValue]];
                    }
                    if (recipeCount.integerValue >= kRecipeCountWarning) {
                        ConfirmationMessageController* dialogController = [[ConfirmationMessageController alloc] initWithMessage:NSLocalizedString(@"RECIPE_COUNT_WARNING_MESSAGE", nil) eventBlock:^(BOOL accepted) {
                            _selectorType = kSelectorTypeNone;
                            if (accepted) {
                                _treatment.dosage.recipeCount = recipeCount;
                                [self updateUI];
                                DLog(@"Set recipe count: %@", recipeCount);
                            } else {
                                [textField becomeFirstResponder];
                            }
                            [self.popoverController dismissPopoverAnimated:YES];
                            self.popoverController = nil;
                        }];
                        dialogController.acceptButtonTitle = NSLocalizedString(@"RECIPE_COUNT_WARNING_OK_BUTTON", nil);
                       
        
                        dialogController.cancelButtonTitle = NSLocalizedString(@"RECIPE_COUNT_WARNING_CANCEL_BUTTON", nil);
                        _selectorType = kSelectorTypeRecipeCountWarning;
                        [self.popoverController dismissPopoverAnimated:NO];
                        self.popoverController = [[[UIPopoverController alloc] initWithContentViewController:dialogController] autorelease];
                        self.popoverController.popoverContentSize = dialogController.view.frame.size;
                        self.popoverController.delegate = self;
                        [self.popoverController presentPopoverFromRect:textField.frame inView:textField.superview permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
                        [dialogController release];
                    }
                    
                    _treatment.dosage.recipeCount = recipeCount;
                    DLog(@"Set recipe count: %@", recipeCount);
                    [self updateUI];
                }
            }
        }];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark UITextViewDelegate
-(void)textViewDidEndEditing:(UITextView *)textView {
    if (!self.isRefreshing && textView == _dosageCommentTextArea.textArea) {
        [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
            if (accepted) {
                _treatment.dosage.comments = textView.text;
                DLog(@"Set comments: %@", textView.text);
                [self.popoverController dismissPopoverAnimated:YES];
                self.popoverController = nil;
            }
        }];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    BOOL allowChange = YES;
    if (textView == _dosageCommentTextArea.textArea) {
        if ([text isEqualToString:@"\n"]) {
            allowChange = NO;
            [textView resignFirstResponder];
        } else {
            // Check text length
            NSInteger maxTreatmentCommentsLength = [[_formConstants objectForKey:@"maxTreatmentCommentsLength"] integerValue];
            NSString* inputText = [textView.text stringByReplacingCharactersInRange:range withString:text];
            allowChange = inputText.length <= maxTreatmentCommentsLength;
        }
    }
    return allowChange;
}

#pragma mark CheckBoxDelegate
-(void) checkBoxDidChangeValue:(CheckBox*)checkBox {
    if (!self.isRefreshing) {
        [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
            if (accepted) {
                BOOL containDosageChanges = _treatment.dosage.units != nil || _treatment.dosage.frequency != nil || _treatment.dosage.duration != nil || _treatment.dosage.durationType != nil;
                if (checkBox == _onDemandCheckBox) {
                    NSNumber* ondemandValue = [NSNumber numberWithBool:_onDemandCheckBox.selected];
                    if ([ondemandValue boolValue] && containDosageChanges) {
                        [_changesControlProtocol shouldEditEntityLosingChanges:_treatment withControlBlock:^(BOOL accepted) {
                            if (accepted) {
                                _treatment.dosage.units = nil;
                                _treatment.dosage.frequency = nil;
                                _treatment.dosage.duration = nil;
                                _treatment.dosage.durationType = nil;
                            }
                            _treatment.dosage.onDemand = [NSNumber numberWithBool:accepted];
                            [self updateUI];
                        }];
                    } else {
                        _treatment.dosage.onDemand = ondemandValue;
                    }
                    DLog(@"Set on demand: %@", ondemandValue);
                }
                else if (checkBox == _longTermCheckBox) {
                    NSNumber* longTermValue = [NSNumber numberWithBool:_longTermCheckBox.selected];
                    _treatment.dosage.longDurationTreatment = longTermValue;
                    DLog(@"Set long duration: %@", longTermValue);
                } else if (checkBox == _uniqueDoseCheckBox) {
                    NSNumber* uniqueDoseValue = [NSNumber numberWithBool:_uniqueDoseCheckBox.selected];
                    if ([uniqueDoseValue boolValue] && containDosageChanges) {
                        [_changesControlProtocol shouldEditEntityLosingChanges:_treatment withControlBlock:^(BOOL accepted) {
                            if (accepted) {
                                _treatment.dosage.units = nil;
                                _treatment.dosage.frequency = nil;
                                _treatment.dosage.duration = nil;
                                _treatment.dosage.durationType = nil;
                            }
                            _treatment.dosage.uniqueDose = [NSNumber numberWithBool:accepted];
                            [self updateUI];
                        }];
                    } else {
                        _treatment.dosage.uniqueDose = uniqueDoseValue;
                    }
                    DLog(@"Unique dose: %@", uniqueDoseValue);
                }
            }
            [self hideKeyboard];
            [self updateUI];
        }];
    }
}

#pragma mark UIPopoverControllerDelegate
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    BOOL specialistSelected = NO;
    switch (_selectorType) {
        case kSelectorTypeTherapyChange:
            if (!_therapyChangeSelector.okButton.isEnabled) {
               // _treatment.therapyType = nil;
               //Deepak_Carpenter: Added for therapy election radio button
                _treatment.therapyElection=nil;
            }
            if (_therapyChangeSelector != nil) {
                _therapyChangeSelector = nil;
            }
            [self updateUI];
            break;
        case kSelectorTypeTherapyChoiceSpecialist:
            for (UIButton *button in _specialitySelector.buttons) {
                if (button.isHighlighted) {
                    specialistSelected = YES;
                }
            }
            if (_specialitySelector != nil && !specialistSelected) {
                _treatment.therapyChoiceReason = nil;
                _treatment.recommendationSpecialist = nil;
            }
            _specialitySelector = nil;

            [self updateUI];
            break;
        case kSelectorTypeRecipeCountWarning:
            [_dosageCountTextField becomeFirstResponder];
            break;
        default:
            break;
    }
    return YES;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    self.popoverController = nil;
}

#pragma mark - IBActions
- (IBAction)medicamentTypeFilterChanged:(UISegmentedControl*)sender {
    if (!self.isRefreshing) {
        [_changesControlProtocol shouldEditEntity:_treatment withControlBlock:^(BOOL accepted) {
            if (accepted) {
                ProductType* selectedFilter = [self selectedProductTypeFilter];
                Medicament* currentMedicament = _treatment.medicament;
                if (currentMedicament != nil && selectedFilter != nil && currentMedicament.productType != selectedFilter) {
                    // If selected filter does not match with selected medicament type, reset values
                    [self resetSection:kMedicamentResetSection];
                }
                NSUInteger selectedIndex = sender.selectedSegmentIndex;
                [self updateUI];
                sender.selectedSegmentIndex = selectedIndex;
            }
            else {
                // Restore selection
                [self autoselectProductTypeFilter];
            }
        }];
    }
}

#pragma mark SpecialitySelectionViewControllerDelegate
- (void)specialitySelectionController:(SpecialitySelectionViewController*)popup didSelectSpeciality:(id)speciality {
    self.selectorType = kSelectorTypeNone;
    [self assignSelectedTherapyChoice];
    
    _treatment.recommendationSpecialist = speciality;
    [self.popoverController dismissPopoverAnimated:YES];
    self.popoverController = nil;
    [self updateUI];
}

#pragma mark TherapyTypeChangeViewControllerDelegate
- (void)therapyTypeChangeViewController:(TherapyTypeChangeViewController*)controller didSelectReplacementReason:(TherapyReplacementReason*)replacementReason andMedicament:(Medicament*)medicament {
    [self selectTherapyReplacementReason:replacementReason withMedicament:medicament userMedicament:nil];
    self.popoverController = nil;
}

-(void) therapyTypeChangeViewController:(TherapyTypeChangeViewController*)controller didSelectReplacementReason:(TherapyReplacementReason*)replacementReason andUserMedicament:(NSString*)userMedicament {
    [self selectTherapyReplacementReason:replacementReason withMedicament:nil userMedicament:userMedicament];
    self.popoverController = nil;
}

#pragma mark - Public methods
- (BOOL)requestDismiss {
    [self.view endEditing:NO];
    return self.popoverController == nil || ![self.popoverController isPopoverVisible];
}

@end


