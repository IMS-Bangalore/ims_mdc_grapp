//
//  TherapyTypeChangeViewController.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 12/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "TherapyTypeChangeViewController.h"
#import "EntityFactory.h"

static NSString* const kIdentifierKey = @"identifier";
static NSString* const kDescriptionKey = @"name";

@interface TherapyTypeChangeViewController()
@property (nonatomic, retain) id<CancelableOperation> fetchTextFieldOperation;
@end

@implementation TherapyTypeChangeViewController
@synthesize okButton = _okButton;
@synthesize medicamentTextField = _medicamentTextField;
@synthesize therapyButtonGroup = _therapyButtonGroup;
@synthesize delegate = _delegate;
@synthesize fetchTextFieldOperation = _fetchTextFieldOperation;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)updateUI {
    
       _okButton.enabled = YES && _therapyButtonGroup.selectedValue != nil&& (_medicamentTextField.selectedElement != nil || [_medicamentTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0);
    
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Set radiobutton values
    NSArray* reasonArray = [[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([TherapyReplacementReason class]) filteringByName:@""];
    _therapyButtonGroup.tagIdentifierField = kIdentifierKey;
    _therapyButtonGroup.values = reasonArray;
    UIFont* font = [UIFont boldSystemFontOfSize:16.f];
    for (RadioButton* radioButton in _therapyButtonGroup.buttons) {
        [radioButton setImage:nil forState:UIControlStateNormal];
        [radioButton setImage:nil forState:UIControlStateHighlighted];
        [radioButton setImage:nil forState:UIControlStateDisabled];
        [radioButton setImage:nil forState:UIControlStateSelected];
        radioButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [radioButton setContentMode:UIViewContentModeCenter];
        [radioButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 8)];
        radioButton.titleLabel.font = font;
    }
    // Set description key for medicament textfield
    _medicamentTextField.allowUserValues = YES;
    _medicamentTextField.descriptionField = kDescriptionKey;
    _medicamentTextField.selectedElement = nil;
    [self updateUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    _therapyChangeLabel.text = NSLocalizedString(@"THERAPY_WHY_CHANGED", nil);
    _productReplaceLabel.text = NSLocalizedString(@"THERAPY_WHAT_REPLACES", nil);
    
    [_lackOfEfficiencyButton setTitle:NSLocalizedString(@"THERAPY_LACK_OF_EFFICACY",nil) forState:UIControlStateNormal];
    [_lackOfEfficiencyButton setTitle:NSLocalizedString(@"THERAPY_LACK_OF_EFFICACY",nil) forState:UIControlStateHighlighted];
    
    [_toleranceButton setTitle:NSLocalizedString(@"THERAPY_TOLERANCE",nil) forState:UIControlStateNormal];
    [_toleranceButton setTitle:NSLocalizedString(@"THERAPY_TOLERANCE",nil) forState:UIControlStateHighlighted];

    [_priceButton setTitle:NSLocalizedString(@"THERAPY_PRICE",nil) forState:UIControlStateNormal];
    [_priceButton setTitle:NSLocalizedString(@"THERAPY_PRICE",nil) forState:UIControlStateHighlighted];

    [_reasonButton setTitle:NSLocalizedString(@"THERAPY_OTHER_REASONS",nil) forState:UIControlStateNormal];
    [_reasonButton setTitle:NSLocalizedString(@"THERAPY_OTHER_REASONS",nil) forState:UIControlStateHighlighted];

    [_preferanceButton setTitle:NSLocalizedString(@"THERAPY_PATIENT_PREF",nil) forState:UIControlStateNormal];
    [_preferanceButton setTitle:NSLocalizedString(@"THERAPY_PATIENT_PREF",nil) forState:UIControlStateHighlighted];
    
    [_okButton setTitle:NSLocalizedString(@"MULTICHOICE_ACCEPT",nil) forState:UIControlStateNormal];
    [_okButton setTitle:NSLocalizedString(@"MULTICHOICE_ACCEPT",nil) forState:UIControlStateHighlighted];

}
- (void)viewDidUnload {
    [self setOkButton:nil];
    [self setMedicamentTextField:nil];
    [self setTherapyButtonGroup:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark PredictiveTextFieldDelegate

-(void) predictiveTextField:(PredictiveTextField*)textField elementsForInputString:(NSString*)inputString {
    [self.fetchTextFieldOperation cancel];
    self.fetchTextFieldOperation = [[EntityFactory sharedEntityFactory] fecthMedicamentsInBackgroundWithFilter:inputString ofType:nil onComplete:^(NSArray* resultArray){
        [textField fetchedElements:resultArray forInputString:inputString];
    }];
    
    
    
    
    
    
    [self updateUI];
}

-(void) predictiveTextFieldValueChanged:(PredictiveTextField*)textField {
    [self updateUI];
}

-(void)radioButtonGroupValueChanged:(RadioButtonGroup*)buttonGroup {
    
    NSLog(@"%@",buttonGroup.tagIdentifierField);
    [self updateUI];
}

- (void)dealloc {
    [_okButton release];
    [_medicamentTextField release];
    [_therapyButtonGroup release];
    [_fetchTextFieldOperation cancel];
    [_fetchTextFieldOperation release];
    _delegate = nil;
    [super dealloc];
}
- (IBAction)okButtonClicked:(id)sender {
    if (_medicamentTextField.selectedElement != nil) {
        [_delegate therapyTypeChangeViewController:self didSelectReplacementReason:_therapyButtonGroup.selectedValue andMedicament:_medicamentTextField.selectedElement];
                NSLog(@"medicament text field content %@",_medicamentTextField.selectedElement);
        Medicament *medi=nil;
        medi.presentations=_medicamentTextField.selectedElement;
        
        NSLog(@"medi.presentation text field content %@",medi.presentations);
  
    } else {
        [_delegate therapyTypeChangeViewController:self didSelectReplacementReason:_therapyButtonGroup.selectedValue andUserMedicament:_medicamentTextField.text];
        NSLog(@"meicament text field content 1 %@",_medicamentTextField.selectedElement);
    }
}



- (void)clear {
    _therapyButtonGroup.selectedValue = nil;
    _medicamentTextField.selectedElement = nil;
    [self updateUI];
}
@end
