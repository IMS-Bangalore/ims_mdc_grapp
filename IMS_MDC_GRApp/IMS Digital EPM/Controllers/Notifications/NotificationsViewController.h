//
//  NotificationsViewController.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 05/12/12.
//
//

#import <UIKit/UIKit.h>
#import "ModalControllerProtocol.h"
#import "Notification.h"

@class NotificationsViewController;
@protocol NotificationsViewControllerDelegate <ModalControllerProtocol>

- (void)notificationsControllerDidFinishLoading:(NotificationsViewController*)notificationsController;
- (void)notificationsInboxWillDisappear;

@end

@interface NotificationsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate>

#pragma mark - Properties
@property (retain, nonatomic) NSArray* notifications;
@property (assign, nonatomic) id<NotificationsViewControllerDelegate> delegate;
@property (copy, nonatomic) NSString* deviceId;
@property (copy, nonatomic) NSString* pushToken;
@property (nonatomic, copy) NSString* userId;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UIWebView *webView;

//Ravi_Bukka: added for localization

@property (retain, nonatomic) IBOutlet UILabel *mailBoxLabel;
@property (retain, nonatomic) IBOutlet UILabel *messagesLabel;

#pragma mark - Public methods
- (void)loadNotifications ;
- (NSInteger)numberOfUnreadNotificationsForDate:(NSDate*)lastDate;
- (void)selectNotification:(Notification*)notification;

@end
