//
//  NotificationCell.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 12/12/12.
//
//

#import "NotificationCell.h"

static NSString* const kDateFormat = @"dd/MM/yyyy HH:mm";

@implementation NotificationCell

- (BOOL)highlightOnSelected {
    return YES;
}

#pragma mark - Public methods

- (void)setLoading:(BOOL)loading {
    if (loading) {
        [self.loadingIndicator startAnimating];
    } else {
        [self.loadingIndicator stopAnimating];
    }
}

#pragma mark - Setters

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self refreshStatusAnimated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    [self refreshStatusAnimated:animated];
}

- (void)refreshStatusAnimated:(BOOL)animated {
    BOOL highlighted = self.highlighted || (self.highlightOnSelected && self.selected);
    for (id element in self.highlightElements) {
        if ([element respondsToSelector:@selector(setHighlighted:animated:)]) {
            [element setHighlighted:highlighted animated:animated];
        }
        else if ([element respondsToSelector:@selector(setHighlighted:)]) {
            [element setHighlighted:highlighted];
        }
    }
}

- (void)setNotification:(Notification *)notification {
    _notification = notification;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:kDateFormat];
    if ([notification.date isKindOfClass:[NSDate class]]) {
        self.notificationDateLabel.text = [dateFormatter stringFromDate:_notification.date];
    }
    if ([notification.description isKindOfClass:[NSString class]]) {
        self.notificationDescriptionLabel.text = notification.descriptionText;
    }
    [dateFormatter release];
}

#pragma mark - Dealloc

- (void)dealloc {
    [_notificationDateLabel release];
    [_notificationDescriptionLabel release];
    [_loadingIndicator release];
    [_selectedImageView release];
    [super dealloc];
}

@end
