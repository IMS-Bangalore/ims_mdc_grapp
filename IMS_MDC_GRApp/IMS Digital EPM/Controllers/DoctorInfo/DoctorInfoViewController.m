//
//  DoctorInfoViewController.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 10/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "DoctorInfoViewController.h"

#import "EntityFactory.h"
#import "FuncionesGenerales.h"
#import "University.h"
#import "Doctor.h"
#import "MedicalCenter.h"
#import "Gender.h"
#import "Specialty.h"
#import "ChangeManager.h"

//--------

#import "PatientViewController.h"
#import "DiagnosticViewController.h"
#import "UIView+FillWithSubview.h"
#import "Age.h"
#import "AgeType.h"
#import "Gender.h"
#import "ConsultType.h"
#import "EntityFactory.h"
#import "UIView+NIBLoader.h"
#import "Diagnosis.h"
#import "Diagnosis+Complete.h"
#import "Doctor.h"
#import "Dosage.h"
#import "ConfirmationMessageController.h"
#import "Patient+Complete.h"
#import "MultipleChoiceViewController.h"
#import "DoctorInfo+SortedPatients.h"
#import "Patient+SortedDiagnosis.h"
#import "Patient+Complete.h"
#import "ChangeManager.h"
#import "WeekdaySelectionView.h"
#import "WeekdaySelectionPopoverContentViewController.h"
#import "UIView+NIBLoader.h"
#import "StatsReporter.h"
#import "Smoker.h"
#import "PrescriptionAndSickFundVViewController.h"
#import "PlaceOfVisitTableViewController.h"
#import "PlaceOfVisit.h"
#import "SickFund.h"
#import "Insurance.h"

//Ravi Bukk: Added for logging feature

#import "diagnosisType.h"
#import "visitType.h"
#import "pathology.h"
#import "Presentation.h"
#import "Effect.h"
#import "DurationType.h"
#import "Frequency.h"
#import "TherapyType.h"
#import "TherapyElection.h"
#import "OtherSpecialty.h"
#import "ChangeNotifier.h"
#import "Diagnosis+SortedTreatments.h"
#import "Doctor.h"
#import "DoctorInfo.h"
#import "UnitType.h"


static NSString* const kDescriptionField = @"name";
static NSString* const kIdentifierField = @"identifier";

static NSString* const kGenderMaleId = @"1";
static NSString* const kGenderFemaleId = @"2";

static const NSInteger kTrueTag = 1;
static const NSInteger kFalseTag = 0;

static const NSInteger kMaxCommentsLength = 160;
static const NSInteger kMinDoctorAge = 23;
static const NSInteger kMinDoctorGraduationYear = 1942;
static const NSInteger kMaxOtherMedicalCenterLength = 22;
static const NSInteger kMaxUniversityLength = 53;

static const NSInteger kMaxDaysInPicker = 13;


#pragma mark - Private extension
@interface DoctorInfoViewController()
@property (nonatomic, retain) MedicalCenter* otherMedicalCenter;
@property (nonatomic, retain) University* userDefinedUniversity;
@property (nonatomic, retain) SpecialitySelectionViewController* specialitySelector;
@property (nonatomic, retain) UIPopoverController* specialityPopoverController;
@property (nonatomic, assign) BOOL selectingSecondarySpeciality;
@property (nonatomic, retain) UIPopoverController* warningPopoverController;
@property (nonatomic, retain) PopUpMensajeViewController* confirmationMessageController;
@end

#pragma mark - Implementation
@implementation DoctorInfoViewController
@synthesize keyboardAvoidingScroll = _keyboardAvoidingScroll;
@synthesize cancelButton = _cancelButton;
@synthesize saveButton = _saveButton;
@synthesize ageTextField = _ageTextField;
@synthesize genderSegmentControl = _genderSegmentControl;
@synthesize universityPredictiveTextField = _universityPredictiveTextField;
@synthesize graduationYearTextField = _graduationYearTextField;
@synthesize provincePredictiveTextField = _provincePredictiveTextField;
@synthesize mainSpecialityTextField = _mainSpecialityTextField;
@synthesize secondarySpecialityLabel = _secondarySpecialityLabel;
@synthesize secondarySpecialityButton = _secondarySpecialityButton;
@synthesize secondarySpecialityButtonGroup = _secondarySpecialityButtonGroup;
@synthesize medicalCenterCheckBoxes = _medicalCenterCheckBoxes;
@synthesize otherMedicalCenterCheckBox = _otherMedicalCenterCheckBox;
@synthesize otherMedicalCenterTextField = _otherMedicalCenterTextField;
@synthesize weeklyActivityHours = _weeklyActivityHours;
@synthesize averageWeeklyPatients = _averageWeeklyPatients;
@synthesize commentsTextArea = _commentsTextArea;
@synthesize firstColaborationComponentGroup = _firstColaborationComponentGroup;
@synthesize delegate = _delegate;
@synthesize newRegister = _newRegister;
@synthesize doctorInfo = _doctorInfo;
@synthesize otherMedicalCenter = _otherMedicalCenter;
@synthesize userDefinedUniversity = _userDefinedUniversity;
@synthesize specialitySelector = _specialitySelector;
@synthesize specialityPopoverController = _specialityPopoverController;
@synthesize selectingSecondarySpeciality = _selectingSecondarySpeciality;
@synthesize warningPopoverController = _warningPopoverController;
@synthesize confirmationMessageController = _confirmationMessageController;
@synthesize firstCollaborationDateLabel = _firstCollaborationDateLabel;
@synthesize firstCollaborationDateEndLabel = _firstCollaborationDateEndLabel;
@synthesize secondCollaborationDateLabel = _secondCollaborationDateLabel;
@synthesize secondCollaborationDateEndLabel = _secondCollaborationDateEndLabel;
@synthesize ThirdCollaborationDateLabel=_ThirdCollaborationDateLabel;
@synthesize ThirdCollaborationDateEndLabel=_ThirdCollaborationDateEndLabel;
@synthesize emailTextField = _emailTextField;

//Ravi_Bukka: Added for localization

@synthesize ageLabel = _ageLabel;
@synthesize yearsOldLabel = _yearsOldLabel;
@synthesize genderLabel = _genderLabel;
@synthesize universityLabel = _universityLabel;
@synthesize yearLicLabel = _yearLicLabel;
@synthesize provinceLabel = _provinceLabel;
@synthesize specialityLabel = _specialityLabel;
@synthesize addSpecLabel = _addSpecLabel;
@synthesize exMoreSpecLabel = _exMoreSpecLabel;
@synthesize indicateQuestLabel = _indicateQuestLabel;
@synthesize howManyHrsLabel = _howManyHrsLabel;
@synthesize avgNumberPatientLabel = _avgNumberPatientLabel;
@synthesize firstWeekColabLabel = _firstWeekColabLabel;
@synthesize secondWeekColabLabel= _secondWeekColabLabel;
@synthesize fromLabel = _fromLabel;
@synthesize toLabel = _toLabel;
@synthesize commentTitleLabel = _commentTitleLabel;
@synthesize commentIndicateLabel = _commentIndicateLabel;
@synthesize commentNotAddressedLabel = _commentNotAddressedLabel;
@synthesize firstWeekFromLabel = _firstWeekFromLabel;
@synthesize firstWeekToLabel = _firstWeekToLabel;
@synthesize healthCenterSpecialityCheckBox = _healthCenterSpecialityCheckBox;
@synthesize checkPublicHospitalCheckBox = _checkPublicHospitalCheckBox;
@synthesize privateConsultationCheckBox = _privateConsultationCheckBox;
@synthesize chekcPvtHospitalCheckBox = _chekcPvtHospitalCheckBox;

//Ravi_Bukka: Added for greece localization
@synthesize sickFundSurgeryCheckBox = _sickFundSurgeryCheckBox;

//Ravi_Bukka: Added for polish localization
@synthesize outPatientOnlyDoctorButton = _outPatientOnlyDoctorButton;
@synthesize combinedInpatientButton = _combinedInpatientButton;
@synthesize gateKeeperButton = _gateKeeperButton;
@synthesize specialistButton = _specialistButton;
@synthesize notSpecifiedButton = _notSpecifiedButton;
@synthesize doctorTypeLabel = _doctorTypeLabel;
@synthesize practiceTypeLabel = _practiceTypeLabel;
@synthesize doctorTypeButtonGroup = _doctorTypeButtonGroup;
@synthesize practiceTypeButtonGroup = _practiceTypeButtonGroup;


#pragma mark - Init and dealloc
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendLogInfo:)
                                                 name:@"createLogFile"
                                               object:nil];
    return self;
}

- (void)dealloc {
    _delegate = nil;
    [_doctorInfo release];
    [_cancelButton release];
    [_saveButton release];
    [_ageTextField release];
    [_genderSegmentControl release];
    [_universityPredictiveTextField release];
    [_graduationYearTextField release];
    [_provincePredictiveTextField release];
    [_secondarySpecialityLabel release];
    [_secondarySpecialityButtonGroup release];
    [_medicalCenterCheckBoxes release];
    [_otherMedicalCenterCheckBox release];
    [_otherMedicalCenterTextField release];
    [_weeklyActivityHours release];
    [_averageWeeklyPatients release];
    [_commentsTextArea release];
    [_firstColaborationComponentGroup release];
    [_otherMedicalCenter release];
    [_userDefinedUniversity release];
    [_mainSpecialityTextField release];
    [_specialitySelector release];
    [_specialityPopoverController release];
    [_secondarySpecialityButton release];
    [_warningPopoverController release];
    [_keyboardAvoidingScroll release];
    [_confirmationMessageController release];
    [_firstCollaborationDateLabel release];
    [_firstCollaborationDateEndLabel release];
    [_secondCollaborationDateLabel release];
    [_secondCollaborationDateEndLabel release];
    [_emailTextField release];
    [super dealloc];
}
//DeepakCarpenter

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//Ravi Bukk: Added for logging feature
- (void)sendLogInfo:(NSNotification *) notification {
    
    Patient *ourPatientSelection;
    
    //   NSMutableArray* sortedPatients = [[NSMutableArray alloc]initWithArray:[self sortedPatients]];
    //Deepak_Carpenter: added for clear array objects
    NSMutableArray *sortedPatients = nil;
    NSLog(@"Patient Count %d",[sortedPatients count]);
    sortedPatients = [NSMutableArray arrayWithArray:[self sortedPatients]];
    
    
    //_completePatientLogString = [NSString stringWithFormat:@"number of patinets -- %d\n\n", [sortedPatients count]-1];
    _completePatientLogString= [NSString string];
    
    NSLog(@"number of patinets -- %d", [sortedPatients count]);
    NSLog(@"\n\n");
    
    
    for (int i = [sortedPatients count]-2; i >= 0; i --) {
        //
        NSString *recurrenceId=nil;
        NSString *insuranceId=nil;
        NSString *insuranceOther=nil;
        
        
        //Deepak_Carpenter : Added for new changes
        NSString *diagnosis_id=nil;
        NSString *diagnosis_value=nil;
        NSString *no_Treatment=nil;
        
        NSString *product_Code=nil;
        NSString *Product_Id=nil;
        NSString* Products_Description=nil;
        NSString *Products_Presentation=nil;
        NSString *DesiredEffect_Id=nil;
        NSString *DesiredEffectManual=nil;
        NSString *dosageUnits_Id=nil;
        NSString *UniqueDose=nil;
        NSString *Long_Duration=nil;
        NSString *OnDemand=nil;
        NSString *MedicineNameReplaced_Id;
        NSString *MedicineNameReplacedManual;
        NSString *OtherComments_Value=nil;
        NSString *TherapyElectionOther_Id=nil;
        NSString *TherapyReasons_Id=nil;
        
        
        NSLog(@"patient Identifier: %@\n", [[sortedPatients objectAtIndex:i] identifier]);
        
        ourPatientSelection = [sortedPatients objectAtIndex:i];
        
        //Deepak_Carpenter : Added for new changes
        
        
        NSString* visitDate = nil;
        if ([[sortedPatients objectAtIndex:i] visitDate]) {
            // Visit date
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy/MM/dd"];
            visitDate = [dateFormatter stringFromDate:[[sortedPatients objectAtIndex:i] visitDate]];
            [dateFormatter release];
        }
        
        /////
       
        
        
        if ([[sortedPatients objectAtIndex:i]otherSickFundValue]){
            insuranceOther = [NSString stringWithFormat:@"<InsuranceOther>%@</InsuranceOther>", [[sortedPatients objectAtIndex:i]otherSickFundValue]];
        }else{
             insuranceOther = [NSString stringWithFormat:@"<InsuranceOther></InsuranceOther>"];
        }
        if([[sortedPatients objectAtIndex:i] recurranceValue]){
            recurrenceId = [NSString stringWithFormat:@"<Recurrance_Id>%@</Recurrance_Id>", [[sortedPatients objectAtIndex:i] recurranceValue]];
        }else{
            recurrenceId = [NSString stringWithFormat:@"<Recurrance_Id></Recurrance_Id>"];
        }
        if([[sortedPatients objectAtIndex:i] sickFundValue]){
            insuranceId = [NSString stringWithFormat:@"<Insurance_Id>%@</Insurance_Id>", [[sortedPatients objectAtIndex:i] sickFundValue]];
        }else
            insuranceId = [NSString stringWithFormat:@"<Insurance_Id>2</Insurance_Id>"];

//         if ([[[[sortedPatients objectAtIndex:i]consultType]identifier] integerValue] == 2)  {
//             if ([[sortedPatients objectAtIndex:i]otherSickFundValue]) {
//                insuranceOther = [NSString stringWithFormat:@"<InsuranceOther>%@</InsuranceOther>", [[sortedPatients objectAtIndex:i]otherSickFundValue]];
//             }else{
//                 
//                 if ([[sortedPatients objectAtIndex:i]otherSickFundValue])
//                     insuranceOther = [NSString stringWithFormat:@"<InsuranceOther>%@</InsuranceOther>", [[sortedPatients objectAtIndex:i]otherSickFundValue]];
//                 else
//                     
//                     insuranceOther = [NSString stringWithFormat:@"<InsuranceOther></InsuranceOther>"];
//                 
//                    recurrenceId = [NSString stringWithFormat:@"<Recurrance_Id>%@</Recurrance_Id>", [[sortedPatients objectAtIndex:i] recurranceValue]];
//                    insuranceId = [NSString stringWithFormat:@"<Insurance_Id>%@</Insurance_Id>", [[sortedPatients objectAtIndex:i] sickFundValue]];
//                 
//             }
//
//
//         }
//         else if([[[[sortedPatients objectAtIndex:i]consultType]identifier] integerValue] == 3){
//             recurrenceId = [NSString stringWithFormat:@"<Recurrance_Id></Recurrance_Id>"];
//             insuranceId = [NSString stringWithFormat:@"<Insurance_Id>%d</Insurance_Id>",3];
//             insuranceOther = [NSString stringWithFormat:@"<InsuranceOther>%@</InsuranceOther>",[[sortedPatients objectAtIndex:i]otherSickFundValue]];
//         }
//         else{
//             recurrenceId = [NSString stringWithFormat:@"<Recurrance_Id></Recurrance_Id>"];
//             insuranceId = [NSString stringWithFormat:@"<Insurance_Id></Insurance_Id>"];
//             insuranceOther = [NSString stringWithFormat:@"<InsuranceOther></InsuranceOther>"];
//         }
//
        
        ////
        
        _completePatientLogString = [_completePatientLogString stringByAppendingString:[NSString stringWithFormat:@"%@", [NSString stringWithFormat:@"%@",[@[@"<actionRequest id=\"insertPatient\">",[NSString stringWithFormat:@"<userId>%@</userId>", self.doctorInfo.identifier], [NSString stringWithFormat:@"<PatientId>%@</PatientId>", [[sortedPatients objectAtIndex:i] identifier]],[NSString stringWithFormat:@"<Age>%@</Age>", [[[sortedPatients objectAtIndex:i] age]value]],[NSString stringWithFormat:@"<AgeType_Id>%@</AgeType_Id>", [[[[sortedPatients objectAtIndex:i] age]ageType]identifier]],[NSString stringWithFormat:@"<Gender_Id>%@</Gender_Id>", [[[sortedPatients objectAtIndex:i] gender]identifier]],[NSString stringWithFormat:@"<Smoker_Id>%@</Smoker_Id>", [[[sortedPatients objectAtIndex:i] smoker]identifier]],[NSString stringWithFormat:@"<PlaceOfVisit_Id>%@</PlaceOfVisit_Id>", [[[sortedPatients objectAtIndex:i]placeOfVisit]identifier]],recurrenceId,insuranceId,insuranceOther,[NSString stringWithFormat:@"<VisitDate>%@</VisitDate>", visitDate],[NSString stringWithFormat:@"</actionRequest>\n"]] componentsJoinedByString:@""]]]];
        
        for (Diagnosis *diagnosis1 in ourPatientSelection.sortedDiagnosis)
        {
            //Gives you patient identifier starting from zero
            NSLog(@"Selected Patient %@\n",  ourPatientSelection.sortedDiagnosis);
            
            NSLog(@"selected Patient diagnosis count %d\n\n", ourPatientSelection.diagnostic.count);
            
            //Deepak_Carpenter : Added for new change in
            
            if(diagnosis1.diagnosisType.identifier)
                diagnosis_id = [NSString stringWithFormat:@"<Diagnoses_Id>%@</Diagnoses_Id>", diagnosis1.diagnosisType.identifier];
            else
                diagnosis_id =@"<Diagnoses_Id></Diagnoses_Id>";
            
            if (diagnosis1.userDiagnosis)
                diagnosis_value =[NSString stringWithFormat:@"<DiagnosesDescription>%@</DiagnosesDescription>", diagnosis1.userDiagnosis];
            else
                diagnosis_value = [NSString stringWithFormat:@"<DiagnosesDescription></DiagnosesDescription>"];
            
            // _completePatientLogString = [_completePatientLogString stringByAppendingString:[NSString stringWithFormat:@"<VisitType_Id>%@</VisitType_Id>\n", diagnosis1.visitType.identifier]];
            
            if (!diagnosis1.needsTreatment.boolValue)
                no_Treatment = [NSString stringWithFormat:@"<NoTreatment>%@</NoTreatment>",@"1"];
            else
                no_Treatment =[NSString stringWithFormat:@"<NoTreatment>%@</NoTreatment>",@"2"];
            
            
            
            
            _completePatientLogString = [_completePatientLogString stringByAppendingString:[NSString stringWithFormat:@"%@", [@[@"<actionRequest id=\"insertDiagnosis\">",
                                                                                                                                [NSString stringWithFormat:@"<userId>%@</userId>",self.doctorInfo.identifier],
                                                                                                                                [NSString stringWithFormat:@"<PatientId>%@</PatientId>", [[sortedPatients objectAtIndex:i] identifier]],
                                                                                                                                [NSString stringWithFormat:@"<DiagnosisWindow>%@</DiagnosisWindow>", diagnosis1.identifier],
                                                                                                                                diagnosis_id,
                                                                                                                                diagnosis_value,
                                                                                                                                [NSString stringWithFormat:@"<ConsultType_Id>%@</ConsultType_Id>",diagnosis1.visitType.identifier],
                                                                                                                                
                                                                                                                                [NSString stringWithFormat:@"<PathologyType_Id>%@</PathologyType_Id>",diagnosis1.pathology.identifier]                                                     ,
                                                                                                                                
                                                                                                                                no_Treatment,
                                                                                                                                @"</actionRequest>\n"] componentsJoinedByString:@""]]];
            
            for (Treatment* treatment1 in diagnosis1.sortedTreatments) {
                
                
                BOOL userDefined = treatment1.userMedicament.length > 0 || treatment1.userPresentation.length > 0;
                
                if(userDefined){
                    product_Code = [NSString stringWithFormat:@"<ProductCode_Id>%@</ProductCode_Id>", treatment1.medicament.identifier];
                }else
                    product_Code = @"<ProductCode_Id></ProductCode_Id>";
                
                
                
                
                if(treatment1.presentation.identifier)
                    Product_Id =[NSString stringWithFormat:@"<Products_Id>%@</Products_Id>", treatment1.presentation.identifier];
                else
                    Product_Id = @"<Products_Id></Products_Id>";
                
                
                if (treatment1.userMedicament)
                    Products_Description = [NSString stringWithFormat:@"<ProductsDescription>%@</ProductsDescription>", treatment1.userMedicament];
                else
                    Products_Description = @"<ProductsDescription></ProductsDescription>";
                
                if (treatment1.userPresentation)
                    Products_Presentation = [NSString stringWithFormat:@" <ProductsPresentation>%@</ProductsPresentation>", treatment1.userPresentation];
                else
                    Products_Presentation =[NSString stringWithFormat:@" <ProductsPresentation></ProductsPresentation>"];
                
                if (treatment1.desiredEffect.identifier)
                    DesiredEffect_Id = [NSString stringWithFormat:@"<DesiredEffect_Id>%@</DesiredEffect_Id>", treatment1.desiredEffect.identifier];
                else
                    DesiredEffect_Id = [NSString stringWithFormat:@"<DesiredEffect_Id></DesiredEffect_Id>"];
                
                if (treatment1.userDesiredEffect)
                    DesiredEffectManual = [NSString stringWithFormat:@"<DesiredEffectManual>%@</DesiredEffectManual>", treatment1.userDesiredEffect];
                else
                    DesiredEffectManual = [NSString stringWithFormat:@"<DesiredEffectManual></DesiredEffectManual>"];
                
                if (treatment1.presentation.unitType.identifier) {
                    dosageUnits_Id = [NSString stringWithFormat:@"<DosageUnits_Id>%@</DosageUnits_Id>",treatment1.presentation.unitType.identifier];
                }
                else
                    dosageUnits_Id = [NSString stringWithFormat:@"<DosageUnits_Id></DosageUnits_Id>"];
                //
                if (![treatment1.dosage.uniqueDose boolValue])
                    UniqueDose = [NSString stringWithFormat:@"<UniqueDose>%@</UniqueDose>", @"2"];
                else
                    UniqueDose = [NSString stringWithFormat:@"<UniqueDose>%d</UniqueDose>", treatment1.dosage.uniqueDose.intValue];
                //
                
                if (![treatment1.dosage.longDurationTreatment boolValue])
                    Long_Duration = [NSString stringWithFormat:@"<LongDuration>%@</LongDuration>", @"2"];
                else
                    Long_Duration = [NSString stringWithFormat:@"<LongDuration>%d</LongDuration>", treatment1.dosage.longDurationTreatment.intValue];
                
                if (![treatment1.dosage.onDemand boolValue])
                    OnDemand = [NSString stringWithFormat:@"<OnDemand>%@</OnDemand>", @"2"];
                else
                    OnDemand = [NSString stringWithFormat:@"<OnDemand>%d</OnDemand>", treatment1.dosage.onDemand.intValue];
                
                
                
                if (treatment1.dosage.comments)
                    OtherComments_Value = [NSString stringWithFormat:@"<OtherComments_Value>%@</OtherComments_Value>", treatment1.dosage.comments];
                else
                    OtherComments_Value = [NSString stringWithFormat:@"<OtherComments_Value></OtherComments_Value>"];
                
                //   _completePatientLogString = [_completePatientLogString stringByAppendingString:[NSString stringWithFormat:@"<Therapy_Id>%@</Therapy_Id>\n", treatment1.therapyChoiceReason.identifier]];
                
                if (treatment1.recommendationSpecialist.identifier)
                    TherapyElectionOther_Id =[NSString stringWithFormat:@" <TherapyElectionOther_Id>%@</TherapyElectionOther_Id>", treatment1.recommendationSpecialist.identifier];
                else
                    TherapyElectionOther_Id = @"<TherapyElectionOther_Id></TherapyElectionOther_Id>";
                
                
                if (treatment1.replaceReason.identifier) {
                    TherapyReasons_Id = [NSString stringWithFormat:@" <TherapyReasons_Id>%@</TherapyReasons_Id>", treatment1.replaceReason.identifier];
                }else TherapyReasons_Id = [NSString stringWithFormat:@" <TherapyReasons_Id></TherapyReasons_Id>"];
                //Deepak Carpenter : Added to fetch identifier from Presentation NSSET
                NSArray *array=[treatment1.replacingMedicament.presentations allObjects];
                Presentation *present = (Presentation*)[array objectAtIndex:0];
                
                //Deepak_Carpenter:Added for Manual entered manual Replacemedicine
                if (present.identifier) {
                    MedicineNameReplaced_Id = [NSString stringWithFormat:@"<MedicineNameReplaced_Id>%@</MedicineNameReplaced_Id>", present.identifier];
                    MedicineNameReplacedManual = [NSString stringWithFormat:@"<MedicineNameReplacedManual></MedicineNameReplacedManual>"];
                }else if(treatment1.userReplacingMedicament)
                {
                    MedicineNameReplaced_Id = [NSString stringWithFormat:@"<MedicineNameReplaced_Id></MedicineNameReplaced_Id>"];
                    MedicineNameReplacedManual = [NSString stringWithFormat:@"<MedicineNameReplacedManual>%@</MedicineNameReplacedManual>",treatment1.userReplacingMedicament];
                }else{
                    MedicineNameReplaced_Id = [NSString stringWithFormat:@"<MedicineNameReplaced_Id></MedicineNameReplaced_Id>"];
                    MedicineNameReplacedManual = [NSString stringWithFormat:@"<MedicineNameReplacedManual></MedicineNameReplacedManual>"];
                }
                
                
                _completePatientLogString = [_completePatientLogString stringByAppendingString:[NSString stringWithFormat:@"%@", [@[@"<actionRequest id=\"insertMedicine\">", [NSString stringWithFormat:@"<userId>%@</userId>", self.doctorInfo.identifier], [NSString stringWithFormat:@"<PatientId>%@</PatientId>", [[sortedPatients objectAtIndex:i] identifier]], [NSString stringWithFormat:@"<DiagnosisWindow>%@</DiagnosisWindow>", diagnosis1.identifier], [NSString stringWithFormat:@"<TreatmentWindow>%@</TreatmentWindow>", treatment1.identifier], product_Code, Product_Id, Products_Description, Products_Presentation,DesiredEffect_Id,DesiredEffectManual,dosageUnits_Id,Long_Duration,OnDemand,[NSString stringWithFormat:@"<DoseQuantity>%@</DoseQuantity>", treatment1.dosage.quantity], [NSString stringWithFormat:@"<DoseUnits>%@</DoseUnits>", treatment1.dosage.units],[NSString stringWithFormat:@"<Duration_Id>%@</Duration_Id>", treatment1.dosage.durationType.identifier],[NSString stringWithFormat:@"<DurationQuantity>%@</DurationQuantity>", treatment1.dosage.duration],[NSString stringWithFormat:@"<DoseFrequency_Id>%@</DoseFrequency_Id>", treatment1.dosage.frequency.identifier],[NSString stringWithFormat:@"<PrescribedQuantity_Value>%@</PrescribedQuantity_Value>", treatment1.dosage.recipeCount],OtherComments_Value,[NSString stringWithFormat:@"<TherapyElection_Id>%@</TherapyElection_Id>", treatment1.therapyChoiceReason.identifier],TherapyElectionOther_Id,[NSString stringWithFormat:@"<TherapyType_Id>%@</TherapyType_Id>",treatment1.therapyElection.identifier],TherapyReasons_Id,MedicineNameReplaced_Id,MedicineNameReplacedManual,@"</actionRequest>\n"] componentsJoinedByString:@""]]];
            }
            
            
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:@"PatientLogFile.xls"];
        
        [[NSUserDefaults standardUserDefaults]setValue:filePath forKey:@"filePath"];
        
        //file name to write the data to using the documents directory:
        //NSString *fileName = [NSString stringWithFormat:@"%@/PatientLogFile.xls",documentsDirectory];
        
        
        // check for file exist
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if (![fileManager fileExistsAtPath:filePath]) {
            
            // the file doesn't exist,we can write out the text using the  NSString convenience method
            
            NSError *error = noErr;
            BOOL success = [_completePatientLogString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
            if (!success) {
                // handle the error
                NSLog(@"%@", error);
            }
            
        } else {
            
            
            
            // the file already exists, append the text to the end
            
            // get a handle
            _fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
            
            // move to the end of the file
            //[_fileHandle seekToEndOfFile];
            
            // convert the string to an NSData object
            NSData *textData = [_completePatientLogString dataUsingEncoding:NSUTF8StringEncoding];
            
            
            // write the data to the end of the file
            [_fileHandle writeData:textData];
            
            // clean up
            [_fileHandle closeFile];
            
        }
        
        
    }
}

- (NSArray*)sortedPatients {
    return [self.doctorInfo sortedPatients];
}


#pragma mark - Private methods
- (void)hideKeyboard {
    [_ageTextField becomeFirstResponder];
    [_ageTextField resignFirstResponder];
}

- (void)refreshSpecialitiesUI {
    Doctor* doctor = self.doctorInfo.doctor;
    
    // Update main speciality text field
    //    Specialty* mainSpeciality = doctor.mainSpeciality;
    //    NSLog(@"mainSpeciality... %@",mainSpeciality);
    NSLog(@"Speciality is %@",_specialitySelector.defaultSelectedValue);
    
    //Deepak_Carpenter: CR#4 Added for demographic START
    Specialty* mainSpeciality = doctor.mainSpeciality;
    if (doctor.mainSpeciality==nil)
        self.doctorInfo.doctor.mainSpeciality=_specialitySelector.defaultSelectedValue;
    _mainSpecialityTextField.text = NSLocalizedString( mainSpeciality.name, nil);
    //END
    
    //Ravi_Bukka: Added for localization
    
    // Update secondary speciality text field
    
    Specialty* secondarySpeciality = doctor.secondarySpecialty;
    if ((doctor.secondarySpecialty == nil) && OtherspecialtyIndex!=nil)
        self.doctorInfo.doctor.secondarySpecialty = _specialitySelector.defaultSelectedValueOtherSpeciality;
    NSLog(@"secondarySpeciality... %@",self.doctorInfo.doctor.secondarySpecialty);
    
    _secondarySpecialityLabel.text = NSLocalizedString(secondarySpeciality.name, nil) ;
    _secondarySpecialityButtonGroup.selectedTag = secondarySpeciality != nil ? kTrueTag : kFalseTag;
}

- (void)refreshUI {
    // If the view is not loaded, no need to update the UI
    if ([self isViewLoaded]) {
        DoctorInfo* doctorInfo = self.doctorInfo;
        Doctor* doctor = doctorInfo.doctor;
        
        // Update age textfield
        NSNumber* age = doctor.age;
        if (age != nil && [age intValue] > 0) {
            _ageTextField.text = [NSString stringWithFormat:@"%d", [age intValue]];
        }
        else {
            _ageTextField.text = @"";
        }
        
        // Update Gender segment
//Utpal : CR#4 Added for Demographic - START
        Gender* gender = doctor.gender;
        
        if (doctor.gender == nil) {
            self.doctorInfo.doctor.gender = _genderSegmentControl.customSelectedValue;
        }else{
            _genderSegmentControl.selectedValue = gender;
        }
        
        // _genderSegmentControl.selectedValue = gender;
        
        // Update University texfield
        // Update University texfield
        University* university = doctor.university;
        
        if (university != nil && university.userDefined.boolValue) {
            _universityPredictiveTextField.selectedElement = nil;
            _universityPredictiveTextField.text = university.name;
            
            // Store userdefined university for later use
            self.userDefinedUniversity = university;
        }else if (self.doctorInfo.doctor.university == nil)
        {
            self.doctorInfo.doctor.university = _universityPredictiveTextField.defaultUniversitySelectedValue;
            NSLog(@"defaultUniversitySelectedValue.. %@",self.doctorInfo.doctor.university);
            
        }
        else {
            _universityPredictiveTextField.selectedElement = university;
            self.userDefinedUniversity = nil;
        }
        
        // Update Graduation year textfield
        NSString* graduationYear = doctor.graduationYear;
        _graduationYearTextField.text = graduationYear;
        
        
        // Update Province texfield
        //Utpal : CR#4 Added for Demographic provinces START
        Province* province = doctor.province;
        if (doctor.province==nil) {
            self.doctorInfo.doctor.province=_provincePredictiveTextField.defaultSelectedValue;
            _provincePredictiveTextField.selectedElement=self.doctorInfo.doctor.province;
        }
        else
            _provincePredictiveTextField.selectedElement = province;
        
        
//        Province* province = doctor.province;
//        _provincePredictiveTextField.selectedElement = province;
//
        
//Utpal:CR#4 Demographic information for centers, added below lines
      
  /*
        // Update medical center checkboxes
        NSSet* medicalCenters = doctorInfo.medicalCenters;
        for (CheckBox* checkBox in _medicalCenterCheckBoxes) {
            checkBox.selected = [medicalCenters containsObject:checkBox.value];
           // NSLog(@"Check Box VAlue %c",[medicalCenters containsObject:checkBox.value]);
        }
        
        // Update other medical center checkbox and textfield
        MedicalCenter* otherMedicalCenter = nil;
        for (MedicalCenter* medicalCenter in medicalCenters) {
            // Search for user-defined medical center
            if ([medicalCenter.userDefined boolValue]) {
                otherMedicalCenter = medicalCenter;
                break;
            }
           
        }
        self.otherMedicalCenter = otherMedicalCenter;
        _otherMedicalCenterCheckBox.selected = otherMedicalCenter != nil;
        _otherMedicalCenterTextField.enabled = otherMedicalCenter != nil;
        //_otherMedicalCFenterTextField.text = otherMedicalCenter.name;
        _otherMedicalCenterTextField.text=doctorInfo.otherMedicalCenter;
*/
        //Utpal:CR#4 Demographic information for centers, added below lines
        
        for (int i = 0; i < [[[NSUserDefaults standardUserDefaults] objectForKey:@"demographicCentreArray"] count]; i++ ) {
            
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"demographicCentreArray"]);
            
            NSLog(@"%lu",(unsigned long)[[[NSUserDefaults standardUserDefaults] objectForKey:@"demographicCentreArray"] count]);
            
            switch ([[[[NSUserDefaults standardUserDefaults] objectForKey:@"demographicCentreArray"] objectAtIndex:i] intValue]) {
                case 5:
                    _healthCenterSpecialityCheckBox.selected = 1;
                    break;
                case 4:
                    _checkPublicHospitalCheckBox.selected = 1;
                    break;
                case 2:
                    _sickFundSurgeryCheckBox.selected = 1;
                    break;
                case 1:
                    _chekcPvtHospitalCheckBox.selected = 1;
                    break;
                case 3:
                    _privateConsultationCheckBox.selected = 1;
                    break;
                    
                case 8:
                    _otherMedicalCenterCheckBox.selected = 1;
                    _otherMedicalCenterTextField.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"OtherMedicalFieldText"];
                    _otherMedicalCenterTextField.enabled=YES ; //Utpal: CR#4 Demographic for Enbled the text Field.
                    
                    break;
                default:
                    break;
            }
            
        }
        
        [self refreshSpecialitiesUI];
        
        // Update weekly activity hours
        NSNumber* weeklyActivityHours = doctorInfo.weeklyActivityHours;
        if (weeklyActivityHours != nil && [weeklyActivityHours intValue] > 0) {
            _weeklyActivityHours.text = [NSString stringWithFormat:@"%d", [weeklyActivityHours intValue]];
        }
        else {
            _weeklyActivityHours.text = @"";
        }
        
        // Update weekly average patients
        NSNumber* averageWeeklyPatients = doctorInfo.averageWeeklyPatients;
        if (averageWeeklyPatients != nil && [averageWeeklyPatients intValue] > 0) {
            _averageWeeklyPatients.text = [NSString stringWithFormat:@"%d", [averageWeeklyPatients intValue]];
        }
        else {
            _averageWeeklyPatients.text = @"";
        }
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        
        NSString* colabCount = [[NSUserDefaults standardUserDefaults] valueForKey:@"CollaborationCount"];
        // Second Date
//Ravi_Bukka: Handled multiple collaboration issue
        if ([colabCount isEqualToString:@"1"])
        {
            
            NSDate* firstDate = self.doctorInfo.firstCollaborationDate;
            NSDate* firstEndDate = self.doctorInfo.firstCollaborationDateEnd;
            

                self.firstCollaborationDateLabel.text = [dateFormatter stringFromDate:firstDate];
                self.firstCollaborationDateEndLabel.text = [dateFormatter stringFromDate:firstEndDate];
                self.secondCollaborationDateLabel.text = @"";
                self.secondCollaborationDateEndLabel.text = @"";
                
           

        }
        else if ([colabCount isEqualToString:@"2"])
        {
            
            NSDate* firstDate = self.doctorInfo.firstCollaborationDate;
            NSDate* firstEndDate = self.doctorInfo.firstCollaborationDateEnd;
            NSDate* secondDate = self.doctorInfo.secondCollaborationDate;
            NSDate* secondEndDate = self.doctorInfo.secondCollaborationDateEnd;
            
            if ([firstDate compare:secondDate] == NSOrderedDescending) {
                self.firstCollaborationDateLabel.text = [dateFormatter stringFromDate:secondDate];
                self.firstCollaborationDateEndLabel.text = [dateFormatter stringFromDate:secondEndDate];
                self.secondCollaborationDateLabel.text = [dateFormatter stringFromDate:firstDate];
                self.secondCollaborationDateEndLabel.text = [dateFormatter stringFromDate:firstEndDate];
                
                
            } else {
                self.firstCollaborationDateLabel.text = [dateFormatter stringFromDate:firstDate];
                self.firstCollaborationDateEndLabel.text = [dateFormatter stringFromDate:firstEndDate];
                self.secondCollaborationDateLabel.text = [dateFormatter stringFromDate:secondDate];
                self.secondCollaborationDateEndLabel.text = [dateFormatter stringFromDate:secondEndDate];
                
            }

            
        }

        
        
        // Update email text field
        NSString* email = doctorInfo.email;
        _emailTextField.text = email;
        
        // Update comments text area
        NSString* comments = doctorInfo.comments;
        _commentsTextArea.text = comments;
        
        if (_newRegister) {
            _cancelButton.hidden = YES;
            [_saveButton setTitle:NSLocalizedString(@"DOCTOR_SAVE_BUTTON", @"") forState:UIControlStateNormal];
        }
        else {
            _cancelButton.hidden = NO;
//Ravi_Bukka: Added for localizing Cancel button
            [_cancelButton setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
            [_saveButton setTitle:NSLocalizedString(@"DOCTOR_UPDATE_BUTTON", @"") forState:UIControlStateNormal];
        }

        [dateFormatter release];
    }
}

- (void)cleanTempVariables {
    self.otherMedicalCenter = nil;
    self.userDefinedUniversity = nil;
}

- (void)showSpecialityController:(BOOL)isMainSpeciality {
    [self hideKeyboard];
    
    self.selectingSecondarySpeciality = !isMainSpeciality;
    UIView* view;
    
    if (_specialitySelector == nil)
        _specialitySelector = [[SpecialitySelectionViewController alloc] init];
    
    _specialitySelector.values = [[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([Specialty class]) filteringByName:nil];
    _specialitySelector.delegate = self;
    
//    if (_specialitySelector == nil) {
//        _specialitySelector = [[SpecialitySelectionViewController alloc] init];
//        _specialitySelector.values = [[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([Specialty class]) filteringByName:nil];
//        _specialitySelector.delegate = self;
//    }
    
    if (isMainSpeciality) {
        _specialitySelector.windowTitle = NSLocalizedString(@"DOCTOR_MAIN_SPECIALITY_POPUP_TITLE", @"");
        _specialitySelector.disabled = self.doctorInfo.doctor.secondarySpecialty;
        [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
        view = _mainSpecialityTextField;
    }
    else {
        _specialitySelector.windowTitle = NSLocalizedString(@"DOCTOR_SECONDARY_SPECIALITY_POPUP_TITLE", @"");
        _specialitySelector.disabled = self.doctorInfo.doctor.mainSpeciality;
        view = _secondarySpecialityButton;
    }
    
    CGSize size = CGSizeMake(_specialitySelector.view.frame.size.width,_specialitySelector.view.frame.size.height);
    
    self.specialityPopoverController = [[[UIPopoverController alloc] initWithContentViewController:_specialitySelector] autorelease];
    _specialityPopoverController.popoverContentSize = size;
    _specialityPopoverController.delegate = self;
    
    [_delegate doctorInfoViewControllerWillPresentPopUp:self];
    [_specialityPopoverController presentPopoverFromRect:view.frame inView:view.superview permittedArrowDirections:UIPopoverArrowDirectionDown | UIPopoverArrowDirectionUp animated:YES];
}

- (void)dismissSpecialityPopover {
    [self.specialityPopoverController dismissPopoverAnimated:YES];
    [self popoverControllerDidDismissPopover:_specialityPopoverController];
}

- (void)saveChangesAndClose {
    // Removed userDefined university if not needed
    if (_userDefinedUniversity != nil && self.doctorInfo.doctor.university != _userDefinedUniversity) {
        [[[EntityFactory sharedEntityFactory] managedObjectContext] deleteObject:_userDefinedUniversity];
    }
    
    // Removed userDefined medicalCenter if not needed
    if (_otherMedicalCenter != nil && ![self.doctorInfo.medicalCenters containsObject:_otherMedicalCenter]) {
        [[[EntityFactory sharedEntityFactory] managedObjectContext] deleteObject:_otherMedicalCenter];
    }
    
    // Clean temporal variables
    [self cleanTempVariables];
    
    // Notify the change manager about the update
    [[ChangeManager manager] updateDoctorInfo:self.doctorInfo];
    
    // Save changes
    [[ChangeManager manager] confirmChangesToEntity:self.doctorInfo recursive:NO];
    
    // Notify to delegate
    [_delegate doctorInfoViewController:self didSaveChanges:self.doctorInfo];
}

- (BOOL)checkValidEmail:(NSString*)email {
    BOOL validEmail = NO;
    if (email != nil) {
        NSString *expression = @"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSTextCheckingResult* regexResult = [regex firstMatchInString:email options:0 range:NSMakeRange(0, [email length])];
        validEmail = regexResult != nil;
    }
    return validEmail;
}

- (NSArray*)getWrongFields {
    DoctorInfo* doctorInfo = self.doctorInfo;
    Doctor* doctor = doctorInfo.doctor;
    
//Ravi_Bukka: Changed for Greek language 
    
    // Check wrong fields
    NSMutableArray* wrongFields = [NSMutableArray array];
    
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {
        
        if ([doctor.age integerValue] < kMinDoctorAge) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_AGE:", nil)];
        }
        if (doctor.gender == nil) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_SEX:", nil)];
        }
//        if (doctor.university == nil) {
//            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_AVG_NO_PAT", nil)];
//        }
        if (doctor.graduationYear.intValue < kMinDoctorGraduationYear || doctor.graduationYear.intValue > [FuncionesGenerales AnioActual]) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_HR_ACT", nil)];
        }
        
        if (doctor.province == nil) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_CENTRE", nil)];
        }
        if (doctor.mainSpeciality == nil) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_MAIN_SPL", nil)];
        }
        
        if (doctorInfo.weeklyActivityHours.integerValue == 0) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_YR_DEGREE", nil)];
        }
        if (doctorInfo.averageWeeklyPatients.integerValue == 0) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_BACHELOR_PLACE", nil)];
        }
        if (doctorInfo.email == nil || ![self checkValidEmail:doctorInfo.email]) {
            [wrongFields addObject:@"Email"];
        }
        
    }
    
    else if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"]) {
        
        if ([doctor.age integerValue] < kMinDoctorAge) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_AGE:", nil)];
        }
        if (doctor.gender == nil) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_SEX:", nil)];
        }
        if (doctor.university == nil) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_AVG_NO_PAT", nil)];
        }
        if (doctor.graduationYear.intValue < kMinDoctorGraduationYear || doctor.graduationYear.intValue > [FuncionesGenerales AnioActual]) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_HR_ACT", nil)];
        }
        if (doctor.province == nil) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_CENTRE", nil)];
        }
        
        if (doctor.mainSpeciality == nil) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_MAIN_SPL", nil)];
        }
//        if (doctorInfo.medicalCenters.count == 0) {
//            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_PROVINCE", nil)];
//        }
        //Deepak_Carpenter: Added to show wrong field error for medical center text field
//        if(_otherMedicalCenterCheckBox.selected){
//           
//            if([doctorInfo.otherMedicalCenter length]==0 || [_otherMedicalCenterTextField.text length]==0){
//                [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_MEDICAL_CENTER", nil)];
//            }
//        }
        
//Utpal: CR#4 Added to show wrong field error for medical center text field
            if(_otherMedicalCenterCheckBox.selected){
                
                if([doctorInfo.otherMedicalCenter length]==0 || [_otherMedicalCenterTextField.text length]==0){
                    [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_MEDICAL_CENTER", nil)];
                }
            }
        if (_healthCenterSpecialityCheckBox.selected || _checkPublicHospitalCheckBox.selected || _sickFundSurgeryCheckBox.selected || _privateConsultationCheckBox.selected || _chekcPvtHospitalCheckBox.selected ) {
            
        }
        else {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_CENTRE", nil)];
        }
        
        if (doctorInfo.weeklyActivityHours.integerValue == 0) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_YR_DEGREE", nil)];
        }
        if (doctorInfo.averageWeeklyPatients.integerValue == 0) {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_BACHELOR_PLACE", nil)];
        }
        if (doctorInfo.email == nil || ![self checkValidEmail:doctorInfo.email]) {
            [wrongFields addObject:@"Email"];
        }
        
        
    }
    
    else {
    if ([doctor.age integerValue] < kMinDoctorAge) {
        [wrongFields addObject:NSLocalizedString(@"DOCTOR_AGE:", nil)];
    }
    if (doctor.gender == nil) {
        [wrongFields addObject:NSLocalizedString(@"DOCTOR_SEX:", nil)];
    }
    if (doctor.university == nil) {
        [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_AVG_NO_PAT", nil)];
    }
    if (doctor.graduationYear.intValue < kMinDoctorGraduationYear || doctor.graduationYear.intValue > [FuncionesGenerales AnioActual]) {
        [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_HR_ACT", nil)];
    }
    if (doctor.province == nil) {
        [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_CENTRE", nil)];
    }
    if (doctor.mainSpeciality == nil) {
        [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_MAIN_SPL", nil)];
    }
//    if (doctorInfo.medicalCenters.count == 0) {
//        [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_PROVINCE", nil)];
//    }
        
//Utpal:CR#4 Demographic for centre list
            if(_otherMedicalCenterCheckBox.selected){
                
                if([_otherMedicalCenterTextField.text length]==0){
                    [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_MEDICAL_CENTER", nil)];
                }
            }
        
        if (_healthCenterSpecialityCheckBox.selected || _checkPublicHospitalCheckBox.selected || _sickFundSurgeryCheckBox.selected || _privateConsultationCheckBox.selected || _chekcPvtHospitalCheckBox.selected ) {
            
        }
        else {
            [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_CENTRE", nil)];
        }

        
    if (doctorInfo.weeklyActivityHours.integerValue == 0) {
        [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_YR_DEGREE", nil)];
    }
    if (doctorInfo.averageWeeklyPatients.integerValue == 0) {
        [wrongFields addObject:NSLocalizedString(@"DOCTOR_BLANK_BACHELOR_PLACE", nil)];
    }
    if (doctorInfo.email == nil || ![self checkValidEmail:doctorInfo.email]) {
        [wrongFields addObject:@"Email"];
    }
    }
    return wrongFields;
}

#pragma mark - Public methods
- (void)setDoctorInfo:(DoctorInfo *)doctorInfo {
    [doctorInfo retain];
    [_doctorInfo release];
    _doctorInfo = doctorInfo;

    self.newRegister = [self getWrongFields].count != 0;
    
    [self refreshUI];
}

- (void)willPerformLogout {
    self.doctorInfo = nil;
    [[ChangeManager manager] discardChanges];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Kanchan Nair: CR#5 Sends Notification from doctor to dismiss the update popup from login background
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"todismissUpdate" object:nil];
    

    
//Deepak_Carpenter CR#11 : Segment Control appearance for iOS 8 +
        
[[UISegmentedControl appearance] setTitleTextAttributes:@{
                    NSForegroundColorAttributeName : [UIColor whiteColor]
                                                        } forState:UIControlStateNormal];
[[UISegmentedControl appearance] setTitleTextAttributes:@{
                    NSForegroundColorAttributeName : [UIColor whiteColor]
                                                        } forState:UIControlStateSelected];
    
    
    _keyboardAvoidingScroll.delaysContentTouches = NO;
    
    // Prepare provinces predictive textfield
    NSArray* provinces = [[EntityFactory sharedEntityFactory] fetchProvinces];
    _provincePredictiveTextField.staticElements = provinces;
    _provincePredictiveTextField.descriptionField = kDescriptionField;
    
//Utpal: CR#4 Added for demographic Provinces- START
    
    NSArray *defaultProvinces = [[EntityFactory sharedEntityFactory]fetchProvinces];
    NSMutableArray *defaultProvincesMutableArray = [NSMutableArray new];
    for (int i=1;i<=defaultProvinces.count;i++) {
        [defaultProvincesMutableArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    _provincePredictiveTextField.tagIdentifierField=kIdentifierField;
    _provincePredictiveTextField.tagArray=[NSArray arrayWithArray:defaultProvincesMutableArray];
    _provincePredictiveTextField.values=defaultProvinces;

    //Utpal: CR#4 Added for demographic Speciality- START Speciality
    
    _specialitySelector = [[SpecialitySelectionViewController alloc]init];
    NSArray *defaultSpeciality = [[EntityFactory sharedEntityFactory]fetchEntities:NSStringFromClass([Specialty class]) filteringByName:nil];
    _specialitySelector.tagIdentifierField=kIdentifierField;
    NSMutableArray *defaultSpecialityMutableArray = [NSMutableArray new];
    for (int i=1;i<=defaultSpeciality.count;i++) {
        [defaultSpecialityMutableArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    _specialitySelector.tagArray=[NSArray arrayWithArray:defaultSpecialityMutableArray];
    _specialitySelector.defaultValues=defaultSpeciality;
    //END
    
    //Utpal: CR#4 Added for demographic University- START
    NSString* nameFilter = [[NSUserDefaults standardUserDefaults] valueForKey:@"NameFilter"];
    
    NSArray* universities = [[EntityFactory sharedEntityFactory] fetchUniversities:nameFilter];
    _universityPredictiveTextField.staticElements = universities;
    _universityPredictiveTextField.descriptionField = kDescriptionField;
    _universityPredictiveTextField.allowUserValues = YES;
    _universityPredictiveTextField.maxTextLength = kMaxUniversityLength;
    
    
    NSArray *defaultUniversity = [[EntityFactory sharedEntityFactory]fetchUniversities:nameFilter];
    NSMutableArray *defaultUniversityMutableArray = [NSMutableArray new];
    for (int i=1;i<=defaultUniversity.count;i++) {
        [defaultUniversityMutableArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    _universityPredictiveTextField.tagIdentifierFieldUniversity=kIdentifierField;
    _universityPredictiveTextField.tagArrayUniversity=[NSArray arrayWithArray:defaultUniversityMutableArray];
    _universityPredictiveTextField.valuesUniversity=defaultUniversity;

//    // Prepare university predictive textfield
//    _universityPredictiveTextField.descriptionField = kDescriptionField;
//    _universityPredictiveTextField.allowUserValues = YES;
//    _universityPredictiveTextField.maxTextLength = kMaxUniversityLength;
    
    // Prepare medical center check boxes
    NSArray* medicalCenters = [[EntityFactory sharedEntityFactory] fetchMedicalCenters];
    NSMutableDictionary* medicalCenterByIdentifier = [NSMutableDictionary dictionaryWithCapacity:medicalCenters.count];
    for (MedicalCenter* center in medicalCenters) {
        NSString* identifier = center.identifier;
        [medicalCenterByIdentifier setObject:center forKey:identifier];
    }
    for (CheckBox* checkBox in _medicalCenterCheckBoxes) {
        
        NSString* tagIdentifier = [NSString stringWithFormat:@"%d", checkBox.tag];
      //  NSLog(@"checkbox value %d",checkBox.tag);
        MedicalCenter* center = [medicalCenterByIdentifier objectForKey:tagIdentifier];
        checkBox.value = center;
        [checkBox setTitle:center.name forState:UIControlStateNormal];
      //    NSLog(@"Check Box VAlue %@",center.identifier);
    }
    
    // Prepare gender segmented control
    NSArray* genders = [[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([Gender class]) predicate:nil sortResults:NO];
    _genderSegmentControl.tagIdentifierField = kIdentifierField;
    _genderSegmentControl.tagArray = [NSArray arrayWithObjects:kGenderMaleId, kGenderFemaleId, nil];
    _genderSegmentControl.values = genders;
    
    // Add placeholder for some textfields
    _commentsTextArea.placeholder = NSLocalizedString(@"COMMENTS", nil);
    
    //Utpal: CR#4 Demographic for Enbled the text Field.
    _otherMedicalCenterTextField.enabled=NO;
    OtherspecialtyIndex = [[NSUserDefaults standardUserDefaults] valueForKey:@"OtherSpeciality"];
}

//- (void)viewDidAppear:(BOOL)animated {
//    
//    [super viewDidAppear:animated];
//    
//    //Ravi_Bukka: Added for the localization
//    
//    self.ageLabel.text = NSLocalizedString(@"DOCTOR_AGE:",nil);
//    self.yearsOldLabel.text = NSLocalizedString(@"DOCTOR_YEARS_OLD",nil);
//    self.genderLabel.text = NSLocalizedString(@"DOCTOR_SEX:",nil);
//    self.universityLabel.text = NSLocalizedString(@"DOCTOR_UNIVERSITY:",nil);
//    self.yearLicLabel.text = NSLocalizedString(@"DOCTOR_YEAR_LIC.:",nil);
//    self.provinceLabel.text = NSLocalizedString(@"DOCTOR_PROVINCE:",nil);
//    self.specialityLabel.text = NSLocalizedString(@"DOCTOR_MAIN_SPECIALTY",nil);
//    self.addSpecLabel.text = NSLocalizedString(@"DOCTOR_ADDITIONAL_SPECIALTY",nil);
//    self.exMoreSpecLabel.text = NSLocalizedString(@"DOCTOR_MORE_SPECIALTY?",nil);
//    self.indicateQuestLabel.text = NSLocalizedString(@"DOCTOR_INDICATE_CENTRE:",nil);
//    self.howManyHrsLabel.text = NSLocalizedString(@"DOCTOR_HRS_OF_ACTIVITY?",nil);
//    self.avgNumberPatientLabel.text = NSLocalizedString(@"DOCTOR_AVG_NUMBER_OF_PATIENTS?",nil);
//    self.firstWeekColabLabel.text = NSLocalizedString(@"DOCTOR_FIRSTWEEK_COLLABORATION",nil);
//    self.secondWeekColabLabel.text = NSLocalizedString(@"DOCTOR_SECONDWEEK_COLLABORATION",nil);
//    self.fromLabel.text = NSLocalizedString(@"DOCTOR_FROM_DATE:",nil);
//    self.toLabel.text = NSLocalizedString(@"DOCTOR_TO_DATE:",nil);
//    self.commentTitleLabel.text = NSLocalizedString(@"DOCTOR_COMMENTS.",nil);
//    self.commentIndicateLabel.text = NSLocalizedString(@"DOCTOR_INDICATE_DAYS_WITHIN_PERIOD",nil);
//    self.commentNotAddressedLabel.text = NSLocalizedString(@"DOCTOR_NOTADDRESSED.",nil);
//    self.firstWeekFromLabel.text = NSLocalizedString(@"DOCTOR_FROM_DATE:", nil);
//    self.firstWeekToLabel.text = NSLocalizedString(@"DOCTOR_TO_DATE:", nil);
//    
//    
//
//}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    
//Ravi_Bukka: Added for the localization
    
    self.ageLabel.text = NSLocalizedString(@"DOCTOR_AGE:",nil);
    self.yearsOldLabel.text = NSLocalizedString(@"DOCTOR_YEARS_OLD",nil);
    self.genderLabel.text = NSLocalizedString(@"DOCTOR_SEX:",nil);
    self.universityLabel.text = NSLocalizedString(@"DOCTOR_UNIVERSITY:",nil);
    self.yearLicLabel.text = NSLocalizedString(@"DOCTOR_YEAR_LIC.:",nil);
    self.provinceLabel.text = NSLocalizedString(@"DOCTOR_PROVINCE:",nil);
    self.specialityLabel.text = NSLocalizedString(@"DOCTOR_MAIN_SPECIALTY",nil);
    self.addSpecLabel.text = NSLocalizedString(@"DOCTOR_ADDITIONAL_SPECIALTY",nil);
    self.exMoreSpecLabel.text = NSLocalizedString(@"DOCTOR_MORE_SPECIALTY?",nil);
    self.indicateQuestLabel.text = NSLocalizedString(@"DOCTOR_INDICATE_CENTRE:",nil);
    self.howManyHrsLabel.text = NSLocalizedString(@"DOCTOR_HRS_OF_ACTIVITY?",nil);
    self.avgNumberPatientLabel.text = NSLocalizedString(@"DOCTOR_AVG_NUMBER_OF_PATIENTS?",nil);
    self.firstWeekColabLabel.text = NSLocalizedString(@"DOCTOR_FIRSTWEEK_COLLABORATION",nil);
    self.secondWeekColabLabel.text = NSLocalizedString(@"DOCTOR_SECONDWEEK_COLLABORATION",nil);
    self.fromLabel.text = NSLocalizedString(@"DOCTOR_FROM_DATE:",nil);
    self.toLabel.text = NSLocalizedString(@"DOCTOR_TO_DATE:",nil);
    self.commentTitleLabel.text = NSLocalizedString(@"DOCTOR_COMMENTS.",nil);
    self.commentIndicateLabel.text = NSLocalizedString(@"DOCTOR_INDICATE_DAYS_WITHIN_PERIOD",nil);
    self.commentNotAddressedLabel.text = NSLocalizedString(@"DOCTOR_NOTADDRESSED.",nil);
    self.firstWeekFromLabel.text = NSLocalizedString(@"DOCTOR_FROM_DATE:", nil);
    self.firstWeekToLabel.text = NSLocalizedString(@"DOCTOR_TO_DATE:", nil);
    
    self.mainSpecialityTextField.placeholder = NSLocalizedString(@"DOCTOR_MAIN_SPECIALITY", nil);
    
    [self.secondarySpecialityButton setTitle:NSLocalizedString(@"DOCTOR_YES", nil) forState:UIControlStateNormal];
    [self.secondarySpecialityButton setTitle:NSLocalizedString(@"DOCTOR_YES", nil) forState:UIControlStateHighlighted];
    
    [self.otherMedicalCenterCheckBox setTitle:NSLocalizedString(@"DOCTOR_OTHER_WHAT?", nil) forState:UIControlStateNormal];
    [self.otherMedicalCenterCheckBox setTitle:NSLocalizedString(@"DOCTOR_OTHER_WHAT?", nil) forState:UIControlStateHighlighted];
    
    
    [self.healthCenterSpecialityCheckBox setTitle:NSLocalizedString(@"DOCTOR_HEALTHCENTRE_SPECIALTY", nil) forState:UIControlStateNormal];
     [self.healthCenterSpecialityCheckBox setTitle:NSLocalizedString(@"DOCTOR_HEALTHCENTRE_SPECIALTY", nil) forState:UIControlStateHighlighted];
    
    [self.checkPublicHospitalCheckBox setTitle:NSLocalizedString(@"DOCTOR_CHECK_PUBLIC_HOSPITAL", nil) forState:UIControlStateNormal];
    [self.checkPublicHospitalCheckBox setTitle:NSLocalizedString(@"DOCTOR_CHECK_PUBLIC_HOSPITAL", nil) forState:UIControlStateHighlighted];
    
    [self.privateConsultationCheckBox setTitle:NSLocalizedString(@"DOCTOR_PVT_CONSULTATION", nil) forState:UIControlStateNormal];
    [self.privateConsultationCheckBox setTitle:NSLocalizedString(@"DOCTOR_PVT_CONSULTATION", nil) forState:UIControlStateHighlighted];
    
    [self.chekcPvtHospitalCheckBox setTitle:NSLocalizedString(@"DOCTOR_CHECK_PVT_HOSPITAL", nil) forState:UIControlStateNormal];
    [self.chekcPvtHospitalCheckBox setTitle:NSLocalizedString(@"DOCTOR_CHECK_PVT_HOSPITAL", nil) forState:UIControlStateHighlighted];
    
//Ravi_Bukka: Added for Greece localization
    
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"]) {
    
    [self.sickFundSurgeryCheckBox setTitle:NSLocalizedString(@"DOCTOR_SICK_FUND_SURGERY", nil) forState:UIControlStateNormal];
    [self.sickFundSurgeryCheckBox setTitle:NSLocalizedString(@"DOCTOR_SICK_FUND_SURGERY", nil) forState:UIControlStateHighlighted];
        
    }
    
//Ravi_Bukka: Added for Polish localization
    
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {
        
    
        _doctorTypeLabel.text = NSLocalizedString(@"DOCTOR_TYPE", nil);
        [_outPatientOnlyDoctorButton setTitle:NSLocalizedString(@"DOCTOR_OUTPATIENT_ONLY", nil) forState:UIControlStateNormal];
        [_outPatientOnlyDoctorButton setTitle:NSLocalizedString(@"DOCTOR_OUTPATIENT_ONLY", nil) forState:UIControlStateHighlighted];
        [_combinedInpatientButton setTitle:NSLocalizedString(@"DOCTOR_COMBINED", nil) forState:UIControlStateNormal];
        [_combinedInpatientButton setTitle:NSLocalizedString(@"DOCTOR_COMBINED", nil) forState:UIControlStateHighlighted];
        _practiceTypeLabel.text = NSLocalizedString(@"DOCTOR_PRACTICE_TYPE", nil);
        [_gateKeeperButton setTitle:NSLocalizedString(@"DOCTOR_GATEKEEPER", nil) forState:UIControlStateNormal];
        [_gateKeeperButton setTitle:NSLocalizedString(@"DOCTOR_GATEKEEPER", nil) forState:UIControlStateHighlighted];
        [_specialistButton setTitle:NSLocalizedString(@"DOCTOR_SPECIALIST", nil) forState:UIControlStateNormal];
        [_specialistButton setTitle:NSLocalizedString(@"DOCTOR_SPECIALIST", nil) forState:UIControlStateHighlighted];
        [_notSpecifiedButton setTitle:NSLocalizedString(@"DOCTOR_NOTSPECIFIED", nil) forState:UIControlStateNormal];
        [_notSpecifiedButton setTitle:NSLocalizedString(@"DOCTOR_NOTSPECIFIED", nil) forState:UIControlStateHighlighted];
      
    }
    
    [self.saveButton setTitle:NSLocalizedString(@"DOCTOR_SAVE_BUTTON", nil) forState:UIControlStateNormal];
    [self.saveButton setTitle:NSLocalizedString(@"DOCTOR_SAVE_BUTTON", nil) forState:UIControlStateHighlighted];

    [self.genderSegmentControl setTitle:NSLocalizedString(@"DOCTOR_MALE", nil) forSegmentAtIndex:0];
    [self.genderSegmentControl setTitle:NSLocalizedString(@"DOCTOR_WOMAN", nil) forSegmentAtIndex:1];
 
    
    self.ageLabel.adjustsFontSizeToFitWidth = YES;
    self.yearsOldLabel.adjustsFontSizeToFitWidth = YES;
    self.genderLabel.adjustsFontSizeToFitWidth = YES;
    self.universityLabel.adjustsFontSizeToFitWidth = YES;
    self.yearLicLabel.adjustsFontSizeToFitWidth = YES;
    self.provinceLabel.adjustsFontSizeToFitWidth = YES;
    self.specialityLabel.adjustsFontSizeToFitWidth = YES;
    self.addSpecLabel.adjustsFontSizeToFitWidth = YES;
    self.exMoreSpecLabel.adjustsFontSizeToFitWidth = YES;
    self.indicateQuestLabel.adjustsFontSizeToFitWidth = YES;
    self.howManyHrsLabel.adjustsFontSizeToFitWidth = YES;
    self.avgNumberPatientLabel.adjustsFontSizeToFitWidth = YES;
    self.firstWeekColabLabel.adjustsFontSizeToFitWidth = YES;
    self.secondWeekColabLabel.adjustsFontSizeToFitWidth = YES;
    self.fromLabel.adjustsFontSizeToFitWidth = YES;
    self.toLabel.adjustsFontSizeToFitWidth = YES;
    self.commentTitleLabel.adjustsFontSizeToFitWidth = YES;
    self.commentIndicateLabel.adjustsFontSizeToFitWidth = YES;
    self.commentNotAddressedLabel.adjustsFontSizeToFitWidth = YES;
    self.firstWeekFromLabel.adjustsFontSizeToFitWidth = YES;
    self.firstWeekToLabel.adjustsFontSizeToFitWidth = YES;


    
    [self refreshUI];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[ChangeManager manager] discardChanges];
    if (self.warningPopoverController != nil) {
        [self.warningPopoverController dismissPopoverAnimated:NO];
    }
    if (self.specialityPopoverController != nil) {
        [self.specialityPopoverController dismissPopoverAnimated:NO];
    }
    [super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
    [self setCancelButton:nil];
    [self setSaveButton:nil];
    [self setAgeTextField:nil];
    [self setGenderSegmentControl:nil];
    [self setUniversityPredictiveTextField:nil];
    [self setGraduationYearTextField:nil];
    [self setProvincePredictiveTextField:nil];
    [self setSecondarySpecialityLabel:nil];
    [self setSecondarySpecialityButtonGroup:nil];
    [self setMedicalCenterCheckBoxes:nil];
    [self setOtherMedicalCenterCheckBox:nil];
    [self setOtherMedicalCenterTextField:nil];
    [self setWeeklyActivityHours:nil];
    [self setAverageWeeklyPatients:nil];
    [self setCommentsTextArea:nil];
    [self setFirstColaborationComponentGroup:nil];
    [self setMainSpecialityTextField:nil];
    [self setSecondarySpecialityButton:nil];
    [self setKeyboardAvoidingScroll:nil];
    [self setFirstCollaborationDateLabel:nil];
    [self setFirstCollaborationDateEndLabel:nil];
    [self setSecondCollaborationDateLabel:nil];
    [self setSecondCollaborationDateEndLabel:nil];
    [self setEmailTextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - IBActions
- (IBAction)cancelButtonClicked:(id)sender {
    [self cleanTempVariables];
    
    [[ChangeManager manager] discardChanges];
    [_delegate doctorInfoViewControllerDidCancelChanges:self];
    
}

//Ravi_Bukka: webservice2 need to implemented here
- (IBAction)saveButtonClicked:(id)sender {
    
//Utpal:CR#4 Demographic information for centers, added below lines
    NSString *sendingString = [[NSString alloc] init];
    NSString *otherMedialText = @"";
    [[NSUserDefaults standardUserDefaults] setValue:otherMedialText forKey:@"OtherMedicalFieldText"];
    
    if (_healthCenterSpecialityCheckBox.isSelected) {
        
        sendingString = [sendingString stringByAppendingString:@"5,"];
    }
    if (_checkPublicHospitalCheckBox.isSelected) {
        
        sendingString = [sendingString stringByAppendingString:@"4,"];
        
    }
    if (_sickFundSurgeryCheckBox.isSelected) {
        
        sendingString = [sendingString stringByAppendingString:@"2,"];
        
    }
    
    if (_privateConsultationCheckBox.isSelected) {
        
        sendingString = [sendingString stringByAppendingString:@"1,"];
        
    }
    if (_chekcPvtHospitalCheckBox.isSelected) {
        
        sendingString = [sendingString stringByAppendingString:@"3,"];
        
    }
    if (_otherMedicalCenterCheckBox.isSelected) {
        _otherMedicalCenterTextField.enabled=YES;//Utpal: CR#4 Demographic for Enbled the text Field.
        
        sendingString = [sendingString stringByAppendingString:@"8,"];
        NSString *otherMedialText = _otherMedicalCenterTextField.text;
        [[NSUserDefaults standardUserDefaults] setValue:otherMedialText forKey:@"OtherMedicalFieldText"];
    }
    if (_healthCenterSpecialityCheckBox.selected || _checkPublicHospitalCheckBox.selected || _sickFundSurgeryCheckBox.isSelected || _privateConsultationCheckBox.selected || _chekcPvtHospitalCheckBox.selected || _otherMedicalCenterCheckBox.isSelected ){
        NSString *para=[sendingString substringToIndex:[sendingString length]-1];
        
        [[NSUserDefaults standardUserDefaults] setValue:para forKey:@"centreParameter"];
        
        NSArray *ary = [para componentsSeparatedByString:@","];
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"demographicCentreArray"];
        [[NSUserDefaults standardUserDefaults] setObject:ary forKey:@"demographicCentreArray"];
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"demographicCentreArray"]);
        
        
    }else{
        [self getWrongFields];
        
    }
    
    //Deepak_carpenter : Added for issue while screen goes down when we do not enter email id -Start-
    [_emailTextField resignFirstResponder];
    [_commentsTextArea resignFirstResponder];
    /// End
    [self hideKeyboard];
    [NSThread sleepForTimeInterval:0.1];
    
    // Get the list of wrong fields
    NSArray* wrongFields = [self getWrongFields];
    BOOL allFieldsOK = wrongFields.count == 0;
    
    if (!allFieldsOK) {
        NSMutableString* msg = [NSMutableString stringWithString:NSLocalizedString(@"DOCTOR_BLANK_REVIEW_TITLE:", nil)];
        for (NSString* wrongField in wrongFields) {
            [msg appendFormat:@"%@\n", wrongField];
        }
        
        // I apologize to the readers, I inherited this code
        PopUpMensajeViewController* popView = [[[PopUpMensajeViewController alloc] initWithDelegate:self Tipo:1 msg:msg] autorelease];
        popView.view.frame = CGRectMake(0, 0, popView.view.frame.size.width,
                                        wrongFields.count * 20 + 140);
        [popView.BotonSI setTitle:NSLocalizedString(@"MULTICHOICE_ACCEPT", nil) forState:UIControlStateNormal];
        [popView.BotonSI setTitle:NSLocalizedString(@"MULTICHOICE_ACCEPT", nil) forState:UIControlStateHighlighted];
        
        UIView* originView = _saveButton;
        
        self.warningPopoverController = [[[UIPopoverController alloc] initWithContentViewController:popView] autorelease];
        [_warningPopoverController setDelegate:self];
        [_warningPopoverController setPopoverContentSize:CGSizeMake(popView.view.frame.size.width,popView.view.frame.size.height)];
        [_warningPopoverController presentPopoverFromRect:originView.frame
                             inView:originView.superview
           permittedArrowDirections:(UIPopoverArrowDirectionDown) 
                           animated:YES];
    }
    else {
        // If it's a new register show a confirmation message
//        if (_newRegister) {
//            PopUpMensajeViewController* popView = nil;
//            if ([FuncionesGenerales isSameDay:_firstDate otherDay:_firstColaborationDatePicker.date]) {
//                // Date hasn't been changed
//                popView = [[[PopUpMensajeViewController alloc] initWithDelegate:self Tipo:0 msg:NSLocalizedString(@"DOCTOR_MSG_DATE_NOCH",nil)] autorelease];
//            }
//            else {
//                // Date has been changed
//                popView=[[[PopUpMensajeViewController alloc] initWithDelegate:self Tipo:0 msg:NSLocalizedString(@"DOCTOR_MSG_DATE_CH",nil)] autorelease];
//            }
//            self.confirmationMessageController = popView;
//            
//            UIView* originView = _firstColaborationDatePicker;
//            
//            self.warningPopoverController = [[[UIPopoverController alloc] initWithContentViewController:popView] autorelease];
//            [_warningPopoverController setDelegate:self];
//            [_warningPopoverController setPopoverContentSize:CGSizeMake(popView.view.frame.size.width,popView.view.frame.size.height)];
//            
//            [_warningPopoverController presentPopoverFromRect:originView.frame
//                                                       inView:originView.superview
//                                     permittedArrowDirections:(UIPopoverArrowDirectionDown) 
//                                                     animated:YES];
//        }
//        else {
            [self saveChangesAndClose];
//        }
    }
}

- (IBAction)mainSpecialityButtonClicked:(id)sender {
    [self showSpecialityController:YES];
}

- (IBAction)secondarySpecialityButtonClicked:(id)sender {
    [self showSpecialityController:NO];
}

- (IBAction)discardSecondarySpecialityButtonClicked:(id)sender {
    self.doctorInfo.doctor.secondarySpecialty = nil;
    OtherspecialtyIndex = nil;
    [self refreshSpecialitiesUI];
}

- (IBAction)genderSegmentChanged:(id)sender {
    self.doctorInfo.doctor.gender = _genderSegmentControl.selectedValue;
}

#pragma mark - SpecialitySelectionViewControllerDelegate
- (void)specialitySelectionController:(SpecialitySelectionViewController*)popup didSelectSpeciality:(id)speciality {
    if (self.selectingSecondarySpeciality) {
        self.doctorInfo.doctor.secondarySpecialty = speciality;
        
    }
    else {
        self.doctorInfo.doctor.mainSpeciality = speciality;
    }
    
    [self dismissSpecialityPopover];
}

#pragma mark - PredictiveTextFieldDelegate
-(void) predictiveTextField:(PredictiveTextField*)textField elementsForInputString:(NSString*)inputString {
    NSArray* results = nil;
    NSArray* ProvinceResults = nil;
    if (textField == _universityPredictiveTextField) {
        //Deepak_Carpenter : Added for checkdataversion
       // results = [[EntityFactory sharedEntityFactory] fetchUniversities:inputString];
        
        results =[[EntityFactory sharedEntityFactory]fetchEntities:NSStringFromClass([University class]) filteringByName:inputString sortResults:YES];
    }
    else if (textField == _provincePredictiveTextField){
         results =[[EntityFactory sharedEntityFactory]fetchEntities:NSStringFromClass([University class]) filteringByName:inputString sortResults:YES];
    }
    else {
        results = [NSArray array];
           }
    [textField fetchedElements:results forInputString:inputString];
    
}
-(void) predictiveTextFieldValueChanged:(PredictiveTextField*)textField {
    if (textField == _universityPredictiveTextField) {
        University* university = _universityPredictiveTextField.selectedElement;
        if (university == nil && _universityPredictiveTextField.text.length > 0) {
            if (_userDefinedUniversity == nil) {
                self.userDefinedUniversity = [[EntityFactory sharedEntityFactory] insertNewWithEntityName:NSStringFromClass([University class])];
                _userDefinedUniversity.userDefined = [NSNumber numberWithBool:YES];
            }
            
            // User defined university
            NSString* universityName = _universityPredictiveTextField.text;
            _userDefinedUniversity.name = universityName;
            university = _userDefinedUniversity;
        }
        self.doctorInfo.doctor.university = university;
        
    }
    else if (textField == _provincePredictiveTextField) {
        self.doctorInfo.doctor.province = _provincePredictiveTextField.selectedElement;
        
    }
   
}

#pragma mark - CheckBoxDelegate
-(void) checkBoxDidChangeValue:(CheckBox*)checkBox {
   
    if ([_medicalCenterCheckBoxes containsObject:checkBox]) {
        MedicalCenter* center = [checkBox value];

        if (checkBox.selected) {
            [self.doctorInfo addMedicalCentersObject:center];
            //  NSLog(@"Check Box VAlue %@",center);
        }
        else {
            [self.doctorInfo removeMedicalCentersObject:center];
        }
    }
    else if (checkBox == _otherMedicalCenterCheckBox) {
        // Enable other center textfield
        _otherMedicalCenterTextField.enabled = checkBox.selected;
        if (_otherMedicalCenter == nil) {
            self.otherMedicalCenter = [[EntityFactory sharedEntityFactory] insertNewWithEntityName:NSStringFromClass([MedicalCenter class])];
        }
        if (checkBox.selected) {
            [self.doctorInfo addMedicalCentersObject:_otherMedicalCenter];
        }
        else {
            [self.doctorInfo removeMedicalCentersObject:_otherMedicalCenter];
            _otherMedicalCenterTextField.text=@"";
            _doctorInfo.otherMedicalCenter=nil;
        }
    }else{
        _otherMedicalCenterTextField.text=@"";
        _doctorInfo.otherMedicalCenter=nil;
    }

}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView {
//    [_keyboardAvoidingScroll adjustOffsetToIdealIfNeeded];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (textView == _commentsTextArea.textArea) {
        _doctorInfo.comments = _commentsTextArea.text;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    BOOL allowChange = YES;
    if (textView == _commentsTextArea.textArea) {
        if ([text isEqualToString:@"\n"]) {
            allowChange = NO;
            [textView resignFirstResponder];
        }
        else {
            // Check comments length
            NSUInteger newLength = [textView.text stringByReplacingCharactersInRange:range withString:text].length;
            allowChange = (newLength <= kMaxCommentsLength) ? YES : NO;
        }
    }
    return allowChange;
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
//    [_keyboardAvoidingScroll adjustOffsetToIdealIfNeeded];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == _otherMedicalCenterTextField) {
       // _otherMedicalCenter.name = textField.text;
        _doctorInfo.otherMedicalCenter=textField.text;
    }
    else if (textField == _ageTextField) {
        _doctorInfo.doctor.age = [NSNumber numberWithInt:_ageTextField.text.intValue];
    }
    else if (textField == _graduationYearTextField) {
        _doctorInfo.doctor.graduationYear = _graduationYearTextField.text;
    }
    else if (textField == _weeklyActivityHours) {
        _doctorInfo.weeklyActivityHours = [NSNumber numberWithInt:_weeklyActivityHours.text.intValue];
    }
    else if (textField == _averageWeeklyPatients) {
        _doctorInfo.averageWeeklyPatients = [NSNumber numberWithInt:_averageWeeklyPatients.text.intValue];
    }
    else if (textField == _emailTextField) {
        _doctorInfo.email = _emailTextField.text;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [_keyboardAvoidingScroll adjustOffsetToIdealIfNeeded];
    
    NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
    BOOL allowChange = YES;
    
    // Check correct integer values
    if (textField == _ageTextField || 
        textField == _graduationYearTextField || 
        textField == _weeklyActivityHours || 
        textField == _averageWeeklyPatients) {
        
        // Limit weekly activity hours to two digits
        if (textField == _weeklyActivityHours && resultingString.length > 2) {
            allowChange = NO;
        }
        // Limit patient number to three digits
        else if (textField == _averageWeeklyPatients && resultingString.length > 3) {
            allowChange = NO;
        }
        else if ([resultingString length] != 0) {
            NSInteger holder;
            NSScanner *scan = [NSScanner scannerWithString: resultingString];
            if (textField == _graduationYearTextField) {
                allowChange = resultingString.length <= 4;
                if (allowChange) {
                    // Show error display in custom textfield
                    _graduationYearTextField.displayError = [resultingString intValue] > [FuncionesGenerales AnioActual] || [resultingString intValue] < kMinDoctorGraduationYear;
                }
            }
            else if (textField == _ageTextField) {
                allowChange = resultingString.length <= 2;
                if (allowChange) {
                    // Show error display in custom textfield
                    _ageTextField.displayError = [resultingString intValue] < kMinDoctorAge;
                }
            }
            
            allowChange = allowChange && [scan scanInteger: &holder] && [scan isAtEnd];
        }
    } else if (textField == _otherMedicalCenterTextField) {
        allowChange = resultingString.length <= kMaxOtherMedicalCenterLength;
    } else if (textField == _emailTextField) {
        allowChange = ![string isEqualToString:@" "];
    }
    return allowChange;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    UIView* nextTextField = nil;
    if (textField == _ageTextField) {
        
        nextTextField = _universityPredictiveTextField;
    }
    else if (textField == _graduationYearTextField) {
        nextTextField = _provincePredictiveTextField;
    }
    else if (textField == _otherMedicalCenterTextField) {
        nextTextField = _weeklyActivityHours;
    }
    else if (textField == _weeklyActivityHours) {
        nextTextField = _averageWeeklyPatients;
    }
    else if (textField == _averageWeeklyPatients) {
        nextTextField = _emailTextField;
    }
    else if (textField == _emailTextField) {
        nextTextField = _commentsTextArea.textArea;
    }
    
    if (nextTextField != nil) {
        [nextTextField becomeFirstResponder];
        [_keyboardAvoidingScroll adjustOffsetToIdealIfNeeded];
    }
    else {
        [textField resignFirstResponder];
    }
    
    return NO; // We do not want UITextField to insert line-breaks. 
}

#pragma mark - UIPopoverControllerDelegate
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    if (popoverController == _specialityPopoverController) {
        [_delegate doctorInfoViewControllerWillDismissPopUp:self];
        
        [self refreshSpecialitiesUI];
        
        self.specialityPopoverController = nil;
    }
}

#pragma mark - PopUpMensajeViewControllerProtocol
- (void) CierreModalPopUpMensaje:(id)sender {
    [self.warningPopoverController dismissPopoverAnimated:YES];
    [self popoverControllerDidDismissPopover:sender];
    
    // If the user accepts the confirmation message, save changes and go to menu
    if (sender == self.confirmationMessageController) {
        if (self.confirmationMessageController.Seleccionada == kPopUpMensajeOKButton) {
            [self saveChangesAndClose];
        }
    }
}


- (IBAction)CheckBox:(id)sender {
}
@end
