//
//  DoctorInfoViewController.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 10/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextField.h"
#import "CustomSegmentedControl.h"
#import "CustomTextArea.h"
#import "PredictiveTextField.h"
#import "CheckBox.h"
#import "RadioButton.h"
#import "RadioButtonGroup.h"
#import "ComponentGroup.h"
#import "DoctorInfo.h"
#import "SpecialitySelectionViewController.h"
#import "PopUpMensajeViewController.h"
#import "TPKeyboardAvoidingScrollView.h"

@class DoctorInfoViewController;

#pragma mark - Delegate protocol
@protocol DoctorInfoViewControllerDelegate <NSObject>
- (void)doctorInfoViewController:(DoctorInfoViewController*)controller didSaveChanges:(DoctorInfo*)doctorInfo;
- (void)doctorInfoViewControllerDidCancelChanges:(DoctorInfoViewController*)controller;
- (void)doctorInfoViewControllerWillPresentPopUp:(DoctorInfoViewController*)controller;
- (void)doctorInfoViewControllerWillDismissPopUp:(DoctorInfoViewController*)controller;
@end


#pragma mark - Interface
@interface DoctorInfoViewController : UIViewController<SpecialitySelectionViewControllerDelegate, PredictiveTextFieldDelegate, CheckBoxDelegate, UITextFieldDelegate, UITextViewDelegate, UIPopoverControllerDelegate, PopUpMensajeViewControllerProtocol>
{
    NSNumber *OtherspecialtyIndex;  //Utpal CR#4 for Demographics

}

#pragma mark - Properties
- (IBAction)CheckBoxTap:(id)sender;

@property (assign, nonatomic) id<DoctorInfoViewControllerDelegate> delegate;
@property (nonatomic, assign, getter=isNewRegister) BOOL newRegister;
@property (nonatomic, retain) DoctorInfo* doctorInfo;

#pragma mark - Public methods
- (void)willPerformLogout;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *keyboardAvoidingScroll;
@property (retain, nonatomic) IBOutlet UIButton *cancelButton;
@property (retain, nonatomic) IBOutlet UIButton *saveButton;
@property (retain, nonatomic) IBOutlet CustomTextField *ageTextField;
@property (retain, nonatomic) IBOutlet CustomSegmentedControl *genderSegmentControl;
@property (retain, nonatomic) IBOutlet PredictiveTextField *universityPredictiveTextField;
@property (retain, nonatomic) IBOutlet CustomTextField *graduationYearTextField;
@property (retain, nonatomic) IBOutlet PredictiveTextField *provincePredictiveTextField;
@property (retain, nonatomic) IBOutlet CustomTextField *mainSpecialityTextField;
@property (retain, nonatomic) IBOutlet UILabel *secondarySpecialityLabel;
@property (retain, nonatomic) IBOutlet RadioButton *secondarySpecialityButton;
@property (retain, nonatomic) IBOutlet RadioButtonGroup *secondarySpecialityButtonGroup;
@property (retain, nonatomic) IBOutletCollection(CheckBox) NSArray *medicalCenterCheckBoxes;
@property (retain, nonatomic) IBOutlet CheckBox *otherMedicalCenterCheckBox;
@property (retain, nonatomic) IBOutlet CustomTextField *otherMedicalCenterTextField;
@property (retain, nonatomic) IBOutlet CustomTextField *weeklyActivityHours;
@property (retain, nonatomic) IBOutlet CustomTextField *averageWeeklyPatients;
@property (retain, nonatomic) IBOutlet CustomTextArea *commentsTextArea;
@property (retain, nonatomic) IBOutlet ComponentGroup *firstColaborationComponentGroup;
@property (retain, nonatomic) IBOutlet UILabel *firstCollaborationDateLabel;
@property (retain, nonatomic) IBOutlet UILabel *firstCollaborationDateEndLabel;
@property (retain, nonatomic) IBOutlet UILabel *secondCollaborationDateLabel;
@property (retain, nonatomic) IBOutlet UILabel *secondCollaborationDateEndLabel;
//Deepak Carpenter : Added to handle collaborationdates
@property (retain, nonatomic) IBOutlet UILabel *ThirdCollaborationDateLabel;
@property (retain, nonatomic) IBOutlet UILabel *ThirdCollaborationDateEndLabel;
@property (retain, nonatomic) IBOutlet CustomTextField *emailTextField;


//Ravi_Bukka: Added for localization

@property (retain, nonatomic) IBOutlet UILabel *ageLabel;
@property (retain, nonatomic) IBOutlet UILabel *yearsOldLabel;
@property (retain, nonatomic) IBOutlet UILabel *genderLabel;
@property (retain, nonatomic) IBOutlet UILabel *universityLabel;
@property (retain, nonatomic) IBOutlet UILabel *yearLicLabel;
@property (retain, nonatomic) IBOutlet UILabel *provinceLabel;
@property (retain, nonatomic) IBOutlet UILabel *specialityLabel;
@property (retain, nonatomic) IBOutlet UILabel *addSpecLabel;
@property (retain, nonatomic) IBOutlet UILabel *exMoreSpecLabel;
@property (retain, nonatomic) IBOutlet UILabel *indicateQuestLabel;
@property (retain, nonatomic) IBOutlet UILabel *howManyHrsLabel;
@property (retain, nonatomic) IBOutlet UILabel *avgNumberPatientLabel;
@property (retain, nonatomic) IBOutlet UILabel *firstWeekColabLabel;
@property (retain, nonatomic) IBOutlet UILabel *secondWeekColabLabel;
@property (retain, nonatomic) IBOutlet UILabel *firstWeekFromLabel;
@property (retain, nonatomic) IBOutlet UILabel *firstWeekToLabel;
@property (retain, nonatomic) IBOutlet UILabel *fromLabel;
@property (retain, nonatomic) IBOutlet UILabel *toLabel;
@property (retain, nonatomic) IBOutlet UILabel *commentTitleLabel;
@property (retain, nonatomic) IBOutlet UILabel *commentIndicateLabel;
@property (retain, nonatomic) IBOutlet UILabel *commentNotAddressedLabel;

@property (retain, nonatomic) IBOutlet CheckBox *healthCenterSpecialityCheckBox;
@property (retain, nonatomic) IBOutlet CheckBox *checkPublicHospitalCheckBox;
@property (retain, nonatomic) IBOutlet CheckBox *privateConsultationCheckBox;
@property (retain, nonatomic) IBOutlet CheckBox *chekcPvtHospitalCheckBox;

//Ravi_Bukka: Added for greece localization
@property (retain, nonatomic) IBOutlet CheckBox *sickFundSurgeryCheckBox;

//Ravi_Bukka: Added for polish localization
@property (retain, nonatomic) IBOutlet RadioButton *outPatientOnlyDoctorButton;
@property (retain, nonatomic) IBOutlet RadioButton *combinedInpatientButton;
@property (retain, nonatomic) IBOutlet RadioButton *gateKeeperButton;
@property (retain, nonatomic) IBOutlet RadioButton *specialistButton;
@property (retain, nonatomic) IBOutlet RadioButton *notSpecifiedButton;
@property (retain, nonatomic) IBOutlet RadioButtonGroup *doctorTypeButtonGroup;
@property (retain, nonatomic) IBOutlet RadioButtonGroup *practiceTypeButtonGroup;
@property (retain, nonatomic) IBOutlet UILabel *doctorTypeLabel;
@property (retain, nonatomic) IBOutlet UILabel *practiceTypeLabel;


//Ravi Bukk: Added for logging feature
@property (retain, nonatomic) NSFileHandle *fileHandle;
@property (retain, nonatomic) NSString *completePatientLogString;




#pragma mark - IBActions
- (IBAction)cancelButtonClicked:(id)sender;
- (IBAction)saveButtonClicked:(id)sender;
- (IBAction)mainSpecialityButtonClicked:(id)sender;
- (IBAction)secondarySpecialityButtonClicked:(id)sender;
- (IBAction)discardSecondarySpecialityButtonClicked:(id)sender;
- (IBAction)genderSegmentChanged:(id)sender;


@end
