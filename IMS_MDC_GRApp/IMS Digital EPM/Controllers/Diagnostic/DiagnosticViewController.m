//
//  DiagnosticViewController.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 11/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "DiagnosticViewController.h"
#import "EntityFactory.h"
#import "DiagnosisType.h"
#import "VisitType.h"

#import "Pathology.h"
#import "ComponentGroup.h"
#import "UIView+NIBLoader.h"
#import "ConfirmationMessageController.h"
#import "Diagnosis+Complete.h"
#import "Treatment+Complete.h"
#import "Medicament.h"
#import "Presentation.h"
#import "ChangeManager.h"
#import "Diagnosis+SortedTreatments.h"
#import "MultipleChoiceViewController.h"

#import "DrugIndicator.h"

#define kFormConstantsPList @"formConstants"

static NSString* const kMaxTreatments = @"maxTreatments";
static NSString* const kIdentifierKey = @"identifier";
static NSString* const kDescriptionKey = @"name";
static NSString* const kIndexKey = @"index";

static NSString* const kVisitTypeIndexFirstTime = @"1";
static NSString* const kVisitTypeIndexRepeated = @"2";
//Ravi_Bukka: added for different Greece and poland xibs
static NSString* const kVisitTypeIndexNotSpecified = @"4";

//Ravi_Bukka: For Greece CSV
static NSString* const kVisitTypeIndexFirstTime_pl = @"1";
static NSString* const kVisitTypeIndexRepeated_pl = @"2";
//static NSString* const kVisitTypeIndexNotSpecified_pl = @"9";



static NSString* const kPathologyTypeIndexAcute = @"1";
static NSString* const kPathologyTypeIndexChronic = @"2";

static NSString* const kPathologyTypeIndexAcute_pl = @"1";
static NSString* const kPathologyTypeIndexChronic_pl = @"2";

//smokerTypeSelector
static const CGFloat kCollapsedCellHeight = 44;
static const CGFloat kExpandedCellHeight = 492;// Private methods
@interface DiagnosticViewController()

@property (nonatomic, retain) VisitType *selectedVisitType;
@property (nonatomic, retain) Pathology *selectedPathology;
@property (nonatomic, retain) NSNumber* expandedRow;
@property (nonatomic, retain) UIPopoverController* confirmationPopover;
@property (nonatomic, assign, getter=isSelectedTreatmentReady) BOOL selectedTreatmentReady;
@property (nonatomic, assign, getter=isTreatmentsEnabled) BOOL treatmentsEnabled;
@property (nonatomic, retain) MultipleChoiceViewController* alertViewController;

@property (nonatomic, retain) id<CancelableOperation> fetchDiagnosticOperation;

@property (readonly, getter = isRefreshing) BOOL refreshing;

-(void) updateUI;
-(void) refresh;

@end

@implementation DiagnosticViewController

@synthesize delegate = _delegate;
@synthesize diagnosticTextField = _diagnosticTextField;
@synthesize visitTypeSelector = _visitTypeSelector;

//Kanchan
@synthesize drugIndicatorTextField=_drugIndicatorTextField;


@synthesize pathologyTypeSelector = _pathologyTypeSelector;
@synthesize treatmentsTableView = _treatmentsTableView;
@synthesize addTreatmentButtonComponent = _addTreatmentButtonComponent;
@synthesize noTreatmentCheckBox = _noTreatmentCheckBox;
@synthesize diagnosis = _diagnosis;
@synthesize enabled = _enabled;
@synthesize selectedVisitType = _selectedVisitType;
@synthesize selectedPathology = _selectedPathology;
@synthesize expandedRow = _expandedRow;
@synthesize confirmationPopover = _confirmationPopover;
@synthesize selectedTreatmentReady = _selectedTreatmentReady;
@synthesize treatmentsEnabled = _treatmentsEnabled;
@synthesize refreshing = _refreshing;
@synthesize fetchDiagnosticOperation = _fetchDiagnosticOperation;
@synthesize changesControlProtocol = _changesControlProtocol;
@synthesize addTreatmentButton = _addTreatmentButton;


#pragma mark - Private methods

-(void)refresh {
    _refreshing = YES;
    // Update controls
    if (_diagnosis.diagnosisType == nil && _diagnosis.userDiagnosis.length > 0) {
        _diagnosticTextField.text = _diagnosis.userDiagnosis;
        NSLog(@"User Diagnosis %@",_diagnosis.userDiagnosis);
    }
    else {
        _diagnosticTextField.selectedElement = _diagnosis.diagnosisType;
    }
    //Kanchan
    _drugIndicatorTextField.selectedElement=_diagnosis.drugIndicator;

    
    _pathologyTypeSelector.selectedValue = _diagnosis.pathology;
    _visitTypeSelector.selectedValue = _diagnosis.visitType;
    

    _noTreatmentCheckBox.selected = !_diagnosis.needsTreatment.boolValue;
    _refreshing = NO;
}

-(BOOL) isReady {
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {
    BOOL enableDiagnosticType = self.enabled;
    ((ComponentGroup*) self.view).enabled = enableDiagnosticType;
    _diagnosticTextField.enabled = enableDiagnosticType;
    BOOL enableVisitType = enableDiagnosticType && (_diagnosis.diagnosisType != nil || [_diagnosis.userDiagnosis stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0);
    _visitTypeSelector.enabled = enableVisitType;
        //Commented By Deepak Carpenter To hide segemented control
//    BOOL enablePahology = enableVisitType && _diagnosis.visitType != nil;
//    _pathologyTypeSelector.enabled = enablePahology;
    
    
    BOOL enabledrugIndicator= enableVisitType && _diagnosis.visitType!= nil;
    //BOOL enabledrugIndicator = enablePahology && _pathologyTypeSelector.selectedSegmentIndex != UISegmentedControlNoSegment;
    _drugIndicatorTextField.enabled = enabledrugIndicator;
    
    BOOL enableNoTreatmentCheckbox = enabledrugIndicator && _diagnosis.drugIndicator != nil;
    _noTreatmentCheckBox.enabled = enableNoTreatmentCheckbox;

    BOOL enableTreatment = enableNoTreatmentCheckbox && _diagnosis.needsTreatment.boolValue  &&([_drugIndicatorTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0);
    
    // If treatments became enabled or disabled, reload table view
    if (enableTreatment != _treatmentsEnabled) {
        _treatmentsEnabled = enableTreatment;
        [_treatmentsTableView reloadData];
    }
        
        NSDictionary* formConstants = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:kFormConstantsPList ofType:@"plist"]];
        BOOL enabledAddTreatment = [self.diagnosis isComplete] && self.diagnosis.needsTreatment.boolValue && self.diagnosis.treatments.count < [[formConstants objectForKey:kMaxTreatments] intValue];
        _addTreatmentButtonComponent.hidden = !enabledAddTreatment;
        // Diagnosis is ready when treatment is ready or 'No treatment' is selected
        BOOL isReady = enabledAddTreatment || (enableNoTreatmentCheckbox && !_diagnosis.needsTreatment.boolValue);
        return isReady;
    }

    else {
    BOOL enableDiagnosticType = self.enabled;
    ((ComponentGroup*) self.view).enabled = enableDiagnosticType;
    _diagnosticTextField.enabled = enableDiagnosticType;
    BOOL enableVisitType = enableDiagnosticType && (_diagnosis.diagnosisType != nil || [_diagnosis.userDiagnosis stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0);
    _visitTypeSelector.enabled = enableVisitType;
    BOOL enablePahology = enableVisitType && _diagnosis.visitType != nil;
    _pathologyTypeSelector.enabled = enablePahology;
    

    BOOL enableNoTreatmentCheckbox = enablePahology && _diagnosis.pathology != nil;
    _noTreatmentCheckBox.enabled = enableNoTreatmentCheckbox;
    BOOL enableTreatment = enableNoTreatmentCheckbox && _diagnosis.needsTreatment.boolValue;
    // If treatments became enabled or disabled, reload table view
    if (enableTreatment != _treatmentsEnabled) {
        _treatmentsEnabled = enableTreatment;
        [_treatmentsTableView reloadData];
    }
    
    NSDictionary* formConstants = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:kFormConstantsPList ofType:@"plist"]];
    BOOL enabledAddTreatment = [self.diagnosis isComplete] && self.diagnosis.needsTreatment.boolValue && self.diagnosis.treatments.count < [[formConstants objectForKey:kMaxTreatments] intValue];
    _addTreatmentButtonComponent.hidden = !enabledAddTreatment;
    // Diagnosis is ready when treatment is ready or 'No treatment' is selected
    BOOL isReady = enabledAddTreatment || (enableNoTreatmentCheckbox && !_diagnosis.needsTreatment.boolValue);
    return isReady;
    }
}
-(void) updateUI {
    [self refresh];
    BOOL isReady = [self isReady];
    [_delegate diagnosticViewController:self isReady:isReady];
}

- (void)deleteTreatment:(Treatment*)treatment {
    // Warning confirmed, delete the entity
    if (treatment != nil) {
        // Delete the entity
        [[ChangeManager manager] deleteTreatment:treatment];
        // Reload the data as may be changed
        [_treatmentsTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        
        // If treatments are now empty, create a new one
        if (_diagnosis.treatments.count == 0) {
            [[ChangeManager manager] addTreatmentToDiagnosis:_diagnosis];
            
            NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            self.expandedRow = [NSNumber numberWithInt:0];
            [_treatmentsTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        [self updateUI];
    }
    else {
        DLog(@"Tried to delete a nil treatment");
    }
}

- (void)deleteAllTreatments {
    // Warning confirmed, delete the entity
    if (_diagnosis.treatments != nil) {
        NSSet *treatments = [NSSet setWithSet:_diagnosis.treatments];
        for (Treatment *treatment in treatments) {
            // Delete the entity
            [[ChangeManager manager] deleteTreatment:treatment];
        }
        // Reload the data as may be changed
        [_treatmentsTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        
        // If treatments are now empty, create a new one
        if (_diagnosis.treatments.count == 0) {
            [[ChangeManager manager] addTreatmentToDiagnosis:_diagnosis];
            
            NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            self.expandedRow = [NSNumber numberWithInt:0];
            [_treatmentsTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        [self updateUI];
    }
    else {
        DLog(@"Tried to delete a nil treatment");
    }
}

- (void)showConfirmationToDeleteTreatment:(Treatment*)treatment originView:(UIView*)originView {
    
   
    
    ConfirmationMessageController* confirmationController = [[[ConfirmationMessageController alloc] initWithMessage: NSLocalizedString(@"DIAGNOSTIC_MSG", nil) eventBlock:^(BOOL accepted) {
        if (accepted) {
            [self deleteTreatment:treatment];
        }
        [self.confirmationPopover dismissPopoverAnimated:YES];
        self.confirmationPopover = nil;
    }] autorelease];
    
    self.confirmationPopover = [[[UIPopoverController alloc] initWithContentViewController:confirmationController] autorelease];
    _confirmationPopover.contentViewController = confirmationController;
    _confirmationPopover.popoverContentSize = confirmationController.view.frame.size;
    [_confirmationPopover presentPopoverFromRect:originView.frame inView:originView.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

// Obtains the types of all the other diagnosis of the patient
- (NSSet*)otherDiagnosisTypes {
    NSMutableSet* otherDiagnosisTypes = [NSMutableSet setWithCapacity:self.diagnosis.patient.diagnostic.count];
    for (Diagnosis* diagnosis in self.diagnosis.patient.diagnostic) {
        if (diagnosis != self.diagnosis && diagnosis.diagnosisType != nil) {
            [otherDiagnosisTypes addObject:diagnosis.diagnosisType];
        }
    }
    return otherDiagnosisTypes;
}

// Obtains the names of all the other diagnosis of the patient
- (NSSet*)otherDiagnosisNames {
    NSMutableSet* otherDiagnosisNames = [NSMutableSet setWithCapacity:self.diagnosis.patient.diagnostic.count];
    for (Diagnosis* diagnosis in self.diagnosis.patient.diagnostic) {
        if (diagnosis != self.diagnosis) {
            if (diagnosis.userDiagnosis.length > 0) {
                [otherDiagnosisNames addObject:diagnosis.userDiagnosis];
            }
            else if (diagnosis.diagnosisType != nil) {
                [otherDiagnosisNames addObject:diagnosis.diagnosisType.name];
            }
        }
    }
    return otherDiagnosisNames;
}

- (BOOL)isEqualRemovingWhitespace:(NSString*)string toString:(NSString*)toString {
    NSString* normalizedString = [[string componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString: @""];
    NSString* normalizedToString = [[toString componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString: @""];
    
    return [normalizedString compare:normalizedToString options:NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch] == NSOrderedSame;
}

- (BOOL)checkDuplicatedDiagnosis {
    BOOL isUnique = YES;
    
    NSString* diagnosisName = nil;
    if (self.diagnosis.diagnosisType != nil) {
        diagnosisName = self.diagnosis.diagnosisType.name;
    }
    else if (self.diagnosis.userDiagnosis.length > 0) {
        diagnosisName = self.diagnosis.userDiagnosis;
    }
    
    if (diagnosisName != nil) {
        for (NSString* otherDiagnosisName in [self otherDiagnosisNames]) {
            if ([self isEqualRemovingWhitespace:diagnosisName toString:otherDiagnosisName]) {
                isUnique = NO;
                break;
            }
        }
    }
    
    return isUnique;
}

- (void)showDuplicatedDiagnosisAlert {
    // Message indicating that there are duplicated userPresentation
    // Ask the user to confirm editing before continue
    self.alertViewController = [[MultipleChoiceViewController new] autorelease];
    _alertViewController.message = NSLocalizedString(@"PATIENT_DUPLICATED_DIAGNOSIS_MESSAGE", @"");
    _alertViewController.acceptButtonTitle = nil;
    _alertViewController.cancelButtonTitle = NSLocalizedString(@"PATIENT_DUPLICATED_DIAGNOSIS_CANCEL", @"");
    _alertViewController.otherButtonTitle = nil;
    _alertViewController.eventBlock = ^(MultipleChoiceButton button){
        // Dismiss and release the controller
        [_delegate dismissModalController:_alertViewController];
        self.alertViewController = nil;
    };
    
    // Present modal controller
    [_delegate presentModalController:_alertViewController];
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    //Deepak_Carpenter CR#11 : Segment Control appearance for iOS 8 +
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{
                                                              NSForegroundColorAttributeName : [UIColor whiteColor]
                                                              } forState:UIControlStateNormal];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{
                                                              NSForegroundColorAttributeName : [UIColor whiteColor]
                                                              } forState:UIControlStateSelected];
//Ravi_Bukka
    
    [self.visitTypeSelector setTitle:NSLocalizedString(@"DIAGNOSTIC_FIRST_TIME", nil) forSegmentAtIndex:0];
    [self.visitTypeSelector setTitle:NSLocalizedString(@"DIAGNOSTIC_VISIT_REPEATED", nil) forSegmentAtIndex:1];
    
    //Kanchan Nair
    // Set description key for PlaceOfVisit textfield
    _drugIndicatorTextField.descriptionField = kDescriptionKey;
    _drugIndicatorTextField.allowUserValues = YES;
    _drugIndicatorTextField.popoverArrowDirections=UIPopoverArrowDirectionLeft;

//Ravi_Bukka: Added for Polish localization
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {
        
        [self.visitTypeSelector setTitle:NSLocalizedString(@"DIAGNOSTIC_FIRST_TIME", nil) forSegmentAtIndex:0];
        [self.visitTypeSelector setTitle:NSLocalizedString(@"DIAGNOSTIC_VISIT_REPEATED", nil) forSegmentAtIndex:1];
       //Deepak carpenter: New Changes
        // [self.visitTypeSelector setTitle:NSLocalizedString(@"DIAGNOSTIC_NOT_SPECIFIED", nil) forSegmentAtIndex:2];
    }
    
    [self.pathologyTypeSelector setTitle:NSLocalizedString(@"DIAGNOSTIC_ACUTE_CONDITIONS", nil) forSegmentAtIndex:0];
    [self.pathologyTypeSelector setTitle:NSLocalizedString(@"DIAGNOSTIC_CHRONIC_PATHOLOGY", nil) forSegmentAtIndex:1];
    

    [_noTreatmentCheckBox setTitle:NSLocalizedString(@"DIAGNOSTIC_WITHOUT_TREATMENT", nil) forState:UIControlStateNormal];
    [_noTreatmentCheckBox setTitle:NSLocalizedString(@"DIAGNOSTIC_WITHOUT_TREATMENT", nil) forState:UIControlStateHighlighted];
    
    [self.addTreatmentButton setTitle:NSLocalizedString(@"DIAGNOSTIC_ADD_ANOTHER_DRUG", nil) forState:UIControlStateNormal];
    [self.addTreatmentButton setTitle:NSLocalizedString(@"DIAGNOSTIC_ADD_ANOTHER_DRUG", nil) forState:UIControlStateHighlighted];
    

    
   
    
    
    [super viewDidLoad];
    // Set description key for diagnostic textfield
    _diagnosticTextField.descriptionField = kDescriptionKey;
    _diagnosticTextField.allowUserValues = YES;
   //Deepak Carpenter: Set Font for Greek Version
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"]) {
    [_diagnosticTextField setTextFontSize:[UIFont systemFontOfSize:11.0]];
    [_diagnosticTextField setTextFont:[UIFont systemFontOfSize:11.0]];
    }
    // Set tags and values for segmented controls
    // Visit type
    _visitTypeSelector.tagIdentifierField = kIdentifierKey;
    _visitTypeSelector.values = [[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([VisitType class]) filteringByName:@"" sortResults:NO];
    

    _visitTypeSelector.tagArray = [NSArray arrayWithObjects:kVisitTypeIndexFirstTime, kVisitTypeIndexRepeated, nil];
    
//Ravi_Bukka: Added for Polish localization
    
    // Pathology
    _pathologyTypeSelector.tagIdentifierField = kIdentifierKey;
    _pathologyTypeSelector.values = [[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([Pathology class]) filteringByName:@"" sortResults:NO];
        if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"])
    _pathologyTypeSelector.tagArray = [NSArray arrayWithObjects:kPathologyTypeIndexAcute_pl, kVisitTypeIndexRepeated_pl, nil];
    else
       _pathologyTypeSelector.tagArray = [NSArray arrayWithObjects:kPathologyTypeIndexAcute, kPathologyTypeIndexChronic, nil];
    [self updateUI];
}

- (void)viewDidUnload
{
    [self setDiagnosticTextField:nil];
    [self setVisitTypeSelector:nil];
    [self setPathologyTypeSelector:nil];
    [self setTreatmentsTableView:nil];
    [self setAddTreatmentButtonComponent:nil];
    [self setNoTreatmentCheckBox:nil];
    [self setDrugIndicatorTextField:nil];

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    _delegate = nil;
    _changesControlProtocol = nil;
    [_diagnosticTextField release];
    [_diagnosis release];
    [_selectedVisitType release];
    [_selectedPathology release];
    [_visitTypeSelector release];

    [_pathologyTypeSelector release];
    [_expandedRow release];
    [_treatmentsTableView release];
    [_confirmationPopover release];
    [_addTreatmentButtonComponent release];
    [_noTreatmentCheckBox release];
    [_fetchDiagnosticOperation release];
    [_drugIndicatorTextField release];

    [super dealloc];
}

- (void)setDiagnosis:(Diagnosis *)aDiagnosis {
    if (_diagnosis != aDiagnosis) {
        [aDiagnosis retain];
        [_diagnosis release];
        _diagnosis = aDiagnosis;
    }
    
    // By default expand the first incomplete treatment
    // Expand the first row if the diagnosis doesn't require treatment
    if (![_diagnosis isComplete] || ![[_diagnosis needsTreatment] boolValue]) {
        self.expandedRow = [NSNumber numberWithInt:0];
        // Expand the first incomplete treatment
        NSArray* sortedTreatments = [_diagnosis sortedTreatments];
        for (Treatment* treatment in sortedTreatments) {
            if (![treatment isComplete]) {
                NSUInteger index = [sortedTreatments indexOfObject:treatment];
                self.expandedRow = [NSNumber numberWithInteger:index];
                break;
            }
        }
    }
    else {
        self.expandedRow = nil;
    }
    
    [_treatmentsTableView reloadData];
    [self refresh];
}

- (void)setEnabled:(BOOL)flag {
    _enabled = flag;
    [self isReady];
}

- (void)setSelectedTreatmentReady:(BOOL)selectedTreatmentReady {
    _selectedTreatmentReady = selectedTreatmentReady;
    [self updateUI];
}

- (void)setExpandedRow:(NSNumber *)expandedRow {
    [expandedRow retain];
    [_expandedRow release];
    _expandedRow = expandedRow;
}

#pragma mark - IBActions
- (IBAction)visitValueChanged:(id)sender {
    if (![self isRefreshing]) {
        [_changesControlProtocol shouldEditEntity:_diagnosis withControlBlock:^(BOOL accepted) {
            if (accepted) {
                _diagnosis.visitType = _visitTypeSelector.selectedValue;
            }
            [self updateUI];
        }];
    }
}

- (IBAction)patologyValueChanged:(id)sender {
    if (![self isRefreshing]) {
        [_changesControlProtocol shouldEditEntity:_diagnosis withControlBlock:^(BOOL accepted) {
            if (accepted) {
                _diagnosis.pathology = _pathologyTypeSelector.selectedValue;
            }
            [self updateUI];
        }];
    }
}



- (IBAction)addTreatmentButtonClicked:(id)sender {
    [_changesControlProtocol shouldEditEntity:nil withControlBlock:^(BOOL accepted) {
        if (accepted) {
            // Create and configure the new empty treatment
            [[ChangeManager manager] addTreatmentToDiagnosis:_diagnosis];
            
            self.expandedRow = [NSNumber numberWithInteger:_diagnosis.treatments.count - 1];
            [_treatmentsTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        }
        [self updateUI];
    }];
}

#pragma mark - PredictiveTextFieldDelegate
-(void) predictiveTextField:(PredictiveTextField*)textField elementsForInputString:(NSString*)inputString {
    //Kanchan Nair
if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"])
{
    if (textField == _diagnosticTextField) {
        [self.fetchDiagnosticOperation cancel];
        self.fetchDiagnosticOperation = [[EntityFactory sharedEntityFactory] fetchRemovableEntitiesInBackground:NSStringFromClass([DiagnosisType class]) filteringByName:inputString sortResults:YES onComplete:^(NSArray *results) {
            
            // Remove already used diagnosis from the list
            NSSet* otherDiagnosis = [self otherDiagnosisTypes];
            NSMutableArray* filteredResults = [NSMutableArray arrayWithArray:results];
            [filteredResults removeObjectsInArray:[otherDiagnosis allObjects]];
            
            [textField fetchedElements:filteredResults forInputString:inputString];
        }];
    }
    
    else if (textField == _drugIndicatorTextField) {
        
        [self.fetchDiagnosticOperation cancel];
        self.fetchDiagnosticOperation = [[EntityFactory sharedEntityFactory] fetchRemovableEntitiesInBackground:NSStringFromClass([DrugIndicator class]) filteringByName:inputString sortResults:YES onComplete:^(NSArray *results) {
            [textField fetchedElements:results forInputString:inputString];
        }];
    }
}
    
    else{
        
        
        if (textField == _diagnosticTextField) {
            [self.fetchDiagnosticOperation cancel];
            self.fetchDiagnosticOperation = [[EntityFactory sharedEntityFactory] fetchRemovableEntitiesInBackground:NSStringFromClass([DiagnosisType class]) filteringByName:inputString sortResults:YES onComplete:^(NSArray *results) {
                
                // Remove already used diagnosis from the list
                NSSet* otherDiagnosis = [self otherDiagnosisTypes];
                NSMutableArray* filteredResults = [NSMutableArray arrayWithArray:results];
                [filteredResults removeObjectsInArray:[otherDiagnosis allObjects]];
                
                [textField fetchedElements:filteredResults forInputString:inputString];
            }];
        }

    }
}

-(void) predictiveTextFieldValueChanged:(PredictiveTextField*)textField {
    if (![self isRefreshing]) {
   //Kanchan Nair
        if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"])
        {
        if (textField == _diagnosticTextField) {
            [_changesControlProtocol shouldEditEntity:_diagnosis withControlBlock:^(BOOL accepted) {
                if (accepted) {

                    DiagnosisType* selectedDiagnosisType = _diagnosticTextField.selectedElement;
                    _diagnosis.diagnosisType = selectedDiagnosisType;
                    if (selectedDiagnosisType == nil && _diagnosticTextField.text.length > 0) {
                        _diagnosis.userDiagnosis = _diagnosticTextField.text;
                    }
                    else {
                        _diagnosis.userDiagnosis = nil;
                    }
                    
                    if (![self checkDuplicatedDiagnosis]) {
                        _diagnosis.userDiagnosis = nil;
                        _diagnosis.diagnosisType = nil;
                        [self showDuplicatedDiagnosisAlert];
                    }
                }
                [self updateUI];
            }];
        }
        
        else if (textField == _drugIndicatorTextField) {
            if (_diagnosis.drugIndicator != textField.selectedElement || (textField.selectedElement == nil && textField.text != _diagnosis.userDrugIndicator)) {
                [_changesControlProtocol shouldEditEntity:_diagnosis withControlBlock:^(BOOL accepted) {
                    if (accepted) {
                        DrugIndicator* selectedDrug = _drugIndicatorTextField.selectedElement;
                        _diagnosis.drugIndicator = selectedDrug;
                        _diagnosis.userDrugIndicator = nil;
                        if (_diagnosis.drugIndicator == nil && textField.text > 0) {
                            _diagnosis.userDrugIndicator = textField.text;
                        }
                        DLog(@"Set Drug Indicator: %@", selectedDrug.name);
                    }
                    [self updateUI];
                }];
            } else {
                DLog(@"Entered Drug Indicator is same as registered. Do nothing.");
            }
        }
        
    }
    
        else{
            if (textField == _diagnosticTextField) {
                [_changesControlProtocol shouldEditEntity:_diagnosis withControlBlock:^(BOOL accepted) {
                    if (accepted) {
                        DiagnosisType* selectedDiagnosisType = _diagnosticTextField.selectedElement;
                        _diagnosis.diagnosisType = selectedDiagnosisType;
                        if (selectedDiagnosisType == nil && _diagnosticTextField.text.length > 0) {
                            _diagnosis.userDiagnosis = _diagnosticTextField.text;
                        }
                        else {
                            _diagnosis.userDiagnosis = nil;
                        }
                        
                        if (![self checkDuplicatedDiagnosis]) {
                            _diagnosis.userDiagnosis = nil;
                            _diagnosis.diagnosisType = nil;
                            [self showDuplicatedDiagnosisAlert];
                        }
                    }
                    [self updateUI];
                }];
            }

            
        }
        
    }
}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _diagnosis.treatments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = nil;
    Treatment* treatment = nil;
    if (indexPath.row < _diagnosis.treatments.count) {
        treatment = [[_diagnosis sortedTreatments] objectAtIndex:indexPath.row];
    }
    else {
        DLog(@"Inconsistency warning: row higher than treatments count");
    }
    
    if (_expandedRow != nil && _expandedRow.integerValue == indexPath.row) {
        // Expanded row
        NSString* cellIdentifier = NSStringFromClass([ExpandedMedicamentCell class]);
        ExpandedMedicamentCell* expandedCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (expandedCell == nil) {
            expandedCell = [ExpandedMedicamentCell loadFromNIB:cellIdentifier];
        }
        expandedCell.delegate = self;
        expandedCell.changesControlProtocol = _changesControlProtocol;
       
        if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"])

        {
        //Kanchan Nair : To change the title name to Drug 1
        expandedCell.cellTitle = [NSString stringWithFormat:NSLocalizedString(@"COLLAPSE_CELL_DRUG 1", @""), indexPath.row + 1];
        }
        else{
    expandedCell.cellTitle = [NSString stringWithFormat:NSLocalizedString(@"TREATMENT_CELL_TITLE", @""), indexPath.row + 1];
        }
        expandedCell.textLabel.adjustsFontSizeToFitWidth = YES;
        expandedCell.treatment = treatment;
        expandedCell.enabled = _treatmentsEnabled;
        
        cell = expandedCell;
    }
    else {
        // Collapsed row
        NSString* cellIdentifier = NSStringFromClass([CollapsedMedicamentCell class]);
        CollapsedMedicamentCell* collapsedCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (collapsedCell == nil) {
            collapsedCell = [CollapsedMedicamentCell loadFromNIB:cellIdentifier];
        }
        collapsedCell.delegate = self;
        if ([treatment isComplete]) {
            NSString* name = treatment.userMedicament;
            if (treatment.medicament != nil) {
                name = treatment.medicament.name;
            }
            collapsedCell.cellTitle = [NSString stringWithFormat:NSLocalizedString(@"TREATMENT_CELL_TITLE_WITH_NAME", @""), indexPath.row + 1, name];
            collapsedCell.textLabel.adjustsFontSizeToFitWidth = YES;
            
        }
        else {
            collapsedCell.cellTitle = [NSString stringWithFormat:NSLocalizedString(@"TREATMENT_CELL_TITLE", @""), indexPath.row + 1];
            collapsedCell.textLabel.adjustsFontSizeToFitWidth = YES;
            
        }
        [collapsedCell hideIncompleteTreatmentAlert:[treatment isComplete]];
        collapsedCell.enabled = _treatmentsEnabled;
        cell = collapsedCell;
    }
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 0;
    if (_expandedRow != nil && _expandedRow.integerValue == indexPath.row) {
        height = kExpandedCellHeight;
    }
    else {
        height = kCollapsedCellHeight;
    }
    return height;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Avoid cell selection
    return nil;
}

- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

#pragma mark - ExpandedMedicamentCellDelegate
- (void)expandedMedicamentCellCollapseButtonClicked:(ExpandedMedicamentCell*)cell {
    if (_expandedRow != nil && _expandedRow.integerValue < _diagnosis.treatments.count) {
        NSInteger expandedRow = _expandedRow.integerValue;
        self.expandedRow = nil;
        
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:expandedRow inSection:0];
        [_treatmentsTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)expandedMedicamentCellDeleteButtonClicked:(ExpandedMedicamentCell*)cell {
    [_changesControlProtocol shouldEditEntity:nil withControlBlock:^(BOOL accepted) {
        if (accepted) {
            [self showConfirmationToDeleteTreatment:cell.treatment originView:cell.deleteButton];
        }
    }];
}

- (void)expandedMedicamentCellIsReady:(BOOL)ready {
    self.selectedTreatmentReady = ready;
}

- (void)expandedMedicamentCell:(ExpandedMedicamentCell *)cell willPresentPopover:(UIPopoverController *)popoverController {
    NSIndexPath* indexPath = [self.treatmentsTableView indexPathForCell:cell];
    [self.treatmentsTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

#pragma mark - ModalControllerProtocol
- (void)presentModalController:(UIViewController*)controller {
    [_delegate presentModalController:controller];
}

- (void)dismissModalController:(UIViewController*)controller {
    [_delegate dismissModalController:controller];
}

#pragma mark - CollapsedMedicamentCellDelegate
- (void)collapsedMedicamentCellExpandButtonClicked:(CollapsedMedicamentCell*)cell {
    NSMutableArray* reloadRows = [NSMutableArray arrayWithCapacity:2];
    if (_expandedRow != nil && _expandedRow.integerValue < _diagnosis.treatments.count) {
        // Reload previously selected row
        [reloadRows addObject:[NSIndexPath indexPathForRow:_expandedRow.integerValue inSection:0]];
    }
    
    // Reload selected row
    NSIndexPath* selectedRow = [_treatmentsTableView indexPathForCell:cell];
    [reloadRows addObject:selectedRow];
    
    self.expandedRow = [NSNumber numberWithInteger:selectedRow.row];
    [_treatmentsTableView reloadRowsAtIndexPaths:reloadRows withRowAnimation:UITableViewRowAnimationFade];
    [_treatmentsTableView scrollToRowAtIndexPath:selectedRow atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)collapsedMedicamentCellDeleteButtonClicked:(CollapsedMedicamentCell*)cell {
    [_changesControlProtocol shouldEditEntity:nil withControlBlock:^(BOOL accepted) {
        if (accepted) {
            NSIndexPath* indexPath = [_treatmentsTableView indexPathForCell:cell];
            Treatment* treatment = nil;
            DAssert(indexPath.row < _diagnosis.treatments.count, @"Row higher than treatments count");
            treatment = [[_diagnosis sortedTreatments] objectAtIndex:indexPath.row];
            
            [self showConfirmationToDeleteTreatment:treatment originView:cell.deleteMedicamentButton];
        }
    }];
}

#pragma mark - CheckBoxDelegate

- (void)checkBoxDidChangeValue:(CheckBox*)checkBox {
    if (![self isRefreshing]) {
        [_changesControlProtocol shouldEditEntity:_diagnosis withControlBlock:^(BOOL accepted) {
            if (accepted) {
                _diagnosis.needsTreatment = [NSNumber numberWithBool:!checkBox.selected];
                BOOL treatmentNotEmpty = ([_diagnosis.treatments count] > 1 || ([_diagnosis.treatments count] == 1 && ![[_diagnosis.treatments anyObject] isEmpty]));
                if (![_diagnosis.needsTreatment boolValue] && treatmentNotEmpty) {
                    [_changesControlProtocol shouldEditEntityLosingChanges:_diagnosis withControlBlock:^(BOOL accepted) {
                        if (accepted) {
                            [self deleteAllTreatments];
                        } else {
                            _diagnosis.needsTreatment = [NSNumber numberWithBool:checkBox.selected];
                        }
                        [self updateUI];
                    }];
                }
            }
            [self updateUI];
        }];
    }
}

#pragma mark - Public method
- (BOOL)requestDismiss {
    BOOL treatmentDismiss = YES;
    if (_expandedRow != nil) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:_expandedRow.integerValue inSection:0];
        ExpandedMedicamentCell* medicamentCell = (ExpandedMedicamentCell*)[_treatmentsTableView cellForRowAtIndexPath:indexPath];
        treatmentDismiss = [medicamentCell requestDismiss];
    }
    
    if (treatmentDismiss) {
        [self.view endEditing:YES];
    }
    return treatmentDismiss;

}

@end
