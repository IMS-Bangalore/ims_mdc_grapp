//
//  DiagnosticViewController.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 11/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PredictiveTextField.h"
#import "Diagnosis.h"
#import "CustomSegmentedControl.h"
#import "ExpandedMedicamentCell.h"
#import "CollapsedMedicamentCell.h"
#import "ComponentGroup.h"
#import "ChangesControlProtocol.h"
#import "DrugIndicator.h"


@class DiagnosticViewController;
@protocol DiagnosticViewControllerDelegate<ModalControllerProtocol>
- (void)diagnosticViewController:(DiagnosticViewController*)controller isReady:(BOOL)ready;
@end

@interface DiagnosticViewController : UIViewController <PredictiveTextFieldDelegate, UITableViewDataSource, UITableViewDelegate, ExpandedMedicamentCellDelegate, CollapsedMedicamentCellDelegate, CheckBoxDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>{
    BOOL isHideTxt;
    NSString *newValue;
}

@property (nonatomic, retain) Diagnosis *diagnosis;
@property (nonatomic, assign, getter=isEnabled) BOOL enabled;
@property (nonatomic, assign) id<DiagnosticViewControllerDelegate> delegate;
@property (nonatomic, assign) id<ChangesControlProtocol> changesControlProtocol;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet PredictiveTextField *diagnosticTextField;
@property (retain, nonatomic) IBOutlet CustomSegmentedControl *visitTypeSelector;
@property (retain, nonatomic) IBOutlet CustomSegmentedControl *pathologyTypeSelector;

//Kanchan Nair: added for Polish Drug Indicator
@property (retain, nonatomic) IBOutlet PredictiveTextField *drugIndicatorTextField;

@property (retain, nonatomic) IBOutlet UITableView *treatmentsTableView;
@property (retain, nonatomic) IBOutlet ComponentGroup *addTreatmentButtonComponent;
@property (retain, nonatomic) IBOutlet CheckBox *noTreatmentCheckBox;

//Ravi_Bukka
@property (retain, nonatomic) IBOutlet UIButton *addTreatmentButton;

#pragma mark - IBActions
- (IBAction)visitValueChanged:(id)sender;
- (IBAction)patologyValueChanged:(id)sender;
- (IBAction)addTreatmentButtonClicked:(id)sender;

#pragma mark - Public methods
- (BOOL)requestDismiss;

@end
