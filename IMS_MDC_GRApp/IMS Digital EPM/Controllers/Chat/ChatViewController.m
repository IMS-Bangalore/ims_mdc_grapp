//
//  ChatViewController.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 05/12/12.
//
//

#import "ChatViewController.h"
#import "ChatMessageCell.h"
#import "RequestFactory.h"
#import "BaseRequest.h"
#import "MultipleChoiceViewController.h"
#import "ChatMessageComposerViewController.h"
#import "TwinCodersLibrary.h"
//Deepak_Carpenter : added new reachbility Class
#import "Reachability.h"
//Ravi_Bukka: Added for logging feature
#import "PatientViewController.h"

static NSString* const kQuestionCellIdentifier = @"questionCell";
static NSString* const kAnswerCellIdentifier = @"answerCell";
static NSString* const kNewQuestionCellIdentifier = @"newQuestionCell";

static NSString* const kQuestionCellNib = @"ChatQuestionCell";
static NSString* const kAnswerCellNib = @"ChatAnswerCell";
static NSString* const kNewQuestionCellNib = @"ChatNewQuestionCell";

typedef enum {
    kMessagesSection,
    kSendMessageSection,
} sectionType;

@interface ChatViewController ()

@property (nonatomic, retain) MultipleChoiceViewController* choiceController;
@property (nonatomic, retain) ChatMessageCell* messageCell;
@property (nonatomic, retain) ChatMessageComposerViewController *composerViewController;
@property (nonatomic, retain) RequestFactory* requestFactory;
@property (nonatomic, retain) BaseRequest* request;

@end

@implementation ChatViewController

@synthesize messages = _messages;
@synthesize userId = _userId;
@synthesize choiceController = _choiceController;
@synthesize messageCell = _messageCell;
@synthesize composerViewController = _composerViewController;
@synthesize requestFactory = _requestFactory;
@synthesize request = _request;

//Ravi_Bukka: Added for logging feature
@synthesize patientViewController = _patientViewController;

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.messageCell = [ChatMessageCell loadFromNIB:kQuestionCellNib];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self sendRequestAnimated:NO andShowingErrors:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    if (_choiceController != nil) {
        [_delegate dismissModalController:_choiceController];
        _choiceController = nil;
    }
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public methods

- (void)loadMessages {
    [self sendRequestAnimated:NO andShowingErrors:NO];
}

- (NSInteger)numberOfUnreadMessagesForDate:(NSDate*)lastDate {
    int numberOfUnreadMessages = 0;
    if (lastDate == nil) {
        lastDate = [NSDate dateWithTimeIntervalSince1970:NSTimeIntervalSince1970];
    }
    for (ChatMessage* chatMessage in self.messages) {
        if (![chatMessage isQuestion] && [chatMessage.date compare:lastDate] == NSOrderedDescending) {
            numberOfUnreadMessages++;
        }
    }
    return numberOfUnreadMessages;
}

#pragma mark - IBActions

- (IBAction)refreshMessages:(id)sender {
    [self sendRequestAnimated:YES andShowingErrors:YES];
}

-(IBAction)showPicker:(id)sender
{
	// This sample can run on devices running iPhone OS 2.0 or later
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later.
	// So, we must verify the existence of the above class and provide a workaround for devices running
	// earlier versions of the iPhone OS.
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
         [self launchMailAppOnDevice];
	}
}

#pragma mark -
#pragma mark Compose Mail

// Displays an email composition interface inside the application. Populates all the Mail fields.
-(void)displayComposerSheet
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
	[picker setSubject:@"All Patients logging info"];
	
    
	// Set up recipients
	NSArray *toRecipients = [NSArray arrayWithObjects:@"mcdcsupport@imshealth.com",nil];
	NSArray *ccRecipients = [NSArray arrayWithObjects:@"", nil];
	NSArray *bccRecipients = [NSArray arrayWithObject:@""];
	
	[picker setToRecipients:toRecipients];
	[picker setCcRecipients:ccRecipients];
	[picker setBccRecipients:bccRecipients];
    
 
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];

    //file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/PatientLogFile.xls",
                          documentsDirectory];
    NSData *myData = [NSData dataWithContentsOfFile:fileName];
    
  [picker addAttachmentData:myData mimeType:@"application/vnd.ms-excel" fileName:@"PatientLogFile.xls"];
	
	// Fill out the email body text
	NSString *emailBody = @"Please find attached all Patient information including diagnosis and treatment info";
	[picker setMessageBody:emailBody isHTML:YES];
	
	
    [self presentViewController:picker animated:YES completion:nil];
    [picker release];
}


#pragma mark -
#pragma mark Workaround

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
	NSString *recipients = @"mailto:mcdcsupport@imshealth.com?cc=mcdcsupport@imshealth.com&subject=All Patients logging info";
	NSString *body = @"&body=Please find attached all Patient information including diagnosis and treatment info";
	
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

//Deepak_Carpenter: Added for mailAlert Pop Up
-(void) displayMailAlert:(NSString*)message {
    if (self.choiceController != nil) {
        [_delegate dismissModalController:self.choiceController];
    }
    self.choiceController = [[MultipleChoiceViewController new] autorelease];
    
    // Ravi_Bukka: Added for localizing error messages
    
    _choiceController.message = NSLocalizedString(message,nil);
    _choiceController.messageFont = [UIFont boldSystemFontOfSize:32];
    _choiceController.acceptButtonTitle = NSLocalizedString(@"Email_Ok", @"");
    _choiceController.cancelButtonTitle =nil;
    _choiceController.otherButtonTitle = nil;
    _choiceController.eventBlock = ^(MultipleChoiceButton button){
        
        
        
        // Dismiss and release the controller
        [_delegate dismissModalController:self.choiceController];
        self.choiceController = nil;
        
        switch (button) {
            case kMultipleChoiceAccept: {
                
                break;
            }
            case kMultipleChoiceCancel: {
                // Cancel request
                break;
            }
            case kMultipleChoiceOther: {
                
                break;
            }
                
                
        }
        
    };
    
    // Present modal controller
    [_delegate presentModalController:self.choiceController];
}

- (IBAction)loggingPatientDetails:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        //    _patientViewController = [[PatientViewController alloc]init];
        //    [_patientViewController sendLogInfo];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"createLogFile"
         object:nil];
        
        // [[NSNotificationCenter defaultCenter]postNotificationName:<#(NSString *)#> object:<#(id)#>]
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        
        [picker setSubject:@"All Patients logging info"];
        
        
        // Set up recipients
        NSArray *toRecipients = [NSArray arrayWithObjects:@"mcdcsupport@imshealth.com",nil];
        NSArray *ccRecipients = [NSArray arrayWithObjects:@"", nil];
        NSArray *bccRecipients = [NSArray arrayWithObject:@""];
        
        
        [picker setToRecipients:toRecipients];
        [picker setCcRecipients:ccRecipients];
        [picker setBccRecipients:bccRecipients];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        
        //file name to write the data to using the documents directory:
        NSString *fileName = [NSString stringWithFormat:@"%@/PatientLogFile.xls",
                              documentsDirectory];
        NSData *myData = [NSData dataWithContentsOfFile:fileName];
        
        [picker addAttachmentData:myData mimeType:@"application/vnd.ms-excel" fileName:@"PatientLogFile.xls"];
        
        
        
        // Fill out the email body text
        NSString *emailBody = @"Please find attached all Patient information including diagnosis and treatment info";
        [picker setMessageBody:emailBody isHTML:YES];
        
        //[self presentModalViewController:picker animated:YES];
        [self presentViewController:picker animated:YES completion:nil];
        [picker release];
    }
    else{
        //Deepak Carpenter: Added to inform user about mail configuration
        [self displayMailAlert:NSLocalizedString(@"Email_Alert", @"")];
        
    }
    
    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
//	message.hidden = NO;
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
//			message.text = @"Result: canceled";
            [[NSFileManager defaultManager] createFileAtPath:[[NSUserDefaults standardUserDefaults]valueForKey:@"filePath"] contents:[NSData data] attributes:nil];
			break;
		case MFMailComposeResultSaved:
//			message.text = @"Result: saved";
            [[NSFileManager defaultManager] createFileAtPath:[[NSUserDefaults standardUserDefaults]valueForKey:@"filePath"] contents:[NSData data] attributes:nil];
			break;
		case MFMailComposeResultSent:
//			message.text = @"Result: sent";
            [[NSFileManager defaultManager] createFileAtPath:[[NSUserDefaults standardUserDefaults]valueForKey:@"filePath"] contents:[NSData data] attributes:nil];
            
        {
            //Kanchan: CR#6 Added to send chat msg when log is send
            NSDate *localDate = [NSDate date];
            NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc]init]autorelease];
            dateFormatter.dateFormat = @"MM/dd/yy";
            
            NSString *dateString = [dateFormatter stringFromDate: localDate];
            
            
            
            NSDateFormatter *timeFormatter = [[[NSDateFormatter alloc]init]autorelease];
            timeFormatter.dateFormat = @"HH:mm:ss";
            
            
            NSString *newdateString = [timeFormatter stringFromDate: localDate];
            
            
            ChatMessage* chatMessage = [[ChatMessage alloc] init];
            chatMessage.date = [NSDate date];
            //chatMessage.time = [NSTimer]
            chatMessage.message =[NSString stringWithFormat:@"Log file sent @%@ @%@", dateString,newdateString];
            chatMessage.question = YES;
            //Deepak_carpenter : Added for CR#6 send chat message when log button clicked
            NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
            switch (internetStatus) {
                case NotReachable:
                {
                    [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"Log file sent @%@ @%@", dateString,newdateString] forKey:@"chatMessage"];
                }
                case ReachableViaWiFi:break;
                case ReachableViaWWAN:break;
                    
                default:
                    break;
            }
            
            
            
            self.requestFactory = [RequestFactory sharedInstance];
            self.request = [self.requestFactory createChatSendQuestionRequestWithUserId:self.userId andChatMessage:chatMessage onComplete:^{
                
                [self.newDelegate messageSended:chatMessage];
                [self sendRequestAnimated:NO andShowingErrors:NO];
                
                
                // [self.newdelegate newmessageSended:chatMessage];
            } onError:^(NSError *error) {
                [self displayAlert:error.localizedDescription];
            }];
            
            
            [self.request start];

        }
            
            
            
			break;
		case MFMailComposeResultFailed:
//			message.text = @"Result: failed";
            [[NSFileManager defaultManager] createFileAtPath:[[NSUserDefaults standardUserDefaults]valueForKey:@"filePath"] contents:[NSData data] attributes:nil];
			break;
		default:
//			message.text = @"Result: not sent";
			break;
	}
    [self dismissViewControllerAnimated:YES completion:nil];
	//[self dismissModalViewControllerAnimated:YES];
    
}

//Deepak_Carpenter : added for CR#6 send chat message when log button clicked
-(void)sendconfirmationMessgae{
    ChatMessage* chatMessage = [[ChatMessage alloc] init];
    chatMessage.date = [NSDate date];
    //chatMessage.time = [NSTimer]
    chatMessage.message = [[NSUserDefaults standardUserDefaults]valueForKey:@"chatMessage"];
    chatMessage.question = YES;
    self.requestFactory = [RequestFactory sharedInstance];
    self.request = [self.requestFactory createChatSendQuestionRequestWithUserId:self.userId andChatMessage:chatMessage onComplete:^{
        
        [self.newDelegate messageSended:chatMessage];
        [self sendRequestAnimated:NO andShowingErrors:NO];
        
        
        // [self.newdelegate newmessageSended:chatMessage];
    } onError:^(NSError *error) {
        [self displayAlert:error.localizedDescription];
    }];
    
    
    [self.request start];
    
    
}

#pragma mark - Private methods

- (void)sendRequestAnimated:(BOOL)animated andShowingErrors:(BOOL)showErrors {
    if (self.request != nil) {
        [self.request cancel];
    }
    self.requestFactory = [RequestFactory sharedInstance];
    if (self.userId.length > 0) {
        self.request = [self.requestFactory createGetAnswersRequestWithUserId:self.userId onComplete:^(NSArray *answers) {
            self.messages = [NSMutableArray arrayWithArray:answers];
            
            [self.delegate chatControllerDidFinishLoading:self];
            [_tableView reloadSections:[NSIndexSet indexSetWithIndex:kMessagesSection] withRowAnimation:UITableViewRowAnimationAutomatic];
            [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:kSendMessageSection] atScrollPosition: UITableViewScrollPositionBottom animated:animated];
            self.request = nil;
        } onError:^(NSError *error) {
            if (showErrors) {
                [self displayAlert:error.localizedDescription];
            }
            self.request = nil;
        }];
        [self.request start];
    }

}

- (void)displayAlert:(NSString*)message {
    if (self.choiceController != nil) {
        [_delegate dismissModalController:self.choiceController];
    }
    self.choiceController = [[MultipleChoiceViewController new] autorelease];
    _choiceController.message = message;
    _choiceController.messageFont = [UIFont boldSystemFontOfSize:32];
    _choiceController.acceptButtonTitle = nil;
    _choiceController.cancelButtonTitle = NSLocalizedString(@"CHANGE_PASSWORD_ERROR_BUTTON", @"");
    _choiceController.otherButtonTitle = nil;
    _choiceController.eventBlock = ^(MultipleChoiceButton button){
        // Dismiss and release the controller
        [_delegate dismissModalController:self.choiceController];
        self.choiceController = nil;
    };

    // Present modal controller
    [_delegate presentModalController:self.choiceController];
}

#pragma mark UITableViewDelegate
- (BOOL)hasResults {
    return self.messages.count > 0;
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int rows = 0;
    switch (section) {
        case kMessagesSection:
            if ([self hasResults]) {
                rows = self.messages.count;
                NSLog(@"Number of rows in messageSection %d", rows);
            }
            break;
        case kSendMessageSection:
        {
            rows = 1;
             NSLog(@"Number of rows in SendmessageSection %d", rows);
             break;
        }
           
        default:
            break;
    }
    return rows;
    
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = nil;
    ChatNewQuestionCell* newQuestionCell = nil;
    
    switch (indexPath.section) {
        case kMessagesSection:
            if ([self hasResults]) {
                ChatMessageCell* msgCell = nil;
                BOOL isQuestion = [self.messages[indexPath.row] isQuestion];
                if (isQuestion) {
                    msgCell = [tableView dequeueReusableCellWithIdentifier:kQuestionCellIdentifier];
                    if (msgCell == nil) {
                        msgCell = [ChatMessageCell loadFromNIB:kQuestionCellNib];
                    }
                } else {
                    msgCell = [tableView dequeueReusableCellWithIdentifier:kAnswerCellIdentifier];
                    if (msgCell == nil) {
                        msgCell = [ChatMessageCell loadFromNIB:kAnswerCellNib];
                    }
                }
                msgCell.chatMessage = self.messages[indexPath.row];
                cell = msgCell;
            }
            break;
        case kSendMessageSection:
            newQuestionCell = [tableView dequeueReusableCellWithIdentifier:kNewQuestionCellIdentifier];
            if (newQuestionCell == nil) {
                newQuestionCell = [ChatNewQuestionCell loadFromNIB:kNewQuestionCellNib];
            }
            newQuestionCell.delegate = self;
            cell = newQuestionCell;
            break;
        default:
            break;
    }

//Ravi_Bukka: Added for localization
    newQuestionCell.questionLabel.text = NSLocalizedString(@"ENTER_NEW_QUESTION...", nil);
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 0;
    switch (indexPath.section) {
        case kMessagesSection:
            height = [self.messageCell heightForMessage:[self.messages[indexPath.row] message]];
            break;
        case kSendMessageSection:
            height = tableView.rowHeight;
            break;
        default:
            break;
    }
    return height;
}

#pragma mark - ChatNewQuestionCellDelegate

- (void)openMessageComposer {
    self.composerViewController = [[[ChatMessageComposerViewController alloc] init] autorelease];
    self.composerViewController.delegate = self;
    self.composerViewController.userId = self.userId;
    
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^ {
                        [self.view addSubview:self.composerViewController.view];
                    }
                    completion:nil];
}

#pragma mark - ChatMessageComposerViewControllerDelegate

- (void)cancelComposer {
    [self.composerViewController removeKeyboard];
    [UIView animateWithDuration:0.5
                          delay:0
                        options: UIViewAnimationOptionTransitionCrossDissolve
                     animations:^{
                         self.composerViewController.view.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         [self.composerViewController.view removeFromSuperview];
                     }];
}

- (void)messageSended:(ChatMessage *)chatMessage {
    [self.composerViewController.view removeFromSuperview];
    if (self.messages == nil) {
        self.messages = [NSMutableArray array];
    }
    NSInteger position = self.messages.count;
    [self.messages addObject:chatMessage];
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:position inSection:kMessagesSection];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];

    [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:kSendMessageSection] atScrollPosition: UITableViewScrollPositionBottom animated:YES];
}

- (void)presentModalController:(MultipleChoiceViewController *)choiceController {
    [_delegate presentModalController:choiceController];
}

- (void)dismissModalController:(MultipleChoiceViewController *)choiceController {
    [_delegate dismissModalController:choiceController];
}

- (void)dealloc {
    [_tableView release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}

@end
