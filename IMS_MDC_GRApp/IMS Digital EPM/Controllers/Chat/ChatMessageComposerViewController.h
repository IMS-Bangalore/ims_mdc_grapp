//
//  ChatMessageComposerViewController.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 05/12/12.
//
//

#import <UIKit/UIKit.h>
#import "TwinCodersLibrary.h"
#import "ModalControllerProtocol.h"
#import "MultipleChoiceViewController.h"
#import "ChatMessage.h"

@class ChatMessageComposerViewController;
@protocol ChatMessageComposerViewControllerDelegate <ModalControllerProtocol>

- (void)cancelComposer;
- (void)messageSended:(ChatMessage*)chatMessage;
- (void)presentModalController:(MultipleChoiceViewController*)choiceController;
- (void)dismissModalController:(MultipleChoiceViewController*)choiceController;

@end

@interface ChatMessageComposerViewController : UIViewController <UITextViewDelegate>

#pragma mark - Properties
@property (nonatomic, copy) NSString* userId;
@property (nonatomic, copy) NSString* message;
@property (nonatomic, assign) id<ChatMessageComposerViewControllerDelegate> delegate;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet UITextView *textView;
@property (retain, nonatomic) IBOutlet TCClickableUIView *sendMessageButton;

//Ravi_Bukka: Added for localization
@property (nonatomic, retain) IBOutlet UIButton *cancelButton;
@property (nonatomic, retain) IBOutlet UIButton *sendButton;

#pragma mark - IBActions
- (IBAction)cancel:(id)sender;
- (IBAction)sendMessage:(id)sender;

#pragma mark - Public methods
- (void)removeKeyboard;

@end
