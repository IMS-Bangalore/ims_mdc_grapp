//
//  DiagnosticTabbarItem.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 11/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DiagnosticTabbarItem;
@protocol DiagnosticTabbarItemDelegate <NSObject>

-(void) tabbarItemClicked:(DiagnosticTabbarItem*)tabbarItem;
-(void) tabbarItemCloseButtonClicked:(DiagnosticTabbarItem*)tabbarItem;

@end

@interface DiagnosticTabbarItem : UIView

@property (nonatomic, assign) id<DiagnosticTabbarItemDelegate> delegate;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign, getter=isSelected) BOOL selected;
@property (nonatomic, assign, getter=isEnabled) BOOL enabled;
@property (retain, nonatomic) IBOutlet UIButton *button;
@property (retain, nonatomic) IBOutlet UIButton *closeButton;
@property (nonatomic, retain) NSString *title;

@property (retain, nonatomic) IBOutlet UIImageView *incompleteDiagnosisAlertImageView;

- (IBAction)buttonClicked:(id)sender;
- (IBAction)closeButtonClicked:(id)sender;

- (void)hideIncompleteDiagnosisAlert:(BOOL)hide;

@end
