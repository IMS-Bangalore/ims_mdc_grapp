//
//  WeekdaySelectionView.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 08/10/12.
//
//

#import "WeekdaySelectionView.h"
#import "WeekdayButton.h"
#import "LoginViewController.h"

//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
#import "AppDelegate.h"

@implementation WeekdaySelectionView

@synthesize weekdayButtons = _weekdayButtons;
@synthesize delegate = _delegate;
@synthesize firstDate = _firstDate;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code        
    }
    return self;
}

- (void)reloadUI {
    NSDate* todayDate = [NSDate date];
    
    NSCalendar* calendar = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    NSDateComponents* dateComponents = [[NSDateComponents alloc] init];
    
    for (UIView *buttonView in _weekdayButtons) {
        dateComponents.day = buttonView.tag;
        
        NSDate* date = [calendar dateByAddingComponents:dateComponents toDate:_firstDate options:0];
        
        WeekdayButton *weekdayButton = [WeekdayButton loadFromDefaultNIB];
        buttonView.backgroundColor = [UIColor clearColor];
        weekdayButton.delegate = self;
        weekdayButton.date = date;
        
//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
        //   if ([(AppDelegate*)[[UIApplication sharedApplication] delegate] returnGracePeriondConfirmation]) {
        
        if ([[NSUserDefaults standardUserDefaults] integerForKey: @"ExtendTheGrace"]== 1) {
            
            weekdayButton.enabled = [todayDate timeIntervalSinceDate:date] >= 0 && ([todayDate timeIntervalSinceDate:_firstDate]/kDivideToObtainDays) <= kMaxDayAfterExtendCollaborationEnds;
            
        }
        else {
            weekdayButton.enabled = [todayDate timeIntervalSinceDate:date] >= 0 && ([todayDate timeIntervalSinceDate:_firstDate]/kDivideToObtainDays) <= kMaxDayAfterCollaborationEnds;
        }
        
        [buttonView addSubview:weekdayButton adjust:YES animated:NO];
    }
    [dateComponents release];
}

- (void)setFirstDate:(NSDate *)firstDate {
    [firstDate retain];
    [_firstDate release];
    _firstDate = firstDate;
    
    [self reloadUI];
}

- (void)dealloc {
    [_weekdayButtons release];
    [super dealloc];
}

- (void)dateSelected:(NSInteger)day {
    NSDateComponents* dateComponents = [[[NSDateComponents alloc] init] autorelease];
    
    dateComponents.day = day;
    
    NSDate* date = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:_firstDate options:0];
    [_delegate selectVisitDay:date];
}

@end
