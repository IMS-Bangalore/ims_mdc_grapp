//
//  WeekdayButton.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 08/10/12.
//
//

#import "WeekdayButton.h"

typedef enum {
    kMondayTag = 0,
    kTuesdayTag,
    kWednesdayTag,
    kThursdayTag,
    kFridayTag,
    kSaturdayTag,
    kSundayTag
} WeekDay;

@implementation WeekdayButton
@synthesize date = _date;
@synthesize labelDay = _labelDay;
@synthesize labelDayNumber = _labelDayNumber;

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        // Drawing code
        self.target = self;
        self.action = @selector(buttonClicked);
        [self addObserver:self forKeyPath:@"date" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (WeekDay)weekDayForDate:(NSDate*)date {
    NSCalendar* calendar = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    NSDateComponents* dateComponents = [calendar components:NSWeekdayCalendarUnit fromDate:date];
    NSInteger weekDay = [dateComponents weekday];
    return (weekDay + 5) % 7;
}

- (NSInteger)dayForDate:(NSDate*)date {
    NSCalendar* calendar = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    NSDateComponents* dateComponents = [calendar components:NSDayCalendarUnit fromDate:date];
    return dateComponents.day;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"date"]) {
        switch ([self weekDayForDate:self.date]) {
            case kMondayTag:
                _labelDay.text = NSLocalizedString(@"MON",nil);
                break;
            case kTuesdayTag:
                _labelDay.text = NSLocalizedString(@"TUE",nil);
                break;
            case kWednesdayTag:
                _labelDay.text = NSLocalizedString(@"WED",nil);
                break;
            case kThursdayTag:
                _labelDay.text = NSLocalizedString(@"THU",nil);
                break;
            case kFridayTag:
                _labelDay.text = NSLocalizedString(@"FRI",nil);
                break;
            case kSaturdayTag:
                _labelDay.text = NSLocalizedString(@"SAT",nil);
                [self setWeekendTextColor];
                break;
            case kSundayTag:
                _labelDay.text = NSLocalizedString(@"SUN",nil);
                [self setWeekendTextColor];
                break;
            default:
                break;
        }
        _labelDayNumber.text = [NSString stringWithFormat:@"%d", [self dayForDate:self.date]];
    }
}

- (void)setEnabled:(BOOL)enabled {
    _labelDay.enabled = self.enabled;
    _labelDayNumber.enabled = self.enabled;
    self.actionButton.enabled = self.enabled;
    [super setEnabled:enabled];
}

- (void)buttonClicked {
    [self.delegate dateSelected:[self weekDayForDate:self.date]];
}

- (void)setWeekendTextColor {
    _labelDay.textColor = [UIColor colorWithRed:(45/255.f) green:(119/255.f) blue:(198/255.f) alpha:1];
    _labelDayNumber.textColor = [UIColor colorWithRed:(45/255.f) green:(119/255.f) blue:(198/255.f) alpha:1];
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"date"];
    [_labelDay release];
    [_labelDayNumber release];
    [super dealloc];
}
@end
