//
//  PatientViewController.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 11/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "PatientViewController.h"
#import "DiagnosticViewController.h"
#import "UIView+FillWithSubview.h"
#import "Age.h"
#import "AgeType.h"
#import "Gender.h"
#import "ConsultType.h"
#import "EntityFactory.h"
#import "UIView+NIBLoader.h"
#import "Diagnosis.h"
#import "Diagnosis+Complete.h"
#import "Doctor.h"
#import "Dosage.h"
#import "ConfirmationMessageController.h"
#import "Patient+Complete.h"
#import "MultipleChoiceViewController.h"
#import "DoctorInfo+SortedPatients.h"
#import "Patient+SortedDiagnosis.h"
#import "Patient+Complete.h"
#import "ChangeManager.h"
#import "WeekdaySelectionView.h"
#import "WeekdaySelectionPopoverContentViewController.h"
#import "UIView+NIBLoader.h"
#import "StatsReporter.h"
#import "Smoker.h"
#import "PrescriptionAndSickFundVViewController.h"
#import "PlaceOfVisitTableViewController.h"
#import "PlaceOfVisit.h"
#import "SickFund.h"
#import "Insurance.h"

//Ravi_Bukka: CR#2: Patient Difference and Local Notification
#import "AppDelegate.h"

//Ravi Bukk: Added for logging feature

//#import "diagnosisType.h"
//#import "visitType.h"
//#import "pathology.h"
//#import "Presentation.h"
//#import "Effect.h"
//#import "DurationType.h"
//#import "Frequency.h"
//#import "TherapyType.h"
//#import "OtherSpecialty.h"
//#import "ChangeNotifier.h"
//#import "Diagnosis+SortedTreatments.h"
//#import "Doctor.h"
//#import "DoctorInfo.h"
//#import "UnitType.h"




static NSString* const kIdentifierKey = @"identifier";
static NSString* const kIndexKey = @"index";

static NSString* const kAgeTypeMonths = @"2";
static NSString* const kAgeTypeYears = @"1";

static NSString* const kAgeTypeMonths_pl = @"2";
static NSString* const kAgeTypeYears_pl = @"1";


static NSString* const kGenderMale = @"2";
static NSString* const kGenderFemale = @"3";

//FOR POLISH
static NSString* const kGenderMale_pl = @"1";
static NSString* const kGenderFemale_pl = @"2";

//For Greece
static NSString* const kGenderMale_el = @"1";
static NSString* const kGenderFemale_el = @"2";
static NSString* const kConsultPublic_el = @"1";
static NSString* const kConsultPrivate_el = @"2";


static NSString* const kConsultPublic = @"1";
static NSString* const kConsultPrivate = @"2";

//For Polish
static NSString* const kConsultPublic_pl = @"1";
static NSString* const kConsultPrivate_pl = @"2";

static const NSInteger kMinTabbarButtonWidth = 143;
static const NSInteger kMaxTabbarButtonCount = 9;
static const NSInteger kMinTabbarButtonCount = 1;

static const NSInteger kAgeMaxMonths = 12;
static const NSInteger kAgeMaxYears = 115;
static const NSInteger kAgeTextFieldMaxLength = 3;

static const NSInteger kWeekBetweenDates = 7;
//Added by Deepak Carpenter ( For _SickFundLabel
static NSString* const kDescriptionKey = @"name";
//Ravi_Bukka: Added for greece and polish localization
static NSString* const kSmokerIndex = @"1";
static NSString* const kNonSmokerIndex = @"2";
static NSString* const kExSmokerIndex = @"3";



// Private properties
@interface PatientViewController()

@property (nonatomic, retain) DiagnosticViewController *diagnosticViewController;
@property (nonatomic, retain) NSDateFormatter *dateFormatter;
@property (readonly) NSInteger diagnoseCount;
@property (nonatomic, retain) NSMutableArray *tabbarItemArray;
@property (nonatomic, retain) UIPopoverController* confirmationPopover;
@property (nonatomic, retain) UIPopoverController* ageInfoPopover;
@property (nonatomic, assign, getter=isDiagnosisReady) BOOL diagnosisReady;
@property (nonatomic, assign, getter=isRefreshing) BOOL refreshing;
@property (nonatomic, retain) MultipleChoiceViewController* choiceController;
@property (nonatomic, assign, getter=isEditing) BOOL editing;
@property (nonatomic, assign, getter=isCreating) BOOL creating;
@property (nonatomic, assign, getter = isCollectingStats) BOOL collectingStats;

@property (nonatomic, retain) UIPopoverController* specialityPopoverController;
@property (retain, nonatomic) PrescriptionAndSickFundVViewController *prescAndSickFund;

-(void) createTabbarButtons;
-(void) updateTabbarButtons;

@end

@implementation PatientViewController
@synthesize tabBarView = _tabBarView;
@synthesize addDiagnoseButton = _addDiagnoseButton;
@synthesize selectedPatient = _selectedPatient;
@synthesize doctor = _doctor;
@synthesize diagnosisHolder = _diagnosisHolder;
@synthesize genderGroup = _genderGroup;
@synthesize genderSelector = _genderSelector;
@synthesize ageGroup = _ageGroup;
@synthesize patientNumberLabel = _patientNumberLabel;
@synthesize finishPatientButton = _finishPatientButton;
@synthesize ageTextField = _ageTextField;
@synthesize ageTypeSelector = _ageTypeSelector;
@synthesize agePopoverView = _agePopoverView;
@synthesize consultationGroup = _consultationGroup;
@synthesize consultationSelector = _consultationSelector;
@synthesize dateGroup = _dateGroup;
@synthesize dateButton = _dateButton;
@synthesize dateTextField = _dateTextField;
@synthesize prevPatientButton = _prevPatientButton;
@synthesize selectedTabbarItem = _selectedTabbarItem;
@synthesize confirmationPopover = _confirmationPopover;
@synthesize diagnosisReady = _diagnosisReady;
@synthesize modalController = _modalController;
@synthesize choiceController = _choiceController;
@synthesize ageInfoPopover = _ageInfoPopover;

//Ravi_Bukka
@synthesize patientLabel = _patientLabel;
@synthesize ageLabel = _ageLabel;
@synthesize typeOfVisitLabel = _typeOfVisitLabel;
@synthesize dateOfVisit = _dateOfVisit;
@synthesize genderLabel = _genderLabel;
@synthesize placeOfVisitArray = _placeOfVisitArray;
@synthesize myPopoverController = _myPopoverController;

//Ravi_Bukka: added for different Greece and poland xibs
@synthesize smokerTypeSelector = _smokerTypeSelector;
@synthesize smoker = _smoker;
@synthesize smokerLabel = _smokerLabel;
@synthesize smokerGroup = _smokerGroup;

@synthesize placeOfVisit = _placeOfVisit;
@synthesize placeOfVisitTextField = _placeOfVisitTextField;
@synthesize placeOfVisitGroup = _placeOfVisitGroup;

@synthesize specialityPopoverController = _specialityPopoverController;
@synthesize prescAndSickFund = _prescAndSickFund;
@synthesize delegate = _delegate;
@synthesize prescriptionLabel = _prescriptionLabel;
@synthesize sickFundLabel = _sickFundLabel;
@synthesize dividerLabel = _dividerLabel;
@synthesize placeOfVisitViewController= _placeOfVisitViewController;


// Private 
@synthesize diagnosticViewController = _diagnosticViewController;
@synthesize dateFormatter = _dateFormatter;
@synthesize tabbarItemArray = _tabbarItemArray;
@synthesize refreshing = _refreshing;
@synthesize editing = _editing;
@synthesize creating = _creating;

//Kanchan Nair
@synthesize placeOfVisitGroup_pl = _placeOfVisitGroup_pl;
@synthesize placeOfVisitTextField_pl=_placeOfVisitTextField_pl;

//Ravi Bukk: Added for logging feature
//@synthesize fileHandle = _fileHandle;
//@synthesize completePatientLogString = _completePatientLogString;


#pragma mark Initialization
-(void) initialize {
    _diagnosticViewController = [[DiagnosticViewController alloc] init];
    _diagnosticViewController.delegate = self;
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setFormatterBehavior:NSDateFormatterMediumStyle];
    _selectedTabbarItem = 0;
   
    
}

- (id)init {
    self = [super init];
    if (self) {
        [self initialize];

    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self initialize];
        
       
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - Private methods

-(void) hideKeyboard {
    [_ageTextField resignFirstResponder];
    [_placeOfVisitTextField_pl resignFirstResponder];

}

- (void)updateDateField {
    NSDate* date = _selectedPatient.visitDate;
    _dateTextField.text = [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterNoStyle];
}

- (NSArray*)sortedSelectedDiagnostics {
    return [_selectedPatient sortedDiagnosis];
}

- (NSArray*)sortedPatients {
    return [_doctor.doctorInfo sortedPatients];
}

- (void)showAgePopover:(BOOL)show {
    if (show) {
        if (![self.ageInfoPopover isPopoverVisible] && self.view.window != nil) {
            if (_ageInfoPopover == nil) {
                UIViewController* controller = [UIViewController new];
                controller.view = self.agePopoverView;
                _ageInfoPopover = [[UIPopoverController alloc] initWithContentViewController:controller];
                _ageInfoPopover.popoverContentSize = self.agePopoverView.frame.size;
                _ageInfoPopover.passthroughViews = [NSArray arrayWithObject:self.view.window];
                
                [controller release];
            }
            [self.ageInfoPopover presentPopoverFromRect:_ageTextField.frame inView:_ageTextField.superview permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
    }
    else {
        [self.ageInfoPopover dismissPopoverAnimated:YES];
    }
}

-(void) refresh {
    _refreshing = YES;
    // Age value
    _ageTextField.text = _selectedPatient.age.value.intValue > 0 ? [NSString stringWithFormat:@"%d", _selectedPatient.age.value.intValue] : nil;
    // Age type
    _ageTypeSelector.selectedValue = _selectedPatient.age.ageType;
    // Gender
    _genderSelector.selectedValue = _selectedPatient.gender;
    
    //placeOfVisit
    _placeOfVisitTextField.selectedElement=_selectedPatient.placeOfVisit;
    
    //Kanchan: place of visit
    _placeOfVisitTextField_pl.selectedElement=_selectedPatient.insurance;
    
    //    if(_selectedPatient.userPlaceOfVisit.length>0 && _selectedPatient.placeOfVisit==nil){
    //        _placeOfVisitTextField.text=_selectedPatient.userPlaceOfVisit;
    //    }
    // Consultation
    //Public Private Values
    //    _presSickTextField.text=_selectedPatient.sickfund.name;

    _presSickTextField.selectedElement=_selectedPatient.sickFund;
    
    if(_selectedPatient.userSickFund.length>0){
        _presSickTextField.text=_selectedPatient.userSickFund;
    }
    //    //ravi_bukka: Added for greece and polish localization
    _smokerTypeSelector.selectedValue = _selectedPatient.smoker;
    
  _consultationSelector.selectedValue = _selectedPatient.consultType;
    

    NSLog(@"Value %@ " ,_selectedPatient.consultType);
    //Deepak Carpenter : to restrict value while consult type is private
    if (_consultationSelector.selectedSegmentIndex==0 && _presSickTextField==nil) {
        consultBool=NO;
    }
    else consultBool=YES;
    
    if (_consultationSelector.selectedSegmentIndex==1) {
        _selectedPatient.recurranceValue=nil;
        _selectedPatient.sickFundValue=nil;
    }

    // Visit date
    [self updateDateField];
    _refreshing = NO;
//    _refreshing = YES;
//    // Age value
//    _ageTextField.text = _selectedPatient.age.value.intValue > 0 ? [NSString stringWithFormat:@"%d", _selectedPatient.age.value.intValue] : nil;    
//    // Age type
//    _ageTypeSelector.selectedValue = _selectedPatient.age.ageType;
//    // Gender
//    _genderSelector.selectedValue = _selectedPatient.gender;
//    // Consultation
//    
////    //ravi_bukka: Added for greece and polish localization
////    _smokerTypeSelector.selectedValue = _diagnosis.smoker;
//    
//    _consultationSelector.selectedValue = _selectedPatient.consultType;
//    // Visit date
//    [self updateDateField];
//    _refreshing = NO;
}

-(void) updateUI {
    if ([self isViewLoaded]) {
        [self refresh];
        NSInteger max;
        // If years is selected (or no selection is done yet)
        if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {
            if (_selectedPatient.age.ageType == nil || [_selectedPatient.age.ageType.identifier isEqualToString:kAgeTypeYears_pl]) {
                max = kAgeMaxYears;
            } else {
                max = kAgeMaxMonths;
            }
        }
        else {
            if (_selectedPatient.age.ageType == nil || [_selectedPatient.age.ageType.identifier isEqualToString:kAgeTypeYears]) {
                max = kAgeMaxYears;
            } else {
                max = kAgeMaxMonths;
            }
        }
        
        if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"])
        {
            BOOL ageError = _selectedPatient.age.value.intValue > max;
            _ageTextField.displayError = ageError;
            [self showAgePopover:ageError && [_selectedPatient.age.ageType.identifier isEqualToString:kAgeTypeMonths]];
            BOOL enableGender = [_selectedPatient.age.value integerValue] != 0 && _selectedPatient.age.value.intValue <= max && _selectedPatient.age.ageType != nil;
            
            _genderGroup.enabled = enableGender;
            
            BOOL enableSmoker = enableGender && _selectedPatient.gender != nil;
            _smokerGroup.enabled = enableSmoker;
            
                       
            BOOL enablePlaceOfVisit = enableSmoker
            &&_smokerTypeSelector.selectedSegmentIndex != UISegmentedControlNoSegment;
            
            _placeOfVisitGroup.enabled = enablePlaceOfVisit;
            
            BOOL enableConsultation = enablePlaceOfVisit && _selectedPatient.gender != nil && (_placeOfVisitTextField.text != nil || [_userDefinedPlaceOfVisit.name  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) && _placeOfVisitTextField!=nil && _selectedPatient.placeOfVisit!=nil;
            _consultationGroup.enabled = enableConsultation;
            
            BOOL enableTextField= enableConsultation && _consultationSelector.selectedSegmentIndex != UISegmentedControlNoSegment && consultBool;{
            _presSickTextField.enabled=enableTextField;
            [_presSickTextField setUserInteractionEnabled:NO];
            }
//            BOOL enableDate = enableConsultation && _selectedPatient.gender  != nil &&_consultationSelector.selectedSegmentIndex != UISegmentedControlNoSegment && enableTextField;
//            _dateGroup.enabled = enableDate;

//Ravi_Bukka: Fix for Insurance and Recurrance issue
            if(_consultationSelector.selectedSegmentIndex == 0){
                enableDate = enableConsultation && _selectedPatient.gender  != nil && enableTextField && ![_presSickTextField.text isEqualToString:@""];
                _dateGroup.enabled = enableDate;
            }
            else if (_consultationSelector.selectedSegmentIndex == 1){
                enableDate = YES;
                _dateGroup.enabled = enableDate;
            }
            else{
                enableDate = NO;
                _dateGroup.enabled = enableDate;
            }
            
            BOOL enableDiagnosis = enableDate && _selectedPatient.visitDate != nil;
            _diagnosticViewController.enabled = enableDiagnosis;
            for (DiagnosticTabbarItem* tabbarItem in _tabbarItemArray) {
                tabbarItem.enabled = enableDiagnosis;
            }
            
            // Previous button
            NSArray* sortedPatients = [self sortedPatients];
            BOOL enablePrevButton = NO;
            // Don't allow going back if editing
            if (![self isEditing]) {
                if (sortedPatients.count > 1) {
                    Patient* firstPatient = [sortedPatients objectAtIndex:0];
                    enablePrevButton = firstPatient != _selectedPatient;
                }
            }
            _prevPatientButton.enabled = enablePrevButton;
            // Next Button
            BOOL enableNextButton = [self.selectedPatient isComplete] && enableDiagnosis;
            BOOL isLast = [[self sortedPatients] lastObject] == self.selectedPatient;
            NSString* nextButtonTitle = self.editing || (self.creating && isLast) ? NSLocalizedString(@"SAVE_PATIENT_BUTTON", nil) : NSLocalizedString(@"NEXT_PATIENT_BUTTON", nil);
            [_finishPatientButton setTitle:nextButtonTitle forState:UIControlStateNormal];
            
            _finishPatientButton.enabled = enableNextButton;
            _addDiagnoseButton.enabled = enableNextButton;
            [self updateTabbarButtons];
            
            // If we are creating a new register, save every change ASAP to avoid data loss
            if ([self isCreating]) {
                [[EntityFactory sharedEntityFactory] saveChanges];
            }
            
        }
        //Kanchan Nair
        
        else if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {
            
            BOOL ageError = _selectedPatient.age.value.intValue > max;
            _ageTextField.displayError = ageError;
            [self showAgePopover:ageError && [_selectedPatient.age.ageType.identifier isEqualToString:kAgeTypeMonths]];
            BOOL enableGender = [_selectedPatient.age.value integerValue] != 0 && _selectedPatient.age.value.intValue <= max && _selectedPatient.age.ageType != nil;
            _genderGroup.enabled = enableGender;
            
            BOOL enablePlaceOfVisit = enableGender && _selectedPatient.gender != nil;
            _placeOfVisitGroup_pl.enabled = enablePlaceOfVisit;
            
            BOOL enableDate = enablePlaceOfVisit &&  _selectedPatient.gender!=nil && _placeOfVisitTextField_pl.text!=nil && _selectedPatient.insurance !=nil;
            // _dateGroup.enabled = enableDate;
            _dateGroup.enabled=enableDate;
            
            
            BOOL enableDiagnosis = enableDate && _selectedPatient.visitDate != nil;
            _diagnosticViewController.enabled = enableDiagnosis;
            for (DiagnosticTabbarItem* tabbarItem in _tabbarItemArray) {
                tabbarItem.enabled = enableDiagnosis;
            }
            // Previous button
            NSArray* sortedPatients = [self sortedPatients];
            BOOL enablePrevButton = NO;
            // Don't allow going back if editing
            if (![self isEditing]) {
                if (sortedPatients.count > 1) {
                    Patient* firstPatient = [sortedPatients objectAtIndex:0];
                    enablePrevButton = firstPatient != _selectedPatient;
                }
            }
            _prevPatientButton.enabled = enablePrevButton;
            // Next Button
            BOOL enableNextButton = [self.selectedPatient isComplete] && enableDiagnosis;
            BOOL isLast = [[self sortedPatients] lastObject] == self.selectedPatient;
            NSString* nextButtonTitle = self.editing || (self.creating && isLast) ? NSLocalizedString(@"SAVE_PATIENT_BUTTON", nil) : NSLocalizedString(@"NEXT_PATIENT_BUTTON", nil);
            [_finishPatientButton setTitle:nextButtonTitle forState:UIControlStateNormal];
            
            _finishPatientButton.enabled = enableNextButton;
            _addDiagnoseButton.enabled = enableNextButton;
            [self updateTabbarButtons];
            
            // If we are creating a new register, save every change ASAP to avoid data loss
            if ([self isCreating]) {
                [[EntityFactory sharedEntityFactory] saveChanges];
            }
        }
        
        else {
            BOOL ageError = _selectedPatient.age.value.intValue > max;
            _ageTextField.displayError = ageError;
            if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"])
                [self showAgePopover:ageError && [_selectedPatient.age.ageType.identifier isEqualToString:kAgeTypeMonths_pl]];
            else
                [self showAgePopover:ageError && [_selectedPatient.age.ageType.identifier isEqualToString:kAgeTypeMonths]];
            BOOL enableGender = [_selectedPatient.age.value integerValue] != 0 && _selectedPatient.age.value.intValue <= max && _selectedPatient.age.ageType != nil;
            _genderGroup.enabled = enableGender;
            BOOL enableConsultation = enableGender && _selectedPatient.gender != nil;
            _consultationGroup.enabled = enableConsultation;
            
            
            BOOL enableDate = enableConsultation && _selectedPatient.gender  != nil;
            _dateGroup.enabled = enableDate;
            
            
            BOOL enableDiagnosis = enableDate && _selectedPatient.visitDate != nil;
            _diagnosticViewController.enabled = enableDiagnosis;
            for (DiagnosticTabbarItem* tabbarItem in _tabbarItemArray) {
                tabbarItem.enabled = enableDiagnosis;
            }
            // Previous button
            NSArray* sortedPatients = [self sortedPatients];
            BOOL enablePrevButton = NO;
            // Don't allow going back if editing
            if (![self isEditing]) {
                if (sortedPatients.count > 1) {
                    Patient* firstPatient = [sortedPatients objectAtIndex:0];
                    enablePrevButton = firstPatient != _selectedPatient;
                }
            }
            _prevPatientButton.enabled = enablePrevButton;
            // Next Button
            BOOL enableNextButton = [self.selectedPatient isComplete] && enableDiagnosis;
            BOOL isLast = [[self sortedPatients] lastObject] == self.selectedPatient;
            NSString* nextButtonTitle = self.editing || (self.creating && isLast) ? NSLocalizedString(@"SAVE_PATIENT_BUTTON", nil) : NSLocalizedString(@"NEXT_PATIENT_BUTTON", nil);
            [_finishPatientButton setTitle:nextButtonTitle forState:UIControlStateNormal];
            
            _finishPatientButton.enabled = enableNextButton;
            _addDiagnoseButton.enabled = enableNextButton;
            [self updateTabbarButtons];
            
            // If we are creating a new register, save every change ASAP to avoid data loss
            if ([self isCreating]) {
                [[EntityFactory sharedEntityFactory] saveChanges];
            }
            
        }
    }
    
}
//Deepak Carpenter: for _PlaceofVisit Text Field
#pragma mark - PredictiveTextFieldDelegate
-(void) predictiveTextField:(PredictiveTextField*)textField elementsForInputString:(NSString*)inputString {
    
    NSArray* results = nil;
//Kanchan Nair
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {
      //  NSArray* results = nil;
        if (textField == _placeOfVisitTextField_pl) {
            results =[[EntityFactory sharedEntityFactory]fetchEntities:NSStringFromClass([Insurance class]) filteringByName:inputString sortResults:YES];
            
        }
        else {
            results = [NSArray array];
        }
        [textField fetchedElements:results forInputString:inputString];
        
    }

 else
 {
     if (textField == _placeOfVisitTextField) {
        results =[[EntityFactory sharedEntityFactory]fetchEntities:NSStringFromClass([PlaceOfVisit class]) filteringByName:inputString sortResults:YES];
        
 }
    else {
        results = [NSArray array];
    }
    [textField fetchedElements:results forInputString:inputString];
    }
}
//Deepak Carpenter ::
- (void)predictiveTextFieldValueChanged:(PredictiveTextField*)textField {
    // If change is made due to user interaction, update model
    
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {
        
         if (textField == _placeOfVisitTextField_pl) {
            
            Insurance *selectedPov=_placeOfVisitTextField_pl.selectedElement;
            NSString *userPov=_placeOfVisitTextField_pl.text;
            [self shouldEditEntity:_selectedPatient withControlBlock:^(BOOL accepted){
                //    [self shouldEditEntity:_selectedPatient withControlBlock:^(BOOL accepted) {
                if(accepted){
                    _selectedPatient.insurance=selectedPov;
                    _selectedPatient.userInsurance=userPov;
                    NSLog(@"placeOfVisit %@ ,, userPlaceOfVisit %@",selectedPov,userPov);
                    
                }
                [self updateUI];
            }];
         }
    }
    
else
{   if (textField == _placeOfVisitTextField) {
        
        PlaceOfVisit *selectedPov=_placeOfVisitTextField.selectedElement;
        NSString *userPov=_placeOfVisitTextField.text;
        [self shouldEditEntity:_selectedPatient withControlBlock:^(BOOL accepted){
            //    [self shouldEditEntity:_selectedPatient withControlBlock:^(BOOL accepted) {
            if(accepted){
                _selectedPatient.placeOfVisit=selectedPov;
                NSLog(@"Identifier PlaceofVIsit %@",_selectedPatient.placeOfVisit.identifier);
                _selectedPatient.userPlaceOfVisit=userPov;
                NSLog(@"placeOfVisit %@ ,, userPlaceOfVisit %@",selectedPov,userPov);
                
            }
            
            [self updateUI];
            
        }];
    }else if(_presSickTextField.text.length>0){
       // SickFund *selectedSF=_presSickTextField.selectedElement;
        NSString *userSf=_presSickTextField.text;
        [self shouldEditEntity:_selectedPatient withControlBlock:^(BOOL accepted){
            //    [self shouldEditEntity:_selectedPatient withControlBlock:^(BOOL accepted) {
            if(accepted){
//                _selectedPatient.sickFund.name=[NSString stringWithFormat:@"%@",selectedSF];
                _selectedPatient.userSickFund=userSf;
                NSLog(@"sickFund  ,, usersickfund %@",userSf);
                
            }
            
            [self updateUI];
        }];
    }
    else if(_presSickTextField.selectedElement==nil) {
        _selectedPatient.sickFund=nil;
        _selectedPatient.userSickFund=nil;
        [self updateUI];
    }

}}


- (void)hideIncompleteAlertForDiagnosisTabBarItem:(DiagnosticTabbarItem*)tabBarItem {
    BOOL isIncompleteDiagnosis = NO;
    for (Diagnosis *diagnosis in [self.selectedPatient sortedDiagnosis]) {
        if (diagnosis.index.integerValue == tabBarItem.index) {
            isIncompleteDiagnosis = ![diagnosis isComplete];
            BOOL hide = (!tabBarItem.selected && isIncompleteDiagnosis);
            [tabBarItem hideIncompleteDiagnosisAlert:!hide];
            break;
        }
    }
}

-(void)createTabbarButtons {
    self.tabbarItemArray = [[NSMutableArray alloc] init];
    for (int i=0; i < kMaxTabbarButtonCount; i++) {
        //Botón pestaña TAB
        DiagnosticTabbarItem* tabbarItem = [DiagnosticTabbarItem loadFromDefaultNIB];
        tabbarItem.index = i;
        tabbarItem.frame =  CGRectMake((kMinTabbarButtonWidth*i), 0, kMinTabbarButtonWidth, 40);
        tabbarItem.title = [NSString stringWithFormat:NSLocalizedString(@"DIAGNOSE_TABBAR_ITEM", nil),i+1];
        tabbarItem.delegate = self;
        // Select first item
        tabbarItem.selected = i == _selectedTabbarItem;
        // Hide button if is greater than diagnose count
        tabbarItem.hidden=!(i<self.diagnoseCount);
        // Add insets to avoid label to invade close button area
        [_tabBarView addSubview:tabbarItem];
        [_tabbarItemArray addObject:tabbarItem];
        if (!tabbarItem.hidden) {
            // Insert add button beside last visible tab item
            [_addDiagnoseButton setFrame:CGRectMake((kMinTabbarButtonWidth*(i+1)), 0, _addDiagnoseButton.frame.size.width, _addDiagnoseButton.frame.size.height)];
        }
    }
}

-(void) updateTabbarButtons {
    // Check button width
    int buttonWidth=kMinTabbarButtonWidth;
    int availableWidth=_tabBarView.frame.size.width;
    // Check if 'add' button will be visible, and include its size to calculate available
    if (self.diagnoseCount < kMaxTabbarButtonCount) {
        availableWidth-=_addDiagnoseButton.frame.size.width;
    }
    // Obtain button width
    buttonWidth = self.diagnoseCount > 0 ? MIN(buttonWidth, availableWidth / self.diagnoseCount) : buttonWidth;
    
    for (DiagnosticTabbarItem* tabbarItem in _tabbarItemArray) {
        tabbarItem.hidden = tabbarItem.index >= self.diagnoseCount;
        tabbarItem.selected = tabbarItem.index == self.selectedTabbarItem;
        if (!tabbarItem.hidden) {
            [self hideIncompleteAlertForDiagnosisTabBarItem:tabbarItem];
        }
        // Set new size
        CGRect frame=tabbarItem.frame;
        [tabbarItem setFrame:CGRectMake((buttonWidth * tabbarItem.index), 0, buttonWidth, frame.size.height)];
    }
    
    _addDiagnoseButton.hidden = self.diagnoseCount == kMaxTabbarButtonCount;
    // Set 'add' button at right of last visible tabbar item
    if (self.diagnoseCount < kMaxTabbarButtonCount) {
        [_addDiagnoseButton setFrame:CGRectMake((buttonWidth * self.diagnoseCount), 0, _addDiagnoseButton.frame.size.width, _addDiagnoseButton.frame.size.height)]; 
    }
}

- (Treatment*)createEmptyTreatmentInDiagnosis:(Diagnosis*)diagnosis {
    return [[ChangeManager manager] addTreatmentToDiagnosis:diagnosis];
}

- (Diagnosis*)createEmptyDiagnosisInPatient:(Patient*)patient {
    Diagnosis* diagnosis = [[ChangeManager manager] addDiagnosisToPatient:patient];
    [self createEmptyTreatmentInDiagnosis:diagnosis];
    return diagnosis;
}


//Ravi_Bukka: Initial Patient Creation
- (Patient*)createEmptyPatient {
    Patient* newPatient = [[ChangeManager manager] addPatientToDoctorInfo:self.doctor.doctorInfo];
    [self createEmptyDiagnosisInPatient:newPatient];
    
    // Select the new patient
    self.selectedPatient = newPatient;
    
    // Save the new empty patient (but don't commit changes to change manager)
    [[EntityFactory sharedEntityFactory] saveChanges];
    
    return newPatient;
}

- (void)showConfirmationToDeleteDiagnosis:(Diagnosis*)diagnosis originView:(UIView*)originView {
    [self shouldEditEntity:nil withControlBlock:^(BOOL accepted) {
        if (accepted) {
            ConfirmationMessageController* confirmationController = [[[ConfirmationMessageController alloc] initWithMessage:@"¿Está seguro de que quiere eliminar los datos del diagnóstico?" eventBlock:^(BOOL accepted) {
                if (accepted) {
                    // Warning confirmed, delete the entity
                    if (diagnosis != nil) {
                        // Delete the entity
                        [[ChangeManager manager] deleteDiagnosis:diagnosis];
                        
                        // If it was the last diagnosis, create an empty one
                        if (self.diagnoseCount == 0) {
                            [self createEmptyDiagnosisInPatient:_selectedPatient];
                        }
                        
                        // Reload the data as may be changed
                        if (self.selectedTabbarItem >= self.diagnoseCount) {
                            self.selectedTabbarItem = self.diagnoseCount - 1;
                        }
                        else {
                            self.selectedTabbarItem = _selectedTabbarItem;
                        }
                    }
                    else {
                        DLog(@"Tried to delete a nil diagnostic");
                    }
                }
                [self.confirmationPopover dismissPopoverAnimated:YES];
                self.confirmationPopover = nil;
            }] autorelease];
            
            self.confirmationPopover = [[[UIPopoverController alloc] initWithContentViewController:confirmationController] autorelease];
            _confirmationPopover.contentViewController = confirmationController;
            _confirmationPopover.popoverContentSize = confirmationController.view.frame.size;
            [_confirmationPopover presentPopoverFromRect:originView.frame inView:originView.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }];
}

- (void)showConfirmationToDeletePatient:(Patient*)patient originView:(UIView*)originView {
    ConfirmationMessageController* confirmationController = [[[ConfirmationMessageController alloc] initWithMessage:@"¿Está seguro de que quiere eliminar los datos del paciente?" eventBlock:^(BOOL accepted) {
        if (accepted) {
            // Warning confirmed, delete the entity
            if (patient != nil) {
                NSArray* sortedPatients = [self sortedPatients];
                
                NSInteger index = [sortedPatients indexOfObject:patient];
                
                // Deleted currently creating patient, reset creating flag
                if (index == sortedPatients.count - 1) {
                    self.creating = NO;
                }
                
                // Delete the entity and confirm changes immediatly
                [[ChangeManager manager] deletePatient:patient];
                [[ChangeManager manager] confirmChangesToEntity:patient];
                
                // If it was the last diagnosis, create an empty one
                if (self.doctor.doctorInfo.patients.count == 0) {
                    [self createEmptyPatient];
                }
                else {
                    sortedPatients = [self sortedPatients];
                    if (index < sortedPatients.count) {
                        self.selectedPatient = [sortedPatients objectAtIndex:index];
                    }
                    else {
                        self.selectedPatient = [sortedPatients lastObject];
                    }
                }
                if ([self isEditing]) {
                    // End updating patient
                    [self endUpdateEvent];
                } else if ([self isCreating]) {
                    // End creating patient
                    [self endCreateEvent];
                }
            }
            else {
                DLog(@"Tried to delete a nil patient");
            }
        }
        [self.confirmationPopover dismissPopoverAnimated:YES];
        self.confirmationPopover = nil;
    }] autorelease];
    
    self.confirmationPopover = [[[UIPopoverController alloc] initWithContentViewController:confirmationController] autorelease];
    _confirmationPopover.contentViewController = confirmationController;
    _confirmationPopover.popoverContentSize = confirmationController.view.frame.size;
    [_confirmationPopover presentPopoverFromRect:originView.frame inView:originView.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(NSInteger)diagnoseCount {
    return _selectedPatient.diagnostic.count;
}

- (void)selectNextPatient {
    
    NSArray* sortedPatients = [self sortedPatients];
    Patient* lastPatient = [sortedPatients lastObject];
    
    if (lastPatient != _selectedPatient) {
        NSInteger currentIndex = [sortedPatients indexOfObject:_selectedPatient];
        if (currentIndex < sortedPatients.count - 1) {
            Patient* nextPatient = [sortedPatients objectAtIndex:currentIndex + 1];
            self.selectedPatient = nextPatient;
        }
        else {
            DLog(@"Inconsistency error");
        }
        
    }
    else {
        [self createEmptyPatient];
        
        // Reset creating flag as we already finished editing the new patient
        self.creating = NO;
    }
    [self hideKeyboard];
    [self updateUI];
}

- (BOOL)checkDuplicatedTreatments {
    
    int numOfCoincidences = 0;
    // Iterate the diagnosis
    
    NSMutableSet* checkedDiagnosis = [NSMutableSet setWithSet:_selectedPatient.diagnostic];
    for (Diagnosis* diagnosis1 in _selectedPatient.diagnostic) {
        [checkedDiagnosis removeObject:diagnosis1];
        for (Diagnosis* diagnosis2 in checkedDiagnosis) {
            if (diagnosis1.index != diagnosis2.index) {
                // Check the diagnosis names
                if (([diagnosis1.userDiagnosis isEqualToString:diagnosis2.userDiagnosis] || [[diagnosis1.diagnosisType name] isEqualToString:[diagnosis2.diagnosisType name]]) && ([diagnosis1.treatments count] == [diagnosis2.treatments count])) {
                    // Iterate the treatments
                    if ([diagnosis1.needsTreatment intValue] == 0 && [diagnosis2.needsTreatment intValue] == 0) {
                        return YES;
                    }
                    for (Treatment *treatment1 in diagnosis1.treatments) {
                        for (Treatment *treatment2 in diagnosis2.treatments) {
                            if (([treatment1.userMedicament isEqualToString:treatment2.userMedicament] || [[treatment1.medicament name] isEqualToString:[treatment2.medicament name]]) && ([treatment1.userPresentation isEqualToString:treatment2.userPresentation] || [[treatment1.presentation name] isEqualToString:[treatment2.presentation name]])) {
                                numOfCoincidences++;
                                if (numOfCoincidences == [diagnosis1.treatments count]) {
                                    return YES;
                                }
                                break;
                            }
                        }
                    }
                    numOfCoincidences = 0;
                }
            }
        }
    }
    return NO;
}

- (void)performIntegrityCheck:(Patient*)patient {
    for (Diagnosis* diagnosis in _selectedPatient.diagnostic) {
        if (!diagnosis.needsTreatment.boolValue) {
            // Remove treatments in diagnosis that don't need them
            for (Treatment* treatment in diagnosis.treatments) {
                [[ChangeManager manager] deleteTreatment:treatment];
            }
        }
        else {
            for (Treatment* treatment in diagnosis.treatments) {
                Dosage* dosage = treatment.dosage;
                if (dosage.onDemand.boolValue) {
                    dosage.frequency = nil;
                    dosage.units = [NSNumber numberWithInt:0];
                    dosage.longDurationTreatment = [NSNumber numberWithBool:NO];
                    dosage.duration = [NSNumber numberWithInt:0];
                    dosage.durationType = nil;
                }
                else if (dosage.longDurationTreatment.boolValue) {
                    dosage.duration = [NSNumber numberWithInt:0];
                    dosage.durationType = nil;
                }
            }
        }
    }
}

- (void)saveChangesToPatient {
    // Integrity check, remove incomplete diagnosis and treatments
    [self performIntegrityCheck:_selectedPatient];
    
    [[ChangeManager manager] confirmChangesToEntity:_selectedPatient];
}

#pragma mark - View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
    //Deepak_Carpenter CR#11 : Segment Control appearance for iOS 8 +
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{
                                                              NSForegroundColorAttributeName : [UIColor whiteColor]
                                                              } forState:UIControlStateNormal];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{
                                                              NSForegroundColorAttributeName : [UIColor whiteColor]
                                                              } forState:UIControlStateSelected];
    
    if([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"]) {
//        _dividerLabel.hidden = YES;
//        _prescriptionLabel.hidden = YES;
//        _sickFundLabel.hidden = YES;
//        
   
        
    }
    //Deepak Carpenter
    pres=[[NSString alloc]init];
    sick=[[NSString alloc]init];
    presSick=[[NSString alloc]init];
    // Set description key for PlaceOfVisit textfield
    _placeOfVisitTextField.descriptionField = kDescriptionKey;
    _placeOfVisitTextField.allowUserValues = YES;
    //_presSickTextField textfield
    _presSickTextField.descriptionField = kDescriptionKey;
    _presSickTextField.allowUserValues = YES;
    _presSickTextField.clearButtonMode=NO;
    [_presSickTextField setAlignment:NSTextAlignmentCenter];
    [_presSickTextField setBackgroundColor:[UIColor clearColor]];
    
    //////////////

    //Kanchan Nair
    // Set description key for PlaceOfVisit textfield
    _placeOfVisitTextField_pl.descriptionField = kDescriptionKey;
    _placeOfVisitTextField_pl.allowUserValues = YES;

//Ravi_Bukka
    
    [_prevPatientButton setTitle:NSLocalizedString(@"PATIENT_PREVIOUS", nil) forState:UIControlStateNormal];
    [_prevPatientButton setTitle:NSLocalizedString(@"PATIENT_PREVIOUS", nil) forState:UIControlStateHighlighted];
    
    _patientLabel.text = NSLocalizedString(@"PATIENT", nil);
    _ageLabel.text = NSLocalizedString(@"PATIENT_AGE:", nil);
    _typeOfVisitLabel.text = NSLocalizedString(@"PATIENT_TYPE_OF_VISIT:", nil);
    _dateOfVisit.text = NSLocalizedString(@"PATIENT_DATE_OF_VISIT:", nil);
    _genderLabel.text = NSLocalizedString(@"DOCTOR_SEX:", nil);
    

    [_finishPatientButton setTitle:NSLocalizedString(@"PATIENT_PATIENT_FINISH", nil) forState:UIControlStateNormal];
    [_finishPatientButton setTitle:NSLocalizedString(@"PATIENT_PATIENT_FINISH", nil) forState:UIControlStateHighlighted];

// Ravi_Bukka: Commented to fix the diagnosis new tab error.
//    [_addDiagnoseButton setTitle:NSLocalizedString(@"PATIENT_PREVIOUS", nil) forState:UIControlStateNormal];
//    [_addDiagnoseButton setTitle:NSLocalizedString(@"PATIENT_PREVIOUS", nil) forState:UIControlStateHighlighted];
    
    
    
    [self.ageTypeSelector setTitle:NSLocalizedString(@"PATIENT_YRS_OLD", nil) forSegmentAtIndex:0];
    [self.ageTypeSelector setTitle:NSLocalizedString(@"PATIENT_MONTHS", nil) forSegmentAtIndex:1];
    
   //  BotonAyuda.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    self.ageTypeSelector.apportionsSegmentWidthsByContent = YES;
    
    [self.genderSelector setTitle:NSLocalizedString(@"PATIENT_MALE", nil) forSegmentAtIndex:0];
    [self.genderSelector setTitle:NSLocalizedString(@"PATIENT_WOMAN", nil) forSegmentAtIndex:1];
    
    
    
    [self.consultationSelector setTitle:NSLocalizedString(@"PATIENT_PUBLIC", nil) forSegmentAtIndex:0];
    [self.consultationSelector setTitle:NSLocalizedString(@"PATIENT_PVT", nil) forSegmentAtIndex:1];
    
//Ravi_Bukka: Added for greece and polish localization
    
    if([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"])
    {
        _placeOfVisit.text = NSLocalizedString(@"DIAGNOSTIC_PLACE_OF_VISIT", nil);
        _smokerLabel.text = NSLocalizedString(@"DIAGNOSTIC_SMOKER_LABEL", nil);
        
        [self.smokerTypeSelector setTitle:NSLocalizedString(@"DIAGNOSTIC_SMOKER", nil) forSegmentAtIndex:0];
        [self.smokerTypeSelector setTitle:NSLocalizedString(@"DIAGNOSTIC_NONSMOKER", nil) forSegmentAtIndex:1];
        [self.smokerTypeSelector setTitle:NSLocalizedString(@"DIAGNOSTIC_EXSMOKER", nil) forSegmentAtIndex:2];
    }
    

    
    _diagnosticViewController.changesControlProtocol = self;
    
    // Add diagnosis view
    [_diagnosisHolder fillWithSubview:_diagnosticViewController.view];
    // Set tags and values for segmented controls
    // Age type
    _ageTypeSelector.tagIdentifierField = kIdentifierKey;
    _ageTypeSelector.values = [[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([AgeType class]) filteringByName:@"" sortResults:NO];
    NSLog(@"age type values %@",[_ageTypeSelector.values objectAtIndex:0]);
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"])
    _ageTypeSelector.tagArray = [NSArray arrayWithObjects:kAgeTypeYears_pl, kAgeTypeMonths_pl, nil];
    else
    _ageTypeSelector.tagArray = [NSArray arrayWithObjects:kAgeTypeYears, kAgeTypeMonths, nil];
    // Gender
    _genderSelector.tagIdentifierField = kIdentifierKey;
    _genderSelector.values = [[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([Gender class]) filteringByName:@"" sortResults:NO];
    if ([[[[NSBundle mainBundle]preferredLocalizations]objectAtIndex:0] isEqualToString:@"pl"])
    {
    _genderSelector.tagArray = [NSArray arrayWithObjects:kGenderMale_pl, kGenderFemale_pl, nil];
    }
    else
        _genderSelector.tagArray = [NSArray arrayWithObjects:kGenderMale_el, kGenderFemale_el, nil];

    
    

//Deepak Carpenter : For Smoker SegmentedControl
        if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"]) {
            _smokerTypeSelector.tagIdentifierField = kIdentifierKey;
            _smokerTypeSelector.values = [[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([Smoker class]) filteringByName:@"" sortResults:NO];
            _smokerTypeSelector.tagArray = [NSArray arrayWithObjects:kSmokerIndex, kNonSmokerIndex, kExSmokerIndex, nil];
        }
        
        
  // Consultation
    _consultationSelector.tagIdentifierField = kIdentifierKey;
    _consultationSelector.values = [[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([ConsultType class]) filteringByName:@"" sortResults:NO];
    if ([[[[NSBundle mainBundle]preferredLocalizations]objectAtIndex:0] isEqualToString:@"pl"])
    {
    _consultationSelector.tagArray = [NSArray arrayWithObjects:kConsultPublic_pl, kConsultPrivate_pl, nil];
    }
    else if ([[[[NSBundle mainBundle]preferredLocalizations]objectAtIndex:0] isEqualToString:@"el"]){
    _consultationSelector.tagArray = [NSArray arrayWithObjects:kConsultPublic_el, kConsultPrivate_el, nil];
    
    }else
         _consultationSelector.tagArray = [NSArray arrayWithObjects:kConsultPublic, kConsultPrivate, nil];

    // Trigger update UI
    [self createTabbarButtons];
    self.selectedTabbarItem = _selectedTabbarItem;
    [self updateUI];
   
}

- (void)viewWillDisappear:(BOOL)animated {
    // Discard changes
    if (_choiceController != nil) {
        [_modalController dismissModalController:_choiceController];
        _choiceController = nil;
    }
    if (self.confirmationPopover != nil) {
        [self.confirmationPopover dismissPopoverAnimated:NO];
    }
    if (self.ageInfoPopover != nil) {
        [self.ageInfoPopover dismissPopoverAnimated:NO];
    }
    [[EntityFactory sharedEntityFactory] discardChanges];
    [self endUpdateEvent];
    [super viewWillDisappear:animated];
    
}

- (void)viewDidUnload
{
    [self setPrevPatientButton:nil];
    [self setPatientNumberLabel:nil];
    [self setAgeGroup:nil];
    [self setAgeTextField:nil];
    [self setAgeTypeSelector:nil];
    [self setDateButton:nil];
    [self setGenderGroup:nil];
    [self setGenderSelector:nil];
    [self setConsultationGroup:nil];
    [self setConsultationSelector:nil];
    [self setDateGroup:nil];
    [self setDiagnosisHolder:nil];
    [self setTabBarView:nil];
    [self setAddDiagnoseButton:nil];
    [self setDateTextField:nil];
    [self setFinishPatientButton:nil];
    [self setAgePopoverView:nil];
    //Kanchan Nair
    [self PlaceOFVisitgroup_pl:nil];

    [super viewDidUnload];
}

- (void)dealloc {
    [_selectedPatient release];
    [_doctor release];
    [_prevPatientButton release];
    [_patientNumberLabel release];
    [_ageGroup release];
    [_ageTextField release];
    [_ageTypeSelector release];
    [_dateButton release];
    [_genderGroup release];
    [_genderSelector release];
    [_consultationGroup release];
    [_consultationSelector release];
    [_dateGroup release];
    [_diagnosticViewController release];
    [_diagnosisHolder release];
    [_tabBarView release];
    [_addDiagnoseButton release];
    [_tabbarItemArray release];
    [_dateTextField release];
    [_finishPatientButton release];
    [_confirmationPopover release];
    [_choiceController release];
    [_agePopoverView release];
    [_ageInfoPopover release];
    [_smokerTypeSelector release];
//Kanchan Nair
    [_placeOfVisitTextField_pl release];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}

- (void)setDoctor:(Doctor *)doctor {
    [doctor retain];
    [_doctor release];
    _doctor = doctor;
    
    NSSet* patients = doctor.doctorInfo.patients;
    if (doctor != nil && patients.count == 0) {
        // Create patient if it doesn't exist
        [self createEmptyPatient];
    }
    
    NSArray* sortedPatients = [self sortedPatients];
    // Select last patient
    Patient* lastPatient = [sortedPatients lastObject];
    self.selectedPatient = lastPatient;
    if (![self.selectedPatient isComplete]) {
        self.creating = YES;
    }
}

- (void)setSelectedPatient:(Patient *)aSelectedPatient {
    if (_selectedPatient != aSelectedPatient) {
        [aSelectedPatient retain];
        [_selectedPatient release];
        _selectedPatient = aSelectedPatient;
    }
    
    // Reset the 'editing' flag
    self.editing = NO;
    
    NSString* indexText = nil;
    if (_selectedPatient != nil) {
        NSInteger index = [[self sortedPatients] indexOfObject:_selectedPatient];
        if (index != NSNotFound) {
            indexText = [NSString stringWithFormat:@"%d", index + 1];
        }
    }
    self.patientNumberLabel.text = indexText;
    
    self.selectedTabbarItem = 0;
    
    [self updateUI];
}

-(void)setSelectedTabbarItem:(NSInteger)selectedTabbarItem {
    _selectedTabbarItem = selectedTabbarItem;
    [self updateTabbarButtons];
    
    NSArray* diagnostics = [self sortedSelectedDiagnostics];
    Diagnosis* selectedDiagnosis = nil;
    if (selectedTabbarItem < diagnostics.count) {
        selectedDiagnosis = [diagnostics objectAtIndex:selectedTabbarItem];
    }
    
    if (selectedDiagnosis != nil && selectedDiagnosis.treatments.count == 0) {
        // No treatments for this diagnosis, create a new one
        [self createEmptyTreatmentInDiagnosis:selectedDiagnosis];
    }
    
    _diagnosticViewController.diagnosis = selectedDiagnosis;
    [self updateUI];
}

#pragma mark UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == _ageTextField) {
        [self shouldEditEntity:_selectedPatient withControlBlock:^(BOOL accepted) {
            if (accepted) {
                NSString *inputText = [textField.text stringByReplacingCharactersInRange:range withString:string];
                if (inputText.length <= kAgeTextFieldMaxLength) {
                    // Clear all non numeric characters
                    NSString *pureNumbers = [[inputText componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
                    NSNumber* age = [NSNumber numberWithInt:pureNumbers.integerValue];
                    // If age is negative or zero, set nil
                    if (age.intValue <= 0) {
                        age = nil;
                    }
                    _selectedPatient.age.value = age;
                    [self updateUI];
                }
            } else {
                [textField resignFirstResponder];
            }
        }];
        return NO;
    } else {
        return YES;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField == _placeOfVisitTextField) {
    if([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"]) {
    _placeOfVisitViewController = [[PlaceOfVisitTableViewController alloc] initWithNibName:@"PlaceOfVisitTableViewController" bundle:nil];
 //Ravi_Bukka: Delegate method set
    _placeOfVisitViewController.delegate = self;

    CGSize size = CGSizeMake(300.0,300.0);
        
    if(_myPopoverController == nil){   //make sure popover isn't displayed more than once in the view
        _myPopoverController = [[UIPopoverController alloc]initWithContentViewController:_placeOfVisitViewController];
    }
        _myPopoverController.popoverContentSize = size;
        _myPopoverController.delegate = self;
        
    [_myPopoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];

    
    }
}
}

#pragma mark Component events
- (IBAction)prevPatientClicked:(id)sender {
    NSArray* sortedPatients = [self sortedPatients];
   
    if (sortedPatients.count > 1) {
        Patient* firstPatient = [sortedPatients objectAtIndex:0];
        
        if (firstPatient != _selectedPatient) {
            NSInteger currentIndex = [sortedPatients indexOfObject:_selectedPatient];
            Patient* previousPatient = [sortedPatients objectAtIndex:currentIndex - 1];
            self.selectedPatient = previousPatient;
             
        }
        else {
            DLog(@"Already first patient");
        }
    }
    else {
        DLog(@"Nothing to do, no more patients");
    }
    

    [self hideKeyboard];
     [self updateUI];
    
    
}



#pragma mark SpecialitySelectionViewControllerDelegate
//- (void)specialitySelectionController:(SpecialitySelectionViewController*)popup didSelectSpeciality:(id)speciality {
//    self.selectorType = kSelectorTypeNone;
//    [self assignSelectedTherapyChoice];
//    
//    _treatment.recommendationSpecialist = speciality;
//    [self.popoverController dismissPopoverAnimated:YES];
//    self.popoverController = nil;
//    [self updateUI];
//}

#pragma mark  PlaceOfVisitViewControllerDelegate
//
//    
//- (void)placeOfVisitDismissPopOverController:(PlaceOfVisitTableViewController*)controller didSelectTableView:(UITableView *)tableView rowAtIndexPath:(NSIndexPath *)indexPath didSelectString:(NSString*)selectedCellString {
//
//
//    _placeOfVisitTextField.text = selectedCellString;
//    
//   // _placeOfVisitTextField.font = [UIFont systemFontOfSize:14.0];
//    
//    [_myPopoverController dismissPopoverAnimated:YES];
//}
#pragma mark  PrescriptionAndSickFundVViewControllerDelegate

- (void)PrescriptionAndSickFundSelectionController:(PrescriptionAndSickFundVViewController*)popup didSelectPrescription:(NSString*)prescriptionName {
    
//    _prescriptionLabel.hidden = NO;
//    _sickFundLabel.hidden = NO;
//    _dividerLabel.hidden = NO;
//    _prescriptionLabel.text = prescriptionName;
    pres=prescriptionName;
    
        
    if ([prescriptionName isEqualToString:@"Απλή"]) {
        _selectedPatient.recurranceValue=[NSNumber numberWithInt:1];
    }else if ([prescriptionName isEqualToString:@"Δίμηνη"]){
    _selectedPatient.recurranceValue=[NSNumber numberWithInt:2];
    }else if ([prescriptionName isEqualToString:@"Πάνω από 3Μ"]){
        _selectedPatient.recurranceValue=[NSNumber numberWithInt:4];
    }else if ([prescriptionName isEqualToString:@"Τρίμηνη"]){
        _selectedPatient.recurranceValue=[NSNumber numberWithInt:3];
    }
    NSLog(@"%@",_selectedPatient.recurranceValue);

    //
  //  NSLog(@"SickFund Value %@",_selectedPatient.recurranceValue);
    [self.specialityPopoverController dismissPopoverAnimated:YES];
}
- (void)PrescriptionAndSickFundSelectionController:(PrescriptionAndSickFundVViewController*)popup didSelectionSickFund:(NSString*)sickfundName didSelectionOtherSickFund:(NSString *)OtherSickfundName{
//    
//    1|ΙΚΑ
//    2|ΟΓΑ
//    3|ΟΑΕΕ (ΤΕΒΕ)
//    4|ΟΠΑΔ (ΔΗΜΟΣΙΟ)
//    5|ΆΛΛΟ
    if ([sickfundName isEqualToString:@"ΙΚΑ"]) {
        _selectedPatient.sickFundValue=[NSNumber numberWithInt:1];
        _selectedPatient.otherSickFundValue=nil;
    }else if ([sickfundName isEqualToString:@"ΟΓΑ"]) {
        _selectedPatient.sickFundValue=[NSNumber numberWithInt:5];
         _selectedPatient.otherSickFundValue=nil;
    }else if ([sickfundName isEqualToString:@"ΟΑΕΕ (ΤΕΒΕ)"]) {
        _selectedPatient.sickFundValue=[NSNumber numberWithInt:3];
         _selectedPatient.otherSickFundValue=nil;
    }else if ([sickfundName isEqualToString:@"ΟΠΑΔ (ΔΗΜΟΣΙΟ)"]) {
        _selectedPatient.sickFundValue=[NSNumber numberWithInt:6];
         _selectedPatient.otherSickFundValue=nil;
    }else
    {_selectedPatient.sickFundValue=[NSNumber numberWithInt:8];
     _selectedPatient.otherSickFundValue=OtherSickfundName;
    }
  NSLog(@"%@",_selectedPatient.sickFundValue);
    sick=sickfundName;
   // _selectedPatient.sickFundValue=sick;
    presSick=[NSString stringWithFormat:@"%@ / %@",pres,sick];
    NSLog(@"Pres & SICK VALUE %@",presSick);
    [_presSickTextField setText:presSick];
    [self performSelector:@selector(predictiveTextFieldValueChanged:)];
   //_consultationSelector.selectedSegmentIndex=-1;
    [self.specialityPopoverController dismissPopoverAnimated:YES];
    
}


- (IBAction)ageSelectorChanged:(id)sender {
    if (!self.isRefreshing) {
        [self shouldEditEntity:_selectedPatient withControlBlock:^(BOOL accepted) {
            if (accepted) {
                // Set value to entity
                _selectedPatient.age.ageType = _ageTypeSelector.selectedValue;
                NSLog(@"age type select --%@",_ageTypeSelector.selectedValue);
            }
            [self hideKeyboard];
            [self updateUI];
        }];
    }
}

- (IBAction)genderSelectorChanged:(id)sender {
    if (!self.isRefreshing) {
        [self shouldEditEntity:_selectedPatient withControlBlock:^(BOOL accepted) {
            if (accepted) {
                // Set value to entity
                    
                _selectedPatient.gender = _genderSelector.selectedValue;
            }
            [self hideKeyboard];
            [self updateUI];
        }];
    } 
}

//Ravi_Bukka: Added for greec and polish localization
- (IBAction)smokerValueChanged:(id)sender {
    //Deepak Carpenter
    if (!self.isRefreshing) {
        [self shouldEditEntity:_selectedPatient withControlBlock:^(BOOL accepted) {
            if (accepted) {
                // Set value to entity
                
                _selectedPatient.smoker = _smokerTypeSelector.selectedValue;
                NSLog(@"selected index name %@", _smokerTypeSelector.selectedValue);
                   NSLog(@"selected index %@", _selectedPatient.smoker.identifier);
            }
            [self hideKeyboard];
            [self updateUI];
        }];
    }

}
- (IBAction)consultationSelectorChanged:(id)sender {
    
    if (!self.isRefreshing) {
        [self shouldEditEntity:_selectedPatient withControlBlock:^(BOOL accepted) {
            if (accepted) {
                // Set value to entity
                _selectedPatient.consultType = _consultationSelector.selectedValue;
                NSLog(@"Value :: %@ ",_consultationSelector.selectedValue);
                NSLog(@"Tag value is %@",_selectedPatient.consultType.identifier);
                NSLog(@"selected index name %@", _selectedPatient.consultType.name);
               
//Ravi_Bukka: added for new feature with respect to greek localization
                if (([_selectedPatient.consultType.name isEqualToString:@"Δημόσιο Ταμείο"]||[_selectedPatient.consultType.name isEqualToString:@"First Visit"])&& [[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"]) {
                    
                    [self showPrescriptionAndSickFundController];
                }
                else if (([_selectedPatient.consultType.name isEqualToString:@"Ιδιωτική δαπάνη"] || [_selectedPatient.consultType.name isEqualToString:@"Repeat Visit"]) && [[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"])
                {
                    //Deepak Carpenter
                    _presSickTextField.selectedElement=nil;
                    [self performSelector:@selector(predictiveTextFieldValueChanged:)];
                }
               
            }
            [self hideKeyboard];
            [self updateUI];
        }];
    }
 
    
}

//Ravi_Bukka: added for new feature with respect to greek localization
- (void) showPrescriptionAndSickFundController {
    
    UIViewController* viewController = nil;
        if (_prescAndSickFund == nil) {
            _prescAndSickFund = [[PrescriptionAndSickFundVViewController alloc] init];
            _prescAndSickFund.delegate = self;
        }

        viewController = _prescAndSickFund;

//    [self.specialityPopoverController dismissPopoverAnimated:NO];
//    self.specialityPopoverController = [[[UIPopoverController alloc] initWithContentViewController:viewController] autorelease];
//    self.specialityPopoverController.popoverContentSize = viewController.view.frame.size;
//    self.specialityPopoverController.delegate = self;
//    
//    // Notify delegate before showing the popover
//    [_delegate treatmentViewController:self willPresentPopover:self.popoverController];
//    
//    // Queue the popover for the next cycle of the Main thread to let the scroll update
//    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//        [self.popoverController presentPopoverFromRect:sourceView.frame inView:sourceView.superview permittedArrowDirections:UIPopoverArrowDirectionDown | UIPopoverArrowDirectionUp animated:YES];
//    }];

    UIView* view;
    
    view = _consultationSelector;
//    CGSize size = CGSizeMake(_prescAndSickFund.view.frame.size.width,_prescAndSickFund.view.frame.size.height);
    CGSize size = CGSizeMake(502.0,340.0);
    
    
    self.specialityPopoverController = [[[UIPopoverController alloc] initWithContentViewController:_prescAndSickFund] autorelease];
    _specialityPopoverController.popoverContentSize = size;
    _specialityPopoverController.delegate = self;
    
    [_delegate PatientViewControllerWillPresentPopUp:self];
    [_specialityPopoverController presentPopoverFromRect:view.frame inView:view.superview permittedArrowDirections: UIPopoverArrowDirectionLeft animated:YES];
    
}

//#pragma mark - UIPopoverControllerDelegate
//- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
//    if (popoverController == _specialityPopoverController) {
//        [_delegate doctorInfoViewControllerWillDismissPopUp:self];
//        
//        
//    }
//}

- (BOOL)showDuplicatedTreatmentsMultipleChoice {
    // Check if there aren't duplicated treatments
    BOOL duplicatedTreatments = [self checkDuplicatedTreatments];
    if (duplicatedTreatments) {
        // Message indicating that there are duplicated diagnosis or treatments
        // Ask the user to confirm editing before continue
        self.choiceController = [[MultipleChoiceViewController new] autorelease];
        _choiceController.message = NSLocalizedString(@"PATIENT_DUPLICATED_DIAGNOSIS_MESSAGE", @"");
        _choiceController.acceptButtonTitle = nil;
        _choiceController.cancelButtonTitle = NSLocalizedString(@"PATIENT_DUPLICATED_DIAGNOSIS_CANCEL", @"");
        _choiceController.otherButtonTitle = nil;
        _choiceController.eventBlock = ^(MultipleChoiceButton button){
            // Dismiss and release the controller
            [_modalController dismissModalController:self.choiceController];
            self.choiceController = nil;
        };
        
        // Present modal controller
        [_modalController presentModalController:self.choiceController];
    }
    return duplicatedTreatments;
}

//Ravi_Bukka: CR#2: Patient Difference and Local Notification
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

//Ravi_Bukka: webservice2 need to be integrated at loginButtonClicked
- (IBAction)nextPatientClicked:(id)sender {
    if ([self isEditing]) {
        // Ask the user to confirm editing before continue
        self.choiceController = [[MultipleChoiceViewController new] autorelease];
        _choiceController.message = NSLocalizedString(@"PATIENT_EDIT_CONFIRMATION_MESSAGE", @"");
        _choiceController.acceptButtonTitle = NSLocalizedString(@"PATIENT_EDIT_CONFIRMATION_ACCEPT", @"");
        _choiceController.cancelButtonTitle = NSLocalizedString(@"PATIENT_EDIT_CONFIRMATION_CANCEL", @"");
        _choiceController.otherButtonTitle = NSLocalizedString(@"PATIENT_EDIT_CONFIRMATION_DISCARD", @"");
        _choiceController.eventBlock = ^(MultipleChoiceButton button){
            // Dismiss and release the controller
            [_modalController dismissModalController:self.choiceController];
            self.choiceController = nil;
            
            switch (button) {
                case kMultipleChoiceAccept: {
                    BOOL duplicatedTreatments = [self showDuplicatedTreatmentsMultipleChoice];
                    if (!duplicatedTreatments) {
                        // Save changes in current patient and show next patient
                        [self saveChangesToPatient];
                        [self endUpdateEvent];
                        [self selectNextPatient];
                    }
                    break;
                }
                case kMultipleChoiceCancel: {
                    // Cancel request
                    break;
                }
                case kMultipleChoiceOther: {
                    // Revert changes
                    [[EntityFactory sharedEntityFactory] discardChanges];
                    self.selectedPatient = _selectedPatient;
                    [self endUpdateEvent];
                    break;
                }
            }
        };
        
        // Present modal controller
        [_modalController presentModalController:self.choiceController];
    }
    else if ([self isCreating]) {
        if (![self showDuplicatedTreatmentsMultipleChoice]) {
            // Save changes in current patient and show next patient
            Patient* lastPatient = [[self sortedPatients] lastObject];
            [self saveChangesToPatient];
            if (self.selectedPatient == lastPatient) {
                [self endCreateEvent];
            }
            [self selectNextPatient];
        }
    }
    else {
        [self selectNextPatient];
    }
    
//Ravi_Bukka: CR#2: Patient Difference and Local Notification
    if ([self connected]) {
        
        //    if ([(AppDelegate *)[[UIApplication sharedApplication] delegate] HayConexionInternet]) {
        
        NSLog(@"When internet is connected");
        
    }
    else {
        
        NSLog(@"When internet is not connected");
        NSInteger patientCount = [[NSUserDefaults standardUserDefaults]integerForKey:@"patientCountFromServer"];
        NSLog(@"Patient count is %d",patientCount);
        [(AppDelegate *)[[UIApplication sharedApplication] delegate] patientDifference:patientCount];
    }
}

- (IBAction)addDiagnoseButtonClicked:(id)sender {
    [self shouldEditEntity:nil withControlBlock:^(BOOL accepted) {
        if (accepted) {
            if (_selectedPatient.diagnostic.count < kMaxTabbarButtonCount) {
                // Create an empty diagnostic
                [self createEmptyDiagnosisInPatient:_selectedPatient];
                
                self.selectedTabbarItem = _selectedPatient.diagnostic.count - 1;
                
                [self hideKeyboard];
                [self updateUI];
            }
            else {
                DLog(@"Cannot create more diagnoses to the same patient");
            }
        }
    }];
}

- (void)selectVisitDay:(NSDate*)date {
    [self.confirmationPopover dismissPopoverAnimated:YES];
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDateComponents *dayComponent = [theCalendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    dayComponent.hour = 12;
    date = [theCalendar dateFromComponents:dayComponent];
    _selectedPatient.visitDate = date;
    [self updateDateField];
    [self updateUI];
}

- (IBAction)visitDateClicked:(id)sender {
    [self shouldEditEntity:_selectedPatient withControlBlock:^(BOOL accepted) {
        if (accepted) {
            // Configure the minimum and maximum date
            NSDate* firstCollaborationDate = self.doctor.doctorInfo.firstCollaborationDate;
            NSDate* secondCollaborationDate = self.doctor.doctorInfo.secondCollaborationDate;
            NSCalendar *cal = [NSCalendar currentCalendar];
            
            // First Date
            NSDateComponents *components = [cal components:( NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:firstCollaborationDate];
            [components setDay:[components day] - ([components weekday] - 2)]; // Monday = 2
            NSDate* firstDay = [cal dateFromComponents:components];
            [components setDay:[components day] + 6];
            
            //Utpal: Based on Collaboration Count setting the date button.
            
            NSString* colabCount = [[NSUserDefaults standardUserDefaults] valueForKey:@"CollaborationCount"];
            // Second Date
            if ([colabCount isEqualToString:@"1"])
            {
                
                NSLog(@"do nothing");
                
            }  else if ([colabCount isEqualToString:@"2"])
            {
                components = [cal components:( NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:secondCollaborationDate];
                [components setDay:[components day] - ([components weekday] - 2)]; // Monday = 2
            }
            
            // Second Date
           // components = [cal components:( NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:secondCollaborationDate];
           // [components setDay:[components day] - ([components weekday] - 2)]; // Monday = 2
            NSDate* secondDateFirstDay = [cal dateFromComponents:components];

            UIView* originView = _dateButton;
            
            // Create the wrapper controller
            WeekdaySelectionPopoverContentViewController* controller = [[WeekdaySelectionPopoverContentViewController alloc] init];
            controller.delegate = self;
            controller.firstDate = firstDay;
            controller.secondDate = secondDateFirstDay;
            
            // Create the popover controller
            self.confirmationPopover = [[[UIPopoverController alloc] initWithContentViewController:controller] autorelease];
            
            //Utpal : resizing the popover based on Collaboration Count
                        
            if ([colabCount isEqualToString:@"1"]) {
                CGSize halfSize = CGSizeMake(controller.view.frame.size.width, controller.view.frame.size.height/2);
                _confirmationPopover.popoverContentSize = halfSize;
            }
            else if ([colabCount isEqualToString:@"2"]){
                _confirmationPopover.popoverContentSize = controller.view.frame.size;
            }

           // _confirmationPopover.popoverContentSize = controller.view.frame.size;
            [_confirmationPopover presentPopoverFromRect:originView.frame inView:originView.superview permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            [self hideKeyboard];
            [controller release];
        }
    }];
}


- (IBAction)deletePatientButtonClicked:(id)sender {
    Patient* patient = self.selectedPatient;
    [self hideKeyboard];
    [self showConfirmationToDeletePatient:patient originView:sender];
    
}

- (void)startCreateEvent {
    if (![self isCollectingStats]) {
        [[StatsReporter sharedStatsReporter] startEvent:kStatsEventTypeCreatePatient];
        DLog(@"STATS - Comienza evento crear paciente");
        self.collectingStats = YES;
    }
}

- (void)endCreateEvent {
    if ([self isCollectingStats]) {
        [[StatsReporter sharedStatsReporter] endEvent:kStatsEventTypeCreatePatient withPatientId:self.selectedPatient.index.integerValue];
        DLog(@"STATS - Termina evento crear paciente");
        self.collectingStats = NO;
    }
}

- (void)startUpdateEvent {
    if (![self isCollectingStats]) {
        [[StatsReporter sharedStatsReporter] startEvent:kStatsEventTypeUpdatePatient];
        DLog(@"STATS - Comienza evento actualizar paciente");
        self.collectingStats = YES;
    }
}

- (void)endUpdateEvent {
    if ([self isCollectingStats]) {
        [[StatsReporter sharedStatsReporter] endEvent:kStatsEventTypeUpdatePatient withPatientId:self.selectedPatient.index.integerValue];
        DLog(@"STATS - Termina evento actualizar paciente");
        self.collectingStats = NO;
    }
}

#pragma mark - DiagnosticTabbarItemDelegate

-(void) tabbarItemClicked:(DiagnosticTabbarItem*)tabbarItem {
    [self hideKeyboard];
    self.selectedTabbarItem = tabbarItem.index;
    
}
-(void) tabbarItemCloseButtonClicked:(DiagnosticTabbarItem*)tabbarItem {
    Diagnosis* diagnosis = nil;
    if (tabbarItem.index < self.diagnoseCount) {
        diagnosis = [self.sortedSelectedDiagnostics objectAtIndex:tabbarItem.index];
    }
    [self hideKeyboard];
    [self showConfirmationToDeleteDiagnosis:diagnosis originView:tabbarItem];
}

#pragma mark - DiagnosticViewControllerDelegate
- (void)diagnosticViewController:(DiagnosticViewController*)controller isReady:(BOOL)ready {
    // If we are creating a new register, save every change ASAP to avoid data loss
    if ([self isCreating]) {
        [[EntityFactory sharedEntityFactory] saveChanges];
    }
    
    self.diagnosisReady = ready;
    [self updateUI];
}

#pragma mark - ModalControllerProtocol
- (void)presentModalController:(UIViewController*)controller {
    [_modalController presentModalController:controller];
}

- (void)dismissModalController:(UIViewController*)controller {
    [_modalController dismissModalController:controller];
}

#pragma mark - ChangesControlProtocol
- (void)shouldEditEntity:(Synchronizable*)entity withControlBlock:(ChangesControlBlock)controlBlock {
    Patient* lastPatient = [[self sortedPatients] lastObject];
    if (![self isCreating] && ![self isEditing] && self.selectedPatient == lastPatient && ![self.selectedPatient isComplete]) {
        // If we're trying to edit the last register and it's not complete, we're creating a new register
        // Allow editing and mark it as such
        self.creating = YES;

        [[ChangeManager manager] updateEntity:entity];
        controlBlock(YES);
        [self startCreateEvent];
        DLog(@"Creating");
    }
    else if ([self isCreating] && self.selectedPatient == lastPatient) {
        // If creating a new register, allow editing only that register (that will always be the last one)
        [[ChangeManager manager] updateEntity:entity];
        controlBlock(YES);
        [self startCreateEvent];
        DLog(@"Editing new");
    }
    else if ([self isEditing]) {
        // If already set editing flag, return YES
        [[ChangeManager manager] updateEntity:entity];
        [self startUpdateEvent];
        controlBlock(YES);
        DLog(@"Editing");
    }
    else if (self.selectedPatient != lastPatient && ![lastPatient isEmpty] && ![lastPatient isComplete]) {
        if (self.choiceController == nil) {
            self.choiceController = [[MultipleChoiceViewController new] autorelease];
            _choiceController.message = [NSString stringWithFormat:NSLocalizedString(@"PATIENT_FISNISH_PATIENT", @""), [[self sortedPatients] count]];
            _choiceController.acceptButtonTitle = nil;
            _choiceController.cancelButtonTitle = NSLocalizedString(@"PATIENT_FINISH_CANCEL", @"");
            _choiceController.otherButtonTitle = nil;
            _choiceController.eventBlock = ^(MultipleChoiceButton button){
                // Dismiss and release the controller
                [_modalController dismissModalController:self.choiceController];
                self.choiceController = nil;
            };
            
            [_modalController presentModalController:self.choiceController];
            
            controlBlock(NO);
            DLog(@"Already editing new");
        }
    }
    else if (self.choiceController == nil) {
        // Ask the user to confirm editing before continue
        self.choiceController = [[MultipleChoiceViewController new] autorelease];
        _choiceController.message = NSLocalizedString(@"PATIENT_ALLOW_EDITING_MESSAGE", @"");
        _choiceController.acceptButtonTitle = NSLocalizedString(@"PATIENT_ALLOW_EDITING_ACCEPT", @"");
        _choiceController.cancelButtonTitle = NSLocalizedString(@"PATIENT_ALLOW_EDITING_CANCEL", @"");
        _choiceController.otherButtonTitle = nil;
        _choiceController.eventBlock = ^(MultipleChoiceButton button){
            // Save the response
            self.editing = button == kMultipleChoiceAccept;
            
            if (self.editing) {
                [[ChangeManager manager] updateEntity:entity];
                [self startUpdateEvent];
            }
            
            // Dismiss and release the controller
            [_modalController dismissModalController:self.choiceController];
            self.choiceController = nil;
            
            // Notify the user choice to the caller
            controlBlock([self isEditing]);
            
            // Refresh UI as prev and next button may have changed
            [self updateUI];
        };
        
        [_modalController presentModalController:self.choiceController];
        DLog(@"Asking user");
    }
    else {
        // Any other case, don't allow editing
        controlBlock(NO);
        DLog(@"Other cases");
    }
}

- (void)shouldEditEntityLosingChanges:(Synchronizable*)entity withControlBlock:(ChangesControlBlock)controlBlock {
    
    // Ask the user to confirm editing before continue
    self.choiceController = [[MultipleChoiceViewController new] autorelease];

    if (!_diagnosticViewController.noTreatmentCheckBox.selected) {
        _choiceController.message = NSLocalizedString(@"PATIENT_ON_DEMAND_EDITING_MESSAGE", @"");
        _choiceController.acceptButtonTitle = NSLocalizedString(@"PATIENT_ON_DEMAND_EDITING_ACCEPT", @"");
        _choiceController.cancelButtonTitle = NSLocalizedString(@"PATIENT_ON_DEMAND_EDITING_CANCEL", @"");
    } else {
        _choiceController.message = NSLocalizedString(@"PATIENT_NO_TREATMENT_EDITING_MESSAGE", @"");
        _choiceController.acceptButtonTitle = NSLocalizedString(@"PATIENT_NO_TREATMENT_EDITING_ACCEPT", @"");
        _choiceController.cancelButtonTitle = NSLocalizedString(@"PATIENT_NO_TREATMENT_EDITING_CANCEL", @"");
    }
    
    _choiceController.otherButtonTitle = nil;
    _choiceController.eventBlock = ^(MultipleChoiceButton button){
        
        // Dismiss and release the controller
        [_modalController dismissModalController:self.choiceController];
        self.choiceController = nil;
        
        // Notify the user choice to the caller
        controlBlock(button == kMultipleChoiceAccept);
    };
    
    [_modalController presentModalController:self.choiceController];
    DLog(@"Asking user");
}

#pragma mark - Public methods
-(void) requestCloseWithBlock:(PatientClosedBlock)onCloseBlock {
    [self hideKeyboard];
    [self showAgePopover:NO];
    // If patient view can close without prompting, do it
    if (self.editing) {
        NSInteger max;
        // If years is selected (or no selection is done yet)
        if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {
        if (_selectedPatient.age.ageType == nil || [_selectedPatient.age.ageType.identifier isEqualToString:kAgeTypeYears_pl]) {
            max = kAgeMaxYears;
        } else {
            max = kAgeMaxMonths;
        }
        }
        else {
            if (_selectedPatient.age.ageType == nil || [_selectedPatient.age.ageType.identifier isEqualToString:kAgeTypeYears]) {
                max = kAgeMaxYears;
            } else {
                max = kAgeMaxMonths;
            }
            
        }
        BOOL canSave = [_selectedPatient isComplete] && _selectedPatient.age.value.intValue <= max;
        // Ask the user to confirm editing before continue
        self.choiceController = [[MultipleChoiceViewController new] autorelease];
        _choiceController.message = NSLocalizedString(@"CHANGES_PENDING_DIALOG_MESSAGE", @"");
        _choiceController.acceptButtonTitle = canSave ? NSLocalizedString(@"CHANGES_PENDING_DIALOG_SAVE_BUTTON", @"") : nil;
        _choiceController.cancelButtonTitle = NSLocalizedString(@"CHANGES_PENDING_DIALOG_CANCEL_BUTTON", @"");
        _choiceController.otherButtonTitle =  NSLocalizedString(@"CHANGES_PENDING_DIALOG_DISCARD_BUTTON", @"");
        _choiceController.eventBlock = ^(MultipleChoiceButton button){
            
            // Dismiss and release the controller
            [_modalController dismissModalController:self.choiceController];
            self.choiceController = nil;
            
            BOOL response;
            switch (button) {
                case kMultipleChoiceAccept: {
                    DLog(@"Saving changes");
                    BOOL duplicatedTreatments = [self showDuplicatedTreatmentsMultipleChoice];
                    if (!duplicatedTreatments) {
                        // Save changes
                        [self saveChangesToPatient];
                        // Close view
                        response = YES;
                    } else {
                        response = NO;
                    }
                    [self endUpdateEvent];
                    break;
                }
                case kMultipleChoiceOther: {
                    DLog(@"Discarding changes");
                    // Discard changes
                    [[EntityFactory sharedEntityFactory] discardChanges];
                    // Close view
                    response = YES;
                    [self endUpdateEvent];
                    break;
                }
                case kMultipleChoiceCancel: {
                    // Cancel, don't close view
                    response = NO;
                    break;
                }
                    
            }
            onCloseBlock(response);
        };
        [_modalController presentModalController:self.choiceController];
    }
    // If we're creating a new patient, confirm changes 
    else if (self.creating) {
    //Deepak_Carpenter : Added to restrict "WS CAlls" in menu & exit buttons
//        Patient* newPatient = [[_doctor.doctorInfo sortedPatients] lastObject];
//        NSLog(@" Age -- %@\n  visitDate -- %@", newPatient.age, newPatient.visitDate );
//        [[ChangeManager manager] confirmChangesToEntity:newPatient];
//        [self endCreateEvent];
        onCloseBlock(YES);
    }
    else {
        onCloseBlock(YES);
    }
    
}
//Ravi_Bukka: Commented to remove insertpatient issue on tapping on Menu/Exit when app goes in background
- (void)willGoBackground {
    // If we're creating a new patient, confirm changes 
    if (self.creating) {
//        Patient* newPatient = [[_doctor.doctorInfo sortedPatients] lastObject];
//        [[ChangeManager manager] confirmChangesToEntity:newPatient];
    }
}

- (BOOL)requestDismiss {
    BOOL diagnosticDismiss = [_diagnosticViewController requestDismiss];
    if (diagnosticDismiss) {
        [self.view endEditing:NO];
    }
    return diagnosticDismiss;
}

 


@end
