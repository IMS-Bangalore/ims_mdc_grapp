//
//  HelpViewController.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 17/10/12.
//
//

#import "HelpViewController.h"
#import "HelpPopoverContentViewController.h"
#import "BSCHWhiteView.h"
#import "StatsReporter.h"
#import <QuartzCore/QuartzCore.h>

static const CGFloat kMidScreen = 384;
static const CGFloat kDraggingDistance = 736;

@interface HelpViewController ()

@property (nonatomic, retain) BSCHWhiteView *whiteView;
@property (nonatomic, retain) NSArray *helpTexts;
@property CGFloat pointsTranslated;
@property BOOL mustShowHelp;
@property CGFloat alpha;
@property (nonatomic, retain) UIPopoverController *helpTextPopover;

@end

@implementation HelpViewController

@synthesize showHelpButton;
@synthesize helpImageView;
@synthesize offsetPlaceholderView;
@synthesize invisibleShowHelpButton;
@synthesize actionButtons;
@synthesize containerView;
@synthesize helpButtonArrowImageView;
@synthesize helpMessageView = _helpMessageView;
@synthesize helpMessageLabel = _helpMessageLabel;
@synthesize shadowHelpMessageView = _shadowHelpMessageView;
@synthesize helpMessageTitleLabel = _helpMessageTitleLabel;
@synthesize helpTexts;
@synthesize pointsTranslated = _pointsTranslated;
@synthesize mustShowHelp = _mustShowHelp;
@synthesize whiteView = _whiteView;
@synthesize delegate = _delegate;
@synthesize alpha = _alpha;
@synthesize helpTextPopover = _helpTextPopover;

#pragma mark Initializers

- (id)init {
    self = [super init];
    if (self) {
        // Custom initialization
        _mustShowHelp = YES;
    }
    return self;
}

#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _alpha = 0;
    _helpMessageView.layer.cornerRadius = 10;
    _shadowHelpMessageView.alpha = 0;
    _helpMessageLabel.text = NSLocalizedString(@"HELP_MESSAGE_CONTENT", @"");
    _helpMessageTitleLabel.text = NSLocalizedString(@"HELP_MESSAGE_TITLE", @"");
    _helpAyudaButtonTitleLabel.text = NSLocalizedString(@"HELP_BUTTON_TITLE", nil);
    
    
    if (!_mustShowHelp) {
        _shadowHelpMessageView.hidden = YES;
    }
    
    _pointsTranslated = 0;
    
    for (UIButton* button in self.actionButtons) {
        [button addTarget:self action:@selector(didClickActionButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"helpTexts" ofType:@"plist"];
    helpTexts = [[NSArray alloc] initWithContentsOfFile:path];
    
    UIPanGestureRecognizer *gesture = [[[UIPanGestureRecognizer alloc]
                                        initWithTarget:self
                                        action:@selector(buttonDragged:)] autorelease];
	[self.showHelpButton addGestureRecognizer:gesture];
}

- (void)viewWillDisappear:(BOOL)animated {
    if (self.helpTextPopover != nil) {
        [self.helpTextPopover dismissPopoverAnimated:NO];
    }
    [self animateWhiteViewWithAlpha:0];
    [super viewWillDisappear:animated];
}

- (void)viewDidUnload {
    [self setShowHelpButton:nil];
    [self setHelpImageView:nil];
    [self setOffsetPlaceholderView:nil];
    [self setInvisibleShowHelpButton:nil];
    [self setActionButtons:nil];
    [self setContainerView:nil];
    [self setHelpButtonArrowImageView:nil];
    [self setHelpMessageView:nil];
    [self setHelpMessageLabel:nil];
    [self setShadowHelpMessageView:nil];
    [self setHelpMessageTitleLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)dealloc {
    [showHelpButton release];
    [helpImageView release];
    [offsetPlaceholderView release];
    [invisibleShowHelpButton release];
    [actionButtons release];
    [containerView release];
    [helpTexts release];
    [helpButtonArrowImageView release];
    [_helpMessageView release];
    [_helpMessageLabel release];
    [_shadowHelpMessageView release];
    [_helpMessageTitleLabel release];
    [super dealloc];
}

#pragma mark Public methods

- (IBAction)showHelp:(id)sender {
    if (![_delegate helpViewControllerShouldShow]) {
        return;
    }
    
    _pointsTranslated = 0;
    [self.view setUserInteractionEnabled:YES];
    BOOL beginsAtBottom = self.view.frame.origin.y == 0;
    if (!beginsAtBottom) {
        [[StatsReporter sharedStatsReporter] startEvent:kStatsEventTypeEnterHelp];
        [[StatsReporter sharedStatsReporter] endEvent:kStatsEventTypeEnterHelp];
    }
    CGFloat endPosition = beginsAtBottom ? 0-self.view.frame.size.height + [self viewOffset] : 0;
    _alpha = beginsAtBottom ? 0 : 1;
    
    self.invisibleShowHelpButton.hidden = beginsAtBottom;
    [self rotateArrowUp:beginsAtBottom];
    [self animateHelpViewToX:0 andY:endPosition andAlpha:_alpha];
}

- (IBAction)dismissHelpMessage:(id)sender {
    _mustShowHelp = NO;
    [self animateShadowHelpMessageViewWithAlpha:0];
}

- (CGFloat)viewOffset {
    return offsetPlaceholderView.frame.size.height;
}

#pragma mark Private methods

- (void)animateShadowHelpMessageViewWithAlpha:(CGFloat)alpha {
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:0
                     animations:^{
                         _shadowHelpMessageView.alpha = alpha;
                     }
                     completion:^(BOOL finished){
                     }];
}

- (void)animateHelpViewToX:(CGFloat)x andY:(CGFloat)y andAlpha:(CGFloat)alpha {
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationTransitionCurlDown | UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.view.frame = CGRectMake(x, y, self.view.frame.size.width, self.view.frame.size.height);
                         if (_mustShowHelp) {
                             _shadowHelpMessageView.alpha = alpha;
                         }
                         [_delegate helpViewControllerDidChangePosition:alpha animated:YES];
                     }
                     completion:^(BOOL finished){
                     }];
}

- (CGFloat)calculateAlphaWhenDragging:(CGFloat)increment {
    CGFloat parts = kDraggingDistance / 100;
    CGFloat numberOfPartsIncreased = increment / parts;
    CGFloat alpha = 0.01 * numberOfPartsIncreased;
    return alpha;
}

- (void)buttonDragged:(UIPanGestureRecognizer *)gesture {
    if (![_delegate helpViewControllerShouldShow]) {
        return;
    }
    
    CGPoint position = [gesture translationInView:self.showHelpButton];
    position = [self.showHelpButton convertPoint:position fromView:self.view.superview];
    
    BOOL aboveMidScreen = fabsf(position.y) > kMidScreen;
    
    switch (gesture.state) {
        case UIGestureRecognizerStateChanged: {
            
            [self.view setUserInteractionEnabled:NO];
            UIButton *button = (UIButton *)gesture.view;
            CGPoint translation = [gesture translationInView:button];
            
            self.view.center = CGPointMake(self.view.center.x, self.view.center.y + translation.y);
            [gesture setTranslation:CGPointZero inView:self.view];
            
            CGFloat increasedAlpha = [self calculateAlphaWhenDragging:translation.y];
            _alpha = _alpha + increasedAlpha;
            
            if (_mustShowHelp) {
                _shadowHelpMessageView.alpha = _alpha;
                [_shadowHelpMessageView setNeedsDisplay];
            }
            
            [_delegate helpViewControllerDidChangePosition:_alpha animated:NO];
            
            [self rotateArrowUp:!aboveMidScreen];
            
            _pointsTranslated = _pointsTranslated + translation.y;
            break;
        }
        case UIGestureRecognizerStateBegan: {
            break;
        }
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded: {
            [self.view setUserInteractionEnabled:YES];
            [self rotateArrowUp:!aboveMidScreen];
            _alpha = aboveMidScreen ? 1 : 0;
            CGFloat endPosition = aboveMidScreen ? 0 : 0 - self.view.frame.size.height + [self viewOffset];
            if (endPosition == 0) {
                [[StatsReporter sharedStatsReporter] startEvent:kStatsEventTypeEnterHelp];
                [[StatsReporter sharedStatsReporter] endEvent:kStatsEventTypeEnterHelp];
                self.offsetPlaceholderView.userInteractionEnabled = NO;
            } else {
                self.offsetPlaceholderView.userInteractionEnabled = YES;
            }
            [self animateHelpViewToX:0 andY:endPosition andAlpha:_alpha];
            _pointsTranslated = 0;
            
            break;
        }
        default:
            break;
    }
}

- (void)animateWhiteViewWithAlpha:(NSInteger)alpha {
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:0
                     animations:^{
                         _whiteView.alpha = alpha;
                     }
                     completion:^(BOOL finished){
                     }];
}

- (void)rotateArrowUp:(BOOL)rotateUp {
    CGAffineTransform transform;
    if (rotateUp) {
        transform = CGAffineTransformMakeRotation(0);
    } else {
        transform = CGAffineTransformMakeRotation(M_PI);
    }
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:0
                     animations:^{
                         self.helpButtonArrowImageView.transform = transform;
                     }
                     completion:^(BOOL finished){
                     }];
}

- (IBAction)didClickActionButton:(UIButton*)sender {
    NSInteger buttonIdentifier = sender.tag;

    CGRect whiteViewFrame = self.containerView.frame;
    whiteViewFrame.size.height = whiteViewFrame.size.height - 15;
    whiteViewFrame.size.width = whiteViewFrame.size.width - 10;
    if (_whiteView == nil) {
        _whiteView = [[BSCHWhiteView alloc] initWithFrame:whiteViewFrame andButtonFrame:sender.frame];
        _whiteView.alpha = 0;
        [self.view addSubview:_whiteView];
    } else {
        [_whiteView setButtonFrame:sender.frame];
    }
    
    [self animateWhiteViewWithAlpha:1];

    NSDictionary *texts = [helpTexts objectAtIndex:buttonIdentifier - 1];
    NSString *title = [texts objectForKey:@"title"];
    NSString *message = [texts objectForKey:@"message"];

    HelpPopoverContentViewController *popoverContent = [[[HelpPopoverContentViewController alloc] initWithTitle:title andMessage:message] autorelease];
    
    [self.helpTextPopover dismissPopoverAnimated:NO];
    self.helpTextPopover = [[[UIPopoverController alloc] initWithContentViewController:popoverContent] autorelease];
    self.helpTextPopover.delegate = self;
    
    [_helpTextPopover presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown | UIPopoverArrowDirectionUp animated:YES];
    [_helpTextPopover setPopoverContentSize:[popoverContent sizeInPopover] animated:NO];
}
    
#pragma mark - UIPopoverControllerDelegate
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    [self animateWhiteViewWithAlpha:0];
    return YES;
}

@end
