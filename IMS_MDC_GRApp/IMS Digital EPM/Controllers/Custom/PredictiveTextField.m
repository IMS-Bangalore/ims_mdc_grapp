//
//  PredictiveTextField.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 07/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "PredictiveTextField.h"
#import "UIView+FillWithSubview.h"
#import "ComponentGroup.h"

@interface PredictiveTextField()

@property (nonatomic, assign, getter=isLoading) BOOL loading;
@property (nonatomic, retain) NSArray *displayElements;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) UIPopoverController *popoverController;
@property (nonatomic, retain) UIImageView *backgroundImage;
@property (nonatomic, retain) UITextField *textField;

-(void) refreshTableView;
-(void) refreshDisplayElements;
-(void) refreshWithStaticElements;
-(void) refreshFetchingDelegateElements;
-(NSString*) descriptionOfElement:(id)element;
-(void) notifyChangeToDelegate;

@end

@implementation PredictiveTextField

const NSInteger kUnlimitedMaxTextLength = -1;
static NSString* const kDefaultDescriptionField = @"description";
static const NSInteger kDefaultMaxDisplayRows = 5;
static const NSInteger kDefaultRowHeight = 35;
static const CGFloat kAnimationLength = 0.3;
static const NSInteger kTextFieldPadding = 10;
static const NSInteger kTextFieldHeight = 30;
static const NSInteger kDefaultMaxTextLength = 50;
static NSString* const kCellIdenfifier = @"elementCell";

@synthesize allowUserValues = _allowUserValues;
@synthesize staticElements = _staticElements;
@synthesize descriptionField = _descriptionField;
@synthesize selectedElement = _selectedElement;
@synthesize delegate = _delegate;
@synthesize maxDisplayRows = _maxDisplayRows;
@synthesize rowHeight = _rowHeight;
@synthesize textColor = _textColor;
@synthesize disabledTextColor = _disabledTextColor;
@synthesize textFont = _textFont;
@synthesize noResultsTextFont = _noResultsTextFont;
@synthesize externalTableView = _externalTableView;
@synthesize popoverArrowDirections = _popoverArrowDirections;
@synthesize maxTextLength = _maxTextLength;
@synthesize textFontSize=_textFontSize;

// Private properties
@synthesize loading = _loading;
@synthesize displayElements = _displayElements;
@synthesize tableView = _tableView;
@synthesize popoverController = _popoverController;
@synthesize backgroundImage = _backgroundImage;
@synthesize textField = _textField;

- (void)dealloc {
    [_backgroundImage release];
    [_textField release];
    _delegate = nil;
    [_tableView release];
    [_displayElements release];
    [_popoverController release];
    [_textColor release];
    [_disabledTextColor release];
    [_textFont release];
    [_noResultsTextFont release];
    [_backgroundImage release];
    [_externalTableView release];
    [super dealloc];
}

#pragma mark Initialize methods

- (void) initialize {
    _maxTextLength = kDefaultMaxTextLength;
    self.backgroundColor = [UIColor clearColor];
    // Add background image
    UIImage* background = [UIImage imageNamed:@"text_field.png"];
    UIImageView* imageView = [[UIImageView alloc] initWithImage:background];
    imageView.backgroundColor = [UIColor clearColor];
    [self fillWithSubview:imageView];
    [imageView release];
    // Add textfield
    _textField = [[UITextField alloc] init];
    // Add padding 
    CGRect frame = CGRectMake(kTextFieldPadding, 
                              (self.frame.size.height - kTextFieldHeight) / 2, 
                              self.frame.size.width - (2 * kTextFieldPadding), 
                              kTextFieldHeight);
    _textField.frame = frame;
    _textField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _textField.autocorrectionType = UITextAutocorrectionTypeNo;
    _textField.delegate = self;
    _textField.text = @"";
    _textField.clearButtonMode = UITextFieldViewModeUnlessEditing;
    [self addSubview:_textField];
    [self bringSubviewToFront:_textField];
    // Set defaults
    _descriptionField = kDefaultDescriptionField;
   
    //Deepak Carpenter: Added for Polish
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {
    _maxDisplayRows = 7;
    }else _maxDisplayRows=kDefaultMaxDisplayRows;
    
    _rowHeight = kDefaultRowHeight;
    self.textFont = [UIFont systemFontOfSize:14];
    self.noResultsTextFont = [UIFont italicSystemFontOfSize:14];
    self.textColor = [UIColor blackColor];
    self.disabledTextColor = [UIColor grayColor];
    // Create popover
    _tableView = [[UITableView alloc] init];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    UIViewController* popOverViewController = [[UIViewController alloc] init];
    popOverViewController.view = _tableView;
    _popoverController = [[UIPopoverController alloc]initWithContentViewController:popOverViewController];
    _popoverController.delegate = self;
    [popOverViewController release];
    _popoverArrowDirections = UIPopoverArrowDirectionUp;

}

- (id)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

-(void)awakeFromNib {
    _textField.font = self.textFont;
    _textField.textColor = self.textColor;
    self.enabled = self.enabled;
    if (_externalTableView != nil) {
        [self refreshDisplayElements];
    }
}
//Utpal: CR#4 Added for demographic University- START

-(id)defaultUniversitySelectedValue {
    id value = nil;
    
    // Check selected segment index
    
    NSNumber* universityInput = [[NSUserDefaults standardUserDefaults] valueForKey:@"University"];
    // Check selected segment index
    NSInteger selectedUniversity = [universityInput integerValue];
    //selectedUniversity--;
    
    // Check selection is not out of tag array bounds
    //   if (selectedUniversity < _tagArrayUniversity.count) {
    // Obtain selected tag
    //    NSInteger selectedTag = [[_tagArrayUniversity objectAtIndex:selectedUniversity] intValue];
    // Find value with identifier corresponding with selected tag
    for (id valueInArray in _valuesUniversity) {
        if ([[valueInArray valueForKey:self.tagIdentifierFieldUniversity] intValue] == selectedUniversity) {
            value = valueInArray;
            break;
        }
    }
    
    //    else {
    //        DLog(@"Tag array is not long enough to support all provinces");
    //    }
    
    return value;
}

//END

//Utpal: CR#4 Added for demographic Provinces- START
-(id)defaultSelectedValue {
    id value = nil;
    
    // Check selected segment index
    
    NSNumber* provinceInput = [[NSUserDefaults standardUserDefaults] valueForKey:@"Province"];
    // Check selected segment index
    NSInteger selectedProvince = [provinceInput integerValue];
    selectedProvince--;
    
    // Check selection is not out of tag array bounds
    if (selectedProvince < _tagArray.count) {
        // Obtain selected tag
        NSInteger selectedTag = [[_tagArray objectAtIndex:selectedProvince] intValue];
        // Find value with identifier corresponding with selected tag
        for (id valueInArray in _values) {
            if ([[valueInArray valueForKey:self.tagIdentifierField] intValue] == selectedTag) {
                value = valueInArray;
                break;
            }
        }
    } else {
        DLog(@"Tag array is not long enough to support all provinces");
    }
    
    return value;
}

//END


- (void)setSelectedElement:(id)aSelectedElement {
    if (_selectedElement != aSelectedElement) {
        [aSelectedElement retain];
        [_selectedElement release];
        _selectedElement = aSelectedElement;
    }
    // Update textfield
//Ravi_Bukka: Added for the localization
    _textField.text = NSLocalizedString([self descriptionOfElement:aSelectedElement], nil);
}

- (void)setExternalTableView:(UITableView *)anExternalTableView {
    if (_externalTableView != anExternalTableView) {
        [anExternalTableView retain];
        [_externalTableView release];
        _externalTableView = anExternalTableView;
        _externalTableView.dataSource = self;
        _externalTableView.delegate = self;
    }
}

- (void)setStaticElements:(NSArray *)aStaticElements {
    if (_staticElements != aStaticElements) {
        [aStaticElements retain];
        [_staticElements release];
        _staticElements = aStaticElements;
        [self refreshDisplayElements];
    }
}
//Deepak Carpenter
-(void)setAlignment:(NSTextAlignment)alignment{
    [_textField setTextAlignment:alignment];
}
-(void)setTextFontSize:(UIFont *)textFontSize{
    [_textField setFont:textFontSize];
}

-(void)setKeyBoardType:(UIKeyboardType)keyBoardType{
    [_textField setKeyboardType:keyBoardType];
}


- (void)setDescriptionField:(NSString *)aDescriptionField {
    if (_descriptionField != aDescriptionField) {
        [aDescriptionField retain];
        [_descriptionField release];
        _descriptionField = aDescriptionField;
        [self refreshDisplayElements];
    }
}

- (void)checkCoincidenceWithDisplayElements {
    int i = 0;
    for (id element in _displayElements) {
        if (([self.text compare:[self descriptionOfElement:[_displayElements objectAtIndex:i]] options:(NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch)]) == NSOrderedSame) {
            self.selectedElement = [_displayElements objectAtIndex:i];
            break;
        }
        i++;
    }
}

- (void)notifyChangeToDelegate {
    if (self.selectedElement == nil) {
        // Assign element if does not allow user values
        if (!self.allowUserValues) {
            // Clear selected element if no concordances or empty input
            if (_displayElements.count == 0 || self.text.length == 0) {
                self.selectedElement = nil;
            } else {
                self.selectedElement = [_displayElements objectAtIndex:0];
            }
        } else {
            [self checkCoincidenceWithDisplayElements];
        }
    }
    
    // Notify to delegate
    if ([self.delegate respondsToSelector:@selector(predictiveTextFieldValueChanged:)]) {
        [self.delegate predictiveTextFieldValueChanged:self];
    }
}

#pragma mark UITextField wrapper

- (NSString *)text {
    return _textField.text;
}

- (void)setText:(NSString *)aText {
    _textField.text = aText != nil ? aText : @"";
}

- (NSString *)placeholder {
    return _textField.placeholder; 
}

- (void)setPlaceholder:(NSString *)aPlaceholder {
    _textField.placeholder = aPlaceholder;
}

-(BOOL)becomeFirstResponder {
    if (self.enabled) {
        [_textField becomeFirstResponder];
        [self textFieldDidBeginEditing:_textField];
    }
    return self.enabled;
}

-(BOOL)resignFirstResponder {
    BOOL resigned = [_textField resignFirstResponder];
    if (resigned) {
        // Remove popover
        if (![self isLoading]) {
            [_popoverController dismissPopoverAnimated:YES];
        }
    }
    return resigned;
}

- (void)setEnabled:(BOOL)flag {
    _textField.enabled = flag;
    _textField.textColor = flag ? self.textColor : self.disabledTextColor;
    self.userInteractionEnabled = flag;
    _externalTableView.userInteractionEnabled = flag;
    _externalTableView.alpha = flag ? 1 : kComponentDisabledAlpha;
    self.alpha = flag ? 1 : kComponentDisabledAlpha;
}

- (BOOL)isEnabled {
    return _textField.enabled;
}

- (UITextFieldViewMode)clearButtonMode {
    return _textField.clearButtonMode;
}
- (void)setClearButtonMode:(UITextFieldViewMode)aClearButtonMode {
    _textField.clearButtonMode = aClearButtonMode;
}

- (NSArray*)obtainsDifferentWordsInAName:(NSString*)word {
    NSArray *wordsAndEmptyStrings = [word componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSArray *words = [wordsAndEmptyStrings filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
    return words;
}

- (void)fetchedElements:(NSArray*)elements forInputString:(NSString*)inputString {
    _loading = NO;
    // Check that response input string is equals to current user input
    if ([inputString isEqualToString:self.text]) {
        

        self.displayElements = elements;
        
        [self refreshTableView];
    }
}

- (void)refreshTableView {
    if (_externalTableView == nil) {
        int rows = MIN(self.maxDisplayRows, [self tableView:_tableView numberOfRowsInSection:0]);
        int newHeight = rows * self.rowHeight;
        CGSize newSize = CGSizeMake(self.frame.size.width, newHeight);
        [_popoverController setPopoverContentSize:newSize animated:YES];
        [_tableView reloadData];
        [_tableView flashScrollIndicators];
    } else {
        [_externalTableView reloadData];
        [_externalTableView flashScrollIndicators];
    }
}

- (void)refreshDisplayElements {
    _loading = YES;
    [self refreshTableView];
    if (_staticElements != nil && _staticElements.count > 0) {
        [self refreshWithStaticElements];
    } else {
        [self refreshFetchingDelegateElements];
    }
}

- (void)refreshWithStaticElements {
    NSString* inputString = self.text;
    NSMutableArray* resultArray = [[NSMutableArray alloc] init];
    if (inputString.length == 0) {
        [resultArray addObjectsFromArray:_staticElements];
    } else {
        // Search for coincidences
        for (id element in _staticElements) {
            // Obtain element description
            NSString* elementDescription = [self descriptionOfElement:element];
            // Checks coincidence
            NSArray *words = [self obtainsDifferentWordsInAName:inputString];
            NSInteger numberOfWordsFound = 0;
            for (NSString *word in words) {
                NSRange range = [elementDescription rangeOfString:word options:NSDiacriticInsensitiveSearch | NSCaseInsensitiveSearch];
                if (range.location != NSNotFound) {
                    numberOfWordsFound++;
                    if (numberOfWordsFound == [words count]) {
                        [resultArray addObject:element];
                    }
                }
            }
        }
    }
    // Notify results
    [self fetchedElements:[resultArray autorelease] forInputString:inputString];
}

- (void)refreshFetchingDelegateElements {
    NSString* inputString = self.text;
    [self.delegate predictiveTextField:self elementsForInputString:inputString];
}

- (NSString*)descriptionOfElement:(id)element {
    // Get description from element
    id value = [element valueForKey:self.descriptionField];
    // Update textfield
    NSString* description = value == nil ? @"" : [NSString stringWithFormat:@"%@", value];
    return description;
}

- (void)displayPopover {
    if (_externalTableView == nil) {
        [_popoverController presentPopoverFromRect:_textField.frame inView:self permittedArrowDirections:_popoverArrowDirections animated:YES];
    }
}

#pragma mark UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    _selectedElement = nil;
    NSString* inputString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    BOOL onlyWhitespaces = [inputString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0;
    if (_maxTextLength == kUnlimitedMaxTextLength || inputString.length <= _maxTextLength) {
        if (onlyWhitespaces) {
            inputString = @"";
        }
        if (![textField.text isEqualToString:inputString]) {
            textField.text = inputString;
            [self refreshDisplayElements];
            [self displayPopover];
        }
    }
    return NO;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    [self refreshDisplayElements];
    [self performSelector:@selector(displayPopover) withObject:nil afterDelay:kAnimationLength];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self resignFirstResponder];
    [self notifyChangeToDelegate];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField {
    _selectedElement = nil;
    return YES;
}

#pragma mark UITableViewDataSource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int rows = 0;
    // If we are loading results or empty results, display a row to notify it
    if (_loading || _displayElements.count == 0) {
        rows = 1;
    } else {
        rows = _displayElements.count;
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [_tableView dequeueReusableCellWithIdentifier:kCellIdenfifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdenfifier] autorelease];
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = self.noResultsTextFont;
    cell.textLabel.textColor = self.disabledTextColor;
    // If loading, display loading cell
    if (_loading) {
        cell.textLabel.text = NSLocalizedString(@"FETCHING_COINCIDENCES", nil);
    } else if (_displayElements.count == 0) {
        // If no elements found, display noResults cell
        cell.textLabel.text = NSLocalizedString(@"NO_COINCIDENCES_FOUND", nil);
    } else {
        // Get description from element
        if (indexPath.row < _displayElements.count) {
            
//Ravi_Bukka: Added for the localization
            NSString* text = NSLocalizedString([self descriptionOfElement:[_displayElements objectAtIndex:indexPath.row]], nil);
            
            
            
            cell.textLabel.text = text;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            cell.textLabel.textColor = self.textColor;
            cell.textLabel.font = self.textFont;
        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row < _displayElements.count) {
        id element = [_displayElements objectAtIndex:indexPath.row];
        self.selectedElement = element;
        
        // If we are using an external tableview and the textfield is not selected, notify changes now
        if (tableView == _externalTableView && ![_textField isFirstResponder]) {
            [self notifyChangeToDelegate];
        }
        [self resignFirstResponder];
    }
}

#pragma mark UIPopoverControllerDelegate

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    if (![self isLoading]) {
        [self resignFirstResponder];
        return YES;
    } else {
        return NO;
    }
}

@end