//
//  CustomTextArea.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 09/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPlaceholderTextArea.h"

@interface CustomTextArea : UIView

@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) NSString *placeholder;
@property (nonatomic, assign, getter=isEnabled) BOOL enabled;
@property (nonatomic, retain) IBOutlet id<UITextViewDelegate> delegate;
@property (readonly) UIPlaceholderTextArea *textArea;

@end
