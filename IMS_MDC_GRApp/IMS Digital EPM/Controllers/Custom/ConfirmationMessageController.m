//
//  ConfirmationMessageController.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 12/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "ConfirmationMessageController.h"

@implementation ConfirmationMessageController
@synthesize messageLabel = _messageLabel;
@synthesize acceptButton = _acceptButton;
@synthesize cancelButton = _cancelButton;
@synthesize message = _message;
@synthesize acceptButtonTitle = _acceptButtonTitle;
@synthesize cancelButtonTitle = _cancelButtonTitle;
@synthesize delegate = _delegate;
@synthesize eventBlock = _eventBlock;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithMessage:(NSString*)message eventBlock:(ConfirmationButtonEventBlock)eventBlock {
    self = [super initWithNibName:@"ConfirmationMessageController" bundle:nil];
    if (self) {
        self.message = message;
        self.eventBlock = eventBlock;
    }
    return self;
}
- (id)initWithMessage:(NSString*)message delegate:(id<ConfirmationMessageControllerDelegate>)delegate {
    self = [super initWithNibName:@"ConfirmationMessageController" bundle:nil];
    if (self) {
        self.message = message;
        self.delegate = delegate;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Public methods
- (void)setMessage:(NSString *)message
{
    if (_message != message) {
        [message retain];
        [_message release];
        _message = message;
    }
    //_messageLabel.text = _message;
   
    _messageLabel.text = NSLocalizedString(@"PATIENT_ALLOW_EDITING_MESSAGE", nil);
}

- (void)setAcceptButtonTitle:(NSString *)acceptButtonTitle
{
    if (_acceptButtonTitle != acceptButtonTitle) {
        [acceptButtonTitle retain];
        [_acceptButtonTitle release];
        _acceptButtonTitle = acceptButtonTitle;
    }
    if (_acceptButtonTitle != nil) {
        [_acceptButton setTitle:_acceptButtonTitle forState:UIControlStateNormal];
        [_acceptButton setTitle:_acceptButtonTitle forState:UIControlStateHighlighted];
    }
}

- (void)setCancelButtonTitle:(NSString *)cancelButtonTitle
{
    if (_cancelButtonTitle != cancelButtonTitle) {
        [cancelButtonTitle retain];
        [_cancelButtonTitle release];
        _cancelButtonTitle = cancelButtonTitle;
    }
    if (_cancelButtonTitle != nil) {
        [_cancelButton setTitle:_cancelButtonTitle forState:UIControlStateNormal];
        [_cancelButton setTitle:_cancelButtonTitle forState:UIControlStateHighlighted];
    }
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Trigger UI changes
    
    //Ravi_Bukka
    self.messageLabel.text = NSLocalizedString(@"PATIENT_ALLOW_EDITING_MESSAGE", nil);
    
    [self.acceptButton setTitle:NSLocalizedString(@"DOCTOR_YES", nil) forState:UIControlStateNormal];
    [self.acceptButton setTitle:NSLocalizedString(@"DOCTOR_YES", nil) forState:UIControlStateHighlighted];
    
    self.message = _message;
    
    self.acceptButtonTitle = _acceptButtonTitle;
    self.cancelButtonTitle = _cancelButtonTitle;
}

- (void)viewDidUnload
{
    [self setAcceptButton:nil];
    [self setCancelButton:nil];
    [self setMessageLabel:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (void)dealloc {
    [_acceptButton release];
    [_cancelButton release];
    [_messageLabel release];
    [_message release];
    [_acceptButtonTitle release];
    [_cancelButtonTitle release];
    [_eventBlock release];
    _delegate = nil;
    [super dealloc];
}
- (IBAction)acceptButtonClicked:(id)sender {
    if (_delegate != nil) {
        [_delegate didClickConfirmationButton:YES];
    }
    if (_eventBlock != nil) {
        _eventBlock(YES);
    }
}

- (IBAction)cancelButtonClicked:(id)sender {
    if (_delegate != nil) {
        [_delegate didClickConfirmationButton:NO];
    }
    if (_eventBlock != nil) {
        _eventBlock(NO);
    }
}

@end
