//
//  PredictiveTextField.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 07/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PredictiveTextField;

extern const NSInteger kUnlimitedMaxTextLength;

/** @brief Protocol that must be implemented to handle PredictiveTextField events */
@protocol PredictiveTextFieldDelegate <NSObject>

/**
 @brief Method that is called from PredictiveTextField to obtain source elements that will be displayed in prediction table for given user input string.
 @param textField Source textfield
 @param inputString Input Text in the moment of the method call */
-(void) predictiveTextField:(PredictiveTextField*)textField elementsForInputString:(NSString*)inputString;

@optional
/** @brief Method that is called when value of the predictive textfield has changed */
-(void) predictiveTextFieldValueChanged:(PredictiveTextField*)textField;

@end

/** @brief Object wrapper for textfield that includes popover with options to autocomplete textfield depending on user input */
@interface PredictiveTextField : UIView <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate>

/** @brief Defines is the textfield will allow user to insert values not in selectable elements array */
@property (nonatomic, assign, getter=isAllowUserValues) BOOL allowUserValues;
/** @brief Array of static elements to be used by the textfield instead of dynamic delegate search */
@property (nonatomic, retain) NSArray *staticElements;
/** @brief Object property that will be used to display object description. Default is 'description'. */
@property (nonatomic, retain) NSString *descriptionField;
/** @brief Selected element */
@property (nonatomic, retain) id selectedElement;
/** @brief Current text of the textfield */
@property (nonatomic, retain) NSString *text;
/** @brief Defines if the component is enabled for user interaction */
@property (nonatomic, assign, getter=isEnabled) BOOL enabled;
/** @brief Object that will handle predictive textfield events */
@property (nonatomic, assign) IBOutlet id<PredictiveTextFieldDelegate> delegate;
/** @brief Maximum number of rows to be displayed on popover table before start scrolling */
@property (nonatomic, assign) NSInteger maxDisplayRows;
/** @brief Heigth of popover table rows */
@property (nonatomic, assign) NSInteger rowHeight;
/** @brief Placeholder textfield hint */
@property (nonatomic, retain) NSString* placeholder;
/** @brief Text color for default status */
@property (nonatomic, retain) UIColor *textColor;
/** @brief Text color for disabled status */
@property (nonatomic, retain) UIColor *disabledTextColor;
/** @brief Font style for default status */
@property (nonatomic, retain) UIFont *textFont;
/** @brief Font style for 'loading' and 'no results' rows */
@property (nonatomic, retain) UIFont *noResultsTextFont;
/** @brief Defines an external table view when predictive info will be displayed */
@property (nonatomic, retain) IBOutlet UITableView *externalTableView;
/** @brief Allowed arrow directions for hints popover */
@property (nonatomic, assign) UIPopoverArrowDirection popoverArrowDirections;
/** @brief Mode of textfield clear button */
@property (nonatomic, assign) UITextFieldViewMode clearButtonMode;
/** @brief Property  */
@property (nonatomic, assign) NSInteger maxTextLength;
//Deepak Carpenter :Set Text Alignment
@property (nonatomic, assign) NSTextAlignment alignment;
@property (nonatomic ,assign) UIFont *textFontSize;
@property (nonatomic,assign)UIKeyboardType keyBoardType;

//Utpal:CR#4 Added for demographic Provinces - START
@property (nonatomic, retain) NSArray *tagArray;
@property (nonatomic, retain) NSArray *values;
@property (nonatomic, retain) NSString *tagIdentifierField;

@property (nonatomic, retain) NSArray *tagArrayUniversity;
@property (nonatomic, retain) NSArray *valuesUniversity;
@property (nonatomic, retain) NSString *tagIdentifierFieldUniversity;

@property (nonatomic, assign) id defaultSelectedValue;
@property (nonatomic, assign) id defaultUniversitySelectedValue;

/**
 @brief Method that is called when the delegate found result elements for textfield input string
 @param elements Elements found. Can be an empty array.
 @param inputString String used in search
 */
-(void) fetchedElements:(NSArray*)elements forInputString:(NSString*)inputString;

@end
