//
//  CustomSegmentedControl.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 09/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "CustomSegmentedControl.h"
#import "ComponentGroup.h"

@implementation CustomSegmentedControl

@synthesize tagArray = _tagArray;
@synthesize values = _values;
@synthesize tagIdentifierField = _tagIdentifierField;

-(void) initialize {
    // iOS 4.x compatibility
    if ([self respondsToSelector:@selector(setTitleTextAttributes:forState:)]) {
        // Set defaults
        UIFont *font = [UIFont boldSystemFontOfSize:12.0f];
        
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:font forKey:UITextAttributeFont];
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 30);

        [self setTitleTextAttributes:attributes forState:UIControlStateNormal];
    }
}

- (id)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)dealloc {
    [_tagArray release];
    [_values release];
    [_tagIdentifierField release];
    [super dealloc];
}
//Utpal: CR#4 Added for Demographic  Gender  - START

-(id)customSelectedValue {
    id value = nil;
    
    NSNumber* genderInput = [[NSUserDefaults standardUserDefaults] valueForKey:@"Gender"];
    // Check selected segment index
    NSInteger selectedSegment = [genderInput integerValue];
    selectedSegment--;
    // Check selection is not out of tag array bounds
    if (selectedSegment < _tagArray.count) {
        // Obtain selected tag
        NSInteger selectedTag = [[_tagArray objectAtIndex:selectedSegment] intValue];
        // Find value with identifier corresponding with selected tag
        for (id valueInArray in _values) {
            if ([[valueInArray valueForKey:self.tagIdentifierField] intValue] == selectedTag) {
                value = valueInArray;
                break;
            }
        }
    } else {
        DLog(@"Tag array is not long enough to support all segments");
    }
    
    return value;
}
//END

-(id)selectedValue {
    id value = nil;
    // Check selected segment index
    NSInteger selectedSegment = self.selectedSegmentIndex;
    // Check selection is not out of tag array bounds
    if (selectedSegment < _tagArray.count) {
        // Obtain selected tag
        NSInteger selectedTag = [[_tagArray objectAtIndex:selectedSegment] intValue];
        // Find value with identifier corresponding with selected tag
        for (id valueInArray in _values) {
            if ([[valueInArray valueForKey:self.tagIdentifierField] intValue] == selectedTag) {
                value = valueInArray;
                break;
            }
        }
    } else {
        DLog(@"Tag array is not long enough to support all segments");
    }
    
    return value;
}

-(void)setSelectedValue:(id)selectedValue {
    // Deselect
    self.selectedSegmentIndex = UISegmentedControlNoSegment;
    if (selectedValue != nil) {
        NSInteger selectedSegment = 0;
        // Obtain corresponding tag
        NSInteger selectedTag = [[selectedValue valueForKey:self.tagIdentifierField] intValue];
        // Find tag index in tag array
        for (id tag in _tagArray) {
            // If tag corresponds with selected
            if ([tag intValue] == selectedTag) {
                // Set tag index in array as selected
                selectedSegment = [_tagArray indexOfObject:tag];
                break;
            }
        }
        
        if (selectedSegment < self.numberOfSegments) {
            self.selectedSegmentIndex = selectedSegment;
        } else {
            DLog(@"Index out of bounds for segmented control: %i", selectedSegment);
        }
    }
}

-(void)setEnabled:(BOOL)enabled {
    super.enabled = enabled;
    self.alpha = enabled ? 1 : kComponentDisabledAlpha;
}

-(void)setSegmentedControlStyle:(UISegmentedControlStyle)segmentedControlStyle {
    // iOS 4.x compatibility
    if ([self respondsToSelector:@selector(setTitleTextAttributes:forState:)]) {
        // Style will always be Plain
        [super setSegmentedControlStyle:UISegmentedControlStylePlain];
    }
}

@end
