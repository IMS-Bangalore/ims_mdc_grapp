//
//  CustomPickerView.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 07/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "CustomPickerView.h"
#import "ComponentGroup.h"
#import "UIView+FillWithSubview.h"
#import "CustomLabel.h"

#pragma mark - Private Methods

static const NSInteger kPickerPadding = -10;
static const NSInteger kLabelHeight = 30;
static const NSInteger kLabelPadding = 18;
static const NSInteger kLabelTop = -3;

@interface CustomPickerView()

@property (nonatomic, retain) UILabel *overlappedLabel;

@end

@implementation CustomPickerView

@synthesize delegate = _delegate;
@synthesize pickerView = _pickerView;
@synthesize enabled = _enabled;
@synthesize overlappedText = _overlappedText;
@synthesize overlappedLabel = _overlappedLabel;
@synthesize overlappedTextFont=_overlappedTextFont;

-(void)initialize {
    self.clipsToBounds = YES;
    _pickerView = [[UIPickerView alloc] init];
    _pickerView.delegate = self;
    _pickerView.dataSource = self.dataSource;
    _pickerView.showsSelectionIndicator = YES;
    
    // Set frame
    CGRect pickerFrame = CGRectMake(kPickerPadding, 
                                   (self.frame.size.height - _pickerView.frame.size.height) / 2, 
                                   self.frame.size.width - (kPickerPadding * 2),
                                   _pickerView.frame.size.height);
    _pickerView.frame = pickerFrame;
    _pickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self addSubview:_pickerView];
    // Add shadow images
    // Top shadow image
    UIImage* topImage = [UIImage imageNamed:@"sombra_picker.png"];
    UIImageView* topImageView = [[UIImageView alloc] initWithImage:topImage];
    topImageView.contentMode = UIViewContentModeScaleToFill;
    topImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    topImageView.frame = CGRectMake(0, 0, self.frame.size.width, topImageView.frame.size.height);
    [self addSubview:topImageView];
    [topImageView release];
    // Bottom shadow image
    UIImage* bottomImage = [UIImage imageNamed:@"sombra_picker_down.png"];
    UIImageView* bottomImageView = [[UIImageView alloc] initWithImage:bottomImage];
    bottomImageView.contentMode = UIViewContentModeScaleToFill;
    bottomImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    bottomImageView.frame = CGRectMake(0, self.frame.size.height - bottomImageView.frame.size.height, self.frame.size.width, bottomImageView.frame.size.height);
    [self addSubview:bottomImageView];
    [bottomImageView release];
    // Add overlayed label
    //DEEPAK CARPENTER // for units pickerview in polish
    if ([[[[NSBundle mainBundle]preferredLocalizations]objectAtIndex:0] isEqualToString:@"pl"]){
        _overlappedLabel = [[CustomLabel alloc] initWithFrame:CGRectMake((kLabelPadding+35), ((self.frame.size.height - kLabelHeight) / 2) + kLabelTop , self.frame.size.width - (kLabelPadding * 2), (kLabelHeight+3))];
    }else{
    _overlappedLabel = [[CustomLabel alloc] initWithFrame:CGRectMake(kLabelPadding, ((self.frame.size.height - kLabelHeight) / 2) + kLabelTop , self.frame.size.width - (kLabelPadding * 2), kLabelHeight)];
    }
    _overlappedLabel.text = @"";
    _overlappedLabel.font = [UIFont systemFontOfSize:17];
    _overlappedLabel.textColor = [UIColor blackColor];
    //DEEPAK CARPENTER // for units pickerview in polish
    if ([[[[NSBundle mainBundle]preferredLocalizations]objectAtIndex:0] isEqualToString:@"pl"])
    {
    _overlappedLabel.textAlignment = NSTextAlignmentLeft;
    }else
        _overlappedLabel.textAlignment = NSTextAlignmentRight;

    _overlappedLabel.backgroundColor = [UIColor clearColor];
    _overlappedLabel.userInteractionEnabled = NO;
    [self addSubview:_overlappedLabel];
}

- (id)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)setEnabled:(BOOL)flag {
    _enabled = flag;
    self.userInteractionEnabled = flag;
    self.alpha = flag ? 1 : kComponentDisabledAlpha;
    _overlappedLabel.textColor = self.enabled ? [UIColor blackColor] : [UIColor grayColor];
    [_pickerView reloadAllComponents];
}

- (void)dealloc {
    _delegate = nil;
    [_pickerView release];
    [_overlappedText release];
    [_overlappedLabel release];
    [super dealloc];
}

- (void)setDataSource:(id<UIPickerViewDataSource>)aDataSource {
    _pickerView.dataSource = aDataSource;
}

-(id<UIPickerViewDataSource>)dataSource {
    return _pickerView.dataSource;
}

- (void)setOverlappedText:(NSString *)anOverlayedText {
    if (_overlappedText != anOverlayedText) {
        [anOverlayedText retain];
        [_overlappedText release];
        _overlappedText = anOverlayedText;
        _overlappedLabel.text = anOverlayedText;
        _overlappedLabel.hidden = anOverlayedText == nil;
    }
}

- (UIView *)pickerViewTextFont:(UIPickerView *)pickerView
            viewForRow:(NSInteger)row
          forComponent:(NSInteger)component
           reusingView:(UIView *)view {
        UILabel *pickerLabel = (UILabel *)view;
    
        if (pickerLabel == nil) {
            pickerLabel = [[[UILabel alloc] init] autorelease];
            pickerLabel.textAlignment = NSTextAlignmentLeft;
            pickerLabel.backgroundColor = [UIColor clearColor];
            pickerLabel.font = [UIFont boldSystemFontOfSize:14];
        }
    //
        pickerLabel.textColor = [UIColor blackColor];
        [pickerLabel setText:[NSString stringWithFormat:@"   %@", [_delegate pickerView:_pickerView titleForRow:row forComponent:component]]];
    //    
        return pickerLabel;

}

#pragma mark - UIPickerViewDelegate

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [_delegate pickerView:_pickerView titleForRow:row forComponent:component];
}

// Commented for enhance iOS 4.3
//Deepak Carpenter
- (UIView *)pickerView:(UIPickerView *)pickerView 
            viewForRow:(NSInteger)row
          forComponent:(NSInteger)component
           reusingView:(UIView *)view {
    
    UILabel *pickerLabel = (UILabel *)view;

    if (pickerLabel == nil) {
        pickerLabel = [[[UILabel alloc] init] autorelease];
        pickerLabel.textAlignment = NSTextAlignmentLeft;
        pickerLabel.backgroundColor = [UIColor clearColor];
        pickerLabel.font = [UIFont boldSystemFontOfSize:16];
    }
    
    pickerLabel.textColor = [UIColor blackColor];
    [pickerLabel setText:[NSString stringWithFormat:@"   %@", [_delegate pickerView:_pickerView titleForRow:row forComponent:component]]];
    
    return pickerLabel;

}
//////
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if ([_delegate respondsToSelector:@selector(pickerView:didSelectRow:inComponent:)]) {
        [_delegate pickerView:pickerView didSelectRow:row inComponent:component];
    }
}

@end
