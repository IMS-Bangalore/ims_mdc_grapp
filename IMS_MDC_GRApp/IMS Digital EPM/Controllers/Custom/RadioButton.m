//
//  RadioButton.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 08/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "RadioButton.h"

@implementation RadioButton

static NSString* const kDefaultImage = @"radio_unselec.png";
static NSString* const kSelectedImage = @"radio_selec.png";
static NSString* const kDisabledImage = @"radio_unselec.png";

#pragma mark Initialization methods

-(void) initRadioButton {
    // Set defaults
    [self setImage:[UIImage imageNamed:kDefaultImage] forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:kSelectedImage] forState:UIControlStateSelected];
    [self setImage:[UIImage imageNamed:kDisabledImage] forState:UIControlStateDisabled];
}

- (id)init {
    self = [super init];
    if (self) {
        [self initRadioButton];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initRadioButton];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initRadioButton];
    }
    return self;
}

- (void)buttonClicked:(UIButton*)button {
    if (!self.selected) {
        self.selected = YES;
        [self.checkBoxDelegate checkBoxDidChangeValue:self];
    }
}


@end
