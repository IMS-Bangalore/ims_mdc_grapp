//
//  LoadingDialogViewController.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 03/04/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingDialogViewController : UIViewController
@property (retain, nonatomic) IBOutlet UILabel *label;
@property (nonatomic, retain) NSString *text;

@end
