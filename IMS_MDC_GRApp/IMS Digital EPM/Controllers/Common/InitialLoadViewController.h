//
//  InitialLoadViewController.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 12/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "InitialLoadProcessor.h"
#import "ModalControllerProtocol.h"

@protocol InitialLoadViewControllerDelegate <ModalControllerProtocol>
- (void)initialLoadViewControllerDidFinishLoading;
@end

@interface InitialLoadViewController : UIViewController<InitialLoadProcessorDelegate>
@property (retain, nonatomic) IBOutlet UIProgressView *progressView;
@property (assign, nonatomic) id<InitialLoadViewControllerDelegate> delegate;

- (void)performInitialLoad;

@end