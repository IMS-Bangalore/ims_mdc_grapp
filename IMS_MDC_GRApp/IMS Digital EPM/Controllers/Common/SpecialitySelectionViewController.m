//
//  SpecialitySelectionViewController.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 11/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "SpecialitySelectionViewController.h"

@interface SpecialitySelectionViewController()
- (IBAction)buttonClicked:(id)sender;
@end

@implementation SpecialitySelectionViewController
@synthesize delegate = _delegate;
@synthesize disabled = _disabled;
@synthesize values = _values;
@synthesize windowTitle = _windowTitle;
@synthesize labelTitulo = _labelTitulo;
@synthesize buttons = _buttons;
@synthesize otherSpecialityValues=_otherSpecialityValues;



//**********************
// initWithDelegate
//**********************
- (id)init {
    self = [super initWithNibName:@"SpecialitySelectionViewController" bundle:nil];
    if (self) {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Private methods
- (void)refreshUI {
    if ([self isViewLoaded]) {
        // Assign labels
        for( UIButton* button in _buttons) {
            [button.titleLabel setTextAlignment:NSTextAlignmentCenter];
            
            id specialityForButton = nil;
            NSInteger index = button.tag;
            if (index < _values.count) {
                specialityForButton = [_values objectAtIndex:index];

            }
            
            if (specialityForButton != nil) {
               
//Ravi_Bukka: Added for localization
                
                [button setTitle: NSLocalizedString([specialityForButton name],nil) forState:UIControlStateNormal];
                button.enabled = ![specialityForButton isEqual:_disabled];
            }
            button.hidden = specialityForButton == nil;
        }
        
        // Adapt window size
        int numFilas = _values.count / 7;
        if (_values.count % 7 > 0) {
            numFilas++;
        }
        
        CGRect frame = self.view.frame;
        frame.size.height = 62 + numFilas * 113;
        self.view.frame = frame;
    }
}

#pragma mark - Public methods
- (void)setValues:(NSArray *)values {
    [values retain];
    [_values release];
    _values = values;
    
    [self refreshUI];
}

- (void)setDisabled:(id)disabled {
    [disabled retain];
    [_disabled release];
    _disabled = disabled;
    
    [self refreshUI];
}

- (void)setWindowTitle:(NSString *)windowTitle {
    [windowTitle retain];
    [_windowTitle release];
    _windowTitle = windowTitle;
    
    self.labelTitulo.text = _windowTitle;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _labelTitulo.text = NSLocalizedString(@"DOCTOR_SECONDARY_SPECIALITY_POPUP_TITLE", nil);
    
    // Link button events
    for (UIButton* button in _buttons) {
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    // Trigger UI refresh
    [self refreshUI];
    self.windowTitle = _windowTitle;
}

- (void)viewDidUnload
{
    [self setLabelTitulo:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (IBAction)buttonClicked:(id)sender {
    id specialityForButton = nil;
    NSInteger index = [sender tag];
    if (index < _values.count) {
        specialityForButton = [_values objectAtIndex:index];
        NSLog(@"Therapy Selection %@",[_values objectAtIndex:index]);
    }
    
    [_delegate specialitySelectionController:self didSelectSpeciality:specialityForButton];
}
//Utpal: CR#4 Added for Demographic START  Speciality

-(id)defaultSelectedValue{
    id value = nil;
    
    // Check selected segment index
    
    NSNumber* specialityInput = [[NSUserDefaults standardUserDefaults] valueForKey:@"Speciality"];
    // Check selected segment index
    NSLog(@"NSintegerValue Speciality.. %@",specialityInput);
    NSInteger selectedSpeciality = [specialityInput integerValue];
    // selectedSpeciality--;
    NSLog(@"NSintegerValue Speciality.. %d",selectedSpeciality);
    
    //    NSNumber* otherSpecialityInput = [[NSUserDefaults standardUserDefaults] valueForKey:@"OtherSpeciality"];
    //    NSLog(@"NSintegerValue OtherSpeciality.. %@",otherSpecialityInput);
    //    NSInteger selectedOtherSpeciality = [otherSpecialityInput integerValue];
    //    selectedOtherSpeciality--;
    //    NSLog(@"NSintegerValue OtherSpeciality.. %d",selectedOtherSpeciality);
    
    
    // Check selection is not out of tag array bounds
    //    if (selectedSpeciality < _tagArray.count) {
    // Obtain selected tag
    //    NSInteger selectedTag = [[_tagArray objectAtIndex:selectedSpeciality] intValue];
    // Find value with identifier corresponding with selected tag
    for (id valueInArray in _defaultValues) {
        if ([[valueInArray valueForKey:self.tagIdentifierField] intValue] == selectedSpeciality) {
            value = valueInArray;
            break;
        }
    }
    //    }else {
    //        DLog(@"Tag array is not long enough to support all speciality");
    //    }
    
    return value;
}

//END

////Utpal: CR#4 Added for Demographic START Other Speciality
//
-(id)defaultSelectedValueOtherSpeciality
{
    id value = nil;
    
    // Check selected segment index
    
    NSNumber* otherSpecialityInput = [[NSUserDefaults standardUserDefaults] valueForKey:@"OtherSpeciality"];
    NSInteger selectedOtherSpeciality = [otherSpecialityInput integerValue];
    for (id valueInArray in _defaultValues) {
        if ([[valueInArray valueForKey:self.tagIdentifierField] intValue] == selectedOtherSpeciality) {
            value = valueInArray;
            break;
        }
    }
    
    return value;
    
}


- (void)dealloc {
    [_disabled release];
    [_values release];
    [_labelTitulo release];
    [_windowTitle release];
    [_buttons release];
    [super dealloc];
}

@end
