//
//  PrescriptionAndSickFundVViewController.h
//  IMS Digital EPM
//
//  Created by Bukka, Ravi (Bangalore) on 16/12/13.
//
//

#import <UIKit/UIKit.h>
#import "PredictiveTextField.h"
#import "Patient.h"
#import "MultipleChoiceViewController.h"
#import "ModalControllerProtocol.h"
#import "CustomTextField.h"
@class PrescriptionAndSickFundVViewController;
@protocol PrescriptionAndSickFundVViewControllerDelegate <NSObject,ModalControllerProtocol>

- (void)PrescriptionAndSickFundSelectionController:(PrescriptionAndSickFundVViewController*)popup didSelectPrescription:(NSString*)prescriptionName;

- (void)PrescriptionAndSickFundSelectionController:(PrescriptionAndSickFundVViewController*)popup didSelectionSickFund:(NSString*)sickfundName didSelectionOtherSickFund:(NSString*)OtherSickfundName;


@end



@interface PrescriptionAndSickFundVViewController : UIViewController <PredictiveTextFieldDelegate,UITextFieldDelegate> {
}


@property (retain, nonatomic) IBOutlet UIButton *okButton;
@property (retain, nonatomic) IBOutlet UIButton *singlePrescrButton;
@property (retain, nonatomic) IBOutlet UIButton *monthlyPrescrButton;
//Deepak_Carpenter :Added for new change request on 27/01/2015
@property (retain, nonatomic) IBOutlet UIButton *over3monthsPrescrButton;
@property (retain, nonatomic) IBOutlet UIButton *trimesterPrescrButton;
@property (retain, nonatomic) IBOutlet UIButton *otherSickFundButton;
@property (retain, nonatomic) IBOutlet UITextField *otherTextField;

@property (retain, nonatomic) IBOutlet UIButton *okaButton;
@property (retain, nonatomic) IBOutlet UIButton *ogaButton;
@property (retain, nonatomic) IBOutlet UIButton *oaeeButton;
@property (retain, nonatomic) IBOutlet UIButton *opadButton;




@property (retain, nonatomic) IBOutletCollection(UIButton) NSArray* buttons;

@property (retain, nonatomic) NSString *prescriptionString;
@property (retain, nonatomic) NSString *sickFundString;
@property (retain, nonatomic) NSString *OtherSickFundString;





@property (retain, nonatomic) IBOutlet UILabel *prescriptionTypeLabel;
@property (retain, nonatomic) IBOutlet UILabel *sickFundLabel;
@property (nonatomic, assign) BOOL buttonSelectedPres;
@property (nonatomic, assign) BOOL buttonSelectedSickFund;
@property (nonatomic, retain) Patient *selectedPatient;

- (IBAction)accepterButtonClicked:(id)sender;
//Deepak_Carpenter: Added for Alert Pop Up
@property (assign, nonatomic) id<PrescriptionAndSickFundVViewControllerDelegate> delegate;
@property (nonatomic, retain) MultipleChoiceViewController* choiceController;

@property (retain, nonatomic) IBOutlet CustomTextField *OTextField;
@end
