//
//  PrescriptionAndSickFundVViewController.m
//  IMS Digital EPM
//
//  Created by Bukka, Ravi (Bangalore) on 16/12/13.
//
//

#import "PrescriptionAndSickFundVViewController.h"
#import "Patient.h"
#import "OtherSickFund.h"
#import "MultipleChoiceViewController.h"
@interface PrescriptionAndSickFundVViewController ()

- (IBAction)buttonClicked:(id)sender;
@end

@implementation PrescriptionAndSickFundVViewController

@synthesize okButton = _okButton;
@synthesize otherTextField = _otherTextField;
@synthesize OTextField=_OTextField;
@synthesize selectedPatient=_selectedPatient;

@synthesize prescriptionTypeLabel = _prescriptionTypeLabel;
@synthesize sickFundLabel = _sickFundLabel;
@synthesize choiceController=_choiceController;


@synthesize buttons = _buttons;

@synthesize singlePrescrButton = _singlePrescrButton;
@synthesize monthlyPrescrButton = _monthlyPrescrButton;
@synthesize trimesterPrescrButton = _trimesterPrescrButton;
//Deepak_Carpenter :Added for new change request on 27/01/2015
@synthesize over3monthsPrescrButton=_over3monthsPrescrButton;

@synthesize otherSickFundButton = _otherSickFundButton;

@synthesize buttonSelectedPres = _buttonSelectedPres;
@synthesize buttonSelectedSickFund = _buttonSelectedSickFund;

@synthesize prescriptionString = _prescriptionString;
@synthesize sickFundString = _sickFundString;
@synthesize OtherSickFundString=_OtherSickFundString;

@synthesize okaButton = _okaButton;
@synthesize ogaButton = _ogaButton;
@synthesize oaeeButton = _oaeeButton;
@synthesize opadButton = _opadButton;



//**********************
// initWithDelegate
//**********************
- (id)init {
    self = [super initWithNibName:@"PrescriptionAndSickFundVViewController" bundle:nil];
    if (self) {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}




- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
//    
//    PATIENT_IFPUBLIC
//

//Ravi_Bukka: Fix for Insurance and Recurrance issue
    for( UIButton* button in _buttons)
        [button setBackgroundImage:[UIImage imageNamed:@"bola_modal_inact.png"] forState:UIControlStateNormal];
    
    _otherTextField.text = nil;
    _OTextField.text=nil;
    _OTextField.displayError=NO;
    _prescriptionString = NULL;
    _sickFundString = NULL;
    _OTextField.placeholder=nil;
    _OTextField.userInteractionEnabled=NO;
    
    _buttonSelectedPres = NO;
    _buttonSelectedSickFund = NO;
    
    [_okButton setTitle:NSLocalizedString(@"MULTICHOICE_ACCEPT",nil) forState:UIControlStateNormal];
    [_okButton setTitle:NSLocalizedString(@"MULTICHOICE_ACCEPT",nil) forState:UIControlStateHighlighted];
    
    [_singlePrescrButton setTitle:NSLocalizedString(@"PATIENT_SINGLE",nil) forState:UIControlStateNormal];
    [_singlePrescrButton setTitle:NSLocalizedString(@"PATIENT_SINGLE",nil) forState:UIControlStateHighlighted];
    
    [_monthlyPrescrButton setTitle:NSLocalizedString(@"PATIENT_MONTHLY",nil) forState:UIControlStateNormal];
    [_monthlyPrescrButton setTitle:NSLocalizedString(@"PATIENT_MONTHLY",nil) forState:UIControlStateHighlighted];
    
    [_trimesterPrescrButton setTitle:NSLocalizedString(@"PATIENT_TRIMESTER",nil) forState:UIControlStateNormal];
    [_trimesterPrescrButton setTitle:NSLocalizedString(@"PATIENT_TRIMESTER",nil) forState:UIControlStateHighlighted];
    //Deepak_Carpenter :Added for new change request on 27/01/2015
    [_over3monthsPrescrButton setTitle:NSLocalizedString(@"PATIENT_OVER3MONTHS",nil) forState:UIControlStateNormal];
    [_over3monthsPrescrButton setTitle:NSLocalizedString(@"PATIENT_OVER3MONTHS",nil) forState:UIControlStateHighlighted];
    
    [_otherSickFundButton setTitle:NSLocalizedString(@"PATIENT_OTHER",nil) forState:UIControlStateNormal];
    [_otherSickFundButton setTitle:NSLocalizedString(@"PATIENT_OTHER",nil) forState:UIControlStateHighlighted];
    

    [_okaButton setTitle:NSLocalizedString(@"PATIENT_IKA",nil) forState:UIControlStateNormal];
    [_okaButton setTitle:NSLocalizedString(@"PATIENT_IKA",nil) forState:UIControlStateHighlighted];
    
    [_ogaButton setTitle:NSLocalizedString(@"PATIENT_OGA",nil) forState:UIControlStateNormal];
    [_ogaButton setTitle:NSLocalizedString(@"PATIENT_OGA",nil) forState:UIControlStateHighlighted];
    
    [_oaeeButton setTitle:NSLocalizedString(@"PATIENT_OAEE",nil) forState:UIControlStateNormal];
    [_oaeeButton setTitle:NSLocalizedString(@"PATIENT_OAEE",nil) forState:UIControlStateHighlighted];
    
    [_opadButton setTitle:NSLocalizedString(@"PATIENT_OPAD",nil) forState:UIControlStateNormal];
    [_opadButton setTitle:NSLocalizedString(@"PATIENT_OPAD",nil) forState:UIControlStateHighlighted];
    
    _sickFundLabel.text = NSLocalizedString(@"PATIENT_SICKFUND",nil);
    _prescriptionTypeLabel.text = NSLocalizedString(@"PATIENT_PRESCRIPTION_TYPE",nil);
 
    

    
}



- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Private methods
- (void)refreshUI {
}




#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _prescriptionString = NULL;
    _sickFundString = NULL;
    _otherTextField.userInteractionEnabled = NO;
    _OTextField.userInteractionEnabled=NO;
    
     UIFont* font = [UIFont boldSystemFontOfSize:10.f];
    
    
    
    // Link button events
    for (UIButton* button in _buttons) {
        NSLog(@"Object is %@",[_buttons objectAtIndex:0]);
        [button setImage:nil forState:UIControlStateNormal];
        [button setImage:nil forState:UIControlStateHighlighted];
        [button setImage:nil forState:UIControlStateDisabled];
        [button setImage:nil forState:UIControlStateSelected];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [button setContentMode:UIViewContentModeCenter];
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 8)];
        button.titleLabel.font = font;
        
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    // Trigger UI refresh
    [self refreshUI];
    
}

- (void)viewDidUnload
{
    [self setOkButton:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (IBAction)buttonClicked:(id)sender {
    
    NSInteger index = [sender tag];
    if (!(index==8)) {
        _otherTextField.text=nil;
        _OTextField.text=nil;
        
    }
    if (index == 0 || index == 1 || index == 2 || index == 3) {
    
    for(int i=0;i<4;i++){
        
		[[self.buttons objectAtIndex:i] setBackgroundImage:[UIImage imageNamed:@"bola_modal_inact.png"] forState:UIControlStateNormal];
	}
	[sender setBackgroundImage:[UIImage imageNamed:@"bola_modal_act.png"] forState:UIControlStateNormal];
        
        UIButton *button = [_buttons objectAtIndex:index];
        _prescriptionString = button.titleLabel.text;
       
        
    }
    
    if (index == 4 || index == 5 || index == 6 || index == 7 || index == 8 ) {
        for(int i=4;i<9;i++){
            [[self.buttons objectAtIndex:i] setBackgroundImage:[UIImage imageNamed:@"bola_modal_inact.png"] forState:UIControlStateNormal];
            _OTextField.displayError=NO;

        }
        [sender setBackgroundImage:[UIImage imageNamed:@"bola_modal_act.png"] forState:UIControlStateNormal];
        
     //   UIButton *button = [_buttons objectAtIndex:index];
        _sickFundString = [sender titleLabel].text;
        
        if ([_sickFundString isEqualToString:@"ΆΛΛΟ"]) {
            
            _otherTextField.userInteractionEnabled = YES;
            _otherTextField.placeholder=@"Παρακαλώ εισάγετε Αξία";
            _otherTextField.font = [UIFont systemFontOfSize:14.0];
            
            
            _OTextField.userInteractionEnabled = YES;
            _OTextField.placeholder=@"Παρακαλώ εισάγετε Αξία";
            _OTextField.font = [UIFont systemFontOfSize:14.0];
            
            }
        else {
            _otherTextField.userInteractionEnabled = NO;
            _otherTextField.placeholder=nil;
            
            _OTextField.userInteractionEnabled = NO;
            _OTextField.placeholder=nil;
        }
        
    }
    
}

- (IBAction)accepterButtonClicked:(id)sender {
    
//Ravi_Bukka: Fix for Insurance and Recurrance issue
//    if ([_sickFundString isEqualToString:@"ΆΛΛΟ"] && _otherTextField.text!=nil) {
//        _okButton.enabled=YES;
//        _sickFundString = _otherTextField.text;
//        
//        _OtherSickFundString=_otherTextField.text;
//        NSLog(@"sickfund string %@", _sickFundString);
//
//                  NSLog(@"otherd string %@", _selectedPatient.otherSickFundValue);
//    }
//    
//
//    if (_prescriptionString != NULL && _sickFundString != NULL) {
//        [_delegate PrescriptionAndSickFundSelectionController:self didSelectPrescription:_prescriptionString];
//        [_delegate PrescriptionAndSickFundSelectionController:self didSelectionSickFund:_sickFundString didSelectionOtherSickFund:_OtherSickFundString];
//        
//    }
    
//Ravi_Bukka: Fix for Insurance and Recurrance issue
    if (_prescriptionString != NULL && _sickFundString != NULL) {
       
        
        if ([_sickFundString isEqualToString:@"ΆΛΛΟ"] && [_OTextField.text length]>0){
           _okButton.enabled=YES;
            _OTextField.displayError=NO;
            _sickFundString = _OTextField.text;
            
            _OtherSickFundString=_OTextField.text;
            NSLog(@"sickfund string %@", _sickFundString);
            
            NSLog(@"otherd string %@", _selectedPatient.otherSickFundValue);
            
            [_delegate PrescriptionAndSickFundSelectionController:self didSelectPrescription:_prescriptionString];
            [_delegate PrescriptionAndSickFundSelectionController:self didSelectionSickFund:_sickFundString didSelectionOtherSickFund:_OtherSickFundString];
        }
        
        else {
            
            if ([_sickFundString isEqualToString:@"ΆΛΛΟ"]) {
               
           //     self displayAlert:@"Παρακαλούμε, εισάγετε τιμή στην παραπάνω πεδίο κειμένου"
              
               // [ self displayAlert:@"Παρακαλούμε, εισάγετε τιμή στην παραπάνω πεδίο κειμένου" :@"Παρακαλώ εισάγετε Αξία"];
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Παρακαλώ εισάγετε Αξία" message:@"Παρακαλούμε, εισάγετε τιμή στην παραπάνω πεδίο κειμένου" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [alert show];
//                [alert release];
            //    [self dismissViewControllerAnimated:YES completion:nil];
                _OTextField.displayError=YES;
              //  [[self presentingViewController]dismissViewControllerAnimated:YES completion:nil];
               // [_delegate dismissModalController:self.choiceController];
            }
            else {
                
                [_delegate PrescriptionAndSickFundSelectionController:self didSelectPrescription:_prescriptionString];
                [_delegate PrescriptionAndSickFundSelectionController:self didSelectionSickFund:_sickFundString didSelectionOtherSickFund:_OtherSickFundString];
                
                _prescriptionString = NULL;
                _sickFundString = NULL;
                
            }
            
            
            
        }
        
    }
    else {
        NSLog(@"Select both prescription and sickfund");
        
    //     [ self displayAlert:@"Παρακαλούμε, εισάγετε τιμή στην παραπάνω πεδίο κειμένου" :@"Παρακαλώ εισάγετε Αξία"];
    //[self displayAlert:[NSString stringWithFormat:@"Επιλέξτε Δύο Bubbles"]];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Κενό πεδίο" message:@"Επιλέξτε Δύο Bubbles" delegate:self cancelButtonTitle:@"ενημέρωση" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        
       
        
    }
    
}
- (void)dealloc {

    [super dealloc];
}
#pragma mark- Alert Popup
//Deepak_Carpenter : Added for Alert View
//added for alert display
-(void) displayAlert:(NSString*)message {
    if (self.choiceController != nil) {
        [_delegate dismissModalController:self.choiceController];
    }

    self.choiceController = [[MultipleChoiceViewController new] autorelease];
    [self presentModalViewController:self.choiceController animated:NO];
    // Ravi_Bukka: Added for localizing error messages
    _choiceController.title=@"Κενό πεδίο";
    _choiceController.message = NSLocalizedString(message,nil);
    _choiceController.messageFont = [UIFont boldSystemFontOfSize:32];
    _choiceController.cancelButtonTitle =@"ενημέρωση";
    _choiceController.otherButtonTitle = nil;
    _choiceController.eventBlock = ^(MultipleChoiceButton button){
        
        
        
        // Dismiss and release the controller
        [_delegate dismissModalController:self.choiceController];
        self.choiceController = nil;
        
        switch (button) {
            case kMultipleChoiceAccept: {
                
                break;
            }
            case kMultipleChoiceCancel: {
                // Cancel request
                break;
            }
            case kMultipleChoiceOther: {
                
                break;
            }
                
                
        }
        
    };
    
    // Present modal controller
    [_delegate presentModalController:self.choiceController];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    [self performSelector:@selector(textcheck) withObject:nil afterDelay:0.01];
    return YES;
}
-(void)textcheck{
    if ([_OTextField.text length]>0) {
        _OTextField.displayError=NO;
    }else
        _OTextField.displayError=YES;
}





@end

