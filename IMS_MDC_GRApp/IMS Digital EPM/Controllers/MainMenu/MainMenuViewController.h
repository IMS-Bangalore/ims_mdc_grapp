//
//  MainMenuViewController.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/10/12.
//
//

#import <UIKit/UIKit.h>
#import "MainMenuItem.h"
#import "MainMenuElementView.h"

//Kanchan: Added for CR#5 update functionality
@class VentanaPrincipalViewController;
@protocol MainMenuViewControllerDelegate;

@interface MainMenuViewController : UIViewController<UIGestureRecognizerDelegate, MainMenuElementViewDelegate>
{
    //Kanchan: Added for CR#5 update functionality
    VentanaPrincipalViewController *VentanaObj;

}
#pragma mark - Properties
@property (nonatomic, assign) id<MainMenuViewControllerDelegate> delegate;
@property (nonatomic, retain) NSArray* menuItems;
@property (nonatomic) NSInteger numberOfUnreadMessages;

#pragma mark - Public methods
- (void)refreshItemWithBadgeCount:(NSInteger)badgeCount andMenuItemId:(NSInteger)identifier;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet UIPanGestureRecognizer *panGestureRecognizer;
@property (retain, nonatomic) IBOutlet UIView *menuContentView;

#pragma mark - IBActions
- (IBAction)gestureRecognizerDidDetectEvent:(UIPanGestureRecognizer *)sender;

@end



@protocol MainMenuViewControllerDelegate <NSObject>

- (void)mainMenuController:(MainMenuViewController*)controller didSelectMenuItem:(MainMenuItem*)menuItem;

@end