//
//  MainMenuElementView.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/10/12.
//
//

#import "MainMenuElementView.h"

@implementation MainMenuElementView
@synthesize titleLabel = _titleLabel;
@synthesize selectedBackgroundImage = _selectedBackgroundImage;
@synthesize backgroundImage = _backgroundImage;
@synthesize highlightView = _highlightView;
@synthesize menuItem = _menuItem;
@synthesize buttonSelected = _buttonSelected;
@synthesize delegate = _delegate;

#pragma mark - Init and dealloc

- (void)dealloc {
    [_menuItem removeObserver:self forKeyPath:@"badgeCount"];
    [_menuItem release];
    [_titleLabel release];
    [_selectedBackgroundImage release];
    [_backgroundImage release];
    [_highlightView release];
    [_notificationBallView release];
    [_notificationNumberLabel release];
    [super dealloc];
}

#pragma mark - Private methods
- (void)reloadUI {
    
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"]) {
        
//Ravi_Bukka: added to adjust greek text
        _titleLabel.text = _menuItem.title;
        [_titleLabel setNumberOfLines:3];
        _titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:27];

    }
    else 
    _titleLabel.text = _menuItem.title;
    
}

- (void)bounceAnimation {
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    animation.values = [NSArray arrayWithObjects:
                        [NSNumber numberWithFloat:1.0],
                        [NSNumber numberWithFloat:0.95],
                        [NSNumber numberWithFloat:1.3],
                        [NSNumber numberWithFloat:0.95],
                        [NSNumber numberWithFloat:1.0],
                        [NSNumber numberWithFloat:1.0], nil];
    animation.duration = 0.6f;
    
    [_highlightView.layer addAnimation:animation forKey:@"bounce"];
}

#pragma mark - Public methods
- (void)setButtonSelected:(BOOL)buttonSelected {
    [self setButtonSelected:buttonSelected animated:NO];
}

- (void)setButtonSelected:(BOOL)buttonSelected animated:(BOOL)animated {
    _buttonSelected = buttonSelected;
    _highlightView.selected = buttonSelected;
}

- (void)setMenuItem:(MainMenuItem *)menuItem {
    [_menuItem removeObserver:self forKeyPath:@"badgeCount"];
    [menuItem addObserver:self forKeyPath:@"badgeCount" options:NSKeyValueObservingOptionNew context:nil];
    [menuItem retain];
    [_menuItem release];
    _menuItem = menuItem;
    DLog(@"-------Menu identifier: %@", _menuItem.description);
    [self reloadUI];
}


#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"badgeCount"]) {
        if (self.menuItem.badgeCount == 0) {
            self.notificationBallView.hidden = YES;
            DLog(@"--------------El menu item: %d no tiene notificaciones", self.menuItem.identifier);
        } else {
            self.notificationBallView.hidden = NO;
            self.notificationNumberLabel.text = [NSString stringWithFormat:@"%d", self.menuItem.badgeCount];
             DLog(@"--------------El menu item: %d tiene %d notificaciones", self.menuItem.identifier, self.menuItem.badgeCount);
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - IBActions
- (IBAction)buttonClicked:(id)sender {
    if ([_delegate mainMenuElementshouldAnimateClick:self]) {
        _selectedBackgroundImage.hidden = NO;
        _backgroundImage.hidden = YES;
        self.userInteractionEnabled = NO;
        
        [CATransaction begin];
        [CATransaction setCompletionBlock:^{
            [_delegate mainMenuElementClicked:self];
            _selectedBackgroundImage.hidden = YES;
            _backgroundImage.hidden = NO;
            self.userInteractionEnabled = YES;
        }];
        
        [self bounceAnimation];
        [CATransaction commit];
    }
    else {
        [_delegate mainMenuElementClicked:self];
    }
}

@end
