//
//  UIImage+NinePatch.h
//  FnacSocios
//
//  Created by Guillermo Gutiérrez on 13/04/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (NinePatch)

#pragma mark - Generic stretchable images
+ (UIImage*)stretchableImageNamed:(NSString*)imageName;
+ (UIImage*)stretchableImageNamed:(NSString*)imageName withEdgeInsets:(UIEdgeInsets)edgeInsets;

#pragma mark - 9Patch images
+ (id)ninePatchImageNamed:(NSString*)imageName;
- (id)ninePatchImage;
- (BOOL)isStretchable;

@end
