//
//  TCRequestParam.h
//  TwinCodersLibrary
//
//  Created by Alex Gutiérrez on 08/10/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCRequestParam : NSObject

extern NSString* const kRequestParamEmptyValue;

@property (readonly, getter = isComplex) bool complex;
@property (readonly, getter = isArray) bool array;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *value;
@property (nonatomic, strong) NSArray *params;

-(id) initWithKey:(NSString*)key andValue:(NSString*)value;
-(id) initWithKey:(NSString*)key andArrayValue:(NSArray*)value;
-(id) initWithKey:(NSString*)key andParams:(NSArray*)params;

+(TCRequestParam*) paramWithKey:(NSString*)key andValue:(NSString*)value;
+(TCRequestParam*) paramWithKey:(NSString*)key andArrayValue:(NSArray*)value;
+(TCRequestParam*) paramWithKey:(NSString*)key andParams:(NSArray*)params;
+(TCRequestParam*) paramWithKey:(NSString*)key andDictionary:(NSDictionary*)dictionary;

@end