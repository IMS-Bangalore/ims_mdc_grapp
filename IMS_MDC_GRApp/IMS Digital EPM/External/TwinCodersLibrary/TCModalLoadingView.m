//
//  TCModalLoadingView.m
//  TwinCodersLibrary
//
//  Created by Alex Guti on 22/06/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCModalLoadingView.h"
#import "UIView+TCSuperView.h"
#import "TCAlertView.h"

@interface TCModalLoadingView ()
@property (nonatomic, assign) CGFloat textOneLineHeight;
@property (nonatomic, assign) CGFloat containerVsLabelOffset;
@property (nonatomic, assign, getter=isCustomModal) BOOL customModal;
@property (nonatomic, strong) TCAlertView* alertView;
@end

@implementation TCModalLoadingView

#pragma mark - Private methods
- (void)adjustLabelsFrame {
    CGFloat textHeight = 0;
    if (_textLabel.text != nil) {
        CGSize titleSize = [_textLabel.text sizeWithFont:_textLabel.font constrainedToSize:CGSizeMake(_textLabel.frame.size.width, self.frame.size.height) lineBreakMode:_textLabel.lineBreakMode];
        textHeight = titleSize.height - [_textLabel.font lineHeight] + _textOneLineHeight;
    }
    
    CGRect popupFrame = _contentView.frame;
    popupFrame.size.height = textHeight + _containerVsLabelOffset;
    _contentView.frame = popupFrame;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _containerVsLabelOffset = _contentView.frame.size.height - _textLabel.frame.size.height;
    _textOneLineHeight = _textLabel.frame.size.height;
    self.customModal = YES;
}

- (id)init {
    if (( self = [super init] )) {
        self.customModal = NO;
    }
    return self;
}

#pragma mark Dialog methods

- (void)showLoadingViewWithText:(NSString*)text inView:(UIView*)view animated:(BOOL)animated {
    if ([self isShowing]) {
        TCLog(@"Already showing a loading dialog, canceling previous...");
        [self dismiss:NO];
    }
    self.showing = YES;
    
    if ([self isCustomModal]) {
        _textLabel.text = text;
        
        [self adjustLabelsFrame];
        // Show the view
        [view addSubview:self adjust:YES animated:animated];
    }
    else {
        self.alertView = [TCAlertView alertViewWithTitle:nil message:text cancelButtonTitle:nil];
        
        [_alertView show];
        
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        indicator.center = CGPointMake(_alertView.bounds.size.width / 2, _alertView.bounds.size.height - 45);
        [indicator startAnimating];
        [_alertView addSubview:indicator];
    }
}

- (void)dismiss:(BOOL)animated {
    if ([self isCustomModal]) {
        [self removeFromSuperviewAnimated:animated];
    }
    else {
        [_alertView dismissWithClickedButtonIndex:_alertView.cancelButtonIndex animated:animated];
    }
}

- (void)moveToView:(UIView *)view {
    if ([self isShowing]) {
        [self removeFromSuperviewAnimated:NO];
        [view addSubview:self adjust:YES animated:NO];
    }
}
@end
