//
//  TCTableViewCell.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 01/02/13.
//  Copyright (c) 2013 TwinCoders S.L. All rights reserved.
//

#import "TCTableViewCell.h"

@implementation TCTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    [self refreshStatusAnimated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    [self refreshStatusAnimated:animated];
}

- (void)refreshStatusAnimated:(BOOL)animated {
    BOOL highlighted = self.highlighted || (self.highlightOnSelected && self.selected);
    for (id element in self.highlightElements) {
        if ([element respondsToSelector:@selector(setHighlighted:animated:)]) {
            [element setHighlighted:highlighted animated:animated];
        }
        else if ([element respondsToSelector:@selector(setHighlighted:)]) {
            [element setHighlighted:highlighted];
        }
    }
}

@end
