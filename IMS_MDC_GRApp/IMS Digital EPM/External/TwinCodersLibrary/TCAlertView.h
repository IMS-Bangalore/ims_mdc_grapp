//
//  TCAlertView.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 07/09/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void (^TCAlertDismissBlock)(int buttonIndex);
typedef void (^TCAlertCancelBlock)();

/** @brief Block-enabled subclass of UIAlertView */
@interface TCAlertView : UIAlertView

+ (id) alertViewWithTitle:(NSString*) title
                            message:(NSString*) message
                  cancelButtonTitle:(NSString*) cancelButtonTitle
                  otherButtonTitles:(NSArray*) otherButtons
                          onDismiss:(TCAlertDismissBlock) dismissed
                           onCancel:(TCAlertCancelBlock) cancelled;

+ (id) alertViewWithTitle:(NSString*) title
                            message:(NSString*) message;

+ (id) alertViewWithTitle:(NSString*) title
                            message:(NSString*) message
                  cancelButtonTitle:(NSString*) cancelButtonTitle;

@end
