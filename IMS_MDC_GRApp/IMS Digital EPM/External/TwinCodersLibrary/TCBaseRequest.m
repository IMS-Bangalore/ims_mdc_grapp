//
//  TCBaseRequest.m
//  TwinCodersLibrary
//
//  Created by Alex Gutiérrez on 08/10/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCBaseRequest.h"
#import "TCRequestLauncher.h"
#import "TCRequestParam.h"
#import "ASIFormDataRequest.h"


// Error domain
static NSString* const kErrorDomain  = @"com.twincoders.TCBaseRequest";
static NSString* const kContentTypeHeaderKey = @"Content-Type";

@interface TCBaseRequest()

@property (nonatomic, strong) NSArray *endDelegateArray;
@property (nonatomic, strong, readwrite) NSString* requestId;

@end

@implementation TCBaseRequest

- (id)init {
    self = [super init];
    if (self) {
        _canceled = NO;
        _endDelegateArray = [[NSMutableArray alloc] init];
        _contentParams = [[NSMutableArray alloc] init];
        _name =  NSStringFromClass([self class]);
        _requestId = [self createID];
    }
    return self;
}

- (NSString*)createID {
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidStr = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
    CFRelease(uuid);
    return uuidStr;
}

- (void)notifyEndDelegates {
    for (id<TCRequestEndDelegate> endDelegate in _endDelegateArray) {
        [endDelegate requestDidFinish:self];
    }
}

- (void)start {
    self.canceled = NO;
    [_requestLauncher launchRequest:self];
}

- (void)cancel {
    [_requestLauncher cancelRequest:self];
    self.canceled = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self notifyEndDelegates];
    });
}

- (void)addRequestEndDelegate:(NSObject<TCRequestEndDelegate>*)endDelegate {
    if (![_endDelegateArray containsObject:endDelegate]) {
        [_endDelegateArray addObject:endDelegate];
    }
}

- (void)removeRequestEndDelegate:(NSObject<TCRequestEndDelegate>*)endDelegate {
    if ([_endDelegateArray containsObject:endDelegate]) {
        [_endDelegateArray removeObject:endDelegate];
    }
}

#pragma mark - ASIRequest Delegate
- (void)requestFinished:(ASIHTTPRequest *)request {
    // Perform operations in main thread and retaining self
    dispatch_async(dispatch_get_main_queue(), ^{
        [self notifyEndDelegates];
        [self onRequestFinished:request];
    });
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    // Perform operations in main thread and retaining self
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.isDummy) {
            // Notify delegates
            [self notifyEndDelegates];
            [self onRequestError:request];
        } else {
            [self requestFinished:request];
        }
    });
}

-(void)addParam:(TCRequestParam*)param {
    [_contentParams addObject:param];
}

-(void)addParam:(NSString*)paramValue forKey:(NSString*)paramKey {
    NSString* paramString = paramValue != nil ? paramValue : @"";
    [_contentParams addObject:[TCRequestParam paramWithKey:paramKey andValue:paramString]];
}

-(void)addNumberParam:(NSNumber*)paramValue forKey:(NSString*)paramKey {
    NSString* stringValue = nil;
    if (paramValue != nil) {
        stringValue = [NSString stringWithFormat:@"%i", paramValue.integerValue];
    }
    [self addParam:stringValue forKey:paramKey];
}

-(void)addDictionaryParam:(NSDictionary*)dictionary forKey:(NSString*)paramKey {
    [self addParam:[TCRequestParam paramWithKey:paramKey andDictionary:dictionary]];
}

+ (NSString*)errorDomain {
    return kErrorDomain;
}

#pragma mark - Request launch methods

-(ASIHTTPRequest*)createAsiRequest {
    NSURL* url = [NSURL URLWithString:self.url];
    TCLog(@"\nREQUEST: %@ (%@)", self.name, url);
    // Instance ASI Request with url
    ASIFormDataRequest* asiRequest = [[ASIFormDataRequest alloc] initWithURL: url];
    [asiRequest setStringEncoding:NSUTF8StringEncoding];
    asiRequest.requestMethod = [self stringFromMethod:self.requestMethod];
    // Set content type
    if (self.contentType != nil) {
        [asiRequest addRequestHeader:kContentTypeHeaderKey value:self.contentType];
    }

    return asiRequest;
}

-(NSString*) stringFromMethod:(TCRequestMethod)method {
    NSString* string;
    switch (method) {
        case kTCRequestMethodDELETE:
            string = @"DELETE";
            break;
        case kTCRequestMethodGET:
            string = @"GET";
            break;
        case kTCRequestMethodPOST:
            string = @"POST";
            break;
        case kTCRequestMethodPUT:
            string = @"PUT";
    }
    return string;
}

#pragma mark - Request interception methods

-(void)onRequestFinished:(ASIHTTPRequest *)request {
    
    if (![self isCanceled]) {
        // Use when fetching text data
        NSString *responseString = nil;
        if (self.encoding != 0) {
            responseString = [[NSString alloc] initWithData:request.responseData encoding:self.encoding];
        }
        if (responseString == nil) {
            responseString = [[NSString alloc] initWithData:request.responseData encoding:NSUTF8StringEncoding];
        }
        if (responseString == nil) {
            responseString = [[NSString alloc] initWithData:request.responseData encoding:NSASCIIStringEncoding];
        }
        TCLog(@"\nOUTPUT:\n%@", responseString);
        NSDictionary* responseDictionary = nil;
        if (responseString.length > 0) {
            responseDictionary = [self dictionaryForResponseString:responseString];
        }
        NSError* error = nil;
        NSDictionary* dictionary = nil;
        if (!self.isDummy) {
            dictionary = [self onProcessResponseDictionary:responseDictionary withError:&error];
        }
        
        if (error == nil) {
            self.onComplete(dictionary);
        } else {
            self.onError(error);
        }
    }
}

- (void)onRequestError:(ASIHTTPRequest *)request {
    TCLog(@"Request error: %@", request.error);
    _onError(request.error);
}

- (NSDictionary*)onProcessResponseDictionary:(NSDictionary*)response withError:(NSError**) error {
    return response;
}

-(NSDictionary*) dictionaryForResponseString:(NSString*)string {
    return nil;
}

-(NSString*) stringFromRequestParamsArray:(NSArray*)paramsArray {
    return nil;
}

@end