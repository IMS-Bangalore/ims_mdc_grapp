//
//  TCSerializableDict.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 01/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCSerializableDict.h"
#import "MARTNSObject.h"
#import "RTProperty.h"

#if ! __has_feature(objc_arc)
#error This file must be compiled with ARC.
#endif

static NSString* const CLASS_NAME = @"__class__";

@implementation TCSerializableDict

- (NSArray*)serializableFieldNames {
    // By default, serialize all the properties
    NSArray* properties = [[self class] rt_properties];
    NSMutableArray* array = [NSMutableArray arrayWithCapacity:properties.count];
    for (RTProperty* property in properties) {
        [array addObject:[property name]];
    }
    return [NSArray arrayWithArray:array];
}

- (id)marshalObject:(id)object {
    id serializedObject = object;
    
    // Recursive calls to toDictionary method
    if ([object isKindOfClass:[TCSerializableDict class]]) {
        serializedObject = [object toDictionary];
    }
    else if ([object isKindOfClass:[NSDate class]]) {
        serializedObject = [NSNumber numberWithLong:[(NSDate*)object timeIntervalSince1970]];
    }
    else if ([object isKindOfClass:[NSArray class]]) {
        NSMutableArray* array = [NSMutableArray arrayWithCapacity:[object count]];
        for (id element in object) {
            [array addObject:[self marshalObject:element]];
        }
        serializedObject = [NSArray arrayWithArray:array];
    }
    
    return serializedObject;
}

- (id)unmarshalObject:(id)object toClass:(Class)class {
    // If it's a NSDictionary, try to obtain the className from it
    if ([object isKindOfClass:[NSDictionary class]]) {
        NSString* className = [object objectForKey:CLASS_NAME];
        if (className != nil) {
            class = NSClassFromString(className);
        }
    }
    
    id unmarshalValue = nil;
    if (class == nil) {
        unmarshalValue = object;
    }
    else if (class != nil && object != nil) {
        if ([class isSubclassOfClass:[TCSerializableDict class]]) {
            if ([object isKindOfClass:[NSDictionary class]]) {
                unmarshalValue = [[class alloc] initFromDictionary:object];
            }
            else {
                TCLog(@"Expected NSDictionary, found '%@'", object);
            }
        }
        else if ([class isSubclassOfClass:[NSDate class]]) {
            if ([object isKindOfClass:[NSNumber class]]) {
                unmarshalValue = [NSDate dateWithTimeIntervalSince1970:[object longValue]];
            }
            else {
                TCLog(@"Expected NSNumber, found '%@'", object);
            }
        }
        else if ([class isSubclassOfClass:[NSArray class]]) {
            if ([object isKindOfClass:[NSArray class]]) {
                NSMutableArray* array = [NSMutableArray arrayWithCapacity:[object count]];
                for (id element in object) {
                    [array addObject: [self unmarshalObject:element toClass:nil]];
                }
                unmarshalValue = [[class alloc] initWithArray:array];
            }
            else {
                TCLog(@"Expected NSArray, found '%@'", object);
            }
        }
        else if ([class isSubclassOfClass:[NSString class]]) {
            unmarshalValue = object;
        }
        else if ([class isSubclassOfClass:[NSNumber class]]) {
            if ([object isKindOfClass:[NSNumber class]]) {
                unmarshalValue = object;
            }
            else if ([object isKindOfClass:[NSString class]]) {
                unmarshalValue = [NSNumber numberWithDouble:[object doubleValue]];
            }
        }
        else {
            TCLog(@"Unknown type %@, using default value %@", NSStringFromClass(class), object);
            unmarshalValue = object;
        }
    }
    return unmarshalValue;
}

- (NSDictionary*)toDictionary {
    NSArray* fields = self.serializableFieldNames;
    
    NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithCapacity:fields.count];
    for (NSString* fieldName in fields) {
        id value = [self marshalObject:[self valueForKey:fieldName]];
        [dict setValue:value forKey:fieldName];
    }
    
    [dict setObject:NSStringFromClass([self class]) forKey:CLASS_NAME];
    
    return dict;
}

- (NSString*)cleanTypeEncoding:(NSString*)typeEncoding {
    NSString* result = typeEncoding;
    if ([typeEncoding hasPrefix:@"@\""] && [typeEncoding hasSuffix:@"\""]) {
        result = [typeEncoding substringWithRange:NSMakeRange(2, typeEncoding.length - 3)];
    }
    return result;
}

- (id)initFromDictionary:(NSDictionary*)dictionary {
    if (( self = [super init] )) {
        for (NSString* fieldName in self.serializableFieldNames) {
            id value = [dictionary objectForKey:fieldName];
            
            if (value != nil) {
                RTProperty* prop = [[self class] rt_propertyForName:fieldName];
                NSString* typeEncoding = [self cleanTypeEncoding:[prop typeEncoding]];
                
                Class class = NSClassFromString(typeEncoding);
            
                id unmarshalVaue = [self unmarshalObject:value toClass:class];
                
                [self setValue:unmarshalVaue forKey:fieldName];
            }
        }
    }
    return self;
}

@end
