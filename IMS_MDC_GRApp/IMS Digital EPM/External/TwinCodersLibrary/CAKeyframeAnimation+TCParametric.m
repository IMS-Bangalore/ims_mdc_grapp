//
//  CAKeyframeAnimation+TCParametric.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 10/10/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "CAKeyframeAnimation+TCParametric.h"

static const NSInteger kDefaultStepsValue = 100;

@implementation CAKeyframeAnimation (TCParametric)

+ (id)animationWithKeyPath:(NSString *)path function:(TCKeyframeParametricBlock)block fromValue:(double)fromValue toValue:(double)toValue {
    return [self animationWithKeyPath:path function:block fromValue:fromValue toValue:toValue steps:kDefaultStepsValue];
}

+ (id)animationWithKeyPath:(NSString *)path
                  function:(TCKeyframeParametricBlock)block
                 fromValue:(double)fromValue
                   toValue:(double)toValue
                     steps:(NSInteger)steps {
    // get a keyframe animation to set up
    CAKeyframeAnimation *animation =
    [CAKeyframeAnimation animationWithKeyPath:path];
    // break the time into steps
    //  (the more steps, the smoother the animation)
    NSMutableArray *values = [NSMutableArray arrayWithCapacity:steps];
    double time = 0.0;
    double timeStep = 1.0 / (double)(steps - 1);
    for(NSUInteger i = 0; i < steps; i++) {
        double value = fromValue + (block(time) * (toValue - fromValue));
        [values addObject:[NSNumber numberWithDouble:value]];
        time += timeStep;
    }
    // we want linear animation between keyframes, with equal time steps
    animation.calculationMode = kCAAnimationLinear;
    // set keyframes and we're done
    [animation setValues:values];
    
    return animation;
}

+ (id)animationWithKeyPath:(NSString *)path
                  function:(TCKeyframeValueParametricBlock)block {
    return [self animationWithKeyPath:path function:block steps:kDefaultStepsValue];
}

+ (id)animationWithKeyPath:(NSString *)path
                  function:(TCKeyframeValueParametricBlock)block
                     steps:(NSInteger)steps {
    // get a keyframe animation to set up
    CAKeyframeAnimation *animation =
    [CAKeyframeAnimation animationWithKeyPath:path];
    // break the time into steps
    //  (the more steps, the smoother the animation)
    NSMutableArray *values = [NSMutableArray arrayWithCapacity:steps];
    double time = 0.0;
    double timeStep = 1.0 / (double)(steps - 1);
    for (NSUInteger i = 0; i < steps; i++) {
        NSValue* value = block(time);
        [values addObject:value];
        time += timeStep;
    }
    // we want linear animation between keyframes, with equal time steps
    animation.calculationMode = kCAAnimationLinear;
    // set keyframes and we're done
    [animation setValues:values];
    
    return animation;
}

@end
