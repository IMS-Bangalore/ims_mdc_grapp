//
//  TCCustomNavigationBar.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 13/06/11.
//  Copyright TwinCoders All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TCCustomNavigationBar : UINavigationBar

@property(nonatomic, strong) UIImage* customImage;

@end
