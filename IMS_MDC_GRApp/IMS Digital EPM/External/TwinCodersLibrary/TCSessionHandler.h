//
//  TCSessionHandler.h
//  TwinCodersLibrary
//
//  Created by Alex Gutiérrez on 11/10/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
@class TCSessionHandler;
typedef void(^TCSessionKeepAliveBlock)(TCSessionHandler* sessionHandler);

@protocol SessionExpiredDelegate <NSObject>

-(void) sessionExpired;

@end

@interface TCSessionHandler : NSObject

/** @brief Duration of the session in seconds before timeout */
@property NSInteger sessionDurationInSeconds;
/** @brief Minimum elapsed time between keep-alive requests */
@property NSInteger keepAliveMarginInSeconds;
/** @brief Block that will be executed to keep session opened with server (if necessary) */
@property (nonatomic, copy) TCSessionKeepAliveBlock keepAliveBlock;
/** @brief Token of the active session, to be included in requests */
@property (nonatomic, strong) NSString* sessionToken;
/** @brief Returns YES if there is a session active */
@property (nonatomic, readonly, getter=isSessionActive) BOOL sessionActive;

#pragma mark - Session methods
-(void) startSession;
-(void) keepAliveSession;
-(void) closeSession;
-(void) awakeFromDeepSleep;

-(void) addSessionExpiredDelegate:(id<SessionExpiredDelegate>) sessionExpiredDelegate;
-(void) removeSessionExpiredDelegate:(id<SessionExpiredDelegate>) sessionExpiredDelegate;

#pragma mark - User info methods
-(void) setUserInfoValue:(id)value forKey:(NSString *)key;
-(id) userInfoValueForKey:(NSString *)key;

@end