//
//  TCParentViewController.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez Doral on 10/01/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCParentViewController.h"
#import "UIView+TCNIBLoader.h"
#import "UIView+TCSuperView.h"
#import "TCAlertView.h"

#pragma mark - ParentViewController private interface
@interface TCParentViewController()
@property (strong, nonatomic) UIView* mainView; // Used to store the main view while presenting an alternative orientation view
@property (assign, nonatomic, getter = isModalDialog) BOOL modalDialog;
@property (nonatomic, assign, getter = isModalInitialized) BOOL modalInitialized;
@end

#pragma mark - ParentViewController implementation
@implementation TCParentViewController

#pragma mark - Init and dealloc
- (void)dealloc {
    // Stop listening for orientation changes
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    [_modalAlertView removeFromSuperview];
    [_modalLoadingView removeFromSuperview];
}

#pragma mark - Private class methods
static NSString* _genericModalAlertNibName = nil;
+ (void)setGenericModalAlertNibName:(NSString*)alertNibName {
    _genericModalAlertNibName = alertNibName;
}

static NSString* _genericModalLoadingNibName = nil;
+ (void)setGenericModalLoadingNibName:(NSString*)loadingNibName {
    _genericModalLoadingNibName = loadingNibName;
}

#pragma mark - Private methods
- (void)updateOrientationController {
    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    // First step: hide alternative views that should not be shown anymore
    if (UIDeviceOrientationIsLandscape(deviceOrientation) && self.showingPortraitController) {
        // Dismiss portrait controller
        if ([self respondsToSelector:@selector(willDismissAlternativePortraitController)]) {
            [self willDismissAlternativePortraitController];
        }
        [self.portraitController dismissViewControllerAnimated:YES completion:nil];
    }
    if (UIDeviceOrientationIsPortrait(deviceOrientation) && self.showingLandscapeController) {
        // Dismiss landscape controller
        if ([self respondsToSelector:@selector(willDismissAlternativeLandscapeController)]) {
            [self willDismissAlternativeLandscapeController];
        }
        [self.landscapeController dismissViewControllerAnimated:YES completion:nil];
    }
    
    // Second step: present alternative views that should be shown
    if (UIDeviceOrientationIsLandscape(deviceOrientation) && self.landscapeController != nil && !self.showingLandscapeController) {
        // Show landscape controller
        if ([self respondsToSelector:@selector(willPresentAlternativeLandscapeController)]) {
            [self willPresentAlternativeLandscapeController];
        }
        [self presentViewController:self.landscapeController animated:YES completion:nil];
    }
    if (UIDeviceOrientationIsPortrait(deviceOrientation) && self.portraitController != nil && !self.showingPortraitController) {
        // Show landscape controller
        if ([self respondsToSelector:@selector(willPresentAlternativePortraitController)]) {
            [self willPresentAlternativePortraitController];
        }
        [self presentViewController:self.portraitController animated:YES completion:nil];
    }
}

- (void)updateOrientationViewTo:(UIInterfaceOrientation)interfaceOrientation withDuration:(NSTimeInterval)duration {
    
    UIView* newView = nil;
    // Landscape orientation
    if (UIInterfaceOrientationIsLandscape(interfaceOrientation)) {
        // Notify portrait view dismiss
        if (self.showingPortraitView && [self respondsToSelector:@selector(willDismissAlternativePortraitView)]) {
            [self willDismissAlternativePortraitView];
        }
        // Set alternative landscape view
        if (self.landscapeView != nil) {
            if ([self respondsToSelector:@selector(willPresentAlternativeLandscapeView)]) {
                [self willPresentAlternativeLandscapeView];
            }
            newView = self.landscapeView;
        }
        // Restore main view if needed
        else if (self.showingPortraitView) {
            newView = self.mainView;
        }
    }
    // Portrait orientation
    else {
        // Notify landscape view dismiss
        if (self.showingLandscapeView && [self respondsToSelector:@selector(willDismissAlternativeLandscapeView)]) {
            [self willDismissAlternativeLandscapeView];
        }
        // Set alternative portrait view
        if (self.portraitView != nil) {
            if ([self respondsToSelector:@selector(willPresentAlternativePortraitView)]) {
                [self willPresentAlternativePortraitView];
            }
            newView = self.portraitView;
        }
        // Restore main view if needed
        else if (self.showingLandscapeView) {
            newView = self.mainView;
        }
    }
    
    // Animate the view change
    if (newView != nil && newView != self.view) {
        UIViewAnimationOptions options = UIViewAnimationOptionTransitionCrossDissolve;
        
        // iOS version prior to 5.0 won't show this 'cross dissolve' animation, use another
        if (SYSTEM_VERSION_LESS_THAN(@"5.0")) {
            options = UIViewAnimationOptionTransitionFlipFromLeft;
        }
        
        [UIView transitionFromView:self.view 
                            toView:newView 
                          duration:duration
                           options:options
                        completion:nil];
        
        self.view = newView;
        
        // Update the parent view of the modal alert
        if (![self isModalDialog]) {
            if (_modalLoadingView.isShowing) {
                [_modalLoadingView moveToView:newView];
            } else if (_modalAlertView.isShowing) {
                [_modalAlertView moveToView:newView];
            }
        }
    }
    
}

- (void)updateOrientationViews {
    [self updateOrientationViewTo:self.interfaceOrientation withDuration:0.0];
}

#pragma mark - Modal alerts initialization
- (void)initializeModals {
    if (![self isModalInitialized]) {
        @synchronized (self) {
            if (![self isModalInitialized]) {
                if (_modalAlertView == nil) {
                    if (_genericModalAlertNibName != nil) {
                        self.modalAlertView = [TCModalAlertView loadFromNIB:_genericModalAlertNibName];
                    }
                    else {
                        self.modalAlertView = [TCModalAlertView loadFromNIB:nil silentError:YES];
                    }
                    if (_modalAlertView == nil) {
                        self.modalAlertView = [[TCModalAlertView alloc] init];
                    }
                }
                
                if (_modalLoadingView == nil) {
                    if (_genericModalLoadingNibName != nil) {
                        self.modalLoadingView = [TCModalLoadingView loadFromNIB:_genericModalLoadingNibName];
                    }
                    else {
                        self.modalLoadingView = [TCModalLoadingView loadFromNIB:nil silentError:YES];
                    }
                    if (_modalLoadingView == nil) {
                        self.modalLoadingView = [[TCModalLoadingView alloc] init];
                    }
                }
            }
        }
        self.modalInitialized = YES;
    }
}

#pragma mark - Orientation change events
- (void)orientationChanged:(NSNotification*)notification {
    [self performSelector:@selector(updateOrientationController) withObject:nil afterDelay:0];
}

#pragma mark - Public methods
- (BOOL)isShowingPortraitController {
    return self.portraitController.view.superview != nil;
}

- (BOOL)isShowingLandscapeController {
    return self.landscapeController.view.superview != nil;
}

- (BOOL)isShowingPortraitView {
    return self.portraitView.superview != nil;
}

- (BOOL)isShowingLandscapeView {
    return self.landscapeView.superview != nil;
}

- (TCModalAlertView *)modalAlertView {
    [self initializeModals];
    return _modalAlertView;
}

- (TCModalLoadingView *)modalLoadingView {
    [self initializeModals];
    return _modalLoadingView; 
}

- (UIView*) parentViewForDialogModal:(BOOL)modal {
    UIView* parentView = self.view;
    
    if (modal) {
        parentView = [[[[UIApplication sharedApplication] keyWindow] rootViewController] view];
    }
    else if (_landscapeView && [self isShowingLandscapeView]) {
        parentView = _landscapeView;
    }
    else if (_portraitView && [self isShowingPortraitView]) {
        parentView = _portraitView;
    }
    
    return parentView;
}

- (void)showAlertWithTitle:(NSString*)title
               description:(NSString*)description
         acceptButtonTitle:(NSString*)okButtonTitle
         cancelButtonTitle:(NSString*)cancelButtonTitle
           alertAppearance:(TCAlertAppearance)alertAppearance
                     modal:(BOOL)modal
                  animated:(BOOL)animated
                   onEvent:(TCAlertEventBlock)onEvent {
    // Dismiss loading view if it's being displayed
    if (_modalLoadingView != nil && _modalLoadingView.isShowing) {
        [_modalLoadingView dismiss:animated];
    }
    
    self.modalDialog = modal;
    
    UIView* parentView = [self parentViewForDialogModal:modal];
    
    [self.modalAlertView showAlertWithTitle:title description:description acceptButtonTitle:okButtonTitle cancelButtonTitle:cancelButtonTitle alertAppearance:alertAppearance inView:parentView animated:animated onEvent:onEvent];
}

- (void)showAlertWithTitle:(NSString*)title
               description:(NSString*)description
         acceptButtonTitle:(NSString*)okButtonTitle
         cancelButtonTitle:(NSString*)cancelButtonTitle
                      icon:(UIImage*)iconImage
                     modal:(BOOL)modal
                  animated:(BOOL)animated
                   onEvent:(TCAlertEventBlock)onEvent {
    
    // Dismiss loading view if it's being displayed
    if (_modalLoadingView != nil && _modalLoadingView.isShowing) {
        [_modalLoadingView dismiss:animated];
    }
    
    self.modalDialog = modal;
    
    UIView* parentView = [self parentViewForDialogModal:modal];
    
    [self.modalAlertView showAlertWithTitle:title description:description acceptButtonTitle:okButtonTitle cancelButtonTitle:cancelButtonTitle icon:iconImage inView:parentView animated:animated onEvent:onEvent];
    
}

- (void) showLoadingViewWithText:(NSString*)text
                           modal:(BOOL)modal
                        animated:(BOOL)animated {
    // Dismiss loading view if it's being displayed
    if (_modalAlertView != nil && _modalAlertView.isShowing) {
        [_modalAlertView dismiss:animated];
    }
    
    self.modalDialog = modal;
    
    UIView* parentView = [self parentViewForDialogModal:modal];
    
    [[self modalLoadingView] showLoadingViewWithText:text inView:parentView animated:animated];
    
}

- (void) showModalErrorWithMessage:(NSString*)message title:(NSString*)title buttonText:(NSString*)buttonText {
    [self showAlertWithTitle:title description:message acceptButtonTitle:buttonText cancelButtonTitle:nil alertAppearance:kTCAlertAppearanceError modal:YES animated:YES onEvent:nil];
}


- (void) dismissLoadingViewAnimated:(BOOL)animated {
    [_modalLoadingView dismiss:animated];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mainView = self.view;
    
    // If the landscape or portrait alternative view controllers are set, register for orientation change notifications
    if (self.landscapeController != nil || self.portraitController != nil) {
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
        self.landscapeController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        self.landscapeController.modalPresentationStyle = UIModalPresentationCurrentContext;
        self.portraitController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        self.portraitController.modalPresentationStyle = UIModalPresentationCurrentContext;
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
    [_modalAlertView removeFromSuperview];
    [_modalLoadingView removeFromSuperview];
    self.landscapeView = nil;
    self.portraitView = nil;
    self.mainView = nil;
    self.modalAlertView = nil;
    self.modalLoadingView = nil;
    self.modalInitialized = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // If the landscape or portrait alternative view controllers update them
    if (self.landscapeController != nil || self.portraitController != nil) {
        [self updateOrientationController];
    }
    
    [self updateOrientationViews];
}

#pragma mark - Rotation notifications
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateOrientationViewTo:toInterfaceOrientation withDuration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

@end
