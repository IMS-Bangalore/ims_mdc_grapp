//
//  TCNinePatchImageView.h
//  FnacSocios
//
//  Created by Guillermo Gutiérrez on 13/04/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCNinePatchImageView : UIImageView

@end
