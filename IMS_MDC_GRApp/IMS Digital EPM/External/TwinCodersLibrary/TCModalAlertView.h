//
//  TCModalAlertView.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 14/05/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @brief Event block to indicate that an alert button has been clicked */
typedef void (^TCAlertEventBlock)(BOOL accepted);

typedef enum {
    kTCAlertAppearanceInfo,
    kTCAlertAppearanceError
} TCAlertAppearance;

@interface TCModalAlertView : UIView

@property (nonatomic, assign, getter = isShowing) BOOL showing;

#pragma mark - IBOutlets
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIButton *acceptButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIImageView *infoIconImageView;
@property (strong, nonatomic) IBOutlet UIImageView *errorIconImageView;
@property (strong, nonatomic) IBOutlet UIImageView *customIconImageView;
@property (strong, nonatomic) IBOutlet UIView *singleButtonPlaceholderView;
@property (strong, nonatomic) IBOutlet UIView *labelsPlaceholder;

#pragma mark - IBActions
- (IBAction)buttonClicked:(id)sender;

#pragma mark - Show and dismiss
- (void)showAlertWithTitle:(NSString*)title
               description:(NSString*)description
         acceptButtonTitle:(NSString*)okButtonTitle
         cancelButtonTitle:(NSString*)cancelButtonTitle
           alertAppearance:(TCAlertAppearance)alertAppearance
                    inView:(UIView*)view
                  animated:(BOOL)animated
                   onEvent:(TCAlertEventBlock)onEvent;

- (void)showAlertWithTitle:(NSString*)title
               description:(NSString*)description
         acceptButtonTitle:(NSString*)okButtonTitle
         cancelButtonTitle:(NSString*)cancelButtonTitle
                      icon:(UIImage*)iconImage
                    inView:(UIView*)view
                  animated:(BOOL)animated
                   onEvent:(TCAlertEventBlock)onEvent;

- (void)dismiss:(BOOL)animated;

// Moves the current alert to the view
- (void)moveToView:(UIView*)view;

@end
