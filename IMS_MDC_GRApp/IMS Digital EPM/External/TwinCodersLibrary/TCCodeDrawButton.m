//
//  TCCodeDrawButton.m
//  SantanderChile
//
//  Created by Guillermo Gutiérrez on 02/11/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCCodeDrawButton.h"
#import "UIImage+BBlock.h"
#import <QuartzCore/QuartzCore.h>

@interface TCCodeDrawButton()
@property (nonatomic, strong) UIImageView* contentImageView;
@end

@implementation TCCodeDrawButton

#pragma mark - Init
- (id)init {
    self = [super init];
    if (self) {
        [self initializeObject];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initializeObject];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initializeObject];
    }
    return self;
}

- (void)initializeObject {
    self.backgroundColor = [UIColor clearColor];
    [self createImageView];
    [self addSubview:self.contentImageView];
    [self sendSubviewToBack:self.contentImageView];
}

#pragma mark - Public methods
- (CGSize)imageSizeFromViewSize:(CGSize)viewSize {
    return viewSize;
}

- (void)redrawImage {
    if (self.superview != nil && self.contentImageView != nil && self.drawBlock != nil) {
        [self forceRedrawImage];
    }
}

- (NSString*)imageIdentifier {
    NSMutableString* imageIdentifier = [NSMutableString stringWithCapacity:128];
    [imageIdentifier appendString:NSStringFromClass([self class])];
    if ([self redrawOnSizeChange]) {
        [imageIdentifier appendFormat:@"-%@", NSStringFromCGSize(self.frame.size)];
    }
    if ([self redrawOnStatusChange]) {
        [imageIdentifier appendFormat:@"-state(%d)", self.state];
    }
    return imageIdentifier;
}

- (CGSize)imageSize {
    return self.contentImageView.frame.size;
}

- (void)didMoveToSuperview {
    [super didMoveToSuperview];
    [self redrawImage];
}

#pragma mark - Property accessors
- (NSString *)reuseIdentifier {
    if (_reuseIdentifier == nil) {
        return NSStringFromClass([self class]);
        
    }
    return _reuseIdentifier;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    if ([self redrawOnStatusChange]) {
        [self redrawImage];
    }
}

-(void)setEnabled:(BOOL)enabled animated:(BOOL)animated {
    [super setEnabled:enabled];
    if ([self redrawOnStatusChange]) {
        if (animated) {
            CATransition* transition = [[CATransition alloc] init];
            transition.type = kCATransitionFade;
            transition.duration = 0.2;
            [self.layer removeAllAnimations];
            [self.layer addAnimation:transition forKey:@"Fade-in-out"];
        }
        [self redrawImage];
    }
}

- (void)setEnabled:(BOOL)enabled {
    [self setEnabled:enabled animated:NO];
}


- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    if ([self redrawOnStatusChange]) {
        [self redrawImage];
    }
}

- (void)setFrame:(CGRect)frame {
    CGSize previousSize = self.frame.size;
    [super setFrame:frame];
    if (!CGSizeEqualToSize(frame.size, previousSize)) {
        [self layoutImageView];
        if ([self redrawOnSizeChange]) {
            [self redrawImage];
        }
    }
}

#pragma mark - Private methods
- (void)createImageView {
    self.contentImageView = [[UIImageView alloc] init];
    _contentImageView.contentMode = UIViewContentModeScaleToFill;
    _contentImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |
    UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleTopMargin |
    UIViewAutoresizingFlexibleHeight |
    UIViewAutoresizingFlexibleBottomMargin;
    
    [self layoutImageView];
}

- (void)layoutImageView {
    CGSize size = [self imageSizeFromViewSize:self.frame.size];
    CGRect frame = CGRectMake((self.frame.size.width - size.width) / 2,
                              (self.frame.size.height - size.height) / 2,
                              size.width,
                              size.height);
    _contentImageView.frame = frame;
}

- (void)forceRedrawImage {
    UIImage* image = [UIImage imageWithIdentifier:[self imageIdentifier]
                                           opaque:self.opaque
                                          forSize:self.imageSize
                                  andDrawingBlock:^{
                                      self.drawBlock(self);
                                  }];
    
    self.contentImageView.image = image;
}

@end
