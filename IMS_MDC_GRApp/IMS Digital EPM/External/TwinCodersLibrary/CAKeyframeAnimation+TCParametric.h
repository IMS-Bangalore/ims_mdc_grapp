//
//  CAKeyframeAnimation+TCParametric.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 10/10/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CAKeyframeAnimation (TCParametric)

// this should be a function that takes a time value between
//  0.0 and 1.0 (where 0.0 is the beginning of the animation
//  and 1.0 is the end) and returns a scale factor where 0.0
//  would produce the starting value and 1.0 would produce the
//  ending value
typedef double (^TCKeyframeParametricBlock)(double);

+ (id)animationWithKeyPath:(NSString *)path
                  function:(TCKeyframeParametricBlock)block
                 fromValue:(double)fromValue
                   toValue:(double)toValue;

+ (id)animationWithKeyPath:(NSString *)path
                  function:(TCKeyframeParametricBlock)block
                 fromValue:(double)fromValue
                   toValue:(double)toValue
                     steps:(NSInteger)steps;

// this should be a function that takes a time value between
//  0.0 and 1.0 (where 0.0 is the beginning of the animation
//  and 1.0 is the end) and returns the value to be set at
//  that specific time to the keypath
typedef NSValue* (^TCKeyframeValueParametricBlock)(double);

+ (id)animationWithKeyPath:(NSString *)path
                  function:(TCKeyframeValueParametricBlock)block;

+ (id)animationWithKeyPath:(NSString *)path
                  function:(TCKeyframeValueParametricBlock)block
                     steps:(NSInteger)steps;

@end
