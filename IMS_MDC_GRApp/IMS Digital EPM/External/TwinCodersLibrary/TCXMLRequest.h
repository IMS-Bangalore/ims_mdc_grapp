//
//  TCXMLRequest.h
//  TwinCodersLibrary
//
//  Created by Alex Gutiérrez on 09/10/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCBaseRequest.h"

@interface TCXMLRequest : TCBaseRequest

@property (nonatomic, strong) NSString* paramsSchemaName;

/** @brief Defines if the request should obtain the XML atributes from the response */
@property (nonatomic) BOOL obtainXMLAtributes;

#pragma mark - Request launch

/** @brief Returns the body content for the given request */
-(NSString*) createBodyContent;

#pragma mark - Request response

/** @brief Returns a string by cleaning trash info from the given key */
-(NSString*) stringByCleaningKey:(NSString*)key;

/** @brief Returns a string by cleaning trash info from the given value */
-(NSString*) stringByCleaningValue:(NSString*)value;

@end
