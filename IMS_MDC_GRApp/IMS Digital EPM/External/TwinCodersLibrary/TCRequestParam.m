//
//  TCRequestParam.m
//  TwinCodersLibrary
//
//  Created by Alex Gutiérrez on 08/10/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCRequestParam.h"

@implementation TCRequestParam

NSString* const kRequestParamEmptyValue = @"";

-(id) initWithKey:(NSString*)key andValue:(NSString*)value {
    self = [super init];
    if (self) {
        self.key = key;
        if (value == nil) {
            self.value = kRequestParamEmptyValue;
        } else {
            self.value = value;
        }
        _complex = NO;
    }
    return self;
}

-(id) initWithKey:(NSString*)key andParams:(NSArray*)params {
    self = [super init];
    if (self) {
        self.key = key;
        self.params = params;
        _complex = YES;
    }
    return self;
}

-(id) initWithKey:(NSString*)key andArrayValue:(NSArray*)values {
    self = [super init];
    if (self) {
        self.key = key;
        self.params = values;
        _array = YES;
    }
    return self;
}

+(TCRequestParam*) paramWithKey:(NSString*)key andValue:(NSString*)value {
    return [[TCRequestParam alloc] initWithKey:key andValue:value];
}

+(TCRequestParam*) paramWithKey:(NSString*)key andParams:(NSArray*)params {
    return [[TCRequestParam alloc] initWithKey:key andParams:params];
}
+(TCRequestParam*) paramWithKey:(NSString*)key andArrayValue:(NSArray*)value {
    return [[TCRequestParam alloc] initWithKey:key andArrayValue:value];
}

+(TCRequestParam*) paramWithKey:(NSString*)key andDictionary:(NSDictionary*)dictionary {
    NSMutableArray* innerParams = [[NSMutableArray alloc] init];
    // Iterate dictionary items
    for (NSString* dictKey in dictionary.allKeys) {
        TCRequestParam* innerParam = nil;
        // Ckeck value
        id value = [dictionary objectForKey:dictKey];
        if (value == nil) {
            innerParam = [TCRequestParam paramWithKey:dictKey andValue:kRequestParamEmptyValue];
        } else if ([value isKindOfClass:[NSDictionary class]]) {
            innerParam = [TCRequestParam paramWithKey:dictKey andDictionary:value];
        } else {
            innerParam = [TCRequestParam paramWithKey:dictKey andValue:value];
        }
        
        // Include param in params array
        if (innerParam != nil) {
            [innerParams addObject:innerParam];
        }
    }
    TCRequestParam* param = [TCRequestParam paramWithKey:key andParams:innerParams];
    return param;
}

-(NSString*)description {
    if (_complex) {
        NSString* paramsDesc = [NSString string];
        for (TCRequestParam* param in _params) {
            paramsDesc = [paramsDesc stringByAppendingString:param.description];
        }
        return [NSString stringWithFormat:@"%@ = {%@}; ", _key, paramsDesc];
    } else {
        return [NSString stringWithFormat:@"%@ = %@; ", _key, _value];
    }
}

@end