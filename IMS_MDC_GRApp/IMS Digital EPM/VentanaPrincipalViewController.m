//
//  VentanaPrincipalViewController.m
//  IMS Digital EPM
//
//  Created by Ricardo Berzal on 23/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import "AppDelegate.h"
//#import <TwinCodersLibrary/Reachability.h>
#import "VentanaPrincipalViewController.h"
#import "EntityFactory.h"
//Deepak_Carpenter : added new reachbility Class
#import "Reachability.h"

//Deepak Carpenter added for display alert
@interface VentanaPrincipalViewController()
@property (nonatomic, retain) MultipleChoiceViewController* choiceController;
@end
///
@implementation VentanaPrincipalViewController
@synthesize LedConexion;
@synthesize labelEstadoConexion;
@synthesize labelRecordatorioEntrega;
@synthesize TapaModal;
@synthesize BotonVolverMenu;
@synthesize BotonSalirLogin;
@synthesize VentanaTop;
@synthesize internetActive;

- (id)initWithDelegate:(AppDelegate*)delegate
{
    self = [super initWithNibName:@"VentanaPrincipalViewController" bundle:nil];
    if (self) {
        _delegate = delegate;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Metodos propios

- (void) CompruebaConexion {
    
    //Reachability
    // check for internet connection
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    internetReachable = [[Reachability reachabilityForInternetConnection] retain];
    [internetReachable startNotifier];
    
    // check if a pathway to a random host exists
    hostReachable = [[Reachability reachabilityWithHostName: @"https://ims.lumatagroup.com"] retain];
    [hostReachable startNotifier];
    // now patiently wait for the notification  
}

#pragma mark - Eventos Respuestas Asíncronas

- (void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    
    {
        case NotReachable:
        {
            DLog(@"The internet is down.");
            internetActive = NO;
//            labelEstadoConexion.text=@"No hay conexión";
            labelEstadoConexion.text=NSLocalizedString(@"NO_CONNECTION", nil);
            LedConexion.highlighted=NO;
            [_delegate setHayConexionInternet:NO];
            
//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
            
            if ([[NSUserDefaults standardUserDefaults] integerForKey: @"ExtendTheGrace"]== 1) {
                // if ([[NSUserDefaults standardUserDefaults] valueForKey: @"ExtendTheGrace"] ) {
                [_delegate setNeedToExtendGracePeriod:YES];
                [_delegate setIsInCollaborationPeriod:YES];
            }
            else {
                
                [_delegate setNeedToExtendGracePeriod:NO];
                //[_delegate setIsInCollaborationPeriod:NO];
                
            }
            break;
            
        }
        case ReachableViaWiFi:
        {
            DLog(@"The internet is working via WIFI.");
            internetActive = YES;
//            labelEstadoConexion.text=@"Conectado";
            
//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
            //   if ([[NSUserDefaults standardUserDefaults] valueForKey: @"ExtendTheGrace"] ) {
            
            if ([[NSUserDefaults standardUserDefaults] integerForKey: @"ExtendTheGrace"]== 1) {
                [_delegate setNeedToExtendGracePeriod:YES];
                //    [_delegate setIsInCollaborationPeriod:YES];
            }
            else {
                
                [_delegate setNeedToExtendGracePeriod:NO];
                //   [_delegate setIsInCollaborationPeriod:NO];
                
            }
             [self performSelectorInBackground:@selector(checkForUpdate) withObject:nil];labelEstadoConexion.text=NSLocalizedString(@"CONNECTED", nil);
            LedConexion.highlighted=YES;
            [_delegate setHayConexionInternet:YES];
            
            //Deepak_Carpenter : Added for CR#6 send chat message when log button clicked
            if ([[NSUserDefaults standardUserDefaults]valueForKey:@"chatMessage"]!=nil) {
                ChatViewController *chatVC=[[ChatViewController alloc]init];
                [chatVC sendconfirmationMessgae];
                [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"chatMessage"];
            }

            break;
            
        }
        case ReachableViaWWAN:
        {
            DLog(@"The internet is working via WWAN.");
            internetActive = YES;
//            labelEstadoConexion.text=@"Conectado";
            
//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
            if ([[NSUserDefaults standardUserDefaults] integerForKey: @"ExtendTheGrace"]== 1) {
                [_delegate setNeedToExtendGracePeriod:YES];
                //  [_delegate setIsInCollaborationPeriod:YES];
            }
            else {
                
                [_delegate setNeedToExtendGracePeriod:NO];
                //   [_delegate setIsInCollaborationPeriod:NO];
                
            }
             [self performSelectorInBackground:@selector(checkForUpdate) withObject:nil];labelEstadoConexion.text=NSLocalizedString(@"CONNECTED", nil);
            LedConexion.highlighted=YES;
            [_delegate setHayConexionInternet:YES];
            
            //Deepak_Carpenter : Added for CR#6 send chat message when log button clicked
            if ([[NSUserDefaults standardUserDefaults]valueForKey:@"chatMessage"]!=nil) {
                ChatViewController *chatVC=[[ChatViewController alloc]init];
                [chatVC sendconfirmationMessgae];
                [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"chatMessage"];
            }

            break;
            
        }
    }
    
    NetworkStatus hostStatus = [hostReachable currentReachabilityStatus];
    switch (hostStatus)  
    {
        case NotReachable:
        {
            DLog(@"A gateway to the host server is down.");
            hostActive = NO;
            //labelEstadoConexion.text=@"No hay conexión";
            //LedConexion.highlighted=NO;
            break;          
        }
        case ReachableViaWiFi:
        {
            DLog(@"A gateway to the host server is working via WIFI.");
            hostActive = YES;
            
            
            //labelEstadoConexion.text=@"Conectado";
            //LedConexion.highlighted=YES;
            break;
        }
        case ReachableViaWWAN:
        {
            DLog(@"A gateway to the host server is working via WWAN.");
            hostActive = YES;
            //labelEstadoConexion.text=@"Conectado";
            //LedConexion.highlighted=YES;
            break;     
        }
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    labelRecordatorioEntrega.text = NSLocalizedString(@"NOTE", nil);
    
    [BotonSalirLogin setTitle:NSLocalizedString(@"LEAVE", nil) forState:UIControlStateNormal];
    [BotonSalirLogin setTitle:NSLocalizedString(@"LEAVE", nil) forState:UIControlStateHighlighted];
    
    [BotonVolverMenu setTitle:NSLocalizedString(@"Menu", nil) forState:UIControlStateNormal];
    [BotonVolverMenu setTitle:NSLocalizedString(@"Menu", nil) forState:UIControlStateHighlighted];
    
    //Reachability
    internetActive=NO;
//    labelEstadoConexion.text=@"No hay conexión";
    labelEstadoConexion.text=NSLocalizedString(@"NO_CONNECTION", nil);
    LedConexion.highlighted=NO;
    // check for internet connection
    [self CompruebaConexion];    // now patiently wait for the notification
    //Deepak Carpenter
    [self performSelectorInBackground:@selector(checkForUpdate) withObject:nil];
    
    //Kanchan : CR#5 Recieving dismiss from doctor For Update popup to hide
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(toDismissPopup) name:@"todismissUpdate" object:nil];
    
}


//Kanchan : CR#5 To dismiss Update popup from background when doctor page is present
-(void)toDismissPopup
{
    [_delegate dismissModalController:self.choiceController];
    
}


- (void)viewDidUnload
{
    NSLog(@"viewDidUnload");

    [self setLedConexion:nil];
    [self setLabelEstadoConexion:nil];
    [self setTapaModal:nil];
    [self setBotonVolverMenu:nil];
    [self setVentanaTop:nil];
    [self setLabelRecordatorioEntrega:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc
{
    [labelRecordatorioEntrega release];
    [VentanaTop release];
    [BotonVolverMenu release];
    [BotonSalirLogin release];
    [TapaModal release];
    [labelEstadoConexion release];
    [LedConexion release];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    [super dealloc];
}
    
    
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (IBAction)BotonVolverMenuTouchUpInside:(id)sender
{
    [_delegate showVentana:((UIButton*)sender).tag firstTime:NO];
}

- (IBAction)logoutButtonClicked:(id)sender {
    [_delegate requestLogout];
}

- (IBAction)backToMenuButtonClicked:(id)sender {
    [_delegate requestBackToMenu];
}

#pragma software update Code
-(void) showUpdateAvailable: (NSString *)version
{

    [self displayAlert:[NSString stringWithFormat:@"Έκδοση %@ είναι τώρα διαθέσιμο για κατέβασμα",version]];
    updateLabel.text = [NSString stringWithFormat:@"Version %@ is now available for downloading",version];
    
    updateLabel.hidden = NO;
    updateButton.hidden = NO;
}

-(void) hideUpdateAvailable
{
    updateLabel.hidden = YES;
    updateButton.hidden = YES;
}

-(void)performSoftwareUpdate
{
    [self hideUpdateAvailable];
    
    //Kanchan: CR#5 Added to show popup even when cancel update is clicked
    [self checkForUpdate];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:updateURL]];
}
// Deepak_Carpenter : perform this on a background thread
-(void)checkForUpdate
{
    if (internetActive)
    {
        // http://pollenapps.com/IMAD/api/AppsAPI/CanUpgrade/?bundleID=com.imshealth.coach&version=0.4.7.2
        NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
        NSString *bundleID = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleIdentifierKey];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://pollenapps.com/IMAD/api/AppsAPI/CanUpgrade/?bundleID=%@&version=%@", bundleID, version]];
        NSLog(@"Webservice: %@", [url absoluteString]);
        ASIHTTPRequest *updateRequest = [ASIHTTPRequest requestWithURL:url];
        [updateRequest addRequestHeader:@"Accept" value:@"application/json"];
        [updateRequest startSynchronous];
        NSString *resp = [updateRequest responseString];
        NSLog(@"Response: %@",resp);
        
        if (resp != nil) {
            NSError *error = nil;
            NSData *jSONData = [resp dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:jSONData options:kNilOptions error:&error];
            
            NSNumber *canUpdate = [responseData objectForKey:@"CanUpgrade"];
            
            if ([canUpdate boolValue])
            {
                NSString *updateVersion = [[responseData objectForKey:@"AppVersion"] copy];
               
                    updateURL = [[responseData objectForKey:@"URL"] copy];
                
                [self performSelectorOnMainThread:@selector(showUpdateAvailable:) withObject:updateVersion waitUntilDone:NO];
            }
        }
    }
}
//added for alert display
-(void) displayAlert:(NSString*)message {
    if (self.choiceController != nil) {
        [_delegate dismissModalController:self.choiceController];
    }
    self.choiceController = [[MultipleChoiceViewController new] autorelease];
    
    // Ravi_Bukka: Added for localizing error messages
    
    _choiceController.message = NSLocalizedString(message,nil);
    _choiceController.messageFont = [UIFont boldSystemFontOfSize:32];
    _choiceController.acceptButtonTitle = @"ενημέρωση";
    //Deepak_Carpenter : Added to remove Cancel button from Update alert 
  //  _choiceController.cancelButtonTitle =@"όχι τώρα";
    _choiceController.otherButtonTitle = nil;
    _choiceController.eventBlock = ^(MultipleChoiceButton button){
        
        
        
        // Dismiss and release the controller
        [_delegate dismissModalController:self.choiceController];
        self.choiceController = nil;
        
        switch (button) {
            case kMultipleChoiceAccept: {
                
                [self performSoftwareUpdate];
                               break;
            }
        case kMultipleChoiceCancel: {
                // Cancel request
                break;
            }
            case kMultipleChoiceOther: {
               
                break;
            }


        }
        
    };
    
    // Present modal controller
    [_delegate presentModalController:self.choiceController];
}

-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"View Did Appear");

}
-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"View Will Appear");

}
@end
