//
//  AppDelegate.h
//  IMS Digital EPM
//
//  Created by Ricardo Berzal on 23/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModalContenidosFijosViewController.h"
#import "InitialLoadViewController.h"
#import "LoginViewController.h"
#import "DoctorInfoViewController.h"
#import "ModalWebViewController.h"
#import "ModalControllerProtocol.h"
#import "HelpViewController.h"
#import "MainMenuViewController.h"
#import "ChatViewController.h"
#import "NotificationsViewController.h"
#import "SessionExpiredHandler.h"
#import <HockeySDK/HockeySDK.h>

#define kOFFSET_FOR_KEYBOARD 352.0
#define kAltoCampoTexto 30.0
#define kAltoCampoMemoMedico 100.0

//color de texto
#define kTextColorR 30.0
#define kTextColorG 77.0
#define kTextColorB 136.0

@class VentanaPrincipalViewController;
@class MenuPrincipalViewController;

typedef enum {
    kWindowMainMenu = 1,
    kWindowLogin,
    kWindowDoctorInfo,
    kWindowPatientForm,
    kWindowInstructions,
    kWindowAbout,
    kWindowFAQ,
    kWindowNotifications,
    kWindowChat,
    kWindowCount
} WindowEnum;

@interface AppDelegate : NSObject <UIApplicationDelegate,ModalContenidosFijosViewControllerProtocol,InitialLoadViewControllerDelegate, LoginViewControllerDelegate, DoctorInfoViewControllerDelegate, ModalWebViewControllerDelegate, ModalControllerProtocol, MainMenuViewControllerDelegate, BITUpdateManagerDelegate, BITCrashManagerDelegate, BITHockeyManagerDelegate, HelpViewControllerDelegate, UIAlertViewDelegate, ChatViewControllerDelegate, NotificationsViewControllerDelegate, SessionExpiredHandlerDelegate> {
     
    UIViewController* ventanaActual;
    //Deepak_Carpenter: created this object to call check update method in DidBecomeActive delegate
    VentanaPrincipalViewController *VentanaObj;
    ModalContenidosFijosViewController* modalContenidosFijosViewController;
    
    NSString* _idUser;  //contiene id de usuario logado; nil si no logado
    
}
@property (retain, nonatomic) UIWindow *window;

@property (readonly) BOOL HayConexionInternet;

@property (retain, nonatomic) DoctorInfo* doctorInfo;
@property (retain, nonatomic) VentanaPrincipalViewController *ventanaPrincipalViewController;
@property (retain, nonatomic) MainMenuViewController *mainMenuViewController;
@property (retain, nonatomic) LoginViewController *loginViewController;
@property (retain, nonatomic) DoctorInfoViewController *doctorInfoViewController;
@property (retain, nonatomic) HelpViewController *helpViewController;
@property (retain, nonatomic) ChatViewController* chatViewController;
@property (retain, nonatomic) NotificationsViewController* notificationsViewController;
@property (readonly) NSString* idUser;
@property (retain, nonatomic) NSData* pushToken;
@property (copy, nonatomic) NSString* stringPushToken;
@property (nonatomic, getter = isInCollaborationPeriod) BOOL inCollaborationPeriod;
//Deepak_Carpenter : CR#1 Added for background fetch
@property (nonatomic) UIBackgroundTaskIdentifier backgroundTask;

//firstTime: inidica si es la primera vez que se muestra esa ventana en la aplicación
- (void)showVentana:(WindowEnum)numVentana firstTime:(bool)firstTime;

//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
@property (nonatomic, getter = NeedToExtendGracePeriod) BOOL extendGracePeriod;


//Se llama desde VentanaPrincipalViewController para decir si hay conexión o no
- (void)setHayConexionInternet:(BOOL)Conex;

-(void) AperturaModal;//Muestra y trae al frente la tapa modal de VentanaPrincipal
-(void) CierreModal;//Oculta la tapa modal de VentanaPrincipal 

-(bool)isFirstTime; //devuelve si es la primera vez que doctor introduce datos

-(void) updateApplicationData;
-(void) updateCountdown;

-(void) requestBackToMenu;
-(void) requestLogout;

//Ravi_Bukka: CR#2: Patient Difference and Local Notification
-(void)patientDifference:(int)serverPatientCount;

//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
-(BOOL)returnGracePeriondConfirmation;
@end
