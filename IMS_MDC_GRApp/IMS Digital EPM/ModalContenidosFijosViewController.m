//
//  AgadeceColaboracionViewController.m
//  IMS Digital EPM
//
//  Created by Ricardo Berzal on 24/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ModalContenidosFijosViewController.h"
#import "AppDelegate.h"

@interface ModalContenidosFijosViewController()
@property (nonatomic, retain) NSTimer* timer;
@end

@implementation ModalContenidosFijosViewController
@synthesize timer = _timer;

//**********************
// initWithDelegate
//**********************
- (ModalContenidosFijosViewController*)initWithDelegate: (NSObject<ModalContenidosFijosViewControllerProtocol>*) delegate  fistTime:(bool)firstTime;
{
    self = [super initWithNibName:@"ModalContenidosFijosViewController" bundle:nil];
    if (self) {
        _delegate = delegate;
        _firstTime = firstTime;
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void) cerrar {
    //[self.view setUserInteractionEnabled:NO];//No vaya a ser que toque mientras hace el efecto

    [_timer invalidate];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        self.timer = nil;
    }];
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut  
                     animations:^{
                         CGAffineTransform scale = CGAffineTransformMakeScale(1.2, 1.2);  
                         self.view.transform = scale;         
                     } 
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn  
                                          animations:^{
                                              CGAffineTransform scale = CGAffineTransformMakeScale(0.5, 0.5);  
                                              self.view.transform = scale;
                                              self.view.alpha=0.0;
                                              
                                          } 
                                          completion:^(BOOL finished) {
                                              [_delegate CierreModal];
                                              //[self.view setUserInteractionEnabled:YES];
                                              [self.view removeFromSuperview];
                                          }
                          ];
                     }
     ];
}

-(void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event {
    [self cerrar];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.timer = [NSTimer scheduledTimerWithTimeInterval:6 target:self selector:@selector(cerrar) userInfo:nil repeats:NO];
    if (_firstTime) {
        _labelFirst1.text = NSLocalizedString(@"DOCTOR_WELCOME_FIRSTTIME_1",nil);
        _labelFirst1.hidden = NO;
        _labelFirst2.text = NSLocalizedString(@"DOCTOR_WELCOME_FIRSTTIME_2",nil);
        _labelFirst2.hidden = NO;
        _label.hidden = YES;
        _imgIcon.hidden = NO;
    } else {
        _label.text = NSLocalizedString(@"DOCTOR_WELCOME",nil);
        _labelFirst1.hidden = YES;
        _labelFirst2.hidden = YES;
        _label.hidden = NO;
        _imgIcon.hidden = YES;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)dealloc {
    [_timer invalidate];
    [_timer release];
    [super dealloc];
}

@end
