//
//  VentanaPrincipalViewController.h
//  IMS Digital EPM
//
//  Created by Ricardo Berzal on 23/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDelegate;
@class Reachability;

@interface VentanaPrincipalViewController : UIViewController {
    AppDelegate* _delegate;
    Reachability* internetReachable;
    Reachability* hostReachable;
    BOOL hostActive;
    //Deepak Carpenter added for software update
    IBOutlet UILabel  *updateLabel;
    IBOutlet UIButton *updateButton;
    NSString *updateURL;

}


@property (retain, nonatomic) IBOutlet UIImageView *LedConexion;
@property (retain, nonatomic) IBOutlet UILabel *labelEstadoConexion;
@property (retain, nonatomic) IBOutlet UILabel *labelRecordatorioEntrega;
@property (retain, nonatomic) IBOutlet UIImageView *TapaModal;
@property (retain, nonatomic) IBOutlet UIButton *BotonVolverMenu;
@property (retain, nonatomic) IBOutlet UIButton *BotonSalirLogin;
@property (retain, nonatomic) IBOutlet UIView *VentanaTop;

@property  BOOL internetActive;

- (id)initWithDelegate:(AppDelegate*) delegate;
- (void) checkNetworkStatus:(NSNotification *)notice;//Reachability
- (IBAction)logoutButtonClicked:(id)sender;
- (IBAction)backToMenuButtonClicked:(id)sender;

-(void)checkForUpdate;

@end
