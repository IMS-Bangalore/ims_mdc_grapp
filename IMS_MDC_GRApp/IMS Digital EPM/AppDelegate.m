//
//  AppDelegate.m
//  IMS Digital EPM
//
//  Created by Ricardo Berzal on 23/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"

#import "LoginViewController.h"
#import "VentanaPrincipalViewController.h"
#import "DoctorInfoViewController.h"
#import "PatientViewController.h"
#import "ChangeNotifier.h"

#import "ModalContenidosFijosViewController.h"
#import "LoadingDialogViewController.h"
#import "ModalWebViewController.h"
#import "UpdateDataManager.h"
#import "RequestFactory.h"
#import "ChatMessage.h"
#import "StatsReporter.h"

#pragma Doctor relationships
#import "EntityFactory.h"
#import "University.h"
#import "Gender.h"
#import "MedicalCenter.h"

static NSString* const kHTMLExtension = @"html";
static NSString* const kHTMLFolder = @"HTML";
static NSString* const kInstructionsFile = @"instructions";
static NSString* const kAboutFile = @"about";
static NSString* const kFaqFile = @"faq";

//Ravi_Bukka: Added for Greece html files
static NSString* const kGreeceAboutFile = @"about_el";
static NSString* const kGreeceFaqFile = @"faq_el";

static NSString* const kTimeIntervalChat = @"timeSinceOpenedChat";
static NSString* const kTimeIntervalNotifications = @"timeSinceOpenedNotifications";

static NSString* const kCommunicationsPList = @"communications";
static NSString* const kSessionDurationKey = @"sessionDuration";

#if defined(__OFFLINE_VERSION__)
#define APP_ID @"b27cabdda3ede89fa172ab6e27a96887"
#elif defined(STAGING)
#define APP_ID @"4e3e4befe1f602d09bd711027d75aeee"
#else
#define APP_ID @"24e4742ad87179bff6219a69ef404928"
#endif

@interface AppDelegate()

@property (nonatomic, retain) PatientViewController *patientViewController;
@property (nonatomic, retain) InitialLoadViewController *initialLoadController;
@property (nonatomic, retain) ModalWebViewController *modalWebViewController;
@property (nonatomic, retain) NSTimer *sessionTimer;
@property (nonatomic, readonly) UpdateDataManager *updateDataManager;
@property (nonatomic, retain) BaseRequest *countdownRequest;
@property (nonatomic) NSInteger numberOfUnreadMessages;
@property (nonatomic, retain) NSDate* timeSinceOpenedChat;
@property (nonatomic, retain) NSDate* timeSinceOpenedNotifications;
@property (nonatomic) NSInteger pendingInboxNotifications;
@property (nonatomic) NSInteger pendingChatNotifications;
@property (nonatomic, retain) MultipleChoiceViewController* choiceController;
@property (nonatomic, retain) LoadingDialogViewController* loadingController;
@property (nonatomic, retain) BaseRequest* getDoctorInfoRequest;

-(void) displayModalWebViewWithUrl:(NSURL*) url;

@property (nonatomic, getter = isLoading) BOOL loading;
- (void)setLoading:(BOOL)loading withMessage:(NSString*)message;

@end

@implementation AppDelegate

@synthesize window = _window;
@synthesize HayConexionInternet=_HayConexionInternet;
@synthesize ventanaPrincipalViewController = _ventanaPrincipalViewController;
@synthesize loginViewController = _loginViewController;
@synthesize doctorInfoViewController = _doctorInfoViewController;
@synthesize mainMenuViewController = _mainMenuViewController;
@synthesize chatViewController = _chatViewController;
@synthesize notificationsViewController = _notificationsViewController;
@synthesize idUser = _idUser;
@synthesize doctorInfo = _doctorInfo;
@synthesize patientViewController = _patientViewController;
@synthesize initialLoadController = _initialLoadController;
@synthesize modalWebViewController = _modalWebViewController;
@synthesize helpViewController = _helpViewController;
@synthesize sessionTimer = _sessionTimer;
@synthesize updateDataManager = _updateDataManager;
@synthesize countdownRequest = _countdownRequest;
@synthesize numberOfUnreadMessages = _numberOfUnreadMessages;
@synthesize pushToken = _pushToken;
@synthesize stringPushToken = _stringPushToken;
@synthesize pendingInboxNotifications = _pendingInboxNotifications;
@synthesize loadingController = _loadingController;
@synthesize getDoctorInfoRequest = _getDoctorInfoRequest;

- (void)dealloc {
    [_window release];
    [_ventanaPrincipalViewController release];
    [_mainMenuViewController release];
    [_loginViewController release];
    [_doctorInfoViewController release];
    [_chatViewController release];
    [_idUser release];
    [modalContenidosFijosViewController release];
    [_doctorInfo release];
    [_patientViewController release];
    [_initialLoadController release];
    [_modalWebViewController release];
    [_helpViewController release];
    [_sessionTimer invalidate], [_sessionTimer release];
    [_updateDataManager release];
    [_countdownRequest cancel], [_countdownRequest release];
    [_getDoctorInfoRequest cancel], [_getDoctorInfoRequest release];
    [_loadingController release];
    [super dealloc];
}

/**********************************
 * Interacción de ventanas con el usuario
 **********************************/
-(void) AperturaModal {
    _ventanaPrincipalViewController.TapaModal.hidden=NO;//Esta ventana 2 siempre se abre con modal
    [_ventanaPrincipalViewController.view bringSubviewToFront:_ventanaPrincipalViewController.TapaModal];//La tapa modal al frente
}

-(void) CierreModal {
    [UIView animateWithDuration:0.2
                          delay:0
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         _ventanaPrincipalViewController.TapaModal.alpha = 0.0;
                     } 
                     completion:^(BOOL finished){
                         _ventanaPrincipalViewController.TapaModal.hidden=YES;
                         _ventanaPrincipalViewController.TapaModal.alpha=1.0;
                     }];
}

- (void)showVentana:(WindowEnum)numVentana  firstTime:(bool)firstTime{
    
    //todas las ventanas se muestran desde aquí
    CGRect frame;
    UIViewController* ventanaActualDummy=ventanaActual;
        
    //Oculto todo lo que depende de la ventana mostrada
    _ventanaPrincipalViewController.TapaModal.hidden=YES;
    _ventanaPrincipalViewController.BotonVolverMenu.hidden=numVentana!=kWindowPatientForm && numVentana!=kWindowChat && numVentana!=kWindowNotifications;
    _ventanaPrincipalViewController.BotonSalirLogin.hidden=numVentana==kWindowLogin;
    _ventanaPrincipalViewController.labelRecordatorioEntrega.hidden=YES;
    
    BOOL OcultaDummy=YES;//Si hay modales dejo el dummy que esté, lo controlo con este flag
        
    // Get
    self.timeSinceOpenedChat = (NSDate*)[[NSUserDefaults standardUserDefaults] objectForKey:kTimeIntervalChat];
    self.timeSinceOpenedNotifications = (NSDate*)[[NSUserDefaults standardUserDefaults] objectForKey:kTimeIntervalNotifications];
    
    //VOY A BUSCAR Y A MOSTRAR LA QUE TOQUE
    CGAffineTransform scale;
    switch (numVentana) {
        case kWindowMainMenu: //Menú Principal ************************************************************************************
        {
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            
            [self getUnreadNumberOfNotificationsAndMessages];
            
            if (_mainMenuViewController == nil) {
                _mainMenuViewController = [[MainMenuViewController alloc] init];
                NSArray* menuElements = [NSArray arrayWithObjects:
                                         [NSNumber numberWithInt:kWindowPatientForm],
                                         [NSNumber numberWithInt:kWindowFAQ],
                                         [NSNumber numberWithInt:kWindowAbout],
                                         [NSNumber numberWithInt:kWindowNotifications],
                                         [NSNumber numberWithInt:kWindowChat],
                                         [NSNumber numberWithInt:kWindowDoctorInfo],
                                         [NSNumber numberWithInt:kWindowInstructions],
                                         nil];
                
                NSMutableArray* menuItemArray = [NSMutableArray arrayWithCapacity:kWindowCount];
                for (NSNumber* identifier in menuElements) {
                    MainMenuItem* menuItem = [[[MainMenuItem alloc] init] autorelease];
                    menuItem.identifier = identifier.integerValue;
                    switch (identifier.integerValue) {
                        case kWindowPatientForm:
                            if (_doctorInfo.patients.count > 0) {
                                menuItem.title = NSLocalizedString(@"CONTINUE_PATIENT_FORM", nil);
                            } else {
                                menuItem.title = NSLocalizedString(@"INIT_PATIENT_FORM", nil);
                            }
                            break;
                        case kWindowFAQ:
                            menuItem.title = NSLocalizedString(@"FAQ_MENU_TITLE" , @"");
                            break;
                        case kWindowAbout:
                            menuItem.title = NSLocalizedString(@"ABOUT_MENU_TITLE" , @"");
                            break;
                        case kWindowNotifications:
                            menuItem.title = NSLocalizedString(@"NOTIFICATIONS_MENU_TITLE" , @"");
                            break;
                        case kWindowChat:
                            menuItem.title = NSLocalizedString(@"CHAT_MENU_TITLE" , @"");
                            break;
                        case kWindowDoctorInfo:
                            menuItem.title = NSLocalizedString(@"DOCTOR_INFO_MENU_TITLE" , @"");
                            break;
                        case kWindowInstructions:
                            menuItem.title = NSLocalizedString(@"INSTRUCTIONS_MENU_TITLE" , @"");
                            break;
                        default:
                            break;
                    }
                    [menuItemArray addObject:menuItem];
                }
                
                _mainMenuViewController.menuItems = menuItemArray;
                _mainMenuViewController.delegate = self;
            }
            else {
                for (MainMenuItem* menuItem in _mainMenuViewController.menuItems) {
                    if (menuItem.identifier == kWindowPatientForm) {
                        if (_doctorInfo.patients.count > 0) {
                            menuItem.title = NSLocalizedString(@"CONTINUE_PATIENT_FORM", nil);
                        } else {
                            menuItem.title = NSLocalizedString(@"INIT_PATIENT_FORM", nil);
                        }
                    }
                }
            }
                        
            // Forzamos la carga del NIB
            UIView* mainMenuView = [_mainMenuViewController view];
            
            // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
            [_mainMenuViewController viewWillAppear:NO];
            
            [_ventanaPrincipalViewController.view addSubview:mainMenuView];
            
            // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
            [_mainMenuViewController viewDidAppear:NO];
            
            //VentanaTOP con el logo y demás para adelante
            [_ventanaPrincipalViewController.view bringSubviewToFront:_ventanaPrincipalViewController.VentanaTop];
            
            ventanaActual = _mainMenuViewController;
            
            // Update countdown
            [self updateCountdown];
            
            break;
        }
        case kWindowLogin: //Login *********************************************************************************************
            
            //Kanchan Nair: CR#5 Added to call check for Update method after Quit button is clicked
            VentanaObj = [[VentanaPrincipalViewController alloc]init];
            [VentanaObj performSelector:@selector(CompruebaConexion)];
            
            if (!_loginViewController)
                _loginViewController = [[LoginViewController alloc] initWithDelegate:self];
            
            _loginViewController.view.alpha = 0.0;
            frame=_loginViewController.view.frame;
            frame.origin.y=768-frame.size.height;
            [_loginViewController.view setFrame:frame];
            
            scale = CGAffineTransformMakeScale(0.5, 0.5);
            _loginViewController.view.transform = scale;
            
            // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
            [_loginViewController viewWillAppear:YES];
            
            [_ventanaPrincipalViewController.view addSubview:_loginViewController.view];
            
            // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
            [_loginViewController viewDidAppear:YES];
            
            //VentanaTOP con el logo y demás para adelante
            [_ventanaPrincipalViewController.view bringSubviewToFront:_ventanaPrincipalViewController.VentanaTop];
            
            [UIView animateWithDuration:0.2
                                  delay:0
                                options: UIViewAnimationCurveEaseIn
                             animations:^{
                                 _loginViewController.view.alpha = 1.0;
                                 CGAffineTransform scale = CGAffineTransformMakeScale(1, 1);
                                 _loginViewController.view.transform = scale;
                             } 
                             completion:^(BOOL finished){
                                 DLog(@"Done!");
                             }];
            ventanaActual=_loginViewController;//Para liberar luego
            break;
        case kWindowDoctorInfo: //VentanaDatosMedicoViewController *******************************************************************
            //guardar usuario
            if (_idUser!=nil) [_idUser release];
            _idUser = [[NSString alloc] initWithString:_loginViewController.TextUsuario.text];
            
            //mostar ventana de datos de médico
            if (_doctorInfoViewController == nil) {
                _doctorInfoViewController = [[DoctorInfoViewController alloc] init];
                _doctorInfoViewController.delegate = self;
            }
            
            frame=_doctorInfoViewController.view.frame;
            frame.origin.y=768-frame.size.height;
            [_doctorInfoViewController.view setFrame:frame];
            
            // Set the DoctorInfo instance to the view
            _doctorInfoViewController.doctorInfo = self.doctorInfo;
            
            // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
            [_doctorInfoViewController viewWillAppear:YES];
            
            [_ventanaPrincipalViewController.view addSubview:_doctorInfoViewController.view];
            
            // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
            [_doctorInfoViewController viewDidAppear:YES];
            
            //VentanaTOP con el logo y demás para adelante
            [_ventanaPrincipalViewController.view bringSubviewToFront:_ventanaPrincipalViewController.VentanaTop];
            
                
                       
            //si es la primera vez que entro en la ventana (desde login), MUESTRO LA MODAL
            if (firstTime)
            {
                [self AperturaModal];

                modalContenidosFijosViewController = [[ModalContenidosFijosViewController alloc] initWithDelegate:self fistTime:[self isFirstTime]];
                [modalContenidosFijosViewController.view setFrame:CGRectMake(0,0,1024,768)];
                
                scale = CGAffineTransformMakeScale(0.5, 0.5);
                modalContenidosFijosViewController.view.alpha=0.5;
                modalContenidosFijosViewController.view.transform = scale;
                
                // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
                [modalContenidosFijosViewController viewWillAppear:YES];
                
                [_ventanaPrincipalViewController.view addSubview:modalContenidosFijosViewController.view];

                [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn  
                                 animations:^{
                                     CGAffineTransform scale = CGAffineTransformMakeScale(1.2, 1.2);  
                                     modalContenidosFijosViewController.view.transform = scale;
                                     modalContenidosFijosViewController.view.alpha=1.0;
                                     
                                 } 
                                 completion:^(BOOL finished) {
                                     [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut  
                                                      animations:^{
                                                          CGAffineTransform scale = CGAffineTransformMakeScale(1.0, 1.0);  
                                                          modalContenidosFijosViewController.view.transform = scale;
                                                          
                                                      } 
                                                      completion:^(BOOL finished) {
                                                          
                                                      }
                                      ];
                                 }
                 ];
            }
            
            ventanaActual=_doctorInfoViewController;//Para liberar luego
            break;
        case kWindowPatientForm: //Cuestionario ***************************************************************************************
//            if ([self isInCollaborationPeriod]){
//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
            
            if ([self isInCollaborationPeriod]) {
                if (!_patientViewController) {
                    _patientViewController = [[PatientViewController alloc] init];
                    _patientViewController.modalController = self;
                }
                
                if (!_helpViewController) {
                    _helpViewController = [[HelpViewController alloc] init];
                    _helpViewController.delegate = self;
                }
                
                frame = CGRectMake(0, 45,1024,768-45);
                [_patientViewController.view setFrame:frame];
                
                frame = _helpViewController.view.frame;
                frame.origin.y = [_helpViewController viewOffset] - _helpViewController.view.frame.size.height;
                [_helpViewController.view setFrame:frame];
                _patientViewController.doctor = self.doctorInfo.doctor;
                // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
                [_patientViewController viewWillAppear:YES];
                
                _ventanaPrincipalViewController.TapaModal.alpha = 0;
                [_ventanaPrincipalViewController.view addSubview:_patientViewController.view];
                [_ventanaPrincipalViewController.view addSubview:_helpViewController.view];
                //VentanaTOP con el logo y demás para adelante
                [_ventanaPrincipalViewController.view bringSubviewToFront:_ventanaPrincipalViewController.VentanaTop];
                [_ventanaPrincipalViewController.view bringSubviewToFront:_helpViewController.view];
                
                // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
                [_patientViewController viewDidAppear:YES];
                
                ventanaActual=_patientViewController;//Para liberar luego
            } else {
                [self displayAlert:NSLocalizedString(@"LOGIN_OUT_OF_COLLABORATION_PERIOD", nil)];
                ventanaActualDummy = nil;
                _ventanaPrincipalViewController.BotonVolverMenu.hidden = YES;
            }
            break;
        case kWindowChat: //Chat ************************************************************************************************
            self.timeSinceOpenedChat = [NSDate date];
            // Set
            [[NSUserDefaults standardUserDefaults] setObject:self.timeSinceOpenedChat forKey:kTimeIntervalChat];
            
            self.chatViewController.userId = _idUser;
            
            frame = CGRectMake(0, 45,1024,768-45);
            [_chatViewController.view setFrame:frame];

//            _patientViewController.doctor = self.doctorInfo.doctor;
            // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
            [_chatViewController viewWillAppear:YES];
            
            _ventanaPrincipalViewController.TapaModal.alpha = 0;
            [_ventanaPrincipalViewController.view addSubview:_chatViewController.view];
//            [_ventanaPrincipalViewController.view addSubview:_helpViewController.view];
            //VentanaTOP con el logo y demás para adelante
            [_ventanaPrincipalViewController.view bringSubviewToFront:_ventanaPrincipalViewController.VentanaTop];
//            [_ventanaPrincipalViewController.view bringSubviewToFront:_helpViewController.view];
            
            // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
            [_chatViewController viewDidAppear:YES];
            
            ventanaActual=_chatViewController;//Para liberar luego
            break;
        case kWindowNotifications: //Notificaciones ***************************************************************************************
            
            _notificationsViewController.pushToken = self.stringPushToken;
            
            self.timeSinceOpenedNotifications = [NSDate date];
            // Set
            [[NSUserDefaults standardUserDefaults] setObject:self.timeSinceOpenedNotifications forKey:kTimeIntervalNotifications];
            
            frame = CGRectMake(0, 45,1024,768-45);
            [_notificationsViewController.view setFrame:frame];
            
            //            _patientViewController.doctor = self.doctorInfo.doctor;
            // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
            [_notificationsViewController viewWillAppear:YES];
            
            _ventanaPrincipalViewController.TapaModal.alpha = 0;
            [_ventanaPrincipalViewController.view addSubview:_notificationsViewController.view];
            //            [_ventanaPrincipalViewController.view addSubview:_helpViewController.view];
            //VentanaTOP con el logo y demás para adelante
            [_ventanaPrincipalViewController.view bringSubviewToFront:_ventanaPrincipalViewController.VentanaTop];
            //            [_ventanaPrincipalViewController.view bringSubviewToFront:_helpViewController.view];
            
            // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
            [_notificationsViewController viewDidAppear:YES];
            
            ventanaActual=_notificationsViewController;//Para liberar luego
            break;

        case kWindowInstructions: {
            [self displayBackgroundShadow];
            OcultaDummy = NO;
            NSURL* url = [[NSBundle mainBundle] URLForResource:kInstructionsFile withExtension:kHTMLExtension subdirectory:kHTMLFolder];
            [self displayModalWebViewWithUrl:url];
            break; 
        }
        case kWindowAbout: {
            [self displayBackgroundShadow];
            OcultaDummy = NO;
            if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"]) {

//Ravi_Bukka: Added for Greece localization and loading html files
            NSURL* url = [[NSBundle mainBundle] URLForResource:kGreeceAboutFile withExtension:kHTMLExtension subdirectory:kHTMLFolder];
                [self displayModalWebViewWithUrl:url];
            }
            else {
            NSURL* url = [[NSBundle mainBundle] URLForResource:kAboutFile withExtension:kHTMLExtension subdirectory:kHTMLFolder];
                [self displayModalWebViewWithUrl:url];
            }
            
            
            break;
        }
        case kWindowFAQ:  {
            [self displayBackgroundShadow];
            OcultaDummy = NO;
//Ravi_Bukka: Added for Greece localization and loading html files
            if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"]) {
                
                NSURL* url = [[NSBundle mainBundle] URLForResource:kGreeceFaqFile withExtension:kHTMLExtension subdirectory:kHTMLFolder];
                            [self displayModalWebViewWithUrl:url];
            }
            else {
            NSURL* url = [[NSBundle mainBundle] URLForResource:kFaqFile withExtension:kHTMLExtension subdirectory:kHTMLFolder];
                            [self displayModalWebViewWithUrl:url];
            }

            break;
        }
        default:
            break;
    }
   
    //OCULTO LA VENTANA ACTIVA ANTERIORMENTE
    if ((ventanaActualDummy) && (OcultaDummy)) {
        [UIView animateWithDuration:0.2
                              delay:0
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             ventanaActualDummy.view.alpha = 0.0;
                         } 
                         completion:^(BOOL finished){
                             // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
                             [ventanaActualDummy viewWillDisappear:YES];
                             
                             [ventanaActualDummy.view removeFromSuperview];
                             ventanaActualDummy.view.alpha = 1.0;
                             //[ventanaActualDummy release];
                             DLog(@"RICARDO CORREGIR, AHORA NO LIBERO");
                             
                             // Enviamos manualmente los eventos viewWillAppear, viewWillDisappear, viewDidAppear y viewWillDisappear
                             [ventanaActualDummy viewDidDisappear:YES];
                         }];
    }
}

- (void)displayBackgroundShadow {
    _ventanaPrincipalViewController.TapaModal.hidden = NO;
    _ventanaPrincipalViewController.TapaModal.alpha = 1;
}

-(void) displayModalWebViewWithUrl:(NSURL*) url {
    self.modalWebViewController = [[[ModalWebViewController alloc] init] autorelease];
    _modalWebViewController.delegate = self;
    
    // Notify modal dialog opening to partially hide background
    [self AperturaModal];
    // Animate view
    [_modalWebViewController.view setFrame:CGRectMake(0,0,1024,768)];
    
    CGAffineTransform scale = CGAffineTransformMakeScale(0.5, 0.5);
    _modalWebViewController.view.alpha=0.5;
    _modalWebViewController.view.transform = scale;
    
    [_modalWebViewController viewWillAppear:YES];
    [_ventanaPrincipalViewController.view addSubview:_modalWebViewController.view];
    [_modalWebViewController viewDidAppear:YES];
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn  
                     animations:^{
                         CGAffineTransform scale = CGAffineTransformMakeScale(1.2, 1.2);  
                         _modalWebViewController.view.transform = scale;
                         _modalWebViewController.view.alpha=1.0;
                         
                     } 
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut  
                                          animations:^{
                                              CGAffineTransform scale = CGAffineTransformMakeScale(1.0, 1.0);  
                                              _modalWebViewController.view.transform = scale;
                                              
                                          } 
                                          completion:^(BOOL finished) {
                                              // Once finished animation, set URL
                                              _modalWebViewController.url = url;
                                          }
                          ];
                     }
     ];
}

- (void)setHayConexionInternet:(BOOL)Conex {
    if (Conex && self.doctorInfo != nil) {
        // Start the change notifier when connection is available
        [[ChangeNotifier sharedNotifier] startNotifying];
    }
    
    _HayConexionInternet=Conex;
}

-(bool)isFirstTime
{
    if (_idUser==nil) return NO;
    
    //comprobar si es la primera vez del médico
    NSString* str = [[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"DATARECORDER_%@",_idUser]];
    return (str==nil || [str compare:@"yes"]!=NSOrderedSame);
    
}

#pragma mark - UIApplicationDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//Deepak_Carpenter : Added for Background refresh
// Set Minimum Background Fetch Inteval //
    
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:
     UIApplicationBackgroundFetchIntervalMinimum];

    //Deepak_Carpenter : Revieved Notification to handle logout alert when session expired or invalid session token issue comes
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(logoutNotification:)
                                                 name:@"LogoutNotification"
                                               object:nil];

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    ventanaActual=nil;
    _idUser = nil;

    _ventanaPrincipalViewController = [[VentanaPrincipalViewController alloc] initWithDelegate:self];
    [_ventanaPrincipalViewController view];
    
    _initialLoadController = [[InitialLoadViewController alloc] init];
    _initialLoadController.delegate = self;
    
    [self.window makeKeyAndVisible];
    self.window.rootViewController = _initialLoadController;
    
    [_initialLoadController performInitialLoad];
    
    return YES;
    
    
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:notification.alertAction message:notification.alertBody delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
    DLog(@"This was fired off");
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    DLog(@"%@", userInfo.description);
    
    if (self.chatViewController != nil) {
        [self.chatViewController loadMessages];
    }
    if (self.notificationsViewController != nil) {
        [self.notificationsViewController loadNotifications];
    }
    
    MultipleChoiceViewController *choiceController = [[MultipleChoiceViewController new] autorelease];
    choiceController.message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    choiceController.acceptButtonTitle = NSLocalizedString(@"NOTIFICATION_RECEIVED_ACCEPT", @"");
    choiceController.eventBlock = ^(MultipleChoiceButton button){
        // Dismiss and release the controller
        [self dismissModalController:choiceController];
    };
    [self presentModalController:choiceController];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
//    [[ChangeNotifier sharedNotifier] pauseNotifying];
//    [self.patientViewController willGoBackground];
    //Deepak_Carpenter : CR#1 Added for background functionality
    UIApplication  *app = [UIApplication sharedApplication];
    //    UIBackgroundTaskIdentifier bgTask;
    
    self.backgroundTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:self.backgroundTask];
    }];

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
            NSLog(@"applicationWillEnterForeground");
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
    [[SessionExpiredHandler sharedSessionExpiredHandler] checkIfTimerWasFired];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
     NSLog(@"applicationDidBecomeActive");
    VentanaObj = [[VentanaPrincipalViewController alloc]init];\
   [VentanaObj performSelector:@selector(checkForUpdate)];
    
//    [self performSelectorInBackground:@selector(ve.che) withObject:nil];
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    if (self.doctorInfo != nil) {
        [[ChangeNotifier sharedNotifier] unpauseNotifying];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"YES YES");

    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    self.pushToken = deviceToken;
    
    //Removing the brackets from the device token
    self.stringPushToken = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    DLog(@"Push Notification tokenstring is %@",self.stringPushToken);
 
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {

    DLog(@"Application did fail registering for remote notifications: %@", error);
    
//    NSString* s=[[NSString alloc] initWithFormat:@"%@",error];
//    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:s delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [alert show];
//    [alert release];
//    [s release];
}

#pragma mark - Private methods
- (void)initializeHockeyApp {
#ifdef CONFIGURATION_Beta
    if (APP_ID != nil) {
        [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:APP_ID delegate:self];
        [[[BITHockeyManager sharedHockeyManager] crashManager] setCrashManagerStatus:BITCrashManagerStatusAutoSend];
        [[BITHockeyManager sharedHockeyManager] startManager];
    }
#endif
}

- (void)getUnreadNumberOfNotificationsAndMessages {
    if (_notificationsViewController == nil) {
        _notificationsViewController = [[NotificationsViewController alloc] init];
        _notificationsViewController.delegate = self;
    }
    _notificationsViewController.pushToken = self.stringPushToken;
    _notificationsViewController.userId = _loginViewController.TextUsuario.text;
    [_notificationsViewController loadNotifications];
    
    // Obtenemos el numero de mensajes sin leer del chat
    if (_chatViewController == nil) {
        _chatViewController = [[ChatViewController alloc] init];
        _chatViewController.delegate = self;
    }
    _chatViewController.userId = _loginViewController.TextUsuario.text;
    [_chatViewController loadMessages];
}

-(void) displayAlert:(NSString*)message {
    if (self.choiceController != nil) {
        [self dismissModalController:self.choiceController];
    }
    self.choiceController = [[MultipleChoiceViewController new] autorelease];
    _choiceController.message = message;
    _choiceController.messageFont = [UIFont boldSystemFontOfSize:32];
    _choiceController.acceptButtonTitle = nil;
    _choiceController.cancelButtonTitle = NSLocalizedString(@"CHANGE_PASSWORD_ERROR_BUTTON", @"");
    _choiceController.otherButtonTitle = nil;
    _choiceController.eventBlock = ^(MultipleChoiceButton button){
        // Dismiss and release the controller
        [self dismissModalController:self.choiceController];
        self.choiceController = nil;
    };
    
    // Present modal controller
    [self presentModalController:self.choiceController];
}

-(void) logout {
    // End counting session duration and help counter
    [[StatsReporter sharedStatsReporter] endEvent:kStatsEventTypeStartSession];
    
    [[StatsReporter sharedStatsReporter] sendStats];
    
    _doctorInfo = nil;
    [self showVentana:kWindowLogin firstTime:NO];
}
//Ravi_Bukka Added to handle notification when invalid session token issue occurs
- (void) logoutNotification: (NSNotification *) notification {
    
    if ([notification.name isEqualToString:@"LogoutNotification"]) {
        
        [[StatsReporter sharedStatsReporter] endEvent:kStatsEventTypeStartSession];
        
        [[StatsReporter sharedStatsReporter] sendStats];
        
        _doctorInfo = nil;
        [self showVentana:kWindowLogin firstTime:NO];
        
        
    }
}
-(void) backToMenu {
    [self showVentana:kWindowMainMenu firstTime:NO];
}

-(void) requestBackToMenu {
    // If current window is patient form, notify the close request
    if (ventanaActual == _patientViewController) {
        [_patientViewController requestCloseWithBlock:^(BOOL closed) {
            if (closed) {
                [self backToMenu];
            }
        }];
    } else {
        [self backToMenu];
    }
}

-(void) requestLogout {
    // If current window is patient form, notify the close request
    if (ventanaActual == _patientViewController) {
        [_patientViewController requestCloseWithBlock:^(BOOL closed) {
            if (closed) {
                [self logout];
            }
        }];
    }
    else {
        if (ventanaActual == _doctorInfoViewController) {
            [_doctorInfoViewController willPerformLogout];
        }
        
        [self logout];
    }
}

- (void)scheduleNotifications {
    DoctorInfo* doctorInfo = self.doctorInfo;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSDateFormatter* dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    
    UIApplication *app                = [UIApplication sharedApplication];
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    [app cancelAllLocalNotifications];

    // Notification sent when the first collaboration period ends with the date of the second period
    NSDateComponents *dayComponent = [[[NSDateComponents alloc] init] autorelease];
    dayComponent.day = 1;
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *notificationDate = [theCalendar dateByAddingComponents:dayComponent toDate:doctorInfo.firstCollaborationDateEnd options:0];
    
    dayComponent = [theCalendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:notificationDate];
    dayComponent.hour = 12;
    notificationDate = [theCalendar dateFromComponents:dayComponent];
    
    notification.fireDate  = notificationDate;
    notification.timeZone  = [NSTimeZone systemTimeZone];
    notification.alertBody = [NSString stringWithFormat:NSLocalizedString(@"FIRST_PERIOD_ENDED", nil), [dateFormatter stringFromDate:doctorInfo.secondCollaborationDate]];
    
    if ([notificationDate compare:[NSDate date]] != NSOrderedAscending) {
        [app scheduleLocalNotification:notification];
        DLog(@"First notification scheduled to: %@", [dateFormatter2 stringFromDate:notification.fireDate]);
    }
    
    // Notification sent when there are two days left for beginning the second period of collaboration
    NSString* colabCount = [[NSUserDefaults standardUserDefaults] valueForKey:@"CollaborationCount"];
    // Second Date
//Ravi_Bukka: Handled multiple collaboration issue
    if ([colabCount isEqualToString:@"1"])
    {
        
    }
    else if ([colabCount isEqualToString:@"2"]) {
    dayComponent = [[[NSDateComponents alloc] init] autorelease];
    dayComponent.day = -2;
    
    notificationDate = [theCalendar dateByAddingComponents:dayComponent toDate:doctorInfo.secondCollaborationDate options:0];
    dayComponent = [theCalendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:notificationDate];
    dayComponent.hour = 12;
    notificationDate = [theCalendar dateFromComponents:dayComponent];
    
    notification.fireDate  = notificationDate;
    notification.timeZone  = [NSTimeZone systemTimeZone];
    notification.alertBody = [NSString stringWithFormat:NSLocalizedString(@"SECOND_PERIOD_TO_BEGIN", nil), [dateFormatter stringFromDate:doctorInfo.secondCollaborationDate]];
    
    if ([notificationDate compare:[NSDate date]] != NSOrderedAscending) {
        [app scheduleLocalNotification:notification];
        DLog(@"Second notification scheduled to: %@", [dateFormatter2 stringFromDate:notification.fireDate]);
    }
    
    [notification release];
    [dateFormatter release];
    [dateFormatter2 release];
    }
}

- (void)obtainDoctorInfo:(void(^)())onComplete {
    [self setLoading:YES withMessage:NSLocalizedString(@"LOADING_DOCTOR_INFO", nil)];
    self.getDoctorInfoRequest = [[RequestFactory sharedInstance] createGetDoctorInfoRequestWithUserId:self.doctorInfo.doctor.userID onComplete:^(DoctorInfoResponse *response) {
        if (response != nil) {
            EntityFactory* entityFactory = [EntityFactory sharedEntityFactory];
            DoctorInfo* doctorInfo = self.doctorInfo;
            Doctor* doctor = doctorInfo.doctor;
            
            // Fill doctor information
            doctor.age = @(response.age);
            if (response.manualUnivertityName.length > 0) {
                doctor.university = [entityFactory insertNewWithEntityName:NSStringFromClass([University class])];
                doctor.university.userDefined = @(YES);
                doctor.university.name = response.manualUnivertityName;
            }
            else {
                doctor.university = [entityFactory fetchEntity:NSStringFromClass([University class])
                                               usingIdentifier:@(response.universityId).stringValue];
            }
            doctor.gender = [entityFactory fetchEntity:NSStringFromClass([Gender class])
                                       usingIdentifier:@(response.genderId).stringValue];
            doctor.graduationYear = @(response.yearOfDegree).stringValue;
            doctor.mainSpeciality = [entityFactory fetchEntity:NSStringFromClass([Specialty class])
                                               usingIdentifier:@(response.specializationId).stringValue];
            if (response.secondSpecializationId != nil) {
                doctor.secondarySpecialty = [entityFactory fetchEntity:NSStringFromClass([Specialty class])
                                                       usingIdentifier:response.secondSpecializationId.stringValue];
            }
            doctor.province = [entityFactory fetchEntity:NSStringFromClass([Province class])
                                         usingIdentifier:@(response.countyId).stringValue];
            
            // Fill DoctorInfo information
            MedicalCenter* center = nil;
            if (response.healthCenter) {
                center = [entityFactory fetchEntity:NSStringFromClass([MedicalCenter class])
                                    usingIdentifier:@(kCenterTypeValueHealthCenter).stringValue];
                if (center != nil) {
                    [doctorInfo addMedicalCentersObject:center];
                }
            }
            if (response.publicHospital) {
                center = [entityFactory fetchEntity:NSStringFromClass([MedicalCenter class])
                                    usingIdentifier:@(kCenterTypeValuePublicHospital).stringValue];
                if (center != nil) {
                    [doctorInfo addMedicalCentersObject:center];
                }
            }
            if (response.privateHospital) {
                center = [entityFactory fetchEntity:NSStringFromClass([MedicalCenter class])
                                    usingIdentifier:@(kCenterTypeValuePrivateHospital).stringValue];
                if (center != nil) {
                    [doctorInfo addMedicalCentersObject:center];
                }
            }
            if (response.consultation) {
                center = [entityFactory fetchEntity:NSStringFromClass([MedicalCenter class])
                                    usingIdentifier:@(kCenterTypeValueConsultation).stringValue];
                if (center != nil) {
                    [doctorInfo addMedicalCentersObject:center];
                }
            }
            if (response.otherHealthCenter && response.otherHealthCenterText.length > 0) {
                center = [entityFactory insertNewWithEntityName:NSStringFromClass([MedicalCenter class])];
                center.userDefined = @(YES);
                center.name = response.otherHealthCenterText;
                if (center != nil) {
                    [doctorInfo addMedicalCentersObject:center];
                }
            }
            doctorInfo.weeklyActivityHours = @(response.weeklyHours);
            doctorInfo.averageWeeklyPatients = @(response.weeklyPatients);
            doctorInfo.comments = response.comments;
            doctorInfo.email = response.email;
            
            [entityFactory saveChanges];
        }
        
        [self setLoading:NO];
        onComplete();
        self.getDoctorInfoRequest = nil;
    } onError:^(NSError *error) {
        DLog(@"Failed executing GetDoctorInfo request: %@", error.localizedDescription);
        [self setLoading:NO];
        onComplete();
        self.getDoctorInfoRequest = nil;
    }];
    [self.getDoctorInfoRequest start];
}

#pragma mark - Loading
-(void) setLoading:(BOOL)loading {
    [self setLoading:loading withMessage:NSLocalizedString(@"LOADING_MESSAGE", nil)];
}

-(void) setLoading:(BOOL)loading withMessage:(NSString*)message {
    if (loading) {
        if (_loadingController == nil) {
            _loadingController = [[LoadingDialogViewController alloc] init];
        }
        _loadingController.text = message;
        // Present modal controller
        [self presentModalController:self.loadingController];
    } else {
        // Hide modal controller
        [self dismissModalController:self.loadingController];
    }
}

#pragma mark - HelpViewControllerDelegate
- (void)helpViewControllerDidChangePosition:(CGFloat)position animated:(BOOL)animated {
    [_ventanaPrincipalViewController.view bringSubviewToFront:_ventanaPrincipalViewController.TapaModal];
    _ventanaPrincipalViewController.TapaModal.hidden = NO;
    position = MAX(MIN(position, 1.0), 0.0);
    if (animated) {
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options: UIViewAnimationTransitionCurlDown | UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             _ventanaPrincipalViewController.TapaModal.alpha = position;
                         }
                         completion:^(BOOL finished){
                             if (finished) {
                                 if (_ventanaPrincipalViewController.TapaModal.alpha < 0.0001) {
                                     _ventanaPrincipalViewController.TapaModal.hidden = YES;
                                 }
                             }
                         }];
    } else {
        _ventanaPrincipalViewController.TapaModal.alpha = position;
    }
    
    [_ventanaPrincipalViewController.view bringSubviewToFront:_helpViewController.view];
}

- (BOOL)helpViewControllerShouldShow {
    return [_patientViewController requestDismiss];
}

#pragma mark - InitialLoadViewController
- (void)initialLoadViewControllerDidFinishLoading {
    [self performSelectorOnMainThread:@selector(initializeHockeyApp) withObject:nil waitUntilDone:NO];
    
    self.window.rootViewController = _ventanaPrincipalViewController;
    self.initialLoadController = nil;
    
    [self showVentana:kWindowLogin firstTime:YES];
    
    // Registering for remote notifications
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
}

-(void) updateCountdown {
#ifdef __OFFLINE_VERSION__
    return;
#endif
    
    // Reload countdown
    [self.countdownRequest cancel];
    // Clear textfield
    self.ventanaPrincipalViewController.labelRecordatorioEntrega.alpha = 0;
    self.countdownRequest = [[RequestFactory sharedInstance] createCountdownRequestWithDoctorInfo:self.doctorInfo onComplete:^(NSInteger remainingDays) {
        DLog(@"Sync countdown days: %d", remainingDays);
        NSString* labelString = NSLocalizedString(@"SYNC_REMINDER_ERROR", nil);
        if (remainingDays == 0) {
            labelString = NSLocalizedString(@"SYNC_REMINDER_MESSAGE_TODAY", nil);
        } else if (remainingDays == 1) {
            labelString = NSLocalizedString(@"SYNC_REMINDER_MESSAGE_TOMORROW", nil);
        } else if (remainingDays > 1) {
            labelString = [NSString stringWithFormat:NSLocalizedString(@"SYNC_REMINDER_MESSAGE", nil), remainingDays];
        }
        self.ventanaPrincipalViewController.labelRecordatorioEntrega.text = labelString;
        [UIView transitionWithView:self.ventanaPrincipalViewController.labelRecordatorioEntrega duration:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.ventanaPrincipalViewController.labelRecordatorioEntrega.alpha = 1;
        } completion:nil];
    } onError:^(NSError *error) {
        DLog(@"Sync countdown request error: %@", error);
        self.ventanaPrincipalViewController.labelRecordatorioEntrega.text = NSLocalizedString(@"SYNC_REMINDER_ERROR", nil);
        [UIView transitionWithView:self.ventanaPrincipalViewController.labelRecordatorioEntrega duration:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.ventanaPrincipalViewController.labelRecordatorioEntrega.alpha = 1;
        } completion:nil];
    }];
    [self.countdownRequest start];
}


//Ravi_Bukka: CR#2: Patient Difference and Local Notification
#pragma mark - patientDifference
-(void)patientDifference:(int)serverPatientCount {
    
    
    
    if (serverPatientCount < _doctorInfo.patients.count - 1 ) {
        
        if(self.inCollaborationPeriod) {
            
            NSLog(@"YES");

                
                UILocalNotification *localNotification1 = [[UILocalNotification alloc] init];
            
            
                
                // Notification sent before two days of first collaboration period ends i.e., on Friday afternoon
                NSDateComponents *dayComponent1 = [[[NSDateComponents alloc] init] autorelease];
                dayComponent1.day = 4;
                
                NSCalendar *calendar1 = [NSCalendar currentCalendar];
                NSDate *notificationDate1 =  [calendar1 dateByAddingComponents:dayComponent1 toDate:_doctorInfo.firstCollaborationDateEnd options:0];
            
            
                
                NSDateComponents *componentsForFireDate1 = [calendar1 components:(NSCalendarUnitYear | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear |  NSCalendarUnitHour | NSCalendarUnitMinute| NSCalendarUnitSecond | NSCalendarUnitWeekday) fromDate: notificationDate1];
                
            
                componentsForFireDate1.hour = 12;
                componentsForFireDate1.minute = 30;
            
                
                notificationDate1 = [calendar1 dateFromComponents:componentsForFireDate1];
 
                
                localNotification1.fireDate = notificationDate1;
                localNotification1.timeZone  = [NSTimeZone defaultTimeZone];
                localNotification1.applicationIconBadgeNumber = 1;
                localNotification1.alertBody = @"All your data has not been transferred to us. Please click the 'Log' button, which is in 'My assistant IMS' to finalize the transfer";
                localNotification1.soundName = UILocalNotificationDefaultSoundName;
                
            
                BOOL graceFire = [[NSUserDefaults standardUserDefaults] boolForKey: @"fireInGrace"];
                if (!graceFire ) {
                    
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification1];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey: @"fireInGrace"];
                }
                
            
        }
        
        
        else {
            
            NSLog(@"NO");
        }
        
    }
    
}


#pragma mark - LoginViewControllerDelegate
- (void)didLoginWithUser:(Doctor *)doctor firstTime:(BOOL)firstTime {
    self.doctorInfo = doctor.doctorInfo;
    
    // Start background update
    [self updateApplicationData];
    
    void (^onComplete)() = ^{
        [self scheduleNotifications];
        [self getUnreadNumberOfNotificationsAndMessages];
        
        [[StatsReporter sharedStatsReporter] setUserId:doctor.userID];
        
        // Start counting session duration
        [[StatsReporter sharedStatsReporter] startEvent:kStatsEventTypeStartSession];
        [[StatsReporter sharedStatsReporter] startEvent:kStatsEventTypeLogin];
        [[StatsReporter sharedStatsReporter] endEvent:kStatsEventTypeLogin];
        
        // Start counting for session timeout
        [[SessionExpiredHandler sharedSessionExpiredHandler] setDelegate:self];
        [[SessionExpiredHandler sharedSessionExpiredHandler] startSession];
        
        // Show the doctor info view
        [self showVentana:kWindowDoctorInfo firstTime:firstTime];
        
        [[ChangeNotifier sharedNotifier] unpauseNotifying];
    };
    
    //Ravi_Bukka: Commented for unwanted web service call
    // If some info from doctor info is missing, try to obtain it from the server
//    if (self.doctorInfo.email == nil) {
//        [self obtainDoctorInfo:onComplete];
//    }
//    else {
        onComplete();
//    }
}

- (void)setIsInCollaborationPeriod:(BOOL)inCollaborationPeriod {
    self.inCollaborationPeriod = inCollaborationPeriod;
}

//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
- (void)setNeedToExtendGracePeriod:(BOOL)extendGracePeriod {
    
    self.extendGracePeriod = extendGracePeriod;
}

//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
-(BOOL)returnGracePeriondConfirmation {
    
    return self.extendGracePeriod;
}

#pragma mark - DoctorInfoViewControllerDelegate
- (void)doctorInfoViewController:(DoctorInfoViewController *)controller didSaveChanges:(DoctorInfo *)doctorInfo {
    [self showVentana:kWindowMainMenu firstTime:NO];
}

- (void)doctorInfoViewControllerDidCancelChanges:(DoctorInfoViewController *)controller {
    [self showVentana:kWindowMainMenu firstTime:NO];
}

- (void)doctorInfoViewControllerWillPresentPopUp:(DoctorInfoViewController*)controller {
    [self AperturaModal];
}

- (void)doctorInfoViewControllerWillDismissPopUp:(DoctorInfoViewController*)controller {
    [self CierreModal];
}

#pragma mark - ChatViewControllerDelegate

- (void)chatControllerDidFinishLoading:(ChatViewController *)chatController {
    [self refreshUnreadMessages:[chatController numberOfUnreadMessagesForDate:self.timeSinceOpenedChat]];
}

- (void)refreshUnreadMessages:(NSInteger)numberOfUnreadMessages {
    self.pendingChatNotifications = numberOfUnreadMessages;
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:(_pendingChatNotifications + _pendingInboxNotifications)];
    // Pasarle al menu view controller el numero a poner en el badge y el index de la bola
    [self.mainMenuViewController refreshItemWithBadgeCount:numberOfUnreadMessages andMenuItemId:kWindowChat];
}

#pragma mark - NotificationsViewControllerDelegate

- (void)notificationsInboxWillDisappear {
    self.timeSinceOpenedNotifications = [NSDate date];
}

- (void)notificationsControllerDidFinishLoading:(NotificationsViewController *)notificationsController {
    [self refreshUnreadNotifications:[notificationsController numberOfUnreadNotificationsForDate:self.timeSinceOpenedNotifications]];
}

- (void)refreshUnreadNotifications:(NSInteger)numberOfUnreadNotifications {
    self.pendingInboxNotifications = numberOfUnreadNotifications;
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:(_pendingInboxNotifications + _pendingChatNotifications)];
    // Pasarle al menu view controller el numero a poner en el badge y el index de la bola
    [self.mainMenuViewController refreshItemWithBadgeCount:numberOfUnreadNotifications andMenuItemId:kWindowNotifications];
}

#pragma mark - MainMenuViewControllerDelegate
- (void)mainMenuController:(MainMenuViewController *)controller didSelectMenuItem:(MainMenuItem *)menuItem {
    [self showVentana:menuItem.identifier firstTime:NO];
}

#pragma mark - ModalWebViewControllerDelegate

-(void) modalWebViewControllerRequestClose:(ModalWebViewController*)controller {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut  
                     animations:^{
                         CGAffineTransform scale = CGAffineTransformMakeScale(1.2, 1.2);  
                         controller.view.transform = scale;         
                     } 
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn  
                                          animations:^{
                                              CGAffineTransform scale = CGAffineTransformMakeScale(0.5, 0.5);  
                                              controller.view.transform = scale;
                                              controller.view.alpha=0.0;
                                              
                                          } 
                                          completion:^(BOOL finished) {
                                              [self CierreModal];
                                              [controller.view removeFromSuperview];
                                          }
                          ];
                     }
     ];
}

#pragma mark - ModalControllerProtocol
- (void)presentModalController:(UIViewController*)controller {
    UIView* parentView = self.window.rootViewController.view;
    
    CGSize frameSize = parentView.frame.size;
    controller.view.frame = CGRectMake(0, 0, frameSize.width, frameSize.height);

    [controller viewWillAppear:YES];
    controller.view.alpha = 0;
    [parentView addSubview:controller.view];
    [parentView bringSubviewToFront:controller.view];
    [controller viewDidAppear:YES];
    
    [UIView  animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState animations:^{
        controller.view.alpha = 1;
    } completion:nil];
}

- (void)dismissModalController:(UIViewController*)controller {
    [UIView  animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve | UIViewAnimationOptionCurveEaseInOut animations:^{
        controller.view.alpha = 0;
    } completion:^(BOOL finished) {
        [controller viewWillDisappear:YES];
        [controller.view removeFromSuperview];
        [controller viewDidDisappear:YES];
    }];
}

#pragma mark - SessionExpiredHandlerDelegate

- (void)sessionTimeExpired {
    if (ventanaActual != _loginViewController) {
        if (self.modalWebViewController != nil) {
            [self.modalWebViewController closeModal];
        }
        if (self.helpViewController != nil) {
            [self.helpViewController.view removeFromSuperview];
        }
        if (self.choiceController != nil) {
            [self dismissModalController:self.choiceController];
        }
        if (ventanaActual == _doctorInfoViewController) {
            [_doctorInfoViewController willPerformLogout];
        }
        [self logout];
    }
}
//Deepak_Carpenter : CR#1 Added for background fetch
#pragma mark - Background Fetch Methods
-(void) beginBackgroundUploadTask
{
    if(self.backgroundTask != UIBackgroundTaskInvalid)
    {
        [self endBackgroundUploadTask];
    }
    
    self.backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        
        DLog(@"Background Time:%f",[[UIApplication sharedApplication] backgroundTimeRemaining]);
        // [UIApplication sharedApplication].applicationIconBadgeNumber++;
        
        //   [[ChangeNotifier sharedNotifier] continueNotifying];
        [self endBackgroundUploadTask];
        
    }];
}

-(void)endBackgroundUploadTask
{
    [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTask];
    self.backgroundTask = UIBackgroundTaskInvalid;
}
-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    NSLog(@" $$$$$ Checking Backgroudn Fetch $$$$$ ");
    DLog(@"Fetched New Data");
    //Deepak_Carpenter : Added for background Fetch
    //    [[ChangeNotifier sharedNotifier] continueNotifying];
    [self beginBackgroundUploadTask];
    DLog(@"Background time Remaining: %f",[[UIApplication sharedApplication] backgroundTimeRemaining]);
    completionHandler(UIBackgroundFetchResultNewData);
    
}

#pragma mark - Update Application Data
-(void) updateApplicationData {
#ifndef __OFFLINE_VERSION__
    
//Ravi_Bukka: Added for CheckDataVersion Implementation
    [self.updateDataManager updateWithDoctorInfo:self.doctorInfo andOffset:nil];
#endif
}

-(UpdateDataManager *)updateDataManager {
    if (_updateDataManager == nil) {
        _updateDataManager = [[UpdateDataManager alloc] init];
    }
    return _updateDataManager;
}

#pragma mark - BITHockeyManagerDelegate
- (NSString *)customDeviceIdentifierForUpdateManager:(BITUpdateManager *)updateManager {
#ifdef CONFIGURATION_Beta
    if ([[UIDevice currentDevice] respondsToSelector:@selector(uniqueIdentifier)]) {
        return [[UIDevice currentDevice] uniqueIdentifier];
    }
#endif
    return nil;
}

- (BOOL)updateManagerShouldSendUsageData:(BITUpdateManager *)updateManager {
#ifdef CONFIGURATION_Beta
    return YES;
#else
    return NO;
#endif
}

@end
