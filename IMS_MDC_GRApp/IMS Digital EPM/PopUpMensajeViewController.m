//
//  PopUpMensajeViewController.m
//  IMS Digital EPM
//
//  Created by Ricardo Berzal on 30/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PopUpMensajeViewController.h"

@implementation PopUpMensajeViewController
@synthesize LTexto = _LTexto;
@synthesize Seleccionada,BotonSI=_BotonSI,BotonNO=_BotonNO;

//**********************
// initWithDelegate
//**********************
- (PopUpMensajeViewController*)initWithDelegate: (NSObject<PopUpMensajeViewControllerProtocol>*) delegate Tipo:(int)Tipo msg:(NSString*)msg
{
    self = [super initWithNibName:@"PopUpMensajeViewController" bundle:nil];
    if (self) {
        _delegate = delegate;
        _tipo=Tipo;
        _msg = [[NSString alloc] initWithString:msg];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    _BotonSI.Enabled=YES;
    _BotonNO.Enabled=YES;
    
    [_BotonSI setTitle:NSLocalizedString(@"DOCTOR_YES", nil) forState:UIControlStateNormal];
    [_BotonSI setTitle:NSLocalizedString(@"DOCTOR_YES", nil) forState:UIControlStateHighlighted];
    
    

    
    switch (_tipo)
    {
        case 0: {
            _BotonSI.hidden=NO;
            _BotonNO.hidden=NO;
            break;
        }
        case 1: {
            _BotonSI.hidden=NO;
            _BotonNO.hidden=YES;
            //se centra botón
            CGRect frame = _BotonSI.frame;
            frame.origin.x = (self.view.frame.size.width - frame.size.width) /2;
            _BotonSI.frame = frame;
            break;
        }
        case 2: {
            _BotonSI.hidden=YES;
            _BotonNO.hidden=YES;
            self.view.backgroundColor = [UIColor redColor];
        }
    }
    
//Ravi_Bukka: Added for the localization
    NSLog(@"messgae is ventanaspop %@", _msg);
    if ([_msg isEqualToString:@"¿Es esta la fecha de su primer día de colaboración?"])
        _LTexto.text = NSLocalizedString(@"DOCTOR_MSG_DATE_NOCH", nil);
        else if ([_msg isEqualToString:@"El primer día de colaboración no es el asignado por IMS.\n\n¿Está seguro de que desea cambiarlo?"])
            _LTexto.text = NSLocalizedString(@"DOCTOR_MSG_DATE_CH", nil);
    else
    _LTexto.text = _msg;
    

    
    
}

- (void)viewDidUnload
{
    [self setBotonSI:nil];
    [self setBotonNO:nil];
    
    [self setLTexto:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (IBAction)TouchUpInsideBoton:(id)sender {
    if (sender==_BotonSI)
        _BotonNO.Enabled=NO;
    Seleccionada=((UIButton*) sender).tag;
    if (_delegate!=nil) [_delegate CierreModalPopUpMensaje:self];
}

- (void)dealloc {
    [_BotonSI release];
    [_BotonNO release];
    [_LTexto release];
    [_msg release];
    [super dealloc];
}

@end
