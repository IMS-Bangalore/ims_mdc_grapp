//
//  ChangeNotifier.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 29/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChangeNotificationOperation.h"

@interface ChangeNotifier : NSObject<ChangeNotificationOperationDelegate>

+ (ChangeNotifier*)sharedNotifier;

- (void)startNotifying;
- (void)pauseNotifying;
- (void)unpauseNotifying;
- (void)stopNotifying;

@end
