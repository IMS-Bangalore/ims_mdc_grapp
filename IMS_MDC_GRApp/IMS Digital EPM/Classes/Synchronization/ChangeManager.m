//
//  ChangeManager.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 27/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "ChangeManager.h"
#import "SyncActionQueue.h"
#import "EntityFactory.h"
#import "Doctor.h"
#import "Dosage.h"
#import "TherapyElection.h"
#import "Age.h"
#import "ChangeNotifier.h"
#import "DoctorInfo+SortedPatients.h"
#import "Patient+SortedDiagnosis.h"
#import "Diagnosis+SortedTreatments.h"

#pragma mark - ChangeManager extension
@interface ChangeManager()
@property (nonatomic, assign) SyncActionQueue* syncQueue;
@property (nonatomic, assign) EntityFactory* entityFactory;
@end


#pragma mark - ChangeManager implementation
@implementation ChangeManager
@synthesize syncQueue = _syncQueue;
@synthesize entityFactory = _entityFactory;


#pragma mark - Init and dealloc
- (id)init {
    if (( self = [super init] )) {
        _syncQueue = [SyncActionQueue sharedQueue];
        _entityFactory = [EntityFactory sharedEntityFactory];
    }
    return self;
}

- (void)dealloc {
    [super dealloc];
}

- (void)recursiveCancelActions:(Synchronizable*)entity {
    [_syncQueue cancelSyncActions:entity.actions];
    for (Synchronizable* child in entity.childEntities) {
        [self recursiveCancelActions:child];
    }
}

#pragma mark - Private methods
- (void)deleteObject:(Synchronizable*)object fromObject:(Synchronizable*)fromObject {
    // If there's a pending 'Add' action, there's no need to send the delete action, and we can delete the object
    BOOL needsSyncAction = YES;
    for (SyncAction* action in object.actions) {
        if (!action.readyToSync.boolValue && [action.actionType isEqualToString:kSyncActionAdd]) {
            needsSyncAction = NO;
            break;
        }
    }
    
    if (needsSyncAction) {
        // Cancel all pending actions of this entity and all the children
        [self recursiveCancelActions:object];
        
        // Create sync action
        [_syncQueue createDeleteActionWithObject:object fromObject:fromObject];
    }
    else {
        // No need to notify, we can safely remove the object
        [_entityFactory deleteObject:object];
    }
}

- (void)updateObject:(Synchronizable*)object {
    
    // If there's a pending 'Update' or 'Add' action, there's no need to send another sync action
    BOOL needsSyncAction = YES;
    for (SyncAction* action in object.actions) {
        if (!action.readyToSync.boolValue && ([action.actionType isEqualToString:kSyncActionAdd] || [action.actionType isEqualToString:kSyncActionUpdate])) {
            needsSyncAction = NO;
            break;
        }
    }
    
    if (needsSyncAction) {
        DLog(@"Updating entity %@", object.entity.managedObjectClassName);
        [_syncQueue createUpdateActionWithObject:object];
    }
}

- (void)markAsReady:(Synchronizable*)entity recursive:(BOOL)recursive {
    // Mark the pending actions as ready to sync
    [_syncQueue confirmSyncActions:entity.actions];
    
    // Recursively mark the pending children actions as ready
    if (recursive) {
        for (Synchronizable* child in entity.childEntities) {
            [self markAsReady:child recursive:recursive];
        }
    }
}

#pragma mark - Generic methods
- (void)updateEntity:(Synchronizable *)entity {
    if (entity != nil) {
        [self updateObject:entity];
    }
}


#pragma mark - Public methods
static ChangeManager *changeManagerInstance = nil;
+ (ChangeManager*)manager {
    if (changeManagerInstance == nil) {
        changeManagerInstance = [[ChangeManager alloc] init];
    }
    [[SessionExpiredHandler sharedSessionExpiredHandler] refreshSession];
    
    return changeManagerInstance;
}

- (void)confirmChangesToEntity:(Synchronizable *)entity {
    [self confirmChangesToEntity:entity recursive:YES];
}

- (void)confirmChangesToEntity:(Synchronizable*)entity recursive:(BOOL)recursive {
    DLog(@"Confirming changes to %@", entity.entity.managedObjectClassName);
    
    // Mark the pending actions as ready to sync
    [self markAsReady:entity recursive:recursive];
    
    // Persist in-memory changes
    [_entityFactory saveChanges];
    
    // Start change notifier if needed
    [[ChangeNotifier sharedNotifier] startNotifying];
}

- (void)discardChanges {
    DLog(@"Discarding pending changes");
    
    // Rollback changes in-memory changes
    [_entityFactory discardChanges];
    
    // All in-memory requests should have been canceled at this point
}

#pragma mark DoctorInfo
- (DoctorInfo*)createDoctorInfo {
    DLog(@"Creating DoctorInfo");
    
    // Create an empty doctor
    DoctorInfo* doctorInfo = [_entityFactory insertNewWithEntityName:NSStringFromClass([DoctorInfo class])];
    doctorInfo.doctor = [_entityFactory insertNewWithEntityName:NSStringFromClass([Doctor class])];
  
    [_syncQueue createAddActionWithObject:doctorInfo];
    
    return doctorInfo;
}

- (void)updateDoctorInfo:(DoctorInfo*)doctorInfo {
    [self updateEntity:doctorInfo];
}

- (void)deleteDoctorInfo:(DoctorInfo*)doctorInfo {
    DLog(@"Deleting DoctorInfo");
    
    [self deleteObject:doctorInfo fromObject:nil];
    
    // Confirm pending deletion action as they cannot be reverted
    [_syncQueue confirmSyncActions:doctorInfo.actions];
}

#pragma mark Patient
- (Patient*)addPatientToDoctorInfo:(DoctorInfo*)doctorInfo {
    DLog(@"Creating Patient");
    
    // Create empty objects
    Patient* patient = [[EntityFactory sharedEntityFactory] insertNewWithEntityName:NSStringFromClass([Patient class])];
    patient.age = [[EntityFactory sharedEntityFactory] insertNewWithEntityName:NSStringFromClass([Age class])];
    
    // Assign identifier sequentially from last object
    NSInteger index = 0;
    if (doctorInfo.patients.count > 0) {
        Patient* lastPatient = [[doctorInfo sortedPatients] lastObject];
        index = lastPatient.index.integerValue + 1;
    }
    patient.index = [NSNumber numberWithInteger:index];
    patient.identifier = [NSString stringWithFormat:@"%d", index];
    
    // Assign relationships
    patient.doctorInfo = doctorInfo;
    patient.parentEntity = doctorInfo;
    
    NSLog(@"patient info age -- %@, visitdate --%@, and complete Visit -- %@", patient.age.value, patient.visitDate, patient);
    [_syncQueue createAddActionWithObject:patient];
    
    return patient;
}

- (void)updatePatient:(Patient*)patient {
    [self updateEntity:patient];
}

- (void)deletePatient:(Patient*)patient {
    DLog(@"Deleting Patient");
    
    DoctorInfo* doctorInfo = patient.doctorInfo;
    
    // Remove relationship, the object will be removed when the action is executed
    patient.doctorInfo = nil;
    
    // Create sync action
    [self deleteObject:patient fromObject:doctorInfo];
}

#pragma mark Diagnosis
- (Diagnosis*)addDiagnosisToPatient:(Patient*)patient {
    DLog(@"Creating Diagnosis");
    
    // Create empty objects
    Diagnosis* diagnosis = [_entityFactory insertNewWithEntityName:NSStringFromClass([Diagnosis class])];
    
    // Assign identifier sequentially from last object
    NSInteger index = 0;
    if (patient.diagnostic.count > 0) {
        Diagnosis* lastDiagnosis = [[patient sortedDiagnosis] lastObject];
        index = lastDiagnosis.index.integerValue + 1;
    }
    diagnosis.index = [NSNumber numberWithInteger:index];
    diagnosis.identifier = [NSString stringWithFormat:@"%d", index];
    
    // Assign relationships
    diagnosis.patient = patient;
    diagnosis.parentEntity = patient;
    
    [_syncQueue createAddActionWithObject:diagnosis];
    
    return diagnosis;
}

- (void)updateDiagnosis:(Diagnosis*)diagnosis {
    [self updateEntity:diagnosis];
}

- (void)deleteDiagnosis:(Diagnosis*)diagnosis {
    DLog(@"Creating Diagnosis");
    
    Patient* patient = diagnosis.patient;
    
    // Remove relationship, the object will be removed when the action is executed
    diagnosis.patient = nil;
    
    [self deleteObject:diagnosis fromObject:patient];
}

#pragma mark Treatment
- (Treatment*)addTreatmentToDiagnosis:(Diagnosis*)diagnosis {
    DLog(@"Creating Treatment");
    
    // Create empty objects
    Treatment* treatment = [_entityFactory insertNewWithEntityName:NSStringFromClass([Treatment class])];
    treatment.dosage = [_entityFactory insertNewWithEntityName:NSStringFromClass([Dosage class])];
    
    // Assign identifier sequentially from last object
    NSInteger index = 0;
    if (diagnosis.treatments.count > 0) {
        Treatment* lastTreatment = [[diagnosis sortedTreatments] lastObject];
        index = lastTreatment.index.integerValue + 1;
    }
    treatment.index = [NSNumber numberWithInteger:index];
    treatment.identifier = [NSString stringWithFormat:@"%d", index];
    
    // Assign relationships
    treatment.diagnosis = diagnosis;
    treatment.parentEntity = diagnosis;
    
    // Add sync action
    [_syncQueue createAddActionWithObject:treatment];
    
    return treatment;
}

- (void)updateTreatment:(Treatment*)treatment {
    [self updateEntity:treatment];
}

- (void)deleteTreatment:(Treatment*)treatment {
    DLog(@"Deleting Treatment");
    
    Diagnosis* diagnosis = treatment.diagnosis;
    
    // Remove relationship, the object will be removed when the action is executed
    treatment.diagnosis = nil;
    
    [self deleteObject:treatment fromObject:diagnosis];
}

@end
