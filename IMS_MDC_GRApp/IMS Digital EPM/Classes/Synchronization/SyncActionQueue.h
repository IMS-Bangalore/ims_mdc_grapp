//
//  SyncActionQueue.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 27/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SyncAction.h"
#import "Synchronizable.h"

NSString* const kSyncActionAdd;
NSString* const kSyncActionUpdate;
NSString* const kSyncActionDelete;

@interface SyncActionQueue : NSObject

/** @brief Singleton access */
+ (SyncActionQueue*)sharedQueue;

- (SyncAction*)firstConfirmedSyncAction;
- (NSArray*)confirmedSyncActions;
- (NSArray*)pendingSyncActions;

- (void)confirmSyncAction:(SyncAction*)syncAction;
- (void)confirmSyncActions:(NSSet*)syncActions;
- (void)confirmAllSyncActions;

- (void)cancelSyncAction:(SyncAction*)syncAction;
- (void)cancelSyncActions:(NSSet*)syncActions;
- (void)cancelAllSyncActions;

- (SyncAction*)createDeleteActionWithObject:(Synchronizable*)syncObject fromObject:(Synchronizable*)fromSyncObject;
- (SyncAction*)createAddActionWithObject:(Synchronizable*)syncObject;
- (SyncAction*)createUpdateActionWithObject:(Synchronizable*)syncObject;

@end
