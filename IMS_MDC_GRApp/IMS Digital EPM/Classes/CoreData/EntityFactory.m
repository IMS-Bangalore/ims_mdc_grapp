//
//  EntityFactory.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "EntityFactory.h"
#import "MultipleChoiceViewController.h"

@interface EntityFactory()

@property (readonly) NSOperationQueue *backgroundOperationQueue;
@property (readonly) NSManagedObjectContext *backgroundManagedObjectContext;

@end

static NSString* const kDataBaseName = @"CoreDataEMP.sqlite";
static const NSInteger kFetchLimit = 50;
static const NSInteger kRecentMedicamentCount = 5;

@implementation EntityFactory
@synthesize managedObjectContext = _managedObjectContext;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize backgroundOperationQueue = _backgroundOperationQueue;

- (id)init {
    self = [super init];
    if (self) {
        _backgroundOperationQueue = [[NSOperationQueue alloc] init];
        _backgroundOperationQueue.maxConcurrentOperationCount = 1;
    }
    return self;
}

- (NSManagedObjectContext*)createContext {
    NSManagedObjectContext* context = nil;
    if (self.persistentStoreCoordinator != nil) {
        context = [[[NSManagedObjectContext alloc] init] autorelease];
        context.persistentStoreCoordinator = self.persistentStoreCoordinator;
        // Merge policy gives priority to persistent store changes for other than main context
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
        
        // Register context with the notification center
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self
               selector:@selector(mergeChanges:)
                   name:NSManagedObjectContextDidSaveNotification
                 object:context];
    }
    
    return context;
}

#pragma mark - Singleton access
static EntityFactory *sharedEntityFactoryInstance = nil;
+(EntityFactory *) sharedEntityFactory {
    if (sharedEntityFactoryInstance == nil) {
        @synchronized(self) {
            if (sharedEntityFactoryInstance == nil) {
                sharedEntityFactoryInstance = [[self alloc] init];
            }
        }
        // Initialize model context in main thread
        [sharedEntityFactoryInstance performSelectorOnMainThread:@selector(managedObjectContext) withObject:nil waitUntilDone:YES];
    }
    return sharedEntityFactoryInstance;
}

#pragma mark - Init and dealloc
- (void)dealloc {
    [_managedObjectModel release];
    [_managedObjectContext release];
    [_persistentStoreCoordinator release];
    [_backgroundOperationQueue release];
    [super dealloc];
}

#pragma mark - Accessors
- (NSManagedObjectContext*) backgroundManagedObjectContext {
    return [self createContext];
}

#pragma mark - Private methods
- (void)mergeChanges:(NSNotification *)notification {
    DLog(@"Merging changes from other contexts into Main Context");
    
    NSManagedObjectContext *mainContext = self.managedObjectContext;
    
    // Merge changes into the main context on the main thread
    [mainContext performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:)
                                  withObject:notification
                               waitUntilDone:YES];  
}

#pragma mark -
#pragma mark Saving

/**
 Performs the save action for the application, which is to send the save:
 message to the application's managed object context.
 */
- (void)saveChanges {
	DAssert([NSThread isMainThread], @"This method must be called from Main Thread");
    
    NSError *error;
    if (![[self managedObjectContext] save:&error]) {
		// Update to handle the error appropriately.
		DLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
}

- (void)discardChanges {
    DAssert([NSThread isMainThread], @"This method must be called from Main Thread");
    
    [[self managedObjectContext] rollback];
}

#pragma mark - Object creation
- (id)insertNewWithEntityName:(NSString*)entityName {
    DAssert([NSThread isMainThread], @"This method must be called from Main Thread");
    
    return [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:self.managedObjectContext];
}

#pragma mark - Object deletion
- (void)deleteObject:(NSManagedObject*)object {
    DAssert([NSThread isMainThread], @"This method must be called from Main Thread");
    
    [[self managedObjectContext] deleteObject:object];
}

#pragma mark - Saved searchs
- (Doctor*)fetchDoctorForUserID:(NSString*)userID {
    DAssert([NSThread isMainThread], @"This method must be called from Main Thread");
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"userID == %@", userID];
    
    NSFetchRequest* fetchRequest = [[[NSFetchRequest alloc] initWithEntityName:@"Doctor"] autorelease];
    fetchRequest.predicate = predicate;
    fetchRequest.fetchLimit = 1;
    
    Doctor* result = nil;
    NSError* error = nil;
    NSArray* array = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error != nil) {
        DLog(@"Error fetching Doctor with userID '%@': %@, %@", userID, error, [error userInfo]);
    }
    else if (array.count > 0) {
        result = [array objectAtIndex:0];
    }
    
    return result;
}

- (id<CancelableOperation>) fecthMedicamentsInBackgroundWithFilter:(NSString*)filter ofType:(ProductType*)productType onComplete:(EntityFetchCompleteBlock)block {
    NSPredicate* typeFilterPredicate = nil;
    if (productType != nil) {
        typeFilterPredicate = [NSPredicate predicateWithFormat:@"productType == %@", productType];
    }
    return [self fetchRemovableEntitiesInBackground:@"Medicament" filteringByName:filter additionalPredicate:typeFilterPredicate sortResults:YES onComplete:block];
}

- (NSArray*)fetchProvinces {
    DAssert([NSThread isMainThread], @"This method must be called from Main Thread");
    
    NSFetchRequest* fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Province"];
    
    NSError* error = nil;
    NSArray* results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error != nil) {
        DLog(@"Error fetching provinces: %@, %@", error, [error userInfo]);
    }
    
    // Manually sort by name ascending ignoring case and diacritic characters
    return [results sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [[obj1 name] compare:[obj2 name] options:NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch];
    }];
}

- (NSArray*)fetchUniversities:(NSString*)nameFilter {
    NSMutableArray* predicates = [NSMutableArray arrayWithCapacity:2];
    
    // If no filter is provided, return all the elements
    if (nameFilter.length > 0) {
        NSArray *words = [self obtainsDifferentWordsInAName:nameFilter];
        for (NSString *word in words) {
            [predicates addObject:[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", word]];
        }
    }
    
    [predicates addObject:[NSPredicate predicateWithFormat:@"userDefined == NO"]];
    
    NSPredicate* predicate = [[[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:predicates] autorelease];
    return [self fetchEntities:@"University" predicate:predicate sortResults:YES];
}

//@Deepak Carpenter // Method for placeofVisit
- (NSArray*)fetchPlaceOfVisit:(NSString*)nameFilter {
    
    NSMutableArray* placeOfVisit = [NSMutableArray arrayWithCapacity:2];
    
    // If no filter is provided, return all the elements
    if (nameFilter.length > 0) {
        NSArray *words = [self obtainsDifferentWordsInAName:nameFilter];
        for (NSString *word in words) {
            [placeOfVisit addObject:[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", word]];
        }
    }
    
    [placeOfVisit addObject:[NSPredicate predicateWithFormat:@"deleted == NO"]];
    
    NSPredicate* predicatePlaceOfVisit = [[[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:placeOfVisit] autorelease];
    return [self fetchEntities:@"PlaceOfVisit" predicate:predicatePlaceOfVisit sortResults:YES];
}


- (NSArray*)fetchRecentMedicamentsforDoctorInfo:(DoctorInfo*)doctorInfo andProductType:(ProductType*)productType {
    NSMutableArray* predicates = [NSMutableArray array];
    
    [predicates addObject:[NSPredicate predicateWithFormat:@"doctorInfo == %@", doctorInfo]];
    [predicates addObject:[NSPredicate predicateWithFormat:@"count >= %i", kRecentMedicamentCount]];
    [predicates addObject:[NSPredicate predicateWithFormat:@"medicament.deleted = NO OR medicament.deleted = nil"]];
    if (productType != nil) {
        [predicates addObject:[NSPredicate predicateWithFormat:@"medicament.productType = %@", productType]];
    }

    NSFetchRequest* fetchRequest = [[[NSFetchRequest alloc] initWithEntityName:@"RecentMedicament"] autorelease];
    fetchRequest.predicate = [[[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:predicates] autorelease];
    fetchRequest.fetchLimit = kFetchLimit;
    
    NSError* error = nil;
    NSArray* results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error != nil) {
        DLog(@"Error fetching medicaments: %@, %@", error, [error userInfo]);
    }
    return results;
}

- (NSArray*)fetchMedicalCenters {
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"userDefined == NO"];
    return [self fetchEntities:@"MedicalCenter" predicate:predicate sortResults:YES];
}

- (NSArray*)obtainsDifferentWordsInAName:(NSString*)word {
    NSArray *wordsAndEmptyStrings = [word componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSArray *words = [wordsAndEmptyStrings filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
    return words;
}

#pragma mark - Predicates
- (NSMutableArray*)addPredicateWithNameFilter:(NSString*)nameFilter inPredicateArray:(NSMutableArray*)predicates  {
    if (predicates == nil) {
        predicates = [NSMutableArray arrayWithCapacity:2];
    }
    // If no filter is provided, return all the elements
    if (nameFilter.length > 0) {
        NSArray *words = [self obtainsDifferentWordsInAName:nameFilter];
        for (NSString *word in words) {
            [predicates addObject:[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", word]];
        }
    }
    return predicates;
}

- (NSPredicate*)notDeletedPredicate {
    return [NSPredicate predicateWithFormat:@"deleted = NO OR deleted = nil"];
}

#pragma mark - Generic searches
- (id)fetchEntity:(NSString*)entityName usingIdentifier:(NSString*)identifier withManagedObjectContext:(NSManagedObjectContext*)managedObjectContext{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"identifier == %@", identifier]; 
    NSFetchRequest* fetchRequest = [[[NSFetchRequest alloc] initWithEntityName:entityName] autorelease];
    fetchRequest.predicate = predicate;
    fetchRequest.fetchLimit = 1;
    
    NSError* error = nil;
    NSArray* results = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error != nil) {
        DLog(@"Error fetching entity %@: %@, %@", entityName, error, [error userInfo]);
    }
    
    id result = nil;
    if (results.count > 0) {
        result = [results objectAtIndex:0];
    }
    
    return result;
}

- (id)fetchEntity:(NSString*)entityName usingIdentifier:(NSString*)identifier {
    DAssert([NSThread isMainThread], @"This method must be called from Main Thread");
    return [self fetchEntity:entityName usingIdentifier:identifier withManagedObjectContext:self.managedObjectContext];
    
}

- (NSArray*)fetchEntities:(NSString*)entityName filteringByName:(NSString*)nameFilter {
    return [self fetchEntities:entityName filteringByName:nameFilter sortResults:YES];
}

- (NSArray*)fetchEntities:(NSString*)entityName filteringByName:(NSString*)nameFilter sortResults:(BOOL)sortResults {
    NSMutableArray* predicates = [NSMutableArray arrayWithCapacity:2];
    
    // If no filter is provided, return all the elements
    if (nameFilter.length > 0) {
        NSArray *words = [self obtainsDifferentWordsInAName:nameFilter];
        for (NSString *word in words) {
            [predicates addObject:[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", word]];
        }
    }
    NSPredicate* predicate = [[[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:predicates] autorelease];
    return [self fetchEntities:entityName predicate:predicate sortResults:sortResults];
}

- (NSArray*)fetchEntities:(NSString*)entityName sortByIdentifier:(BOOL)sortByIdentifier {
    NSArray* sortDescriptors = nil;
    if (sortByIdentifier) {
        sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:YES selector:@selector(caseInsensitiveCompare:)]];
    }
    return [self fetchEntities:entityName predicate:nil sortDescriptors:sortDescriptors];
}

- (NSArray*)fetchEntities:(NSString*)entityName predicate:(NSPredicate*)predicate sortResults:(BOOL)sortResults {
    NSArray* sortDescriptors = nil;
    if (sortResults) {
        sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)]];
    }
    return [self fetchEntities:entityName predicate:predicate sortDescriptors:sortDescriptors];
}

- (NSArray*)fetchEntities:(NSString*)entityName predicate:(NSPredicate*)predicate sortDescriptors:(NSArray*)sortDescriptors {
    DAssert([NSThread isMainThread], @"This method must be called from Main Thread");
    
    return [self fetchEntities:entityName predicate:predicate sortDescriptors:sortDescriptors withManagedObjectContext:self.managedObjectContext];
}

- (NSArray*)fetchEntities:(NSString*)entityName predicate:(NSPredicate*)predicate sortDescriptors:(NSArray*)sortDescriptors withManagedObjectContext:(NSManagedObjectContext*)managedObjectContext {
    NSFetchRequest* fetchRequest = [[[NSFetchRequest alloc] initWithEntityName:entityName] autorelease];
    fetchRequest.fetchLimit = kFetchLimit;
    fetchRequest.predicate = predicate;
    fetchRequest.sortDescriptors = sortDescriptors;
    
    NSError* error = nil;
    NSArray* results = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error != nil) {
        DLog(@"Error fetching entities %@: %@, %@", entityName, error, [error userInfo]);
    }
    
    return results;
}

- (id<CancelableOperation>)fetchRemovableEntitiesInBackground:(NSString*)entityName filteringByName:(NSString*)nameFilter sortResults:(BOOL)sortResults onComplete:(EntityFetchCompleteBlock)block {
    return [self fetchRemovableEntitiesInBackground:entityName filteringByName:nameFilter additionalPredicate:nil sortResults:sortResults onComplete:block];
}

    
- (id<CancelableOperation>)fetchRemovableEntitiesInBackground:(NSString*)entityName filteringByName:(NSString*)nameFilter additionalPredicate:(NSPredicate*)additionalPredicate sortResults:(BOOL)sortResults onComplete:(EntityFetchCompleteBlock)block {
    NSMutableArray* predicates = [NSMutableArray array];
    
    // If no filter is provided, return all the elements
    if (nameFilter.length > 0) {
        NSArray *words = [self obtainsDifferentWordsInAName:nameFilter];
        BOOL firstWord = YES;
        NSMutableArray* subpredicatesFirstWord = [NSMutableArray array];
        NSMutableArray* subpredicatesSecondWords = [NSMutableArray array];
        NSString* stringFirstWord = @"";
        NSPredicate* doesNotBeginWithFirstWordPredicate = nil;
        for (NSString *word in words) {
            if (firstWord) {
                [subpredicatesFirstWord addObject:[NSPredicate predicateWithFormat:@"name BEGINSWITH[cd] %@", word]];
                stringFirstWord = word;
                [subpredicatesSecondWords addObject:[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", word]];
                doesNotBeginWithFirstWordPredicate = [NSPredicate predicateWithFormat:@"NOT (name BEGINSWITH[cd] %@)", stringFirstWord];
            } else {
                [subpredicatesFirstWord addObject:[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", word]];
                [subpredicatesSecondWords addObject:doesNotBeginWithFirstWordPredicate];
                [subpredicatesSecondWords addObject:[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", word]];
            }
            firstWord = NO;
        }
        [subpredicatesFirstWord addObject:[self notDeletedPredicate]];
        
        NSPredicate* firstPredicate = [[[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:subpredicatesFirstWord] autorelease];
        [predicates addObject:firstPredicate];
        
        if (words.count == 1) {
            NSPredicate* containsPredicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", stringFirstWord];
            NSPredicate* containsAndDoesNotBeginsPredicate = [[[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:@[containsPredicate, doesNotBeginWithFirstWordPredicate, [self notDeletedPredicate]]] autorelease];
            [predicates addObject:containsAndDoesNotBeginsPredicate];
        }
        else if (words.count > 1) {
            [subpredicatesSecondWords addObject:[self notDeletedPredicate]];
            NSPredicate* secondPredicate = [[[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:subpredicatesSecondWords] autorelease];
            [predicates addObject:secondPredicate];
        }
    } else {
        [predicates addObject:[self notDeletedPredicate]];
    }
    
    if (additionalPredicate != nil) {
        NSMutableArray* compoundPredicates = [NSMutableArray arrayWithCapacity:predicates.count];
        for (NSPredicate* predicate in predicates) {
            NSPredicate* compoundPredicate = [[[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:@[predicate, additionalPredicate]] autorelease];
            [compoundPredicates addObject:compoundPredicate];
        }
        predicates = [NSArray arrayWithArray:compoundPredicates];
    }
    
    return [self fetchEntitiesInBackground:entityName predicates:predicates sortResults:sortResults onComplete:block];;
}

- (id<CancelableOperation>)fetchEntitiesInBackground:(NSString*)entityName filteringByName:(NSString*)nameFilter onComplete:(EntityFetchCompleteBlock)block {
    return [self fetchEntitiesInBackground:entityName filteringByName:nameFilter sortResults:YES onComplete:block];
}

- (id<CancelableOperation>)fetchEntitiesInBackground:(NSString*)entityName filteringByName:(NSString*)nameFilter sortResults:(BOOL)sortResults onComplete:(EntityFetchCompleteBlock)block {
    NSMutableArray* predicates = [self addPredicateWithNameFilter:nameFilter inPredicateArray:nil];
    return [self fetchEntitiesInBackground:entityName predicates:predicates sortResults:sortResults onComplete:block];
}

- (id<CancelableOperation>)fetchEntitiesInBackground:(NSString*)entityName predicates:(NSArray*)predicates sortResults:(BOOL)sortResults onComplete:(EntityFetchCompleteBlock)block {
    NSArray* sortDescriptors = nil;
    if (sortResults) {
        sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)]];
    }
    return [self fetchEntitiesInBackground:entityName predicates:predicates sortDescriptors:sortDescriptors onComplete:block];
}

- (id<CancelableOperation>)fetchEntitiesInBackground:(NSString*)entityName sortByIdentifier:(BOOL)sortByIdentifier onComplete:(EntityFetchCompleteBlock)block {
    NSArray* sortDescriptors = nil;
    if (sortByIdentifier) {
        sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:YES selector:@selector(caseInsensitiveCompare:)]];
    }
    return [self fetchEntitiesInBackground:entityName predicates:nil sortDescriptors:sortDescriptors onComplete:block];
}

- (id<CancelableOperation>)fetchEntitiesInBackground:(NSString*)entityName predicates:(NSArray*)predicates sortDescriptors:(NSArray*)sortDescriptors onComplete:(EntityFetchCompleteBlock)block {
    NSOperation* operation = [NSBlockOperation blockOperationWithBlock:^{
        NSMutableArray* results = [NSMutableArray array];
        for (NSPredicate* predicate in predicates) {
            [results addObjectsFromArray:[self fetchEntities:entityName predicate:predicate sortDescriptors:sortDescriptors withManagedObjectContext:self.backgroundManagedObjectContext]];
        }
        NSMutableArray* idArray = [NSMutableArray arrayWithCapacity:results.count];
        for (NSManagedObject* entity in results) {
            [idArray addObject:entity.objectID];
        }
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            NSMutableArray* resultArray = [NSMutableArray arrayWithCapacity:idArray.count];
            for (NSManagedObjectID* objectId in idArray) {
                NSError* error = nil;
                NSManagedObject* object = [self.managedObjectContext objectWithID:objectId];
                if (error == nil) {
                    [resultArray addObject:object];
                }
            }
            block(resultArray);
        }];
    }];
    [_backgroundOperationQueue addOperation:operation];
    return (id<CancelableOperation>) operation;
}

- (MetaInfo*)getMetaInfoWithContext:(NSManagedObjectContext*)moc {
    
    NSFetchRequest* fetchRequest = [[[NSFetchRequest alloc] initWithEntityName:@"MetaInfo"] autorelease];
    fetchRequest.fetchLimit = 1;
    
    NSError* error = nil;
    NSArray* results = [moc executeFetchRequest:fetchRequest error:&error];
    if (error != nil) {
        DLog(@"Error fetching entity %@: %@, %@", @"MetaInfo", error, [error userInfo]);
    }
    
    MetaInfo* result = nil;
    if (results.count > 0) {
        result = [results objectAtIndex:0];
    }
    
    return result;
}

#pragma mark -
#pragma mark Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *) managedObjectContext {
	
    if (_managedObjectContext == nil && self.persistentStoreCoordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        _managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator;
        // Merge policy gives priority to in-memory changes
        _managedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
    }
    return _managedObjectContext;
}


/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 */
- (NSManagedObjectModel *)managedObjectModel {
	
    if (_managedObjectModel == nil) {
        _managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:nil] retain];
    }
    return _managedObjectModel;
}


/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
	
	NSString *storePath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent: kDataBaseName];
	NSURL *storeUrl = [NSURL fileURLWithPath:storePath];
	
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption,
                             nil];
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    
	NSError *error;
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) {
		// Update to handle the error appropriately.
		DLog(@"Unresolved error %@, %@", error, [error userInfo]);
        self.persistentStoreCoordinator = nil;
    } 
	
    return _persistentStoreCoordinator;
}

- (void)deleteDatabase {
    self.persistentStoreCoordinator = nil;
    self.managedObjectContext = nil;
    self.managedObjectModel = nil;
    
    NSString *storePath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent: kDataBaseName];
	NSURL *storeUrl = [NSURL fileURLWithPath:storePath];
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtURL:storeUrl error:&error];
}

#pragma mark -
#pragma mark Application's documents directory

/**
 Returns the path to the application's documents directory.
 */
- (NSString *)applicationDocumentsDirectory {
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}



@end
