//
//  InitialLoadProcessor.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "InitialLoadProcessor.h"

#import <CoreData/CoreData.h>
#import "EntityFactory.h"
#import "CSVToCoreDataImporter.h"
#import "University.h"
#import "Presentation.h"
#import "Medicament.h"
#import "MedicalCenter.h"

static const NSInteger kCurrentDatabaseVersion = 8;

#pragma mark - Implementation
@implementation InitialLoadProcessor


- (void)dealloc {
    [super dealloc];
}

- (BOOL)needsInitialLoad:(NSManagedObjectContext*)moc {
    MetaInfo* metaInfo = [[EntityFactory sharedEntityFactory] getMetaInfoWithContext:moc];
    return metaInfo == nil;
}

- (BOOL)outdatedDatabase:(NSManagedObjectContext*)moc {
    MetaInfo* metaInfo = [[EntityFactory sharedEntityFactory] getMetaInfoWithContext:moc];
    return metaInfo != nil && metaInfo.initialLoadVersion != nil && metaInfo.initialLoadVersion.integerValue < kCurrentDatabaseVersion;
}

- (void)deleteAllEntities:(NSString*)entityName inContext:(NSManagedObjectContext*)context{
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    [allCars setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [context executeFetchRequest:allCars error:&error];
    [allCars release];
    //error handling goes here
    for (NSManagedObject * car in cars) {
        [context deleteObject:car];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    if (saveError != nil) {
        DLog(@"Error deleting files from context: %@", saveError.localizedDescription);
    }
}

- (void)preLoadData:(id<InitialLoadProcessorDelegate>)delegate {
    // Create a new NSManagedObjectContext to reduce memory peak
    NSManagedObjectContext* moc = [[EntityFactory sharedEntityFactory] createContext];
    
    if (moc == nil || [self outdatedDatabase:moc]) {
        NSError* error = [NSError errorWithDomain:@"es.lumata.initialLoad" code:1 userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"INITIAL_LOAD_ERROR", nil) forKey:NSLocalizedDescriptionKey]];
        [delegate initialLoadProcessorDidFailWithError:error];
    } else {
        // Disable undo manager for this context to reduce memory footprint
        moc.undoManager = nil;
        
        [delegate initialLoadProcessorDidBeginLoading];
        
        // Use a new release pool while importing data
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
        
        // Create variables
        NSString* filePath;
        NSString* entityName;
        NSArray* fieldNames;
        NSArray* fieldTypes;
        CSVToCoreDataImporter* csvImporter;
        NSError* error;
        
        // Frequency
        filePath = [[NSBundle mainBundle] pathForResource:@"ims_frequency_gr" ofType:@"csv"];
        entityName = @"Frequency";
        fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
        fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
        csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:YES context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
        
        error = nil;
        [delegate initialLoadProcessorDidBeginProcessing:entityName];
        [csvImporter parseFile:filePath error:&error];
        [delegate initialLoadProcessorDidEndProcessing:entityName];
        
        if (error != nil) {
            DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
        }
        
        
        // Don't load data if not necessary
        if ([self needsInitialLoad:moc]) {
            
            // Provinces
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_provinces_gr" ofType:@"csv"];
            entityName = @"Province";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            //Kanchan Nair:  placeOfVisit -- > Insurance
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_insurance_gr" ofType:@"csv"];
            entityName = @"Insurance";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:YES context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            //Kanchan Nair:  drugindicaor
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_drugindicator_gr" ofType:@"csv"];
            entityName = @"DrugIndicator";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:YES context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            //Kanchan Nair:  Drug Reimbursement
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_drugreimbursement_gr" ofType:@"csv"];
            entityName = @"DrugReimbursement";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:YES context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            //Kanchan Nair:  Patient Insurance
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_patientinsurance_gr" ofType:@"csv"];
            entityName = @"PatientInsurance";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:YES context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            //placeOfVisit : by Deepak Carpenter
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_placeofvisit_gr" ofType:@"csv"];
            entityName = @"PlaceOfVisit";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:YES context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            //smoker : by Deepak Carpenter
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_smoker_gr" ofType:@"csv"];
            entityName = @"Smoker";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:YES context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            // Universities
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_universities_gr" ofType:@"csv"];
            entityName = @"University";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:^(id targetObject) {
                University* university = targetObject;
                university.userDefined = [NSNumber numberWithBool:NO];
            }] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            // ConsultTypes
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_visit_gr" ofType:@"csv"];
            entityName = @"ConsultType";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            [moc save:&error];
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            [moc reset];
            [pool drain];
            pool = [[NSAutoreleasePool alloc] init];
            
            
            // Recurrence
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_recurrance_gr" ofType:@"csv"];
            entityName = @"Recurrence";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            [moc save:&error];
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            [moc reset];
            [pool drain];
            pool = [[NSAutoreleasePool alloc] init];
            
            // SickFund
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_sickfund_gr" ofType:@"csv"];
            entityName = @"SickFund";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            [moc save:&error];
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            [moc reset];
            [pool drain];
            pool = [[NSAutoreleasePool alloc] init];
            
            
            // DiagnosisType
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_diagnosis_gr" ofType:@"csv"];
            entityName = @"DiagnosisType";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            [moc save:&error];
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            [moc reset];
            [pool drain];
            pool = [[NSAutoreleasePool alloc] init];
            
            // Effects
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_effects_gr" ofType:@"csv"];
            entityName = @"Effect";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            // DurationType
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_durationtype_gr" ofType:@"csv"];
            entityName = @"DurationType";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:YES context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            // Gender
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_gender_gr" ofType:@"csv"];
            entityName = @"Gender";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            // AgeType
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_agetype_gr" ofType:@"csv"];
            entityName = @"AgeType";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            // VisitType
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_consultation_gr" ofType:@"csv"];
            entityName = @"VisitType";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            // Pathology
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_pathology_gr" ofType:@"csv"];
            entityName = @"Pathology";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            // TherapyReplacementReason
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_reasons_gr" ofType:@"csv"];
            entityName = @"TherapyReplacementReason";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            // Medical Center
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_medicalcenter_gr" ofType:@"csv"];
            entityName = @"MedicalCenter";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:^(id targetObject) {
                MedicalCenter* medicalCenter = targetObject;
                medicalCenter.userDefined = [NSNumber numberWithBool:NO];
            }] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            // Specialties
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_specialities_gr" ofType:@"csv"];
            entityName = @"Specialty";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            // Other Specialties
            //Deepak_Carpenter : Added to resolve "0" truncate issue
          //  filePath = [[NSBundle mainBundle] pathForResource:@"ims_specialities_others_gr" ofType:@"csv"];
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_specialities_gr" ofType:@"csv"];

            entityName = @"OtherSpecialty";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            // Therapy Type
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_therapy_gr" ofType:@"csv"];
            entityName = @"TherapyType";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            //Deepak_Carpenter: Added for Therapy Election Radio Buttons
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_therapykind_gr" ofType:@"csv"];
            entityName = @"TherapyElection";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            //
            
            
            
            
            [moc save:&error];
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            [moc reset];
            [pool drain];
            pool = [[NSAutoreleasePool alloc] init];
            
            /* Cache used for medicaments */
            __block NSMutableDictionary* medicamentCache = [NSMutableDictionary dictionary];
            __block NSMutableDictionary* unitTypeCache = [NSMutableDictionary dictionary];
            __block NSMutableDictionary* productTypeCache = [NSMutableDictionary dictionary];
            
            
            // ProductType
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_product_type_gr" ofType:@"csv"];
            entityName = @"ProductType";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:^(id targetObject) {
                // Cache the result for later use
                [productTypeCache setObject:targetObject forKey:[targetObject identifier]];
            }] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            // Unit Type
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_units_gr" ofType:@"csv"];
            entityName = @"UnitType";
            fieldNames = [NSArray arrayWithObjects:@"identifier", @"name", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:^(id targetObject) {
                // Cache the result for later use
                [unitTypeCache setObject:targetObject forKey:[targetObject identifier]];
            }] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            
            // Presentation and Medicament - This step will require some additional processing
            NSString* const medicamentId = @"medicamentId";
            NSString* const medicamentName = @"medicamentName";
            NSString* const unitTypeId = @"unitTypeId";
            NSString* const productTypeId = @"productTypeId";
            
            const NSInteger kLoopCount = 5000;
            __block NSInteger loopCount = 0;
            
            // Cache all ProductTypes and UnitTypes
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_products_gr" ofType:@"csv"];
            entityName = @"Presentation";
            fieldNames = [NSArray arrayWithObjects:@"identifier", medicamentId, medicamentName, unitTypeId, @"name", @"productTypeId", nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], kCSVComplexField, kCSVComplexField, kCSVComplexField, [NSString class], kCSVComplexField, nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:^id(id targetObject, NSString *currentField, NSString *rawValue) {
                
                if (rawValue.length > 0) {
                    Presentation* presentation = targetObject;
                    // Assign medicament
                    if (currentField == medicamentId) {
                        // Load from memory cache if possible
                        Medicament* medicament = [medicamentCache objectForKey:rawValue];
                        if (medicament == nil) {
                            // Create it if it doesn't exist
                            medicament = [NSEntityDescription insertNewObjectForEntityForName:@"Medicament" inManagedObjectContext:moc];
                            medicament.identifier = rawValue;
                            [medicamentCache setObject:medicament forKey:rawValue];
                        }
                        presentation.medicament = medicament;
                    }
                    else if (currentField == medicamentName) {
                        if (presentation.medicament.name == nil) {
                            presentation.medicament.name = rawValue;
                        }
                    }
                    else if (currentField == unitTypeId) {
                        // Load UnitType from memory cache
                        UnitType* unitType = [unitTypeCache objectForKey:rawValue];
                        presentation.unitType = unitType;
                    }
                    else if (currentField == productTypeId) {
                        // Load ProductType from memory cache
                        ProductType* productType = [productTypeCache objectForKey:rawValue];
                        if (productType != nil && presentation.medicament.productType == nil) {
                            presentation.medicament.productType = productType;
                        }
                    }
                }
                
                // Tell the parser to ignore the field, we already processed it
                return nil;
            } completedBlock:^(id targetObject) {
                loopCount = (loopCount + 1) % kLoopCount;
                if (loopCount == 0) {
                    NSError* error = nil;
                    [moc save:&error];
                    if (error != nil) {
                        DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
                    }
                }
            }] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
            [moc save:&error];
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            [moc reset];
            [pool drain];
            
            pool = [[NSAutoreleasePool alloc] init];
            
            // Finally load MetaInfo
            filePath = [[NSBundle mainBundle] pathForResource:@"ims_metainfo_gr" ofType:@"csv"];
            entityName = @"MetaInfo";
            fieldNames = [NSArray arrayWithObjects:@"initialLoadVersion", @"databaseVersion", @"lastUpdate",nil];
            fieldTypes = [NSArray arrayWithObjects:[NSString class], [NSString class], [NSDate class], nil];
            csvImporter = [[[CSVToCoreDataImporter alloc] initWithCurrentEntityName:entityName fieldNames:fieldNames fieldTypes:fieldTypes overwriteIfExists:NO context:moc complexTypeBlock:nil completedBlock:nil] autorelease];
            
            error = nil;
            [delegate initialLoadProcessorDidBeginProcessing:entityName];
            [csvImporter parseFile:filePath error:&error];
            [delegate initialLoadProcessorDidEndProcessing:entityName];
            
            if (error != nil) {
                DLog(@"Error loading %@: %@, %@", entityName, error, [error userInfo]);
            }
            
        }
        
        // Save changes
        error = nil;
        [moc processPendingChanges];
        [moc save:&error];
        
        if (error != nil) {
            DLog(@"Error saving to persistent context %@, %@", error, [error userInfo]);
        }
        
        [pool drain];
        
        [delegate initialLoadProcessorDidFinishLoading];
    }
}

@end
