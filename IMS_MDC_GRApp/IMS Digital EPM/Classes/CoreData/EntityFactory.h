//
//  EntityFactory.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DoctorInfo.h"
#import "Doctor.h"
#import "ProductType.h"
#import "Province.h"
#import "MetaInfo.h"
#import "ModalControllerProtocol.h"
#import "University.h"
#import "PlaceOfVisit.h"

@protocol CancelableOperation <NSObject>

-(void)cancel;

@end

@interface EntityFactory : NSObject

typedef void (^EntityFetchCompleteBlock)(NSArray* results);

/** @brief Access to the singleton instance */
+ (EntityFactory*) sharedEntityFactory;

@property (nonatomic, retain) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, readonly) NSString *applicationDocumentsDirectory;

- (NSManagedObjectContext*)createContext;

#pragma mark - Object creation
- (id)insertNewWithEntityName:(NSString*)entityName;

#pragma mark - Object deletion
- (void)deleteObject:(NSManagedObject*)object;

#pragma mark - Change confirmation and rollback
- (void)saveChanges;
- (void)discardChanges;

#pragma mark - Saved searchs
- (Doctor*)fetchDoctorForUserID:(NSString*)userID;
- (id<CancelableOperation>) fecthMedicamentsInBackgroundWithFilter:(NSString*)filter ofType:(ProductType*)productType onComplete:(EntityFetchCompleteBlock)block;
- (NSArray*)fetchProvinces;
- (NSArray*)fetchUniversities:(NSString*)nameFilter;
- (NSArray*)fetchRecentMedicamentsforDoctorInfo:(DoctorInfo*)doctorInfo andProductType:(ProductType*)productType;
- (NSArray*)fetchMedicalCenters;

#pragma mark - Generic searches
/** 
 * @brief Fetches the first result (if more than one) of the entity with name 'entityName'
 *          by the specified identifier 
 */
- (id)fetchEntity:(NSString*)entityName usingIdentifier:(NSString*)identifier;
- (id)fetchEntity:(NSString*)entityName usingIdentifier:(NSString*)identifier withManagedObjectContext:(NSManagedObjectContext*)managedObjectContext;

/** 
 * @brief Fetches all the entities (limited number of results) sorted by name filtered by
 *          name containing the specified filter. The filter and the sort is
 *          case and diacritic insensitive
 */
- (NSArray*)fetchEntities:(NSString*)entityName filteringByName:(NSString*)nameFilter;
- (NSArray*)fetchEntities:(NSString*)entityName filteringByName:(NSString*)nameFilter sortResults:(BOOL)sortResults;
- (NSArray*)fetchEntities:(NSString*)entityName predicate:(NSPredicate*)predicate sortResults:(BOOL)sortResults;
- (NSArray*)fetchEntities:(NSString*)entityName sortByIdentifier:(BOOL)sortByIdentifier;
- (NSArray*)fetchEntities:(NSString*)entityName predicate:(NSPredicate*)predicate sortDescriptors:(NSArray*)sortDescriptors;
- (id<CancelableOperation>)fetchEntitiesInBackground:(NSString*)entityName filteringByName:(NSString*)nameFilter onComplete:(EntityFetchCompleteBlock)block;
- (id<CancelableOperation>)fetchEntitiesInBackground:(NSString*)entityName filteringByName:(NSString*)nameFilter sortResults:(BOOL)sortResults onComplete:(EntityFetchCompleteBlock)block;
- (id<CancelableOperation>)fetchRemovableEntitiesInBackground:(NSString*)entityName filteringByName:(NSString*)nameFilter sortResults:(BOOL)sortResults onComplete:(EntityFetchCompleteBlock)block;
- (id<CancelableOperation>)fetchRemovableEntitiesInBackground:(NSString*)entityName filteringByName:(NSString*)nameFilter additionalPredicate:(NSPredicate*)additionalPredicate sortResults:(BOOL)sortResults onComplete:(EntityFetchCompleteBlock)block;
- (id<CancelableOperation>)fetchEntitiesInBackground:(NSString*)entityName predicates:(NSArray*)predicates sortResults:(BOOL)sortResults onComplete:(EntityFetchCompleteBlock)block;
- (id<CancelableOperation>)fetchEntitiesInBackground:(NSString*)entityName sortByIdentifier:(BOOL)sortByIdentifier onComplete:(EntityFetchCompleteBlock)block;
- (id<CancelableOperation>)fetchEntitiesInBackground:(NSString*)entityName predicates:(NSArray*)predicates sortDescriptors:(NSArray*)sortDescriptors onComplete:(EntityFetchCompleteBlock)block;
- (void)deleteDatabase;
#pragma mark - Meta info
/** @brief Obtains the database meta info */
- (MetaInfo*)getMetaInfoWithContext:(NSManagedObjectContext*)moc;

@end
