//
//  CSVToCoreDataImporter.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "CSVToCoreDataImporter.h"

NSString const* kCSVIgnoreField = @"___ignore___";
NSString const* kCSVComplexField = @"___complex___";

#pragma mark Private extension
@interface CSVToCoreDataImporter()
@property (nonatomic, retain) NSString* currentEntityName;
@property (nonatomic, retain) NSArray* fieldNames;
@property (nonatomic, retain) NSArray* fieldTypes;
@property (nonatomic, retain) NSMutableArray* fieldValues;
@property (nonatomic, retain) NSManagedObjectContext* context;
@property (nonatomic, copy) CSVComplexTypeBlock complexTypeBlock;
@property (nonatomic, copy) CSVObjectProcessed objectProcessedBlock;
@property (nonatomic) BOOL loading;
@property (nonatomic) NSInteger identifierPosition;
@property (nonatomic) BOOL overwriteIfExists;
@property (nonatomic, retain) NSMutableArray* csvIdentifiers;
@end

@implementation CSVToCoreDataImporter
@synthesize currentEntityName = _currentEntityName;
@synthesize fieldNames = _fieldNames;
@synthesize fieldTypes = _fieldTypes;
@synthesize fieldValues = _fieldValues;
@synthesize context = _context;
@synthesize loading = _loading;
@synthesize complexTypeBlock = _complexTypeBlock;
@synthesize objectProcessedBlock = _objectProcessedBlock;


#pragma mark - Init and dealloc
- (void)dealloc {
    [_currentEntityName release];
    [_fieldNames release];
    [_fieldTypes release];
    [_fieldValues release];
    [_context release];
    Block_release(_complexTypeBlock);
    Block_release(_objectProcessedBlock);
    [super dealloc];
}

- (id)initWithCurrentEntityName:(NSString*)currentEntityName fieldNames:(NSArray*)fieldNames fieldTypes:(NSArray*)fieldTypes overwriteIfExists:(BOOL)overwriteIfExists context:(NSManagedObjectContext*)context complexTypeBlock:(CSVComplexTypeBlock)complexTypeBlock completedBlock:(CSVObjectProcessed)completedBlock {
    self = [super init];
    if (self) {
        _currentEntityName = [currentEntityName retain];
        _fieldNames = [fieldNames retain];
        _fieldTypes = [fieldTypes retain];
        _overwriteIfExists = overwriteIfExists;
        _identifierPosition = [_fieldNames indexOfObject:@"identifier"];
        _context = [context retain];
        _loading = NO;
        _complexTypeBlock = Block_copy(complexTypeBlock);
        _objectProcessedBlock = Block_copy(completedBlock);
        _csvIdentifiers = [NSMutableArray array];
    }
    return self;
}

#pragma mark - Public methods
- (BOOL)parseFile:(NSString*)filePath error:(NSError**)error {
    BOOL resultOK = YES;
    if (!_loading) {
        CHCSVParser* parser = [[CHCSVParser alloc] initWithContentsOfCSVFile:filePath encoding:NSUTF8StringEncoding error:error];
        [parser setDelimiter:@"|"];
        parser.parserDelegate = self;
        [parser parse];
    }
    else {
        DLog(@"Already processing another file");
        if (error != NULL) {
            *error = [[NSError alloc] initWithDomain:@"es.ims" code:1 userInfo:[NSDictionary dictionaryWithObject:@"Already processing another file" forKey:NSLocalizedDescriptionKey]];
        }
        resultOK = NO;
    }
    return resultOK;
}

#pragma mark - Private methods
- (id)fetchEntityWithIdentifier:(NSString*)identifier {
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"identifier == %@", identifier];
    NSFetchRequest* fetchRequest = [[[NSFetchRequest alloc] initWithEntityName:self.currentEntityName] autorelease];
    fetchRequest.predicate = predicate;
    fetchRequest.fetchLimit = 1;
    
    NSError* error = nil;
    NSArray* results = [self.context executeFetchRequest:fetchRequest error:&error];
    if (error != nil) {
        DLog(@"Error fetching entity %@: %@, %@", self.currentEntityName, error, [error userInfo]);
    }
    
    id result = nil;
    if (results.count > 0) {
        result = [results objectAtIndex:0];
    }
    
    return result;
}

- (void)deleteIdentifiersNotContainedInCSV {
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"NOT (identifier in %@)", _csvIdentifiers];
    NSFetchRequest* fetchRequest = [[[NSFetchRequest alloc] initWithEntityName:self.currentEntityName] autorelease];
    fetchRequest.predicate = predicate;
    
    NSError* error = nil;
    NSArray* results = [self.context executeFetchRequest:fetchRequest error:&error];
    if (error == nil) {
        for (NSManagedObject* managedObject in results) {
            [self.context deleteObject:managedObject];
        }
    } else {
        DLog(@"Error fetching entity %@: %@, %@", self.currentEntityName, error, [error userInfo]);
    }
}

- (id)obtainOrCreateEntityWithIdentifier:(NSString*)identifier {
    id object = nil;
    if (_overwriteIfExists) {
        object = [self fetchEntityWithIdentifier:identifier];
    }
    
    if (object == nil) {
        object = [NSEntityDescription insertNewObjectForEntityForName:self.currentEntityName inManagedObjectContext:self.context];
    }
    return object;
}

#pragma mark - CHCSVParserDelegate
- (void) parser:(CHCSVParser *)parser didStartDocument:(NSString *)csvFile {
    // Loading flag to avoid race conditions
    _loading = YES;
}

- (void) parser:(CHCSVParser *)parser didStartLine:(NSUInteger)lineNumber {
    // Reset field values array
    self.fieldValues = [NSMutableArray arrayWithCapacity:self.fieldNames.count];
}

- (void) parser:(CHCSVParser *)parser didEndLine:(NSUInteger)lineNumber {
    
    // Ignore empty lines
    if (self.fieldValues.count > 0) {
        NSString* identifier = nil;
        if (_identifierPosition != NSNotFound && self.fieldNames.count > _identifierPosition) {
            identifier = [self.fieldValues objectAtIndex:_identifierPosition];
            [self.csvIdentifiers addObject:identifier];
        }
        NSManagedObject* object = [self obtainOrCreateEntityWithIdentifier:identifier];
        
        for (NSInteger index = 0; index < self.fieldNames.count; index++) {
            NSString* field = [self.fieldNames objectAtIndex:index];
            if (field != kCSVIgnoreField) {
                if (index < self.fieldTypes.count && index < self.fieldValues.count) {
                    Class type = [self.fieldTypes objectAtIndex:index];
                    id value = [self.fieldValues objectAtIndex:index];
                    
                    if (type == [NSString class]) {
                        value = [value description];
                    }
                    else if (type == [NSNumber class]) {
                        value = [NSNumber numberWithDouble:[value doubleValue]];
                    }
                    else if (type == [NSDate class]) {
                        value = [NSDate dateWithTimeIntervalSince1970:[value doubleValue]];
                    }
                    else if (type == kCSVComplexField) {
                        if (self.complexTypeBlock != nil) {
                            value = _complexTypeBlock(object, field, value);
                        }
                        else {
                            DLog(@"Complex type for field %@ but no block provided", field);
                        }
                    }
                    else {
                        DLog(@"Unrecognized type %@ for field %@", type, field);
                    }
                    
                    // Ignore nil values
                    if (value != nil) {
                        [object setValue:value forKey:field];
                    }
                }
                else {
                    DLog(@"Warning processing %@ in line %d: line incomplete", self.currentEntityName, lineNumber);
                }
            }
        }
        
        if (self.objectProcessedBlock != nil) {
            _objectProcessedBlock(object);
        }
    }
}

- (void) parser:(CHCSVParser *)parser didReadField:(NSString *)field {
    [self.fieldValues addObject:field];
}

- (void) parser:(CHCSVParser *)parser didEndDocument:(NSString *)csvFile {
    if (_overwriteIfExists == YES) {
        [self deleteIdentifiersNotContainedInCSV];
    }
    _loading = NO;
}

- (void) parser:(CHCSVParser *)parser didFailWithError:(NSError *)error {
    DLog(@"Parser failed with error: %@, %@", error, [error userInfo]);
    _loading = NO;
}

@end
