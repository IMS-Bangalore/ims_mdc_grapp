//
//  CSVToCoreDataImporter.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CHCSV.h"

/** @brief Used as field name to indicate that a field should be ignored */
extern NSString const* kCSVIgnoreField;

/** @brief Used as field type to indicate that the ComplexTypeBlock will be called to transform the value */
extern NSString const* kCSVComplexField;

/** @brief Block used to solve complex fields (usually relationships between objects) */
typedef id (^CSVComplexTypeBlock)(id targetObject, NSString* currentField, NSString* rawValue);

/** @brief Block used to notify that an object has been processed, so the caller can add finishing touches */
typedef void (^CSVObjectProcessed)(id targetObject);


@interface CSVToCoreDataImporter : NSObject<CHCSVParserDelegate>

- (id)initWithCurrentEntityName:(NSString*)currentEntityName fieldNames:(NSArray*)fieldNames fieldTypes:(NSArray*)fieldTypes overwriteIfExists:(BOOL)overwriteIfExists context:(NSManagedObjectContext*)context complexTypeBlock:(CSVComplexTypeBlock)complexTypeBlock completedBlock:(CSVObjectProcessed)completedBlock;

- (BOOL)parseFile:(NSString*)filePath error:(NSError**)error;

@end
