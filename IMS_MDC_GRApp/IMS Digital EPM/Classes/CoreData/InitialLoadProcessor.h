//
//  InitialLoadProcessor.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol InitialLoadProcessorDelegate <NSObject>
- (void)initialLoadProcessorDidBeginProcessing:(NSString*)entityName;
- (void)initialLoadProcessorDidEndProcessing:(NSString *)entityName;
- (void)initialLoadProcessorDidBeginLoading;
- (void)initialLoadProcessorDidFinishLoading;
- (void)initialLoadProcessorDidFailWithError:(NSError*)error;

@end

@interface InitialLoadProcessor : NSObject

- (BOOL)needsInitialLoad:(NSManagedObjectContext*)moc;
- (void)preLoadData:(id<InitialLoadProcessorDelegate>)delegate;

@end
