//
//  OtherSpecialitiesSync.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 05/02/15.
//
//

#import "OtherSpecialitiesSync.h"

@implementation OtherSpecialitiesSync
@synthesize identifier = _identifier;
@synthesize name = _name;

- (void)dealloc {
    [_identifier release];
    [_name release];
    [super dealloc];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"%@(%@, %@)", self.class, _identifier, _name];
}
@end
