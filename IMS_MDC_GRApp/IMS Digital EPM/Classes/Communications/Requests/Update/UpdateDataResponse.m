//
//  UpdateDataResponse.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 30/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "UpdateDataResponse.h"

@implementation UpdateDataResponse

@synthesize diagnosisArray = _diagnosisArray;
@synthesize diagnosisDeleteArray = _diagnosisDeleteArray;
@synthesize medicamentArray = _medicamentArray;
@synthesize medicamentDeleteArray = _medicamentDeleteArray;
@synthesize effectsArray = _effectsArray;
@synthesize effectsDeleteArray = _effectsDeleteArray;

//Ravi_Bukka: Added for checkdataversion implementation
@synthesize unitTypeArray=_unitTypeArray;
@synthesize unitTypeDeleteArray=_unitTypeDeleteArray;
//Deepak_Carpenter : Added for checkDataVersion
@synthesize ageTypeArray=_ageTypeArray;
@synthesize ageTypeDeleteArray=_ageTypeDeleteArray;
@synthesize centreArray=_centreArray;
@synthesize centreDeleteArray=_centreDeleteArray;
@synthesize consultTypeArray=_consultTypeArray;
@synthesize consultTypeDeleteArray=_consultTypeDeleteArray;
@synthesize frequencyArray=_frequencyArray;
@synthesize frequencyDeleteArray=_frequencyDeleteArray;
@synthesize durationTypeArray=_durationTypeArray;
@synthesize durationTypeDeleteArray=_durationTypeDeleteArray;
@synthesize genderArray=_genderArray;
@synthesize genderDeleteArray=_genderDeleteArray;
@synthesize insuranceArray=_insuranceArray;
@synthesize insuranceDeleteArray=_insuranceDeleteArray;
@synthesize pathologyArray=_pathologyArray;
@synthesize pathologyDeleteArray=_pathologyDeleteArray;
@synthesize placeOfVisitArray=_placeOfVisitArray;
@synthesize placeOfVisitDeleteArray=_placeOfVisitDeleteArray;
@synthesize provinceArray=_provinceArray;
@synthesize provinceDeleteArray=_provinceDeleteArray;
@synthesize recurrenceArray=_recurrenceArray;
@synthesize recurrenceDeleteArray=_recurrenceDeleteArray;
@synthesize smokerArray=_smokerArray;
@synthesize smokerDeleteArray=_smokerDeleteArray;
@synthesize specialitiesArray=_specialitiesArray;
@synthesize specialitiesDeleteArray=_specialitiesDeleteArray;
@synthesize otherSpecialitiesArray=_otherSpecialitiesArray;
@synthesize otherSpecialitiesDeleteArray=_otherSpecialitiesDeleteArray;
@synthesize therapyElectionArray=_therapyElectionArray;
@synthesize therapyElectionDeleteArray=_therapyElectionDeleteArray;
@synthesize therapyReasonsArray=_therapyReasonsArray;
@synthesize therapyReasonsDeleteArray=_therapyReasonsDeleteArray;
@synthesize therapyTypeArray=_therapyTypeArray;
@synthesize therapyTypeDeleteArray=_therapyTypeDeleteArray;
@synthesize universitiesArray=_universitiesArray;
@synthesize universitiesDeleteArray=_universitiesDeleteArray;
- (void)dealloc {
//    [_diagnosisArray release];
//    [_diagnosisDeleteArray release];
//    [_medicamentArray release];
//    [_medicamentDeleteArray release];
//    [_effectsArray release];
//    [_effectsDeleteArray release];
//    
//    [_unitTypeArray release];
//    [_unitTypeDeleteArray release];
//    [_ageTypeArray release];
//    [_ageTypeDeleteArray release];
//    [_centreArray release];
//    [_centreDeleteArray release];
//    [_consultTypeArray release];
//    [_consultTypeDeleteArray release];
//    
//    [_frequencyArray release];
//    [_frequencyDeleteArray release];
//    [_durationTypeArray release];
//    [_durationTypeDeleteArray release];
//    [_genderArray release];
//    [_genderDeleteArray release];
//    [_insuranceArray release];
//    [_insuranceDeleteArray release];
//    [_pathologyArray release];
//    [_pathologyDeleteArray release];
//    [_placeOfVisitArray release];
//    [_placeOfVisitDeleteArray release];
//    [_provinceArray release];
//    [_provinceDeleteArray release];
//    [_recurrenceArray release];
//    [_recurrenceDeleteArray release];
//    [_smokerArray release];
//    [_smokerDeleteArray release];
//    [_specialitiesArray release];
//    [_specialitiesDeleteArray release];
//    [_therapyElectionArray release];
//    [_therapyElectionDeleteArray release];
//    [_therapyReasonsArray release];
//    [_therapyReasonsDeleteArray release];
//    [_therapyTypeArray release];
//    [_therapyTypeDeleteArray release];
//    [_universitiesArray release];
//    [_universitiesDeleteArray release];
//    [super dealloc];
}

//-(NSString *)description {
//    return [NSString stringWithFormat:
//            @"diagnisisArray: %@;\n"
//            "diagnosisDeleteArray: %@;\n"
//            "medicamentArray: %@;\n"
//            "medicamentDeleteArray: %@;\n"
//            "effectsArray: %@;\n"
//            "centreArray: %@;\n"
//            "ageArray: %@;\n"
//            "consultArray: %@;\n"
//            "effectsDeleteArray: %@",
//            _diagnosisArray, _diagnosisDeleteArray, _medicamentArray, _medicamentDeleteArray, _effectsArray, _centreArray, _ageArray, _consultArray, _effectsDeleteArray];
//}

-(NSString *)description {
    return [NSString stringWithFormat:
            @"diagnisisArray: %@;\n"
            "diagnosisDeleteArray: %@;\n"
            "medicamentArray: %@;\n"
            "medicamentDeleteArray: %@;\n"
            "effectsArray: %@;\n"
            "effectsDeleteArray: %@\n"
            "unitTypeArray: %@;\n"
            "unitTypeDeleteArray: %@\n"
            
            "AgetypeArray: %@;\n"
            "AgeTypeDeleteArray: %@;\n"
            "CenterArray: %@;\n"
            "CenterDeleteArray: %@;\n"
            "ConsultTypeArray: %@;\n"
            "ConsultTypeDeleteArray: %@\n"
            "FrequencyArray: %@;\n"
            "FreauencyDeleteArray: %@\n"
            
            "DurationArray: %@;\n"
            "DurationDeleteArray: %@;\n"
            "GenderArray: %@;\n"
            "GenderDeleteArray: %@;\n"
            "InsuranceTypeArray: %@;\n"
            "InsuranceTypeDeleteArray: %@\n"
            "pathologyTypeArray: %@;\n"
            "pathologyTypeDeleteArray: %@\n"
            
            "placeOfVisitArray: %@;\n"
            "placeOfVisitDeleteArray: %@;\n"
            "ProvinceArray: %@;\n"
            "ProvinceDeleteArray: %@;\n"
            "recurrenceArray: %@;\n"
            "recurrenceDeleteArray: %@\n"
            "smokerArray: %@;\n"
            "smokerDeleteArray: %@\n"
            
            "SpecialitiesArray: %@;\n"
            "SpecialitiesDeleteArray: %@;\n"
            "TherapyElectionArray: %@;\n"
            "TherapyElectionDeleteArray: %@;\n"
            "TherapyReasonsArray: %@;\n"
            "TherapyreasonsDeleteArray: %@\n"
            "TherapyTypeArray: %@;\n"
            "TherapyTypeDeleteArray: %@\n"
            "UniversityArray: %@;\n"
            "UniversityDeleteArray: %@\n"
            ,
            _diagnosisArray, _diagnosisDeleteArray,
            _medicamentArray, _medicamentDeleteArray,
            _effectsArray, _effectsDeleteArray,
            _unitTypeArray,_unitTypeDeleteArray,
            _ageTypeArray,_ageTypeDeleteArray,
            _centreArray,_centreDeleteArray,
            _consultTypeArray,_consultTypeDeleteArray,
            _frequencyArray,_frequencyDeleteArray,
            _durationTypeArray,_durationTypeDeleteArray,
            _genderArray,_genderDeleteArray,
            _insuranceArray,_insuranceDeleteArray,
            _pathologyArray,_pathologyDeleteArray,
            _placeOfVisitArray,_placeOfVisitDeleteArray,
            _provinceArray,_provinceDeleteArray,
            _recurrenceArray,_recurrenceDeleteArray,
            _smokerArray,_smokerDeleteArray,
            _specialitiesArray,_specialitiesDeleteArray,
            _therapyElectionArray,_therapyElectionDeleteArray,
            _therapyReasonsArray,_therapyReasonsDeleteArray,
            _therapyTypeArray,_therapyTypeDeleteArray,
            _universitiesArray,_universitiesDeleteArray];
}


@end
