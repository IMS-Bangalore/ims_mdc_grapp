//
//  AgeSync.m
//  IMS Digital EPM
//
//  Created by Bukka, Ravi (Bangalore) on 04/08/14.
//
//

#import "AgeSync.h"

@implementation AgeSync

@synthesize identifier = _identifier;
@synthesize name = _name;

- (void)dealloc {
    [_identifier release];
    [_name release];
    [super dealloc];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"%@(%@, %@)", self.class, _identifier, _name];
}


@end
