//
//  EffectsSync.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 22/11/12.
//
//

#import <Foundation/Foundation.h>

/** @brief Transport object to insert or update desired effects of a medicament */
@interface EffectsSync : NSObject

/** @brief Identifier provided by remote server */
@property (nonatomic, retain) NSString *identifier;
/** @brief Effect description */
@property (nonatomic, retain) NSString *name;

@end
