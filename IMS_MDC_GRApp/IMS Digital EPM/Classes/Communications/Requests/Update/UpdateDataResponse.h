//
//  UpdateDataResponse.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 30/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpdateDataResponse : NSObject

/** @brief Array of diagnosis objects to be insert or updated */
@property (nonatomic, retain) NSArray *diagnosisArray;
/** @brief Array of diagnosis identifiers to be removed */
@property (nonatomic, retain) NSArray *diagnosisDeleteArray;
/** @brief Array of medicament objects to be insert or updated */
@property (nonatomic, retain) NSArray *medicamentArray;
/** @brief Array of diagnosis identifiers to be removed */
@property (nonatomic, retain) NSArray *medicamentDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *effectsArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *effectsDeleteArray;

//Ravi_Bukka: Added for Checkdataversion Implementation
//Deepak_Carpenter : Added for UnitType in checkDataVersion
@property (nonatomic, retain) NSArray *unitTypeArray;
@property (nonatomic, retain) NSArray *unitTypeDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *ageTypeArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *ageTypeDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *centreArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *centreDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *consultTypeArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *consultTypeDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *frequencyArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *frequencyDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *durationTypeArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *durationTypeDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *genderArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *genderDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *insuranceArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *insuranceDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *pathologyArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *pathologyDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *placeOfVisitArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *placeOfVisitDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *provinceArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *provinceDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *recurrenceArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *recurrenceDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *smokerArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *smokerDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *specialitiesArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *specialitiesDeleteArray;

/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *otherSpecialitiesArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *otherSpecialitiesDeleteArray;


/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *therapyElectionArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *therapyElectionDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *therapyReasonsArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *therapyReasonsDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *therapyTypeArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *therapyTypeDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *universitiesArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *universitiesDeleteArray;

//Deepak_Carpenter : Added for UnitType in checkDataVersion


@end
