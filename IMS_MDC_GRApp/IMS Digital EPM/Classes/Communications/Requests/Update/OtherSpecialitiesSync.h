//
//  OtherSpecialitiesSync.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 05/02/15.
//
//

#import <Foundation/Foundation.h>

@interface OtherSpecialitiesSync : NSObject
/** @brief Identifier provided by remote server */
@property (nonatomic, retain) NSString *identifier;
/** @brief Consult type description */
@property (nonatomic, retain) NSString *name;
@end
