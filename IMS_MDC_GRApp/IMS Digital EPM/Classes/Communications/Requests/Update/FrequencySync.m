//
//  FrequencySync.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 11/09/14.
//
//

#import "FrequencySync.h"

@implementation FrequencySync
@synthesize identifier = _identifier;
@synthesize name = _name;

- (void)dealloc {
    [_identifier release];
    [_name release];
    [super dealloc];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"%@(%@, %@)", self.class, _identifier, _name];
}

@end
