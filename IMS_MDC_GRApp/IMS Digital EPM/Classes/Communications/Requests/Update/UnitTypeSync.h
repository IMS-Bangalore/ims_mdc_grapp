//
//  UnitTypeSync.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 28/08/14.
//
//

#import <Foundation/Foundation.h>

@interface UnitTypeSync : NSObject

/** @brief Identifier provided by remote server */
@property (nonatomic, retain) NSString *identifier;
/** @brief Consult type description */
@property (nonatomic, retain) NSString *name;

@end
