//
//  ChatGetAnswersRequest.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 05/12/12.
//
//

#import "ChatGetAnswersRequest.h"
#import "ChatMessage.h"

@implementation ChatGetAnswersRequest

static NSString* const kDateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
//2014-02-25T08:30:03.853
static NSString* const kIsQuestion = @"0";

// Error domain
static NSString* const kErrorDomain  = @"es.lumata.LoginRequest";
// Request Name
static NSString* const kRequestName  = @"getAnswers";
// Content dictionary
static NSString* const kUserIdKey = @"userId";

// Response
static NSString* const kAnswersListKey = @"Answers";
static NSString* const kAnswersItemKey = @"Answer";
static NSString* const kAnswersTextKey = @"AnswerText";
static NSString* const kAnswerDateKey = @"AnswerDate";
static NSString* const kIsQuestionKey = @"AnswerAdmin";

//NSArray* obtainedMedicamentInfo = [[responseDictionary objectForKey:kMedicineListKey] arrayForKey:kMedicineKey];

- (id)initGetAnswersRequestWithUserId:(NSString *)userId onComplete:(ChatGetAnswersResponseBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    if (self) {
        // Set request name
        self.name = kRequestName;
        // Add request content
       // [self addParam:userId forKey:kUserIdKey];
       
        
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            NSArray* answers = [NSArray arrayWithArray:[self answersFromDictionary:responseDictionary]];
            onComplete(answers);
        };
    }
    return self;
}

- (NSArray*)answersFromDictionary:(NSDictionary*)answersDictionary {
    //NSArray* answersArray = [[answersDictionary objectForKey:kAnswersListKey] arrayForKey:kAnswersItemKey];
    NSArray* answersArray = [answersDictionary objectForKey:kAnswersItemKey];
    
    //Ravi_Bukka: Added this method to resolve chat issue during single message
    if ([answersArray isKindOfClass:[NSDictionary class]]) {
        NSDictionary *oneDic = [[NSDictionary alloc]init];
        oneDic = [answersDictionary objectForKey:kAnswersItemKey];
        return [self answersFromDic:oneDic];
        
    }
    else {
        
        return [self answersFromArray:answersArray];
    }
    
}

//Ravi_Bukka: Added this method to resolve chat issue during single message
- (NSMutableArray*)answersFromDic:(NSDictionary*)answersDictionary {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:kDateFormat];
    NSMutableArray* answers = [NSMutableArray array];
    ChatMessage* answer = [[ChatMessage alloc] init];
    answer.date = [df dateFromString:[answersDictionary objectForKey:kAnswerDateKey]];
    NSLog(@"Date Value %@",[df dateFromString:[answersDictionary objectForKey:kAnswerDateKey]]);
    answer.message = [answersDictionary objectForKey:kAnswersTextKey];
    

    //[[answerDict objectForKey:kIsQuestionKey] isEqualToString:kIsQuestion];
    answer.question =[[answersDictionary objectForKey:kIsQuestionKey] isEqualToString:kIsQuestion];
    
    
    [answers addObject:answer];
    [answer release];
    
    [df release];
    return answers;
    
}



- (NSMutableArray*)answersFromArray:(NSArray*)answersArray {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:kDateFormat];
    
    NSMutableArray* answers = [NSMutableArray array];
    //Deepak Carpenter:
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    for(int i=0; i<[answersArray count]; i++){
        
        [dic setObject:[answersArray objectAtIndex:i] forKey:[NSString stringWithFormat:@"%d",i]];
    }
    
    NSLog(@"Dic Contents- %@",dic);
    for (id type in dic) {
        
        if ([type isKindOfClass:[NSDictionary class]]) {
            NSLog(@"yes it is dict");
        }
    }
   
    for (NSDictionary* answerDict in answersArray) {
        
        NSLog(@"All Keys -->%d",[[answerDict allKeys]count]);
        
        ChatMessage* answer = [[ChatMessage alloc] init];
        NSLog(@"answerDictionary Data is --> %@",answerDict);
        answer.date = [df dateFromString:[answerDict objectForKey:kAnswerDateKey]];
        NSLog(@"Date Value %@",[df dateFromString:[answerDict objectForKey:kAnswerDateKey]]);
        answer.message = [answerDict objectForKey:kAnswersTextKey];
                
        //[[answerDict objectForKey:kIsQuestionKey] isEqualToString:kIsQuestion];
        answer.question =[[answerDict objectForKey:kIsQuestionKey] isEqualToString:kIsQuestion];
        //
        NSLog(@"kISQuestionValue %@",[answerDict objectForKey:kIsQuestionKey]);
        NSLog(@"anser.question %c",[[answerDict objectForKey:@"AnswerText"] isEqualToString:kIsQuestion]);
        NSLog(@"kISQUESTION value %d KISQUESTIONKEY value %d",[kIsQuestion length],[[answerDict objectForKey:kIsQuestionKey]length]);
        
        [answers addObject:answer];
        [answer release];
    }
    [df release];
    return answers;
}

@end
