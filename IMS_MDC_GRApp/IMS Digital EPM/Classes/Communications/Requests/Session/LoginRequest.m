//
//  LoginRequest.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 26/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "LoginRequest.h"

// Error domain
static NSString* const kErrorDomain  = @"es.lumata.LoginRequest";
// Request Name
static NSString* const kRequestName  = @"login";
// Content dictionary
static NSString* const kUserNameKey = @"username";
static NSString* const kPasswordKey = @"password";
static NSString* const kDeviceIdKey = @"idIpad";
static NSString* const kRemoveDataKey = @"removeData";
static NSString* const kVersionKey = @"version";

// Response
static NSString* const kFirstCollaborationDateKey = @"collaborationDate";
static NSString* const kFirstCollaborationDateEndKey = @"firstCollaborationDateEnd";
static NSString* const kSecondCollaborationDateKey = @"secondCollaborationDate";
static NSString* const kSecondCollaborationDateEndKey = @"secondCollaborationDateEnd";

//Ravi_Bukka: 
static NSString* const kMasterPasswordKey = @"masterkey";

@implementation LoginRequest

-(id) initLoginRequestWithUserName:(NSString*)userName password:(NSString*)password appVersion:appVersion removePreviousData:(BOOL)removePreviousData onComplete:(LoginResponseBlock)onComplete onError:(TCRequestErrorBlock)onError {
    self = [super init];
    if (self) {
        // Set request name
        self.name = kRequestName;
        // Add request content
        [self addParam:userName forKey:kUserNameKey];
        [self addParam:password forKey:kPasswordKey];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        NSString *udid = [[UIDevice currentDevice] uniqueIdentifier];
#pragma clang diagnostic pop
        [self addParam:udid forKey:kDeviceIdKey];
        NSString* removeDataString = removePreviousData ? @"1" : @"0";
        [self addParam:removeDataString forKey:kRemoveDataKey];
        [self addParam:appVersion forKey:kVersionKey];
        
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy/MM/dd"];
            NSDate* firstCollaborationDate = [dateFormatter dateFromString:[responseDictionary objectForKey:kFirstCollaborationDateKey]];
            NSDate* firstCollaborationDateEnd = [dateFormatter dateFromString:[responseDictionary objectForKey:kFirstCollaborationDateEndKey]];
            NSDate* secondCollaborationDate = [dateFormatter dateFromString:[responseDictionary objectForKey:kSecondCollaborationDateKey]];
            NSDate* secondCollaborationDateEnd = [dateFormatter dateFromString:[responseDictionary objectForKey:kSecondCollaborationDateEndKey]];
            NSString* masterPassword = [responseDictionary objectForKey:kMasterPasswordKey];
            [dateFormatter release];
            onComplete(firstCollaborationDate, firstCollaborationDateEnd, secondCollaborationDate, secondCollaborationDateEnd, masterPassword);
        };
    }
    return self;
}

@end
