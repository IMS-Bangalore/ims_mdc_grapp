//
//  ActivateUserRequest.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "ActivateUserRequest.h"

// Error domain
static NSString* const kErrorDomain  = @"es.lumata.ActivateUserRequest";
// Request Name
static NSString* const kRequestName  = @"activateUser";
// Content dictionary
static NSString* const kUserIdKey = @"userId";
static NSString* const kDeviceIdKey = @"idIpad";

@implementation ActivateUserRequest

-(id) initActivateUserRequestWithDoctorInfo:(DoctorInfo*)doctorInfo onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    if (self) {
        // Set request name
        self.name = kRequestName;
        // Add request content
        [self addParam:doctorInfo.identifier forKey:kUserIdKey];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        NSString *udid = [[UIDevice currentDevice] uniqueIdentifier];
#pragma clang diagnostic pop
        [self addParam:udid forKey:kDeviceIdKey];
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            onComplete();
        };
    }
    return self;
}

@end