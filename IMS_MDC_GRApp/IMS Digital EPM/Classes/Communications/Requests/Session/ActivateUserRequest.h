//
//  ActivateUserRequest.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "BaseRequest.h"
#import "DoctorInfo.h"

/** @brief Request to activate active user */
@interface ActivateUserRequest : BaseRequest

/**
 @brief Creates a request to activate active user
 @param doctorInfo User to be activated
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(id) initActivateUserRequestWithDoctorInfo:(DoctorInfo*)doctorInfo onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

@end
