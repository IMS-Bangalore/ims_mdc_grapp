//
//  CountdownRequest.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "CountdownRequest.h"

// Error domain
static NSString* const kErrorDomain  = @"es.lumata.CountdownRequest";
static const NSInteger kOperationErrorCode  = 91;

// Request Name
static NSString* const kRequestName  = @"countdown";
// Content dictionary
static NSString* const kUserIdKey = @"userId";
static NSString* const kCollaborationStartKey = @"collaborationStart";
static NSString* const kCollaborationStartDateFormat = @"YYYY-MM-dd";
// Response
static NSString* const kRemainingDaysKey = @"daysRemaining";

@implementation CountdownRequest

-(id) initCountdownRequestWithDoctorInfo:(DoctorInfo*)doctorInfo onComplete:(CountdownResponseBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    if (self) {
        // Set request name
        self.name = kRequestName;
        // Add request content
        [self addParam:doctorInfo.identifier forKey:kUserIdKey];
        // Format collaboration date
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:kCollaborationStartDateFormat];
        [self addParam:[dateFormatter stringFromDate:doctorInfo.firstCollaborationDate] forKey:kCollaborationStartKey];
        [dateFormatter release];
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            NSString* stringValue = [responseDictionary objectForKey:kRemainingDaysKey];
            // Check if response contains valid remaining days info
            if (stringValue != nil) {
                onComplete(stringValue.integerValue);
            }
            // If response value is nil, return error
            else {
                DLog(@"Response dictionary value for key '%@' is nil, return error", kRemainingDaysKey);
                NSError* error = [NSError errorWithDomain:kErrorDomain code:kOperationErrorCode userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"SERVICE_RESPONSE_ERROR", nil) forKey:NSLocalizedDescriptionKey]];
                onError(error);
            }
        };
    }
    return self;
}

@end