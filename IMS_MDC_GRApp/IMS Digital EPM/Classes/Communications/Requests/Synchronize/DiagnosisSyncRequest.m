//
//  DiagnosisSyncRequest.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "DiagnosisSyncRequest.h"
#import "Patient.h"
#import "DoctorInfo.h"
#import "Pathology.h"
#import "VisitType.h"
#import "DiagnosisType.h"
#import "DrugIndicator.h"


// Request Name
static NSString* const kInsertRequestName  = @"insertDiagnosis";
static NSString* const kUpdateRequestName  = @"updateDiagnosis";
static NSString* const kRemoveRequestName  = @"removeDiagnosis";
// Content dictionary
static NSString* const kUserIdKey =         @"userId";
static NSString* const kPatientIdKey =      @"PatientId";
static NSString* const kDiagnosisIdKey =    @"DiagnosisWindow";
static NSString* const kDiagnosisNameKey =  @"Diagnoses_Id";

//Deepak_Carpenter : Added for manually entered Diagnosis
static NSString* const kDiagnosisValueKey =  @"DiagnosesDescription";

static NSString* const kFirstVisitKey =     @"ConsultType_Id";
//
static NSString* const kPathologyTypeKey =  @"PathologyType_Id";

static NSString* const kNoTreatmentKey =    @"NoTreatment";

static NSString* const kEmptyID = @"";
static NSString* const kValueYes =     @"1";
static NSString* const kValueNo =      @"0";

//Kanchan Nair
//static NSString* const kDrugIndicatorKey =    @"KDrugIndicator";
//static NSString* const kuserDrugIndicatorKey =    @"kuserDrugIndicatorKey";

//Ravi_Bukka: Commented for integrating new PatientInfo service
// Error domain
/*static NSString* const kErrorDomain  = @"es.lumata.DiagnosisSyncRequest";
// Request Name
static NSString* const kInsertRequestName  = @"insertDiagnosis";
static NSString* const kUpdateRequestName  = @"updateDiagnosis";
static NSString* const kRemoveRequestName  = @"removeDiagnosis";
// Content dictionary
static NSString* const kUserIdKey =         @"userId";
static NSString* const kPatientIdKey =      @"patientWindowId";
static NSString* const kDiagnosisIdKey =    @"diagnosisWindowId";
static NSString* const kDiagnosisTypeKey =  @"diagnosisId";
static NSString* const kDiagnosisNameKey =  @"diagnosisName";
static NSString* const kFirstVisitKey =     @"firstVisit";
static NSString* const kPathologyTypeKey =  @"pathologyType";
static NSString* const kNoTreatmentKey =    @"noTreatment";

static NSString* const kEmptyID = @"1";
static NSString* const kValueYes =     @"1";
static NSString* const kValueNo =      @"0"; */




@implementation DiagnosisSyncRequest

-(id) initDiagnosisSyncRequestWithSyncType:(DiagnosisSyncType)syncType diagnosis:(Diagnosis*)diagnosis onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    if (self) {
        // Set request name
        switch (syncType) {
            case kDiagnosisSyncTypeInsert:
                self.name = kInsertRequestName;
                break;
            case kDiagnosisSyncTypeUpdate:
                self.name = kUpdateRequestName;
                break;
            case kDiagnosisSyncTypeDelete:
                self.name = kRemoveRequestName;
                break;    
        }
        Patient* patient = diagnosis.patient;
        if (patient == nil) {
            patient = (Patient*)diagnosis.parentEntity;
        }
        
        DoctorInfo* doctorInfo = patient.doctorInfo;
        if (doctorInfo == nil) {
            doctorInfo = (DoctorInfo*)patient.parentEntity;
        }
        
        // Add request content
        [self addParam:doctorInfo.identifier forKey:kUserIdKey];
        [self addParam:patient.identifier forKey:kPatientIdKey];
        [self addParam:diagnosis.identifier forKey:kDiagnosisIdKey];
        //diagnosis.diagnosisType.identifier
        //Kanchan
        //[self addParam:diagnosis.drugIndicator.identifier forKey:kDrugIndicatorKey];
        //[self addParam:diagnosis.userDrugIndicator forKey:kuserDrugIndicatorKey];

        // If action is insert or update, include additional info to request
        if (syncType != kDiagnosisSyncTypeDelete) {
          //  [self addParam:diagnosis.diagnosisType.identifier forKey:kDiagnosisTypeKey defaultValue:kEmptyID];
            if (diagnosis.userDiagnosis) {
            [self addParam:diagnosis.diagnosisType.identifier forKey:kDiagnosisNameKey];
                [self addParam:diagnosis.userDiagnosis forKey:kDiagnosisValueKey];
            }else{
            [self addParam:diagnosis.diagnosisType.identifier forKey:kDiagnosisNameKey];
                [self addParam:diagnosis.userDiagnosis forKey:kDiagnosisValueKey];
            }
            
            [self addParam:diagnosis.visitType.identifier forKey:kFirstVisitKey defaultValue:kEmptyID];
            [self addParam:diagnosis.pathology.identifier forKey:kPathologyTypeKey defaultValue:kEmptyID];
            //Added By Deepak to do changes on 8th may
            if (!diagnosis.needsTreatment.boolValue){
                [self addParam:@"1" forKey:kNoTreatmentKey ];
            }else [self addParam:@"2"forKey:kNoTreatmentKey];
           // [self addParam:diagnosis.pathology.identifier forKey:kPathologyTypeKey];
        }
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            onComplete();
        };
    }
    return self;
}

@end