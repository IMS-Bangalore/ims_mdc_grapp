//
//  GetDoctorInfoRequest.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 15/04/13.
//
//

#import "GetDoctorInfoRequest.h"
#import "Doctor.h"

// Error domain
static NSString* const kErrorDomain = @"es.lumata.GetDoctorInfoRequest";
// Request Name
static NSString* const kRequestName = @"getDoctorProfile";
// Request dictionary
static NSString* const kUserIdKey = @"userId";
// Response dictionary
static NSString* const kResultKey =                 @"result";
static NSString* const kAgeKey =                    @"age";
static NSString* const kGenderKey =                 @"sex";
static NSString* const kPlaceOfDegreeKey =          @"placeOfDegree";
static NSString* const kPlaceOfDegreeHandKey =      @"placeOfDegreeHand";
static NSString* const kYearOfDegreeKey =           @"yearOfDegree";
static NSString* const kCountyOfPracticeKey =       @"countyOfPractice";
static NSString* const kSpecializationKey =         @"specialization";
static NSString* const kSecondSpecializationKey =   @"secondSpecialization";
static NSString* const kHealthCenterTypeKey =       @"healthCenterType";
static NSString* const kPublicHospitalTypeKey =     @"publicHospitalType";
static NSString* const kPrivateHospitalTypeKey =    @"privateHospitalType";
static NSString* const kConsultationTypeKey =       @"consultationType";
static NSString* const kOtherCenterTypeKey =        @"otherCenterType";
static NSString* const kHealthCenterTypeOtheKey =   @"healthCenterTypeOther";
static NSString* const kWeeklyHoursKey =            @"weeklyHours";
static NSString* const kWeeklyPatientsKey =         @"weeklyPatients";
static NSString* const kContributionStartKey =      @"contributionStart"; // Format yyyy/MM/dd
static NSString* const kDoctorCommentsKey =         @"doctorComments";
static NSString* const kDoctorEmailKey =            @"email";

static NSString* const kValueYes =     @"1";
static NSString* const kValueNo =      @"0";

static const NSInteger kHealthCenterTypeValue =       1;
static const NSInteger kPublicHospitalTypeValue =     2;
static const NSInteger kPrivateHospitalTypeValue =    3;
static const NSInteger kConsultationTypeValue =       4;
static const NSInteger kOtherCenterTypeValue =        5;

static const NSInteger kDoctorInfoCode = 100;
static const NSInteger kDoctorInfoOKCode = 0;

@interface GetDoctorInfoRequest ()
@end

@implementation GetDoctorInfoRequest

-(id) initGetDoctorInfoRequestWithUserId:(NSString*)userId onComplete:(GetDoctorInfoResponseBlock)onComplete onError:(RequestErrorBlock)onError {
    if (( self = [super init] )) {
        self.onComplete = ^(NSDictionary* dict) {
            DoctorInfoResponse* response = nil;
            NSString* result = dict[kResultKey];
            if (result.length > 0 && result.integerValue == kDoctorInfoOKCode) {
                response = [[DoctorInfoResponse alloc] init];
                response.age= [dict[kAgeKey] integerValue];
                response.genderId = [dict[kGenderKey] integerValue];
                response.universityId = [dict[kPlaceOfDegreeKey] integerValue];
                response.manualUnivertityName = dict[kPlaceOfDegreeHandKey];
                response.yearOfDegree = [dict[kYearOfDegreeKey] integerValue];
                response.specializationId = [dict[kSpecializationKey] integerValue];
                response.secondSpecializationId = [dict[kSecondSpecializationKey] length] > 0 ? @([dict[kSecondSpecializationKey] integerValue]) : nil;
                response.countyId = [dict[kCountyOfPracticeKey] integerValue];
                response.healthCenter = [kValueYes isEqualToString:dict[kHealthCenterTypeKey]];
                response.publicHospital = [kValueYes isEqualToString:dict[kPublicHospitalTypeKey]];
                response.privateHospital = [kValueYes isEqualToString:dict[kPrivateHospitalTypeKey]];
                response.consultation = [kValueYes isEqualToString:dict[kConsultationTypeKey]];
                response.otherHealthCenter = [kValueYes isEqualToString:dict[kOtherCenterTypeKey]];
                response.otherHealthCenterText = dict[kHealthCenterTypeOtheKey];
                response.weeklyHours = [dict[kWeeklyHoursKey] integerValue];
                response.weeklyPatients = [dict[kWeeklyPatientsKey] integerValue];
                response.comments = dict[kDoctorCommentsKey];
                response.email = dict[kDoctorEmailKey];
            }
            else {
                DLog(@"Received GetDoctorInfo response with error code: %@", result);
            }
            
            onComplete(response);
        };
        self.onError = onError;
        
        self.name = kRequestName;
        
        // Add request content
        [self addParam:userId forKey:kUserIdKey];
    }
    return self;
}

@end
