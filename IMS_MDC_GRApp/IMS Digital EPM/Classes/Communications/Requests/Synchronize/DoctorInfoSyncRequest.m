//
//  DoctorInfoSyncRequest.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "DoctorInfoSyncRequest.h"
#import "Doctor.h"
#import "Gender.h"
#import "Province.h"
#import "Specialty.h"
#import "University.h"
#import "MedicalCenter.h"

// Error domain
static NSString* const kErrorDomain  = @"es.lumata.DoctorInfoSyncRequest";
// Request Name
static NSString* const kInsertRequestName  = @"insertDoctor";
static NSString* const kUpdateRequestName  = @"insertDoctor";


static NSString* const kUserIdKey =                 @"userId";
static NSString* const kAgeKey =                    @"Age";
static NSString* const kGenderKey =                 @"Gender_Id";
;
static NSString* const kDoctorCommentsKey =         @"Comments";
static NSString* const kDoctorEmailKey =            @"Email";
static NSString* const kYearOfDegreeKey =           @"Year";
static NSString* const kPlaceOfDegreeHandKey =      @"University_Value";
static NSString* const kCountyOfPracticeKey =      @"Province_Id";
static NSString* const kSpecializationKey =         @"Speciality_Id";
static NSString* const kSecondSpecializationKey =   @"OtherSpeciality_Id";

static NSString* const kWeeklyHoursKey =            @"WorkingHours";
static NSString* const kWeeklyPatientsKey =         @"PatientsPerWeek";
// Not Verified 
//static NSString* const kPublicHospitalTypeKey =     @"publicHospitalType";
//static NSString* const kPrivateHospitalTypeKey =    @"privateHospitalType";
//static NSString* const kOtherCenterTypeKey =        @"otherCenterType";
//static NSString* const kOtherCenterNameKey =        @"otherCenterName";
//static NSString* const kHealthCenterTypeKey =       @"healthCenterType";
//static NSString* const kHealthCenterTypeOtheKey =   @"healthCenterTypeOther";

//static NSString* const kMedicalCenterKey1 =   @"Centre1_Id";
//static NSString* const kMedicalCenterKey2 =   @"Centre2_Id";
//static NSString* const kMedicalCenterKey3 =   @"Centre3_Id";
//static NSString* const kMedicalCenterKey4 =   @"Centre4_Id";
//static NSString* const kMedicalCenterKey5 =   @"Centre5_Id";
//static NSString* const kMedicalCenterKey6 =   @"Centre6_Id";

static NSString* const kCentreList =   @"CentreList";
static NSString* const kMedicalCenterNameKey =   @"OtherCentre_Value";
/////////////////////////////

static NSString* const kValueYes =     @"1";
static NSString* const kValueNo =      @"0";
static NSString* const kEmptyID =      @"";
//static NSString* const kUniversity =            @"university";

// Content dictionary
//Ravi_Bukka: Commented for integrating new DoctorInfo service
/* static NSString* const kUserIdKey =                 @"userId";
 static NSString* const kAgeKey =                    @"age";
 static NSString* const kGenderKey =                 @"sex";
 static NSString* const kPlaceOfDegreeKey =          @"placeOfDegree";
 static NSString* const kPlaceOfDegreeHandKey =      @"placeOfDegreeHand";
 static NSString* const kYearOfDegreeKey =           @"yearOfDegree";
 static NSString* const kCountyOfPracticeKey =       @"countyOfPractice";
 static NSString* const kSpecializationKey =         @"specialization";
 static NSString* const kSecondSpecializationKey =   @"secondSpecialization";
 static NSString* const kHealthCenterTypeKey =       @"healthCenterType";
 static NSString* const kPublicHospitalTypeKey =     @"publicHospitalType";
 static NSString* const kPrivateHospitalTypeKey =    @"privateHospitalType";
 static NSString* const kConsultationTypeKey =       @"consultationType";
 static NSString* const kOtherCenterTypeKey =        @"otherCenterType";
 static NSString* const kHealthCenterTypeOtheKey =   @"healthCenterTypeOther";
 static NSString* const kWeeklyHoursKey =            @"weeklyHours";
 static NSString* const kWeeklyPatientsKey =         @"weeklyPatients";
 static NSString* const kContributionStartKey =      @"contributionStart"; // Format yyyy/MM/dd
 static NSString* const kDoctorCommentsKey =         @"doctorComments";
 static NSString* const kDoctorEmailKey =            @"email";
 
 static NSString* const kValueYes =     @"1";
 static NSString* const kValueNo =      @"0";  */






@implementation DoctorInfoSyncRequest
//Ravi_Bukka: Webservice here I can do
-(id) initDoctorInfoSyncRequestWithSyncType:(DoctorInfoSyncType)syncType doctorInfo:(DoctorInfo*)doctorInfo onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    
    if (self) {
        // Set request name
        switch (syncType) {
            case kDoctorInfoSyncTypeInsert:
                self.name = kInsertRequestName;
                break;
            case kDoctorInfoSyncTypeUpdate:
                self.name = kUpdateRequestName;
                break;
        }
        Doctor* doctor = [doctorInfo doctor];
        // Add request content
        [self addParam:doctorInfo.identifier forKey:kUserIdKey];
        [self addNumberParam:doctor.age forKey:kAgeKey];
        [self addParam:doctor.gender.identifier forKey:kGenderKey defaultValue:kEmptyID];
        University* university = doctor.university;
       // MedicalCenter *medicalC=doctor.medicalCenter;
       
        NSLog(@"University %@ BOOL VALUE %c",university.identifier,university.userDefined.boolValue);
        [self addParam:(university.userDefined.boolValue ? university.name : university.identifier) forKey:kPlaceOfDegreeHandKey defaultValue:kEmptyID];
        [self addParam:doctor.graduationYear forKey:kYearOfDegreeKey defaultValue:kEmptyID];
        [self addParam:doctor.province.identifier forKey:kCountyOfPracticeKey defaultValue:kEmptyID];
        [self addParam:doctor.mainSpeciality.identifier forKey:kSpecializationKey defaultValue:kEmptyID];
        NSLog(@"Specialization %@ and OS %@",doctor.mainSpeciality.identifier,doctor.secondarySpecialty.identifier);
        [self addParam:doctor.secondarySpecialty.identifier forKey:kSecondSpecializationKey defaultValue:kEmptyID];
        // Weekly
        [self addNumberParam:doctorInfo.weeklyActivityHours forKey:kWeeklyHoursKey defaultValue:kEmptyID];
        [self addNumberParam:doctorInfo.averageWeeklyPatients forKey:kWeeklyPatientsKey defaultValue:kEmptyID];
        
//Utpal:CR#4 Demographic information for centers, commented below lines

     /*
        NSMutableArray *checkBoxArray=[[NSMutableArray alloc]init];
         
        for (MedicalCenter* center in doctorInfo.medicalCenters){
        
           
            if (center.identifier.integerValue==1) {
                     [checkBoxArray addObject:[NSString stringWithFormat:@"%d",center.identifier.integerValue]];          
             }
            
            else  if (center.identifier.integerValue==2) {
                              [checkBoxArray addObject:[NSString stringWithFormat:@"%d",center.identifier.integerValue]];

            }
            else  if (center.identifier.integerValue==3) {
                              [checkBoxArray addObject:[NSString stringWithFormat:@"%d",center.identifier.integerValue]];

            }
            else  if (center.identifier.integerValue==4) {
                [checkBoxArray addObject:[NSString stringWithFormat:@"%d",center.identifier.integerValue ]];

            }
            else  if (center.identifier.integerValue==5) {
                            [checkBoxArray addObject:[NSString stringWithFormat:@"%d",center.identifier.integerValue]];
            }
            else  if (center.userDefined.boolValue) {
        
                [checkBoxArray addObject:@"8"];

                [self addParam:doctorInfo.otherMedicalCenter forKey:kMedicalCenterNameKey defaultValue:kEmptyID];
            }
                
            
           
        }
       
        NSMutableString *exString = [[NSMutableString alloc]init];
        for (NSString *yourVar in checkBoxArray) {
            
            NSLog (@"Your Array elements are = %@", yourVar);
            [exString appendFormat:@"%@,",yourVar];
           }
        */
        
        
        //Utpal:CR#4 Demographic information for centers, added below lines
        NSString *exString = [[NSUserDefaults standardUserDefaults] valueForKey:@"centreParameter"];
        NSString *otherMedicalFieldText = [[NSUserDefaults standardUserDefaults] valueForKey:@"OtherMedicalFieldText"];
        
        if(exString.length>0 ){
            //            NSString *para=[exString substringToIndex:[exString length]-1];
            [self addParam:exString forKey:kCentreList defaultValue:kEmptyID];
            [self addParam:otherMedicalFieldText forKey:kMedicalCenterNameKey defaultValue:kEmptyID];
            
        }
        else
        {
            [self addParam:@"" forKey:kCentreList defaultValue:kEmptyID];
            [self addParam:@"" forKey:kMedicalCenterNameKey defaultValue:kEmptyID];
            
            //  [self addParam:@"" forKey:kOtherSpecialitiesTextNameKey defaultValue:kEmptyID];
        }        // User selected medical centers
//        NSSet* doctorCenters = doctorInfo.medicalCenters;
//        NSString* healthCenter = kValueNo;
        //NSString* publicHospital = [doctorCenters containsObject:kPublicHospitalTypeKey] ? kValueYes : kValueNo;
        //NSString* privateHospital = [doctorCenters containsObject:kPrivateHospitalTypeKey] ? kValueYes : kValueNo;
         //       NSString* consultation = [doctorCenters containsObject:kConsultationTypeKey] ? kValueYes : kValueNo ;
        //NSString* otherCenter = [doctorCenters containsObject:kOtherCenterTypeKey] ? kValueYes : kValueNo;
      
       // NSString* otherCenterName = nil;
        // Iterate through selected medical centers to fill fields
//        for (MedicalCenter* center in doctorInfo.medicalCenters) {
//            if (center.identifier.integerValue == kCenterTypeValueHealthCenter) {
//                healthCenter = kValueYes;
//            }
//            else if (center.identifier.integerValue == kCenterTypeValuePublicHospital) {
//                publicHospital = kValueYes;
//            }
//            else if (center.identifier.integerValue == kCenterTypeValuePrivateHospital) {
//                privateHospital = kValueYes;
//            }
////            else if (center.identifier.integerValue == kCenterTypeValueConsultation) {
////                consultation = kValueYes;
////            }
//            else if (center.userDefined.boolValue) {
//                otherCenter = kValueYes;
//                //otherCenterName = center.name;
//                otherCenterName=doctorInfo.otherMedicalCenter;
//            }
//        }

            
       // [self addParam:healthCenter forKey:kHealthCenterTypeKey];
        //[self addParam:publicHospital forKey:kPublicHospitalTypeKey];
        //Centre_Id
       // [self addParam:privateHospital forKey:kPrivateHospitalTypeKey];
//        NSLog(@"Other Medical Center %@",doctorInfo.otherMedicalCenter);
//          [self addParam:(medicalC.userDefined.boolValue ? kValueNo : doctorInfo.otherMedicalCenter) forKey:kOtherCenterTypeKey];
      //  [self addParam:otherCenter forKey:kOtherCenterTypeKey];
    //[self addParam:otherCenterName forKey:kOtherCenterNameKey];
    //[self addParam:otherCenter forKey:kOtherCenterTypeKey];
   //   [self addParam:doc.medicalCenter forKey:kMedicalCenterKey];
        [self addParam:doctorInfo.comments forKey:kDoctorCommentsKey defaultValue:kEmptyID];
        [self addParam:doctorInfo.email forKey:kDoctorEmailKey defaultValue:kEmptyID];
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            
            
            onComplete();
        };
    }
    return self;
    
    //Ravi_Bukka: Commented for integrating new DoctorInfo service
    /*    if (self) {
     // Set request name
     switch (syncType) {
     case kDoctorInfoSyncTypeInsert:
     self.name = kInsertRequestName;
     break;
     case kDoctorInfoSyncTypeUpdate:
     self.name = kUpdateRequestName;
     break;
     }
     Doctor* doctor = [doctorInfo doctor];
     // Add request content
     [self addParam:doctorInfo.identifier forKey:kUserIdKey];
     [self addNumberParam:doctor.age forKey:kAgeKey];
     [self addParam:doctor.gender.identifier forKey:kGenderKey];
     University* university = doctor.university;
     [self addParam:university.identifier forKey:kPlaceOfDegreeKey];
     [self addParam:(university.userDefined.boolValue ? university.name : nil) forKey:kPlaceOfDegreeHandKey];
     [self addParam:doctor.graduationYear forKey:kYearOfDegreeKey];
     [self addParam:doctor.province.identifier forKey:kCountyOfPracticeKey];
     [self addParam:doctor.mainSpeciality.identifier forKey:kSpecializationKey];
     [self addParam:doctor.secondarySpecialty.identifier forKey:kSecondSpecializationKey];
     // User selected medical centers
     NSSet* doctorCenters = doctorInfo.medicalCenters;
     NSString* healthCenter = kValueNo;
     NSString* publicHospital = [doctorCenters containsObject:kPublicHospitalTypeKey] ? kValueYes : kValueNo;
     NSString* privateHospital = [doctorCenters containsObject:kPrivateHospitalTypeKey] ? kValueYes : kValueNo;
     NSString* consultation = [doctorCenters containsObject:kConsultationTypeKey] ? kValueYes : kValueNo;
     NSString* otherCenter = [doctorCenters containsObject:kOtherCenterTypeKey] ? kValueYes : kValueNo;
     NSString* otherCenterName = nil;
     // Iterate through selected medical centers to fill fields
     for (MedicalCenter* center in doctorInfo.medicalCenters) {
     if (center.identifier.integerValue == kCenterTypeValueHealthCenter) {
     healthCenter = kValueYes;
     }
     else if (center.identifier.integerValue == kCenterTypeValuePublicHospital) {
     publicHospital = kValueYes;
     }
     else if (center.identifier.integerValue == kCenterTypeValuePrivateHospital) {
     privateHospital = kValueYes;
     }
     else if (center.identifier.integerValue == kCenterTypeValueConsultation) {
     consultation = kValueYes;
     }
     else if (center.userDefined.boolValue) {
     otherCenter = kValueYes;
     otherCenterName = center.name;
     }
     }
     [self addParam:healthCenter forKey:kHealthCenterTypeKey];
     [self addParam:publicHospital forKey:kPublicHospitalTypeKey];
     [self addParam:privateHospital forKey:kPrivateHospitalTypeKey];
     [self addParam:consultation forKey:kConsultationTypeKey];
     [self addParam:otherCenter forKey:kOtherCenterTypeKey];
     [self addParam:otherCenterName forKey:kHealthCenterTypeOtheKey];
     // Weekly
     [self addNumberParam:doctorInfo.weeklyActivityHours forKey:kWeeklyHoursKey];
     [self addNumberParam:doctorInfo.averageWeeklyPatients forKey:kWeeklyPatientsKey];
     // First collaboration date
     NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
     [dateFormatter setDateFormat:@"yyyy/MM/dd"];
     NSString* firstCollaborationDate = [dateFormatter stringFromDate:doctorInfo.firstCollaborationDate];
     [dateFormatter release];
     [self addParam:firstCollaborationDate forKey:kContributionStartKey];
     [self addParam:doctorInfo.comments forKey:kDoctorCommentsKey];
     [self addParam:doctorInfo.email forKey:kDoctorEmailKey];
     // Set response handler blocks
     self.onError = onError;
     self.onComplete = ^(NSDictionary* responseDictionary) {
     onComplete();
     };
     }
     
     return self */
}
-(void)dealloc{
    [super dealloc];

}

@end