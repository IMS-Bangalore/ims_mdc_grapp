//
//  TreatmentSyncRequest.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "TreatmentSyncRequest.h"
#import "Diagnosis.h"
#import "Patient.h"
#import "DoctorInfo.h"
#import "Presentation.h"
#import "Effect.h"
#import "Dosage.h"
#import "Medicament.h"
#import "UnitType.h"
#import "Frequency.h"
#import "DurationType.h"
#import "TherapyType.h"
#import "TherapyElection.h"
#import "TherapyReplacementReason.h"
#import "OtherSpecialty.h"
#import "DrugReimbursement.h"
#import "PatientInsurance.h"
#import "ProductType.h"
#import "DurationType.h"

// Request Name
static NSString* const kInsertRequestName  = @"insertMedicine";
static NSString* const kUpdateRequestName  = @"updateMedicine";
static NSString* const kRemoveRequestName  = @"removeMedicine";
// Content dictionary
static NSString* const kUserIdKey =         @"userId";
static NSString* const kPatientIdKey =      @"PatientId";
static NSString* const kDiagnosisIdKey =    @"DiagnosisWindow";
static NSString* const kTreatmentIdKey =    @"TreatmentWindow";
//static NSString* const kProductIdKey  =  @"Medicine_id";
//static NSString* const kPresentationKey =  @"FormatName";
//static NSString* const kPresentationIdKey =  @"PresentationId";
static NSString* const kProductIdKey =  @"Products_Id";
//Deepak_Carpenter: added to resolve manual product
static NSString* const kProductCodeKey =  @"ProductCode_Id";

static NSString* const kProductValueKey =  @"ProductsDescription";
static NSString* const kPresentationValueKey = @"ProductsPresentation";
static NSString* const kDosageCodeKey =  @"DosageUnits_Id";
//static NSString* const kPresentationKey =  @"FormatName";
static NSString* const kDesiredEffectValueKey =  @"DesiredEffectManual";
static NSString* const kDesiredEffectIdKey =  @"DesiredEffect_Id";

//static NSString* const kDesiredEffectNameKey =  @"DesiredEffectName";

static NSString* const kFrequencyKey =  @"DoseFrequency_Id";
static NSString* const kDurationQuantityKey =  @"DurationQuantity";
static NSString* const kDurationType =  @"Duration_Id";

static NSString* const kPrescribedQuantityKey =  @"PrescribedQuantity_Value";

static NSString* const kTherapyElectionKey =  @"TherapyElection_Id";
static NSString* const kTherapyElectionOtherKey =  @"TherapyElectionOther_Id";
static NSString* const kTherapyKindKey =  @"TherapyType_Id";
static NSString* const kTherapyKindChangeKey =  @"TherapyReasons_Id";
static NSString* const kOtherCommentsKey =  @"OtherComments_Value";

static NSString* const kEmptyID = @"";
static NSString* const kUserDefinedValueID = @"1";
static NSString* const kDoseUnitEmptyID = @"A";

static NSString* const kDoseQuantityKey =  @"DoseQuantity";
static NSString* const kDoseUnitKey =  @"DoseUnits";

static NSString* const kLTD = @"LongDuration";
static NSString* const kOnDemand = @"OnDemand";
static NSString* const kUniqueDose = @"UniqueDose";
static NSString* const kReplacedMedicamentNameKey =  @"MedicineNameReplaced_Id";
static NSString* const kReplacedMedicamentvalueKey =  @"MedicineNameReplacedManual";
/*
 Product
 Presentation Form
 Desired Effect
 Dosage
 Frequency
 Duration
 Number of Boxes
 Therapy Selection
 Choice of Therapy
 */
//Kanchan Nair
//static NSString* const kDrugReimbursementkey =  @"drugReimbursementKey";
//static NSString* const kUserDrugReimbursementkey =  @"userDrugReimbursementKey";
//static NSString* const kEmptyDrugReimburseID = @"1";
//static NSString* const kPatientInsurancekey =  @"patientInsuranceKey";
//static NSString* const kUserPatientInsurancekey =  @"userPatientInsuranceKey";
//static NSString* const kEmptyPatientInsuranceID = @"1";


//Ravi_Bukka: Commented for integrating new PatientInfo service
// Error domain
/*static NSString* const kErrorDomain  = @"es.lumata.TreatmentSyncRequest";
// Request Name
static NSString* const kInsertRequestName  = @"insertMedicine";
static NSString* const kUpdateRequestName  = @"updateMedicine";
static NSString* const kRemoveRequestName  = @"removeMedicine";
// Content dictionary
static NSString* const kUserIdKey =         @"userId";
static NSString* const kPatientIdKey =      @"patientWindowId";
static NSString* const kDiagnosisIdKey =    @"diagnosisWindowId";
static NSString* const kTreatmentIdKey =    @"medicineWindowId";
static NSString* const kPresentationIdKey =  @"medicineID";
static NSString* const kMedicamentNameKey =  @"medicineName";
static NSString* const kPresentationKey =  @"formatName";
static NSString* const kRecommendBrandKey =  @"brand";
static NSString* const kRecommendGenericKey =  @"generic";
static NSString* const kDesiredEffectIdKey =  @"desiredEffect";
static NSString* const kDesiredEffectNameKey =  @"desiredEffectName";
static NSString* const kOnDemandKey =  @"onDemand";
static NSString* const kLongDurationKey =  @"longDuration";
static NSString* const kDoseQuantityKey =  @"doseQuantity";
static NSString* const kDoseUnitKey =  @"doseUnit";
static NSString* const kDoseFrequencyKey =  @"doseFrequency";
static NSString* const kDoseFrequencyTypeKey =  @"doseFrequencyId";
static NSString* const kDurationQuantityKey =  @"durationQuantity";
static NSString* const kDurationUnitKey =  @"durationUnit";
static NSString* const kPrescribedQuantityKey =  @"prescribedQuantity";
static NSString* const kOtherCommentsKey =  @"otherComments";
static NSString* const kTherapyElectionKey =  @"therapyElection";
static NSString* const kTherapyElectionOtherKey =  @"therapyElectionOther";
static NSString* const kTherapyKindKey =  @"therapyKind";
static NSString* const kTherapyKindChangeKey =  @"therapyKindChange";
static NSString* const kReplacedMedicamentIdKey =  @"medicineIdReplaced";
static NSString* const kReplacedMedicamentNameKey =  @"medicineNameReplaced";

static NSString* const kEmptyID = @"1";
static NSString* const kDoseUnitEmptyID = @"A";
static NSString* const kUserDefinedValueID = @"1";
static NSString* const kValueYes =     @"1";
static NSString* const kValueNo =      @"0";

static NSString* const kUniqueDoseId = @"8"; */





@implementation TreatmentSyncRequest

-(id) initTreatmentSyncRequestWithSyncType:(TreatmentSyncType)syncType treatment:(Treatment*)treatment onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    if (self) {
        // Set request name
        switch (syncType) {
            case kTreatmentSyncTypeInsert:
                self.name = kInsertRequestName;
                break;
            case kTreatmentSyncTypeUpdate:
                self.name = kUpdateRequestName;
                break;
            case kTreatmentSyncTypeDelete:
                self.name = kRemoveRequestName;
                break;
        }
        
        Diagnosis* diagnosis = treatment.diagnosis;
        if (diagnosis == nil) {
            diagnosis = (Diagnosis*)treatment.parentEntity;
        }
        
        Patient* patient = diagnosis.patient;
        if (patient == nil) {
            patient = (Patient*)diagnosis.parentEntity;
        }
        
        DoctorInfo* doctorInfo = patient.doctorInfo;
        if (doctorInfo == nil) {
            doctorInfo = (DoctorInfo*)patient.parentEntity;
        }
        
        // Add request content
        [self addParam:doctorInfo.identifier forKey:kUserIdKey];
        [self addParam:patient.identifier forKey:kPatientIdKey];
        [self addParam:diagnosis.identifier forKey:kDiagnosisIdKey];
        [self addParam:treatment.identifier forKey:kTreatmentIdKey];
        
        //Kanchan
        //[self addParam:treatment.drugReimbursement.identifier forKey:kDrugReimbursementkey];
        //[self addParam:treatment.userDrugReimbursement forKey:kUserDrugReimbursementkey];
        //[self addParam:treatment.patientInsurance.identifier forKey:kPatientInsurancekey];
        //[self addParam:treatment.userPatientInsurance forKey:kUserPatientInsurancekey];
        
        // If action is insert or update, include additional info to request
        
        
        if (syncType != kTreatmentSyncTypeDelete) {
            BOOL userDefined = treatment.userMedicament.length > 0 || treatment.userPresentation.length > 0;
           
          
                //Deepak_Carpenter:Added to fetch Product code in case doctor enters manual presentation
//                NSArray *array=[treatment.replacingMedicament.presentations allObjects];
//                Presentation *Pres = (Presentation*)[array objectAtIndex:0];
                NSLog(@"product value is %@",treatment.medicament.identifier);
            //Deepak_Carpenter : Added this condition to resolve Manual Presentation form value
            if (treatment.userPresentation.length > 0) {
                 [self addParam:treatment.medicament.identifier forKey:kProductCodeKey defaultValue:kEmptyID];
            }else
                [self addParam:@"" forKey:kProductCodeKey defaultValue:kEmptyID];
            
            [self addParam:treatment.presentation.identifier forKey:kProductIdKey defaultValue:kEmptyID];
                [self addParam:treatment.userPresentation forKey:kPresentationValueKey defaultValue:kEmptyID];
                [self addParam:treatment.userMedicament forKey:kProductValueKey defaultValue:kEmptyID];
           
            //[self addParam:(userDefined ? kUserDefinedValueID : treatment.presentation.identifier) forKey:kPresentationIdKey defaultValue:kEmptyID];
            
    
            
//            NSString* presentationName = treatment.presentation != nil ?
//           treatment.userPresentation : treatment.userPresentation ;
//            [self addParam:(userDefined ? presentationName : nil) forKey:kPresentationKey];
    //      [self addParam:treatment.userPresentation forKey:kPresentationKey];
            NSLog(@"%@",treatment.userPresentation);
            if (treatment.userDesiredEffect) {
                [self addParam:treatment.desiredEffect.identifier forKey:kDesiredEffectIdKey defaultValue:kEmptyID];
                [self addParam:treatment.userDesiredEffect forKey:kDesiredEffectValueKey defaultValue:kEmptyID];
            }else{
            [self addParam:treatment.desiredEffect.identifier forKey:kDesiredEffectIdKey defaultValue:kEmptyID];
            [self addParam:treatment.userDesiredEffect forKey:kDesiredEffectValueKey defaultValue:kEmptyID];
            }
      //      [self addParam:treatment.userDesiredEffect forKey:kDesiredEffectNameKey];
//            NSString* presentationName = treatment.presentation != nil ? treatment.presentation.name : treatment.userPresentation;
//            [self addParam:(userDefined ? presentationName : nil) forKey:kMedicamentNameKey];
            
//            NSLog(@"Presentation form %@",treatment.userPresentation);
//            [self addParam:treatment.userPresentation forKey:kPresentationFormKey];
            
            
            //Kanchan
            //            [self addParam:treatment.drugReimbursement.identifier forKey:kDrugReimbursementkey defaultValue:kEmptyDrugReimburseID];
            //            [self addParam:treatment.userDrugReimbursement forKey:kUserDrugReimbursementkey];
            //
            //            [self addParam:treatment.patientInsurance.identifier forKey:kPatientInsurancekey defaultValue:kEmptyPatientInsuranceID];
            //            [self addParam:treatment.userPatientInsurance forKey:kUserPatientInsurancekey];
            NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"US"] autorelease]];
            [numberFormatter setPositiveFormat:@"#,##0.#"];
            [self addParam:[numberFormatter stringFromNumber:treatment.dosage.quantity] forKey:kDoseQuantityKey defaultValue:kEmptyID];
            [self addNumberParam:treatment.dosage.units forKey:kDoseUnitKey defaultValue:kEmptyID];
            [numberFormatter release];
            
//            [self addNumberParam:treatment.dosage.uniqueDose forKey:kUniqueDose defaultValue:@"0"];
            if (![treatment.dosage.longDurationTreatment boolValue]) {
                [self addParam:@"2" forKey:kLTD];
            } else [self addNumberParam:treatment.dosage.longDurationTreatment forKey:kLTD defaultValue:@"2"];
            if (![treatment.dosage.onDemand boolValue]) {
                [self addParam:@"2" forKey:kOnDemand];
            }else [self addNumberParam:treatment.dosage.onDemand forKey:kOnDemand defaultValue:@"2"];
            
            [self addParam:treatment.dosage.durationType.identifier forKey:kDurationType];
//            [self addParam:treatment.therapyType.identifier forKey:kTherapyKindKey];
            
//            [self addParam:treatment.userDesiredEffect forKey:kDesiredEffectNameKey];
    [self addParam:treatment.dosage.frequency.identifier forKey:kFrequencyKey defaultValue:kEmptyID];
    [self addNumberParam:treatment.dosage.duration forKey:kDurationQuantityKey defaultValue:kEmptyID];
     [self addNumberParam:treatment.dosage.recipeCount forKey:kPrescribedQuantityKey defaultValue:kEmptyID];
            
            [self addParam:treatment.dosage.comments forKey:kOtherCommentsKey defaultValue:kEmptyID];
            [self addParam:treatment.therapyChoiceReason.identifier forKey:kTherapyElectionKey defaultValue:kEmptyID];
            [self addParam:treatment.recommendationSpecialist.identifier forKey:kTherapyElectionOtherKey defaultValue:kEmptyID];
      //      [self addParam:treatment.therapyType.identifier forKey:kTherapyKindKey defaultValue:kEmptyID];
            //Deepak_carpenter: Added for therapy election button group
            [self addParam:treatment.therapyElection.identifier forKey:kTherapyKindKey defaultValue:kEmptyID];
            [self addParam:treatment.replaceReason.identifier forKey:kTherapyKindChangeKey defaultValue:kEmptyID];
          //Deepak Carpenter : Added to fetch identifier from Presentation NSSET
            NSArray *array=[treatment.replacingMedicament.presentations allObjects];
            Presentation *present = (Presentation*)[array objectAtIndex:0];
            
            //Deepak_Carpenter:Added for Manual entered manual Replacemedicine
            if (present.identifier) {
                [self addParam:present.identifier forKey:kReplacedMedicamentNameKey defaultValue:kEmptyID];
                [self addParam:treatment.userReplacingMedicament forKey:kReplacedMedicamentvalueKey defaultValue:kEmptyID];
            }else
                [self addParam:treatment.userReplacingMedicament forKey:kReplacedMedicamentvalueKey defaultValue:kEmptyID];
            
            
            
            // [self addParam:treatment.replacingMedicament.identifier forKey:kReplacedMedicamentNameKey defaultValue:kEmptyID];
            [self addParam:treatment.presentation.unitType.identifier forKey:kDosageCodeKey defaultValue:kEmptyID];
            //            NSLog(@"Presentation value in replace medicament %@",treatment.replacingMedicament.presentations.identifier);

            //_treatment.replacingMedicament.name
            //treatment.dosage.recipeCount
            //treatment.dosage.duration
            
        }
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            onComplete();
        };
    }
    return self;
}

@end