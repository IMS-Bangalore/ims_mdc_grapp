//
//  GetDeviceNotificationsRequest.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 13/12/12.
//
//

#import "GetDeviceNotificationsRequest.h"
#import "NSDictionary+ArrayForKey.h"
#import "Notification.h"

static NSString* const kDateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
static NSString* const kNotificationTypeDelivery = @"Delivery";
static NSString* const kNotificationTypeTag = @"Tag";

/* Request info */
static NSString* const kResourceName = @"devices";
static NSString* const kSegmentParamNotifications = @"search_notifications";
static NSString* const kTags = @"tags";

/* Request values */
static NSString* const kTagRich = @"rich";

/* Response parameters */
static NSString* const kObjectsResponseWrapper = @"objects";
static NSString* const kReferencesResponseWrapper = @"references";
static NSString* const kResponseNotificationsIdKey = @"id";
static NSString* const kResponseReferenceNotificationsIdKey = @"notification_id";
static NSString* const kResponseSoundKey = @"sound";
static NSString* const kResponseAlertKey = @"alert";
static NSString* const kResponseBadgeKey = @"badge";
static NSString* const kResponseCreationDateKey = @"created_at";
static NSString* const kResponseUpdateDateKey = @"updated_at";
static NSString* const kResponseSendDateKey = @"send_at";
static NSString* const kResponseSentDateKey = @"sent_at";
static NSString* const kResponseRichNotificationsWrapperKey = @"custom_properties";
static NSString* const kResponseCallbackKey = @"callback";
static NSString* const kResponseRichUrlKey = @"rich_url";
static NSString* const kResponseTypeKey = @"type";
static NSString* const kResponseDeviceIdKey = @"device_id";

@implementation GetDeviceNotificationsRequest

- (id)initGetDeviceNotificationsRequestWithDevice:(Device*)device onComplete:(GetDeviceNotificationsResponseBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    if (self) {
        [self addSegmentParam:device.deviceId];
        [self addSegmentParam:kSegmentParamNotifications];
        self.requestMethod = kTCRequestMethodPOST;
        // Set resource name
        self.resource = kResourceName;
        // Add param tags
        [self addParam:[TCRequestParam paramWithKey:kTags andArrayValue:@[kTagRich]]];
        
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            NSArray* notifications = nil;
            notifications = [self notificationsFromDictionary:responseDictionary];
            onComplete(notifications);
        };
    }
    return self;
}

- (NSArray*)notificationsFromDictionary:(NSDictionary*)dictionary {
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:kDateFormat];
    
    NSMutableArray* notifications = [NSMutableArray array];
    NSArray* notificationsObjectsFromDictionary = [dictionary arrayForKey:kObjectsResponseWrapper];    
    
    for (NSDictionary* dict in notificationsObjectsFromDictionary) {
        Notification* notification = [[Notification alloc] init];
        notification.descriptionText = [dict objectForKey:kResponseAlertKey];;
        notification.notificationId = [dict objectForKey:kResponseNotificationsIdKey];
        notification.date = [df dateFromString:[dict objectForKey:kResponseSendDateKey]];
        notification.type = [dict objectForKey:kResponseTypeKey];
        notification.contentUrl = [self richNotificationFromDictionary:[dict objectForKey:kResponseRichNotificationsWrapperKey]];
        if (notification.contentUrl != nil) {
            [notifications addObject:notification];
        }
        [notification release];
    }
    [df release];
    return notifications;
}

- (NSString*)richNotificationFromDictionary:(NSDictionary*)dictionary {
    if ([dictionary isKindOfClass:[NSDictionary class]]) {
        return [dictionary objectForKey:kResponseRichUrlKey];
    }
    return nil;
}

@end
