//
//  RequestFactory.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 26/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TwinCodersLibrary.h"
#import "BaseRequest.h"
#import "RESTJSONRequest.h"
// Session 
#import "LoginRequest.h"
#import "ActivateUserRequest.h"
#import "ChangePasswordRequest.h"
#import "RemindPasswordRequest.h"
#import "CountdownRequest.h"
// Synchronize
#import "DoctorInfoSyncRequest.h"
#import "PatientSyncRequest.h"
#import "DiagnosisSyncRequest.h"
#import "TreatmentSyncRequest.h"
// Update 
#import "UpdateDataRequest.h"
#import "GetDoctorInfoRequest.h"
// Chat
#import "ChatGetAnswersRequest.h"
#import "ChatSendQuestionRequest.h"
#import "ChatMessage.h"
// Statistics
#import "SendStatisticsRequest.h"
// Notifications
#import "CreateDeviceRequest.h"
#import "GetDeviceNotificationsRequest.h"



/** @brief Factory that provides instances for requests to allow communication with server */
@interface RequestFactory : NSObject

+(RequestFactory*) sharedInstance;

#pragma mark Session

/**
 @brief Creates a request to login in the system
 @param userName User id for login
 @param password User Password
 @param removePreviousData Defines if previous data stored for the doctor should be removed
 @param onComplete Block that will be executed if login is successful
 @param onError Block that will be executed if login is not successful
 */
-(TCBaseRequest*) createLoginRequestWithUserName:(NSString*)userName password:(NSString*)password appVersion:appVersion removePreviousData:(BOOL)removePreviousData onComplete:(LoginResponseBlock)onComplete onError:(TCRequestErrorBlock)onError;

/**
 @brief Creates a request to activate active user
 @param doctorInfo User to be activated
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(BaseRequest*) createActivateUserRequestWithDoctorInfo:(DoctorInfo*)doctorInfo onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

/**
 @brief Creates a request to change active user password
 @param userName Entered user name
 @param prevPassword Entered previous password
 @param password New password to be set
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(BaseRequest*) createChangePasswordRequestWithUserName:(NSString*)userName prevPassword:(NSString*)prevPassword password:(NSString*)password onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

/**
 @brief Creates a request to remind user password
 @param doctorInfo User to remind password to
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(BaseRequest*) createRemindPasswordRequestWithDoctorId:(NSString*)doctorId onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

/**
 @brief Creates a request to obtain remaining days to final synchronization
 @param doctorInfo User to obtain coundown for
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(BaseRequest*) createCountdownRequestWithDoctorInfo:(DoctorInfo*)doctorInfo onComplete:(CountdownResponseBlock)onComplete onError:(RequestErrorBlock)onError;

#pragma mark Synchronize

/**
 @brief Creates a request to synchronize local doctor info with remote server
 @param syncType Defines the type of synchronization
 @param doctorInfo Doctor info to synchronize
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(BaseRequest*) createDoctorInfoSyncRequestWithSyncType:(DoctorInfoSyncType)syncType doctorInfo:(DoctorInfo*)doctorInfo onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

/**
 @brief Creates a request to synchronize local patient info with remote server
 @param syncType Defines the type of synchronization
 @param patient Patient info to synchronize
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(BaseRequest*) createPatientSyncRequestWithSyncType:(PatientSyncType)syncType patient:(Patient*)patient onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

/**
 @brief Creates a request to synchronize local diagnosis info with remote server
 @param syncType Defines the type of synchronization
 @param diagnosis Diagnosis info to synchronize
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(BaseRequest*) createDiagnosisSyncRequestWithSyncType:(DiagnosisSyncType)syncType diagnosis:(Diagnosis*)diagnosis onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

/**
 @brief Creates a request to synchronize local treatment info with remote server
 @param syncType Defines the type of synchronization
 @param treatment Treatment info to synchronize
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(BaseRequest*) createTreatmentSyncRequestWithSyncType:(TreatmentSyncType)syncType treatment:(Treatment*)treatment onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

/**
 @brief Creates a request to obtain application data updates
 @param syncType Defines the type of synchronization
 @param treatment Treatment info to synchronize
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(BaseRequest*) createUpdateDataRequestWithDoctorInfo:(DoctorInfo*)doctorInfo andOffset:(NSString*)offset lastUpdate:(NSDate*)lastUpdate onComplete:(UpdateDataResponseBlock)onComplete onError:(RequestErrorBlock)onError;

/**
 @brief Creates a request to send a chat message
 @param userId Defines the user Id who send the question
 @param chatMessage Treatment info to synchronize
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
- (BaseRequest*)createChatSendQuestionRequestWithUserId:(NSString *)userId andChatMessage:(ChatMessage*)chatMessage onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;


/**
 @brief Creates a request to get the list of questions and answers
 @param userId Defines the user Id who send the question
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
- (BaseRequest*)createGetAnswersRequestWithUserId:(NSString*)userId onComplete:(ChatGetAnswersResponseBlock)onComplete onError:(RequestErrorBlock)onError;

/**
 @brief Creates a request to get available doctor info from the server
 @param userId Defines the user Id
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
- (BaseRequest*)createGetDoctorInfoRequestWithUserId:(NSString*)userId onComplete:(GetDoctorInfoResponseBlock)onComplete onError:(RequestErrorBlock)onError;

/**
 @brief Creates a request to get send the statistics
 @param userId Defines the user Id who send the question
 @param timeElapsed The time spent in the last session
 @param dateStart The date of start of the session
 @param dateEnd The date of end of the session
 */
- (TCBaseRequest*)createSendStatisticsRequestWithUserId:(NSString*)userId andStats:(NSArray*)stats onComplete:(TCRequestSuccessBlock)onComplete onError:(TCRequestErrorBlock)onError;

/**
 @brief Constructor for CreateDeviceRequest
 @param token Token for getting the device id
 @param deviceAlias (Optional)
 @param onComplete Block that will be executed if login is successful
 @param onError Block that will be executed if login is not successful
 */
- (TCBaseRequest*)createCreateDeviceRequestWithToken:(NSString*)token andDeviceAlias:(NSString*)deviceAlias onComplete:(CreateDeviceResponseBlock)onComplete onError:(RequestErrorBlock)onError;

/**
 @brief Constructor for CreateDeviceRequest
 @param device The Device for which we want to get the notifications
 @param onComplete Block that will be executed if we obtain the notifications for the device
 @param onError Block that will be executed if the device is not correct
 */
- (TCBaseRequest*)createGetDeviceNotificationsRequestWithDevice:(Device*)device onComplete:(GetDeviceNotificationsResponseBlock)onComplete onError:(RequestErrorBlock)onError;



@end
