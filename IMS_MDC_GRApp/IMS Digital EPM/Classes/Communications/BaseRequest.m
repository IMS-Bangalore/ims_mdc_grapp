//
//  BaseRequest.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 26/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "BaseRequest.h"
#import "RequestLauncher.h"
#import "TwinCodersLibrary.h"
#import "XMLReader.h"
#import "RequestParam.h"
#import "GTMNSString+XML.h"

// Error domain
//Ravi_Bukka: CR#2: Patient Difference Local Notification
#import "AppDelegate.h"
NSString* const kBaseRequestErrorDomain  = @"https://native-platform.imshealth.com";
NSString* const kBaseRequestNetworkErrorDomain = @"https://native-platform.imshealth.com";;
NSString* const kRequestErrorCodeKey = @"RequestErrorCodeKey";
static NSInteger const kOperationErrorCode =   90;

// Response handle
static NSString* const kResponseXMLKey = @"actionResponse";
//Added By Deepak Carpenter for Chat WS
static NSString* const kChatResponseXMLKey = @"GetAnswersResult";

//Ravi_Bukka: Added for checkdataversion implement
static NSString* const kCheckDataResponseXMLKey = @"CheckDataVersionResult";


// Response field
static NSString* const kResponseErrorKey =         @"result";
static NSInteger const kResponseErrorOKCode =      0;
static NSString* const kResponseValueKey =         @"text";

//Ravi_Bukka: CR#2: Patient Difference and Local Notification
static NSString* const kPatientInfoResult = @"PatientInfoResult";
static NSString* const kPatientCountFromServer = @"patientsCount";



@interface BaseRequest()
@property (nonatomic, retain, readwrite) NSString* requestId;
@end

@implementation BaseRequest

@synthesize onError = _onError;
@synthesize onComplete = _onComplete;
@synthesize requestMethodKey = _requestMethodKey;
@synthesize contentParams = _contentParams;
@synthesize canceled = _canceled;
@synthesize requestLauncher = _requestLauncher;
@synthesize requestId = _requestId;
@synthesize name = _name;

- (id)init {
    self = [super init];
    if (self) {
        _canceled = NO;
        _endDelegateArray = [[NSMutableArray alloc] init];
        _contentParams = [[NSMutableArray alloc] init];
        self.requestId = [self createID];
    }
    return self;
}

- (void)dealloc {
    [_requestMethodKey release];
    [_contentParams release];
    [_requestId release];
    [_name release];
    [super dealloc];
}

- (NSString*)createID {
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidStr = (NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
    CFRelease(uuid);
    return [uuidStr autorelease];
}

//Ravi_Bukka: CR#2: Patient Difference and Local Notification
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


- (void)notifyEndDelegates {
    for (id<RequestEndDelegate> endDelegate in _endDelegateArray) {
        [endDelegate requestDidFinish:self];
    }
}

- (void)start {
    self.canceled = NO;
    [_requestLauncher launchRequest:self];
}

- (void)cancel {
    [_requestLauncher cancelRequest:self];
    self.canceled = YES;
    [self notifyEndDelegates];
}

- (void)addRequestEndDelegate:(NSObject<RequestEndDelegate>*)endDelegate {
    if (![_endDelegateArray containsObject:endDelegate]) {
        [_endDelegateArray addObject:endDelegate];
    }
}

- (void)removeRequestEndDelegate:(NSObject<RequestEndDelegate>*)endDelegate {
    if ([_endDelegateArray containsObject:endDelegate]) {
        [_endDelegateArray removeObject:endDelegate];
    }
}

/** @brief Method that removes empty values and 'text' fields from response dictionary, returning a clean object */
-(id)cleanObjectFrom:(id)sourceObject {
    id returnObject = nil;
    // Clean object of a nil object returns nil
    if (sourceObject != nil) {
        // Dictionary
        if ([sourceObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary* sourceDictionary = (NSDictionary*) sourceObject;
            // Empty dictionaries are omitted
            if (sourceDictionary.count > 0) {
                NSString* innerValue = [[sourceDictionary objectForKey:kResponseValueKey] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                // Base case. If response value contains info, dictionary is cleaned to contained object
                if (innerValue.length > 0) {
                    returnObject = innerValue;
                } else {
                    // Create dictionary
                    NSMutableDictionary* auxDictionary = [NSMutableDictionary dictionary];
                    // Iterate keys
                    for (NSString* key in [sourceDictionary allKeys]) {
                        // Get clean value of content
                        id innerValue = [self cleanObjectFrom:[sourceDictionary objectForKey:key]];
                        // If clean value is not nil, include it in response dictionary
                        if (innerValue != nil) {
                            [auxDictionary setObject:innerValue forKey:key];
                        }
                    }
                    returnObject = auxDictionary;
                }
            }
        }
        // Array
        else if ([sourceObject isKindOfClass:[NSArray class]]) {
            NSArray* sourceArray = (NSArray*) sourceObject;
            // Create array
            NSMutableArray* auxArray = [NSMutableArray array];
            // Iterate keys
            for (id arrayObject in sourceArray) {
                // Get clean value of content
                id cleanObject = [self cleanObjectFrom:arrayObject];
                // If clean value is not nil, include it in response dictionary
                if (cleanObject != nil) {
                    [auxArray addObject:cleanObject];
                }
            }
            returnObject = auxArray;
        } else {
            returnObject = sourceObject;
        }
    }
    return returnObject;
}

//Ravi_Bukka: Added guillermo's code
- (NSString*)stripXmlHeader:(NSString*)inputString {
    NSString *s = [[inputString copy] autorelease];
    NSRange r = [s rangeOfString:@"\\<\\?[^(?:\\?\\>)]+\\?\\>" options:NSRegularExpressionSearch];
    if (r.location != NSNotFound) {
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    }
    NSLog(@"S Value %@",s);
    return s;
}


#pragma mark - ASIRequest Delegate
- (NSDictionary*)responseDictionaryWithXMLString:(NSString*)responseString {

//Ravi_Bukka: Commented to add new code for Chat and CheckDataVersion Response handling
    
/*    NSLog(@"responseString --::-- %@",responseString);
    responseString = [self stripXmlHeader:responseString];
    NSMutableDictionary* responseDictionary = [NSMutableDictionary dictionary];
    // Read response XML
    NSError* error = nil;
    NSDictionary* xmlDictionary = [XMLReader dictionaryForXMLString:responseString error:&error withEncoding:NSUTF8StringEncoding];
    if (error == nil) {
        DLog(@"Response XML dictionary: %@", xmlDictionary);
        NSDictionary* dictionary = [self cleanObjectFrom:[xmlDictionary objectForKey:kResponseXMLKey]];
//        //Deepak Carpenter : Getting Chat(getAnswer method) Response which is having different key
//        if ([dictionary allKeys]==NULL) {
//            dictionary = [self cleanObjectFrom:[xmlDictionary objectForKey:kChatResponseXMLKey]];
//        }
//
//        
//        if (dictionary.count > 0) {
//            [responseDictionary addEntriesFromDictionary:dictionary];
//        }
//        DLog(@"Response dictionary: %@", responseDictionary);
        
//Ravi_Bukka: Added for Checkdataversion implementation
        if(dictionary)
        {
            //Deepak Carpenter : Getting Chat(getAnswer method) Response which is having different key
            if ([[dictionary allKeys] containsObject:kChatResponseXMLKey]) {
                dictionary = [self cleanObjectFrom:[xmlDictionary objectForKey:kChatResponseXMLKey]];
                
                //Ravi_Bukka: Added for Checkdataversion implementation
            }else if([[dictionary allKeys] containsObject:kCheckDataResponseXMLKey]){
                dictionary = [self cleanObjectFrom:[xmlDictionary objectForKey:kCheckDataResponseXMLKey]];
            }
        }
        else
        {
            // REquired by Bukka
            dictionary = [self cleanObjectFrom:[xmlDictionary objectForKey:kCheckDataResponseXMLKey]];
        }
        
        if (dictionary.count > 0) {
            [responseDictionary addEntriesFromDictionary:dictionary];
        }
        DLog(@"Response dictionary: %@", responseDictionary);
    } else {
        DLog(@"Error parsing response XML: %@", error);
        responseDictionary = nil;
    }
    return responseDictionary;  */
    
//Ravi_Bukka: Added new code for Chat and CheckDataVersion Response handling
    
    NSLog(@"responseString --::-- %@",responseString);
    responseString = [self stripXmlHeader:responseString];
    NSMutableDictionary* responseDictionary = [NSMutableDictionary dictionary];
    // Read response XML
    NSError* error = nil;
    NSDictionary* xmlDictionary = [XMLReader dictionaryForXMLString:responseString error:&error withEncoding:NSUTF8StringEncoding];
    if (error == nil) {
        DLog(@"Response XML dictionary: %@", xmlDictionary);
        NSDictionary* dictionary = [self cleanObjectFrom:[xmlDictionary objectForKey:kResponseXMLKey]];
        //Deepak Carpenter : Getting Chat(getAnswer method) Response which is having different key
        if ([dictionary allKeys]==NULL) {
            // dictionary = [self cleanObjectFrom:[xmlDictionary objectForKey:kChatResponseXMLKey]];
            
            NSLog(@"Keys are %@ Response is == %@",[dictionary allKeys],[self cleanObjectFrom:[xmlDictionary objectForKey:kChatResponseXMLKey]]);
            
            // Required by Bukka
            if ([[xmlDictionary allKeys] containsObject:kCheckDataResponseXMLKey])
                dictionary = [self cleanObjectFrom:[xmlDictionary objectForKey:kCheckDataResponseXMLKey]];
            
            if ([[xmlDictionary allKeys] containsObject:kChatResponseXMLKey])
                dictionary = [self cleanObjectFrom:[xmlDictionary objectForKey:kChatResponseXMLKey]];
            
//Ravi_Bukka: CR#2: Patient Difference and Local Notification
            if ([[xmlDictionary allKeys] containsObject:kPatientInfoResult])
                dictionary = [self cleanObjectFrom:[xmlDictionary objectForKey:kPatientInfoResult]];
        }
        
        
        if (dictionary.count > 0) {
            [responseDictionary addEntriesFromDictionary:dictionary];
        }
        DLog(@"Response dictionary: %@", responseDictionary);
    } else {
        DLog(@"Error parsing response XML: %@", error);
        responseDictionary = nil;
    }
    return responseDictionary;
    
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    // Use when fetching text data
    NSString *responseString = [[[NSString alloc] initWithData:request.responseData encoding:NSUTF8StringEncoding] autorelease];
    if (responseString == nil) {
        responseString = [[[NSString alloc] initWithData:request.responseData encoding:NSISOLatin1StringEncoding] autorelease];
    }
    DLog(@"Request headers: %@", [request requestHeaders]);
    DLog(@"Response string: %@", responseString);
    NSDictionary* responseDictionary = nil;
    // If response String is empty, send response headers as result
    if (responseString.length > 0) {
        responseDictionary = [self responseDictionaryWithXMLString:responseString];
    }
    // Notify delegates
    [self notifyEndDelegates];
    // If response dictionary is nil, notify error
    if (responseDictionary != nil) {
        
//Ravi_Bukka: CR#2: Patient Difference Local Notification
        if ([self.name isEqualToString:@"insertPatient"]) {
            
            NSString *serverPatientCount = [responseDictionary objectForKey:kPatientCountFromServer];
            int patientCountFromServer = [serverPatientCount intValue];
            [[NSUserDefaults standardUserDefaults] setInteger:patientCountFromServer forKey:@"patientCountFromServer"];
            
            if ([self connected]){
                [(AppDelegate *)[[UIApplication sharedApplication] delegate] patientDifference:patientCountFromServer];
            }
            
            
            
        }
        
        
        // Check if request returns error code
        NSInteger errorCode = [[responseDictionary objectForKey:kResponseErrorKey] integerValue];
        if (errorCode == kResponseErrorOKCode) {
            // Call onComplete block
            _onComplete(responseDictionary);
        } else {
            NSString* errorCodeString = [NSString stringWithFormat:@"%d", errorCode];
            NSString* errorDescription = NSLocalizedStringFromTable(errorCodeString, @"ErrorMessages", nil);
            NSMutableDictionary* userInfo = [NSMutableDictionary dictionary];
            [userInfo setObject:errorDescription forKey:NSLocalizedDescriptionKey];
            [userInfo setObject:[NSNumber numberWithInt:errorCode] forKey:kRequestErrorCodeKey];
            NSError* error = [NSError errorWithDomain:kBaseRequestErrorDomain code:errorCode userInfo:userInfo];
            // Call onError block
            //Deepak_Carpenter : Added to resolve invalid session token issue (Causing blank screen)
            if (errorCode==2)
            {
                if ([[NSUserDefaults standardUserDefaults]boolForKey:@"invalidSession"]==YES) {
                    UIAlertView *logoutAlert=[[UIAlertView alloc]initWithTitle:@"Su sesión ha expirado" message:@"Sesión ha finalizado, por favor ingrese a cabo" delegate:self cancelButtonTitle:@"Salir" otherButtonTitles:nil];
                    [logoutAlert show];
                    [logoutAlert release];
                    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"invalidSession"];
                }
            }

            _onError(error);
        }
    } else {
        NSError* error = [NSError errorWithDomain:kBaseRequestErrorDomain code:kOperationErrorCode userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"SERVICE_RESPONSE_ERROR", nil) forKey:NSLocalizedDescriptionKey]];
        // Call onError block
        _onError(error);
    }
}
//Deepak_Carpenter: added to handle alert view action
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        //        AppDelegate *_Delegate=[[AppDelegate alloc]init];
        //        [_Delegate logout];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LogoutNotification" object:self];
    }
}


//Ravi_Bukka: CR#2: Patient Difference Local Notification
- (void)requestFailed:(ASIHTTPRequest *)request {
    
    if (![self connected]) {
        // Internet not connected
        
        NSError* error = [NSError errorWithDomain:kBaseRequestNetworkErrorDomain code:kOperationErrorCode userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"CONNECTION_FAILURE_ERROR", nil) forKey:NSLocalizedDescriptionKey]];
        // Notify delegates
        [self notifyEndDelegates];
        _onError(error);
    } else {
        // Internet connected
        [request setNumberOfTimesToRetryOnTimeout:2];
        
    }
    
}


-(void)addParam:(NSString*)paramValue forKey:(NSString*)paramKey {
    NSString* paramString = paramValue != nil ? [paramValue gtm_stringBySanitizingAndEscapingForXML]  : @"";
    
    [self.contentParams addObject:[RequestParam paramWithKey:paramKey andValue:paramString]];
}

-(void)addParam:(NSString*)paramValue forKey:(NSString*)paramKey defaultValue:(NSString*)defaultValue {
    NSString* value = paramValue;
    if (value.length == 0) {
        value = defaultValue;
    }
    [self addParam:value forKey:paramKey];
}

-(void)addNumberParam:(NSNumber*)paramValue forKey:(NSString*)paramKey {
    NSString* stringValue = nil;
    if (paramValue != nil) {
        stringValue = [NSString stringWithFormat:@"%i", paramValue.integerValue];
    }
    [self addParam:stringValue forKey:paramKey];
}

-(void)addNumberParam:(NSNumber*)paramValue forKey:(NSString*)paramKey defaultValue:(NSString*)defaultValue {
    NSString* stringValue = nil;
    if (paramValue != nil) {
        stringValue = [NSString stringWithFormat:@"%i", paramValue.integerValue];
    }
    [self addParam:stringValue forKey:paramKey defaultValue:defaultValue];
}

@end
