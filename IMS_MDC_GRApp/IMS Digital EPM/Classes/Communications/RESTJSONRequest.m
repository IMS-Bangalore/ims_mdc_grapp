//
//  RESTJSONRequest.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 13/12/12.
//
//

#import "RESTJSONRequest.h"
#import "CommunicationStrings.h"

@interface RESTJSONRequest ()

@property (strong, readonly) CommunicationStrings *communicationStrings;
@property (strong, nonatomic) NSString* appId;
@property (strong, nonatomic) NSString* baseResource;

@end

#pragma mark - Static values

//Ravi_Bukka: Changed for APNS
//static NSString* kServerURLKey = @" http://native-platform-admin-stg.imshealth.com/api/messaging/RegisterDevice";
static NSString* kServerURLKey = @"http://native-platform-admin.imshealth.com/api/Messaging/RegisterDevice";
//static NSString* kBaseResourceName = @"apps";

//#ifdef STAGING
//static NSString* kAppID = @"5";
//#else
//static NSString* kAppID = @"2";
//#endif

@implementation RESTJSONRequest

- (id)init {
    self = [super init];
    if (self) {
//  self.baseServerUrl = [NSString stringWithFormat:@"%@/%@/%@/", kServerURLKey, kBaseResourceName, kAppID];

        self.baseServerUrl = [NSString stringWithFormat:@"%@", kServerURLKey];
        DLog(@"URL: %@", self.baseServerUrl);
    }
    return self;
}

-(NSString *)createSegmentParamsString {
    NSMutableArray* params = [NSMutableArray arrayWithArray:self.segmentParams];
    return [self stringForSegmentParams:params];
}

-(ASIHTTPRequest *)createAsiRequest {
    ASIHTTPRequest* request = [super createAsiRequest];
//    [request addRequestHeader:@"X-TwinPush-REST-API-Token" value:@"a4bd4206dc63742b3637796a91625ab7"];
//    [request addRequestHeader:@"User-Agent" value:@"ASIHTTPRequest"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    return request;
}

-(NSDictionary *)dictionaryForResponseString:(NSString *)string {
    NSDictionary* dictionary = [super dictionaryForResponseString:string];
    // If dictionary contains errors, call onError
    return dictionary;
    
}

@end
