//
//  CommunicationStrings.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 27/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

/** @brief Helper class to access to Communications Property list */
@interface CommunicationStrings : NSObject {
    // Dictionary of properties contained in property file
    NSDictionary* _propertyList;
}

/** @brief Returns the Property with given key contained in Communications Property list */
- (NSString*)stringWithKey:(NSString*) key;

/** @brief Returns the Property with given key contained in Communications Property list */
- (NSNumber*)numberWithKey:(NSString*) key;

@end
