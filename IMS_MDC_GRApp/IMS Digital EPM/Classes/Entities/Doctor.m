//
//  Doctor.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 08/05/14.
//
//

#import "Doctor.h"
#import "DoctorInfo.h"
#import "Gender.h"
#import "MedicalCenter.h"
#import "Province.h"
#import "Specialty.h"
#import "University.h"


@implementation Doctor

@dynamic age;
@dynamic graduationYear;
@dynamic passwordHash;
@dynamic userID;
@dynamic doctorInfo;
@dynamic gender;
@dynamic mainSpeciality;
@dynamic medicalCenter;
@dynamic province;
@dynamic secondarySpecialty;
@dynamic university;

@end
