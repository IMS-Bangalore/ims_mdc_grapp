//
//  Doctor.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 08/05/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DoctorInfo, Gender, MedicalCenter, Province, Specialty, University;

@interface Doctor : NSManagedObject

@property (nonatomic, retain) NSNumber * age;
@property (nonatomic, retain) NSString * graduationYear;
@property (nonatomic, retain) NSString * passwordHash;
@property (nonatomic, retain) NSString * userID;
@property (nonatomic, retain) DoctorInfo *doctorInfo;
@property (nonatomic, retain) Gender *gender;
@property (nonatomic, retain) Specialty *mainSpeciality;
@property (nonatomic, retain) MedicalCenter *medicalCenter;
@property (nonatomic, retain) Province *province;
@property (nonatomic, retain) Specialty *secondarySpecialty;
@property (nonatomic, retain) University *university;

@end
