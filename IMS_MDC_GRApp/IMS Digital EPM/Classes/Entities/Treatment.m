//
//  Treatment.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 19/08/14.
//
//

#import "Treatment.h"
#import "Diagnosis.h"
#import "Dosage.h"
#import "DrugReimbursement.h"
#import "Effect.h"
#import "Medicament.h"
#import "OtherSpecialty.h"
#import "PatientInsurance.h"
#import "Presentation.h"
#import "TherapyElection.h"
#import "TherapyReplacementReason.h"
#import "TherapyType.h"


@implementation Treatment

@dynamic index;
@dynamic userDesiredEffect;
@dynamic userDrugReimbursement;
@dynamic userMedicament;
@dynamic userPatientInsurance;
@dynamic userPresentation;
@dynamic userReplacingMedicament;
@dynamic desiredEffect;
@dynamic diagnosis;
@dynamic dosage;
@dynamic drugReimbursement;
@dynamic medicament;
@dynamic patientInsurance;
@dynamic presentation;
@dynamic recommendationSpecialist;
@dynamic replaceReason;
@dynamic replacingMedicament;
@dynamic therapyChoiceReason;
@dynamic therapyElection;

@end
