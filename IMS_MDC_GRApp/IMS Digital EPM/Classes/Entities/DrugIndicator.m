//
//  DrugIndicator.m
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 11/02/14.
//
//

#import "DrugIndicator.h"
#import "Diagnosis.h"


@implementation DrugIndicator

@dynamic deleted;
@dynamic identifier;
@dynamic name;
@dynamic diagnostics;

@end
