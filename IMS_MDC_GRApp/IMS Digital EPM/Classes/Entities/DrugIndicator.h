//
//  DrugIndicator.h
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 11/02/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Diagnosis;

@interface DrugIndicator : NSManagedObject

@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *diagnostics;
@end

@interface DrugIndicator (CoreDataGeneratedAccessors)

- (void)addDiagnosticsObject:(Diagnosis *)value;
- (void)removeDiagnosticsObject:(Diagnosis *)value;
- (void)addDiagnostics:(NSSet *)values;
- (void)removeDiagnostics:(NSSet *)values;

@end
