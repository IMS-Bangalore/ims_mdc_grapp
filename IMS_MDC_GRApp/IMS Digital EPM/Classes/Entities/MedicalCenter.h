//
//  MedicalCenter.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 21/02/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Doctor, DoctorInfo;

@interface MedicalCenter : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * userDefined;
@property (nonatomic, retain) NSSet *doctorInfos;
@property (nonatomic, retain) NSSet *doctors;
@end

@interface MedicalCenter (CoreDataGeneratedAccessors)

- (void)addDoctorInfosObject:(DoctorInfo *)value;
- (void)removeDoctorInfosObject:(DoctorInfo *)value;
- (void)addDoctorInfos:(NSSet *)values;
- (void)removeDoctorInfos:(NSSet *)values;

- (void)addDoctorsObject:(Doctor *)value;
- (void)removeDoctorsObject:(Doctor *)value;
- (void)addDoctors:(NSSet *)values;
- (void)removeDoctors:(NSSet *)values;

@end
