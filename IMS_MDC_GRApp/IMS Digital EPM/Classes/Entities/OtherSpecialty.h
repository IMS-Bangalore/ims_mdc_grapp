//
//  OtherSpecialty.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 12/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Treatment;

@interface OtherSpecialty : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *treatments;
@end

@interface OtherSpecialty (CoreDataGeneratedAccessors)

- (void)addTreatmentsObject:(Treatment *)value;
- (void)removeTreatmentsObject:(Treatment *)value;
- (void)addTreatments:(NSSet *)values;
- (void)removeTreatments:(NSSet *)values;
@end
