//
//  OtherSpecialty.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 12/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "OtherSpecialty.h"
#import "Treatment.h"


@implementation OtherSpecialty

@dynamic identifier;
@dynamic name;
@dynamic treatments;

@end
