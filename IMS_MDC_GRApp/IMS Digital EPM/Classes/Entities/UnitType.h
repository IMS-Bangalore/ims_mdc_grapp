//
//  UnitType.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 09/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Presentation;

@interface UnitType : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSSet *presentations;
@end

@interface UnitType (CoreDataGeneratedAccessors)

- (void)addPresentationsObject:(Presentation *)value;
- (void)removePresentationsObject:(Presentation *)value;
- (void)addPresentations:(NSSet *)values;
- (void)removePresentations:(NSSet *)values;
@end
