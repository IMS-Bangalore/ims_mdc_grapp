//
//  Dosage.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 09/05/13.
//
//

#import "Dosage.h"
#import "DurationType.h"
#import "Frequency.h"
#import "Treatment.h"


@implementation Dosage

@dynamic comments;
@dynamic duration;
@dynamic longDurationTreatment;
@dynamic onDemand;
@dynamic quantity;
@dynamic recipeCount;
@dynamic units;
@dynamic uniqueDose;
@dynamic durationType;
@dynamic frequency;
@dynamic treatment;

@end
