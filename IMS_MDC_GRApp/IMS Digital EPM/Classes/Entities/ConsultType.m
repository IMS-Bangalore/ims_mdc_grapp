//
//  ConsultType.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "ConsultType.h"
#import "Patient.h"


@implementation ConsultType

@dynamic name;
@dynamic identifier;
@dynamic patients;

@end
