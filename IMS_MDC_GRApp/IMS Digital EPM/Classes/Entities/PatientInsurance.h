//
//  PatientInsurance.h
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 11/02/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Treatment;

@interface PatientInsurance : NSManagedObject

@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *treatment;
@end

@interface PatientInsurance (CoreDataGeneratedAccessors)

- (void)addTreatmentObject:(Treatment *)value;
- (void)removeTreatmentObject:(Treatment *)value;
- (void)addTreatment:(NSSet *)values;
- (void)removeTreatment:(NSSet *)values;

@end
