//
//  Dosage.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 09/05/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DurationType, Frequency, Treatment;

@interface Dosage : NSManagedObject

@property (nonatomic, retain) NSString * comments;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) NSNumber * longDurationTreatment;
@property (nonatomic, retain) NSNumber * onDemand;
@property (nonatomic, retain) NSNumber * quantity;
@property (nonatomic, retain) NSNumber * recipeCount;
@property (nonatomic, retain) NSNumber * units;
@property (nonatomic, retain) NSNumber * uniqueDose;
@property (nonatomic, retain) DurationType *durationType;
@property (nonatomic, retain) Frequency *frequency;
@property (nonatomic, retain) Treatment *treatment;

@end
