//
//  Smoker.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 10/02/14.
//
//

#import "Smoker.h"
#import "Patient.h"


@implementation Smoker

@dynamic identifier;
@dynamic name;
@dynamic deleted;
@dynamic patients;

@end
