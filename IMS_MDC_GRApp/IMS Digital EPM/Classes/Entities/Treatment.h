//
//  Treatment.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 19/08/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Synchronizable.h"

@class Diagnosis, Dosage, DrugReimbursement, Effect, Medicament, OtherSpecialty, PatientInsurance, Presentation, TherapyElection, TherapyReplacementReason, TherapyType,TherapyElection;

@interface Treatment : Synchronizable

@property (nonatomic, retain) NSNumber * index;
@property (nonatomic, retain) NSString * userDesiredEffect;
@property (nonatomic, retain) NSString * userDrugReimbursement;
@property (nonatomic, retain) NSString * userMedicament;
@property (nonatomic, retain) NSString * userPatientInsurance;
@property (nonatomic, retain) NSString * userPresentation;
@property (nonatomic, retain) NSString * userReplacingMedicament;
@property (nonatomic, retain) Effect *desiredEffect;
@property (nonatomic, retain) Diagnosis *diagnosis;
@property (nonatomic, retain) Dosage *dosage;
@property (nonatomic, retain) DrugReimbursement *drugReimbursement;
@property (nonatomic, retain) Medicament *medicament;
@property (nonatomic, retain) PatientInsurance *patientInsurance;
@property (nonatomic, retain) Presentation *presentation;
@property (nonatomic, retain) OtherSpecialty *recommendationSpecialist;
@property (nonatomic, retain) TherapyReplacementReason *replaceReason;
@property (nonatomic, retain) Medicament *replacingMedicament;
@property (nonatomic, retain) TherapyType *therapyChoiceReason;
@property (nonatomic, retain) TherapyElection *therapyElection;

@end
