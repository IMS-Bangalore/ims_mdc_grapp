//
//  DiagnosisType.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 18/12/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Diagnosis;

@interface DiagnosisType : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSSet *diagnostic;
@end

@interface DiagnosisType (CoreDataGeneratedAccessors)

- (void)addDiagnosticObject:(Diagnosis *)value;
- (void)removeDiagnosticObject:(Diagnosis *)value;
- (void)addDiagnostic:(NSSet *)values;
- (void)removeDiagnostic:(NSSet *)values;
@end
