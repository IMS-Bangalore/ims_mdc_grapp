//
//  ProductType.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Medicament;

@interface ProductType : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSSet *medicaments;
@end

@interface ProductType (CoreDataGeneratedAccessors)

- (void)addMedicamentsObject:(Medicament *)value;
- (void)removeMedicamentsObject:(Medicament *)value;
- (void)addMedicaments:(NSSet *)values;
- (void)removeMedicaments:(NSSet *)values;
@end
