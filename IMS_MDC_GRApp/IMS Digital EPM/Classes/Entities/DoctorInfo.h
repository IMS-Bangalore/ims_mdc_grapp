//
//  DoctorInfo.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 22/05/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Synchronizable.h"

@class Doctor, MedicalCenter, Patient, RecentMedicament;

@interface DoctorInfo : Synchronizable

@property (nonatomic, retain) NSNumber * averageWeeklyPatients;
@property (nonatomic, retain) NSString * comments;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSDate * firstCollaborationDate;
@property (nonatomic, retain) NSDate * firstCollaborationDateEnd;
@property (nonatomic, retain) NSNumber * medicalCenterID;
@property (nonatomic, retain) NSString * medicalCenterName;
@property (nonatomic, retain) NSString * otherMedicalCenter;
@property (nonatomic, retain) NSDate * secondCollaborationDate;
@property (nonatomic, retain) NSDate * secondCollaborationDateEnd;
@property (nonatomic, retain) NSNumber * weeklyActivityHours;
@property (nonatomic, retain) Doctor *doctor;
@property (nonatomic, retain) NSSet *medicalCenters;
@property (nonatomic, retain) NSSet *patients;
@property (nonatomic, retain) NSSet *recentMedicaments;
@end

@interface DoctorInfo (CoreDataGeneratedAccessors)

- (void)addMedicalCentersObject:(MedicalCenter *)value;
- (void)removeMedicalCentersObject:(MedicalCenter *)value;
- (void)addMedicalCenters:(NSSet *)values;
- (void)removeMedicalCenters:(NSSet *)values;

- (void)addPatientsObject:(Patient *)value;
- (void)removePatientsObject:(Patient *)value;
- (void)addPatients:(NSSet *)values;
- (void)removePatients:(NSSet *)values;

- (void)addRecentMedicamentsObject:(RecentMedicament *)value;
- (void)removeRecentMedicamentsObject:(RecentMedicament *)value;
- (void)addRecentMedicaments:(NSSet *)values;
- (void)removeRecentMedicaments:(NSSet *)values;

@end
