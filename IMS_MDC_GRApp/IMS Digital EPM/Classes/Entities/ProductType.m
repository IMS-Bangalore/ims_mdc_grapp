//
//  ProductType.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "ProductType.h"
#import "Medicament.h"


@implementation ProductType

@dynamic name;
@dynamic identifier;
@dynamic medicaments;

@end
