//
//  TherapyMedicament.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 11/03/14.
//
//

#import "TherapyMedicament.h"
#import "Treatment.h"


@implementation TherapyMedicament

@dynamic identifier;
@dynamic name;
@dynamic treatmentMedicament;

@end
