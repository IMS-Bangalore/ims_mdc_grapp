//
//  AgeType.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 08/05/14.
//
//

#import "AgeType.h"
#import "Age.h"


@implementation AgeType

@dynamic identifier;
@dynamic name;
@dynamic ages;

@end
