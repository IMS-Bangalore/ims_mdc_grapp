//
//  Effect.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 18/12/12.
//
//

#import "Effect.h"
#import "Treatment.h"


@implementation Effect

@dynamic identifier;
@dynamic name;
@dynamic deleted;
@dynamic treatments;

@end
