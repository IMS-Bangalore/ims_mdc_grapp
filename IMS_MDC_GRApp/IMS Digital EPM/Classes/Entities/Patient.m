//
//  Patient.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 21/03/14.
//
//

#import "Patient.h"
#import "Age.h"
#import "ConsultType.h"
#import "Diagnosis.h"
#import "DoctorInfo.h"
#import "Gender.h"
#import "Insurance.h"
#import "PlaceOfVisit.h"
#import "Recurrence.h"
#import "SickFund.h"
#import "Smoker.h"


@implementation Patient

@dynamic index;
@dynamic recurranceValue;
@dynamic sickFundValue;
@dynamic userInsurance;
@dynamic userPlaceOfVisit;
@dynamic userSickFund;
@dynamic visitDate;
@dynamic otherSickFundValue;
@dynamic age;
@dynamic consultType;
@dynamic diagnostic;
@dynamic doctorInfo;
@dynamic gender;
@dynamic insurance;
@dynamic placeOfVisit;
@dynamic recurrence;
@dynamic sickFund;
@dynamic smoker;

@end
