//
//  StatsEvent.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 09/01/13.
//
//

#import "StatsEvent.h"


@implementation StatsEvent

@dynamic counter;
@dynamic creationDate;
@dynamic endDate;
@dynamic event;
@dynamic sent;
@dynamic sentAtDate;
@dynamic startDate;
@dynamic timeElapsed;
@dynamic patientId;

@end
