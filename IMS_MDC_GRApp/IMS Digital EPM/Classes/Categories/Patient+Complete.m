//
//  Patient+Complete.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 15/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Patient+Complete.h"
#import "Diagnosis+Complete.h"
#import "Age.h"

@implementation Patient (Complete)
- (BOOL)isComplete {
    BOOL isComplete = YES;
    
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"])
    {
    isComplete = isComplete && self.visitDate != nil;
    isComplete = isComplete && self.age.value.intValue > 0 && self.age.ageType != nil;
  //  isComplete = isComplete && self.consultType != nil;
    isComplete = isComplete && self.gender != nil;
    
    //Kanchan
    isComplete=isComplete && (self.insurance != nil || self.userInsurance.length > 0);
    
    isComplete = isComplete && self.diagnostic.count > 0;
    for (Diagnosis* diagnosis in self.diagnostic) {
        isComplete = isComplete && [diagnosis isComplete];
    }
        isComplete = isComplete;
    }
    else{
                
        isComplete = isComplete && self.visitDate != nil;
        isComplete = isComplete && self.age.value.intValue > 0 && self.age.ageType != nil;
        isComplete = isComplete && self.consultType != nil;
        isComplete = isComplete && self.gender != nil;
        
        //Deepak_Carpenter: Added for place of visit
        isComplete=isComplete && (self.placeOfVisit != nil || self.userPlaceOfVisit.length > 0);
        isComplete = isComplete && self.diagnostic.count > 0;
        for (Diagnosis* diagnosis in self.diagnostic) {
            isComplete = isComplete && [diagnosis isComplete];
        }

        isComplete = isComplete;
    }
    
    return isComplete;
}

- (BOOL)isEmpty {
    BOOL isEmpty = YES;
    
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"])
    {
    isEmpty = isEmpty && self.visitDate == nil;
    isEmpty = isEmpty && self.age.value.intValue == 0;
    isEmpty = isEmpty && self.consultType == nil;
    isEmpty = isEmpty && self.gender == nil;
    //Kanchan
    isEmpty =isEmpty && (self.insurance ==nil && self.userInsurance.length == 0);

    isEmpty = isEmpty && self.diagnostic.count <= 1;
    for (Diagnosis* diagnosis in self.diagnostic) {
        isEmpty = isEmpty && [diagnosis isEmpty];
    }
    }
    
    else
    {
        isEmpty = isEmpty && self.visitDate == nil;
        isEmpty = isEmpty && self.age.value.intValue == 0;
        isEmpty = isEmpty && self.consultType == nil;
        isEmpty = isEmpty && self.gender == nil;
        isEmpty = isEmpty && self.diagnostic.count <= 1;
        for (Diagnosis* diagnosis in self.diagnostic) {
            isEmpty = isEmpty && [diagnosis isEmpty];
        }

    }
    return isEmpty;
}

@end
