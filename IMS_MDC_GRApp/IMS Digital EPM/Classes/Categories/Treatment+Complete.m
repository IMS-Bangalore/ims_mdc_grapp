//
//  Treatment+Ready.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 15/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Treatment+Complete.h"
#import "Dosage+Complete.h"

@implementation Treatment (Complete)

- (BOOL)isComplete {
    BOOL isComplete = YES;
   if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {
 //   isComplete = isComplete && (self.desiredEffect != nil || self.userDesiredEffect.length > 0);
    isComplete = isComplete && self.dosage != nil && [self.dosage isComplete];
    isComplete = isComplete && (self.presentation != nil || self.userPresentation.length > 0);
    isComplete = isComplete && (self.medicament != nil || self.userMedicament.length > 0);
    isComplete = isComplete && self.therapyChoiceReason != nil && self.therapyElection != nil;
    
    //Kanchan
    isComplete = isComplete && (self.drugReimbursement != nil || self.userDrugReimbursement.length > 0);
    isComplete = isComplete && (self.patientInsurance != nil || self.userPatientInsurance.length > 0);
   }
   else{
       isComplete = isComplete && (self.desiredEffect != nil || self.userDesiredEffect.length > 0);
       isComplete = isComplete && self.dosage != nil && [self.dosage isComplete];
       isComplete = isComplete && (self.presentation != nil || self.userPresentation.length > 0);
       isComplete = isComplete && (self.medicament != nil || self.userMedicament.length > 0);
       isComplete = isComplete && self.therapyChoiceReason != nil && self.therapyElection != nil;
   }
    
    return isComplete;
}

- (BOOL)isEmpty {
    BOOL isEmpty = YES;
    
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) 
    {
        isEmpty = isEmpty && self.userMedicament.length == 0 && self.medicament == nil;

        //Kanchan
        
        isEmpty = isEmpty && self.userDrugReimbursement.length == 0 && self.userMedicament.length == 0;
        isEmpty = isEmpty && self.drugReimbursement == nil && self.medicament == nil;
        
        isEmpty = isEmpty && self.userPresentation.length == 0 && self.userDrugReimbursement.length == 0;
        isEmpty = isEmpty && self.presentation == nil && self.drugReimbursement == nil;
        
        isEmpty = isEmpty && self.userPatientInsurance.length == 0 && self.userPresentation.length == 0;
        isEmpty = isEmpty && self.patientInsurance == nil && self.presentation == nil;
        
        isEmpty = isEmpty && self.recommendationSpecialist == nil;
        isEmpty = isEmpty && self.replaceReason == nil && self.replacingMedicament == nil;
        isEmpty = isEmpty && self.therapyChoiceReason == nil && self.therapyElection == nil;

    }
    
    isEmpty = isEmpty && self.userMedicament.length == 0 && self.medicament == nil;
    isEmpty = isEmpty && self.userDesiredEffect.length == 0 && self.userPresentation.length == 0;
    isEmpty = isEmpty && self.desiredEffect == nil && self.presentation == nil;
    isEmpty = isEmpty && self.recommendationSpecialist == nil;
    isEmpty = isEmpty && self.replaceReason == nil && self.replacingMedicament == nil;
    isEmpty = isEmpty && self.therapyChoiceReason == nil && self.therapyElection == nil;
    
    return isEmpty;
}

@end
