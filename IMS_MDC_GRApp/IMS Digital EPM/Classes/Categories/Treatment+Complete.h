//
//  Treatment+Ready.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 15/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Treatment.h"

@interface Treatment (Complete)
/** @brief returns true if all the required fields of the treatment are complete */
- (BOOL)isComplete;
/** @brief returns true if all the required fields of the treatment are empty */
- (BOOL)isEmpty;
@end
