//
//  Dosage+Complete.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 15/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Dosage.h"

extern NSString* const kUniqueDoseIdentifier;

@interface Dosage (Complete)
/** @brief returns true if all the required fields of the dosage are complete */
- (BOOL)isComplete;
@end
