//
//  UIView+NIBLoader.m
//  YUM
//
//  Created by Guillermo Gutiérrez on 28/12/11.
//  Copyright (c) 2011 TwinCoders. All rights reserved.
//

#import "UIView+NIBLoader.h"

@implementation UIView (NIBLoader)

+ (id)loadFromNIB:(NSString *)nibName {
    id view = nil;
    // If no nib name is provided, use the classname as nib name
    if (nibName == nil) {
        nibName = NSStringFromClass([self class]);
    }
    NSArray* elements = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id element in elements) {
        if ([element isKindOfClass:[self class]]) {
            view = element;
            break;
        }
    }
    return view;
}

+ (id)loadFromDefaultNIB {
    return [self loadFromNIB:nil];
}

@end
