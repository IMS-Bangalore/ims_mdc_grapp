//
//  Dosage+Complete.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 15/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Dosage+Complete.h"
#import "Frequency.h"

NSString* const kUniqueDoseIdentifier = @"8";

@implementation Dosage (Complete)
- (BOOL)isComplete {
    BOOL isComplete = YES;
    if (!self.onDemand.boolValue && !self.uniqueDose.boolValue) {
        if (![self.frequency.identifier isEqualToString:kUniqueDoseIdentifier]) {
            isComplete = isComplete && self.quantity.floatValue > 0&& self.quantity!=NULL;
            
            isComplete = isComplete && self.units.floatValue > 0;
        }
        isComplete = isComplete && self.frequency != nil;
        
        if (!self.longDurationTreatment.boolValue) {
            isComplete = isComplete && self.duration.floatValue > 0;
            isComplete = isComplete && self.durationType != nil;
        }
    }
    //Deepak_Carpenter : Added for on demand checkbox ( If checkbox selected next pationt button should not be enable)
    isComplete = isComplete && self.quantity.floatValue > 0&& self.quantity!=NULL;
    isComplete = isComplete && self.recipeCount.intValue > 0;
    
    return isComplete;
}
@end
