//
//  UpdateDataManager.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 30/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DoctorInfo;

@interface UpdateDataManager : NSObject

- (void)updateWithDoctorInfo:(DoctorInfo*)doctorInfo andOffset:(NSString*)offset;



@end
