//
//  UpdateDataManager.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 30/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "UpdateDataManager.h"
#import "RequestFactory.h"
#import "EntityFactory.h"
#import "DiagnosisType.h"
#import "Effect.h"
#import "Presentation.h"
#import "Medicament.h"
#import "DiagnosisTypeSync.h"
#import "MedicamentSync.h"
#import "PresentationSync.h"
#import "EffectsSync.h"
#import "UnitType.h"
#import "UnitTypeSync.h"
#import "AgeType.h"
#import "AgeSync.h"
#import "ConsultType.h"
#import "ConsultTypeSync.h"
#import "CentreListSync.h"
#import "MedicalCenter.h"
#import "Frequency.h"
#import "FrequencySync.h"
#import "DurationType.h"
#import "DurationTypeSync.h"
#import "Gender.h"
#import "GenderSync.h"
#import "Insurance.h"
#import "InsuranceSync.h"
#import "Pathology.h"
#import "PathologyTypeSync.h"
#import "PlaceOfVisit.h"
#import "PlaceOfVisitSync.h"
#import "Province.h"
#import "ProvinceSync.h"
#import "Recurrence.h"
#import "RecurrenceSync.h"
#import "Smoker.h"
#import "SmokerSync.h"
#import "SpecialitiesSync.h"
#import "Specialty.h"
#import "TherapyElection.h"
#import "TherapyElectionSync.h"
#import "TherapyType.h"
#import "TherapyTypeSync.h"
#import "University.h"
#import "UniversitiesSync.h"
#import "OtherSpecialty.h"
#import "OtherSpecialitiesSync.h"

static NSString* const kOffsetInitValue = @"0";

#pragma mark Private methods declaration
@interface UpdateDataManager()

@property (nonatomic, retain) BaseRequest *updateRequest;
@property (nonatomic, readonly) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, readonly) EntityFactory *entityFactory;
@property (nonatomic, retain) MetaInfo *metaInfo;
@property (nonatomic, retain) NSOperationQueue* operationQueue;
@property (nonatomic, getter = isUpdating) BOOL updating;

-(void) notifyUpdateError:(NSError*)error;
-(void) updateMedicamentArray:(NSArray*)medicamentArray;
-(void) removePresentationsInArray:(NSArray*)presentationDeleteArray;
-(void) updateDiagnosisArray:(NSArray*)diagnosisArray;
-(void) removeDiagnosisInArray:(NSArray*)diagnosisRemoveArray;
-(void) refreshUpdateDate;

@end

#pragma mark Implementation
@implementation UpdateDataManager

@synthesize updateRequest = _updateRequest;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize entityFactory = _entityFactory;
@synthesize metaInfo = _metaInfo;
@synthesize operationQueue = _operationQueue;
@synthesize updating = _updating;

- (id)init {
    self = [super init];
    if (self) {
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = 1;
        _entityFactory = [EntityFactory sharedEntityFactory];
        _updating = NO;
    }
    return self;
}

- (void)dealloc {
//    [_updateRequest release];
//    [_managedObjectContext release];
//    [_operationQueue cancelAllOperations];
//    [_operationQueue release];
//    [_metaInfo release];
//    [super dealloc];
}

- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext == nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        _managedObjectContext.persistentStoreCoordinator = _entityFactory.persistentStoreCoordinator;
    }
    return _managedObjectContext;
}

#pragma mark Update Action

-(NSDate*) yesterday {
    NSDate *now = [NSDate date];
    int daysToAdd = -1;
    
    // set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:daysToAdd];
    
    // create a calendar
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDate *yesterday = [gregorian dateByAddingComponents:components toDate:now options:0];
    DLog(@"Yesterday: %@", yesterday);
//    [gregorian release];
    return yesterday;
}

//Ravi_Bukka: Added for CheckDataVersion Implementation
-(void) updateWithDoctorInfo:(DoctorInfo*)doctorInfo andOffset:(NSString*)offset {
    // Avoid duplicated requests
    if (self.updating) {
        return;
    }
    
    [_operationQueue addOperationWithBlock:^{
        [self.updateRequest cancel];
        self.metaInfo = [_entityFactory getMetaInfoWithContext:self.managedObjectContext];
        DoctorInfo *doctorInfoInContext = [self.managedObjectContext existingObjectWithID:doctorInfo.objectID error:NULL];
        NSDate* lastUpdate = self.metaInfo.lastUpdate;
        // Check if last update was before yesterday
        //    if ([lastUpdate earlierDate:[self yesterday]] == lastUpdate) {
        self.updateRequest = [[RequestFactory sharedInstance] createUpdateDataRequestWithDoctorInfo:doctorInfoInContext andOffset:offset lastUpdate:lastUpdate onComplete:^(UpdateDataResponse *response, NSString* responseOffset) {
            [_operationQueue addOperationWithBlock:^{
                BOOL endRequest = [responseOffset isEqualToString:kOffsetInitValue];
                [self updateDataWithResponse:response mustSave:endRequest];
                if (!endRequest) {
                    [self updateWithDoctorInfo:doctorInfoInContext andOffset:responseOffset];
                }
                else {
                    self.updating = NO;
                }
            }];
            self.updateRequest = nil;
        } onError:^(NSError *error) {
            [self notifyUpdateError:error];
            self.updating = NO;
            self.updateRequest = nil;
        }];
        [self.updateRequest start];
        //    } else {
        //        DLog(@"Update not needed. Last update: %@", lastUpdate);
        //    }
    }];
}

-(void) updateDataWithResponse:(UpdateDataResponse*)updateResponse mustSave:(BOOL)save {
    [self updateMedicamentArray:updateResponse.medicamentArray];
    [self updateDiagnosisArray:updateResponse.diagnosisArray];
    [self removePresentationsInArray:updateResponse.medicamentDeleteArray];
    [self removeDiagnosisInArray:updateResponse.diagnosisDeleteArray];
    [self updateEffectsArray:updateResponse.effectsArray];
    [self removeEffectsInArray:updateResponse.effectsDeleteArray];
//    [self updateAgeTypeArray:updateResponse.ageTypeArray];
//    [self removeAgeTypeInArray:updateResponse.ageTypeDeleteArray];
//    [self updateConsultTypeArray:updateResponse.consultTypeArray];
//    [self removeConsultTypeInArray:updateResponse.consultTypeArray];
//    [self updateCentreListArray:updateResponse.centreArray];
//    [self removeCentreListInArray:updateResponse.centreDeleteArray];
    [self updateDoseFrequencyArray:updateResponse.frequencyArray];
    [self removeDoseFrequencyInArray:updateResponse.frequencyDeleteArray];
    [self updateDurationArray:updateResponse.durationTypeArray];
    [self removeDurationInArray:updateResponse.durationTypeDeleteArray];
//    [self updateGenderArray:updateResponse.genderArray];
//    [self removeGenderInArray:updateResponse.genderDeleteArray];
//    [self updateInsuranceArray:updateResponse.insuranceArray];
//    [self removeInsuranceInArray:updateResponse.insuranceDeleteArray];
//    [self updatePathologyTypeArray:updateResponse.pathologyArray];
//    [self removePathologyTypeInArray:updateResponse.pathologyDeleteArray];
    [self updatePlaceOfVisitArray:updateResponse.placeOfVisitArray];
    [self removePlaceOfVisitInArray:updateResponse.placeOfVisitDeleteArray];
    [self updateProvinceArray:updateResponse.provinceArray];
    [self removeProvinceInArray:updateResponse.provinceDeleteArray];
    
    [self updateDosageUnitsArray:updateResponse.unitTypeArray];
    [self removeDosageUnitsInArray:updateResponse.unitTypeDeleteArray];
//    [self updateRecurranceArray:updateResponse.recurrenceArray];
//    [self removeRecurranceInArray:updateResponse.recurrenceArray];
//    [self updateSmokerArray:updateResponse.smokerArray];
//    [self removeSmokerInArray:updateResponse.smokerDeleteArray];
    [self updateSpecialityArray:updateResponse.specialitiesArray];
    [self removeSpecialityInArray:updateResponse.specialitiesDeleteArray];
    [self updateOtherSpecialityArray:updateResponse.otherSpecialitiesArray];
    [self removeOtherSpecialityInArray:updateResponse.otherSpecialitiesDeleteArray];
//    [self updateTherapyElectionArray:updateResponse.therapyElectionArray];
//    [self removeTherapyElectionInArray:updateResponse.therapyElectionDeleteArray];
//    [self updateTherapyTypeArray:updateResponse.therapyTypeArray];
//    [self removeTherapyTypeInArray:updateResponse.therapyTypeDeleteArray];
    [self updateUniversitiesArray:updateResponse.universitiesArray];
    [self removeUniversitiesInArray:updateResponse.universitiesDeleteArray];
    
    NSError* error = nil;
    if (save) {
        DLog(@"Saving update changes..");
        [self refreshUpdateDate];
        [_managedObjectContext save:&error];
    }
    if (error != nil) {
        [_managedObjectContext rollback];
        [self notifyUpdateError:error];
    }
}

-(void) notifyUpdateError:(NSError*)error {
    DLog(@"Update error: %@", error);
}

#pragma mark Update methods

-(void) updateMedicamentArray:(NSArray*)medicamentArray {
    DLog(@"Updating %d medicaments...", medicamentArray.count);
    NSLog(@"PageNumber %@",[[NSUserDefaults standardUserDefaults]stringForKey:@"pagenumber"]);
        
    for (MedicamentSync* medicamentSync in medicamentArray) {
        Medicament* medicament = [_entityFactory fetchEntity:NSStringFromClass([Medicament class]) usingIdentifier:medicamentSync.identifier withManagedObjectContext:_managedObjectContext];
        // If medicament is new, insert new instance
        if (medicament == nil) {
            medicament = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Medicament class]) inManagedObjectContext:_managedObjectContext];
            medicament.identifier = medicamentSync.identifier;
            DLog(@"Created medicament: %@", medicamentSync);
        } else {
            DLog(@"Updated medicament: %@", medicamentSync);
        }
        // Set medicament properties
        medicament.name = medicamentSync.name;
        medicament.deleted = [NSNumber numberWithBool:NO];
        // Check if product type update is needed
        if (![medicament.productType.identifier isEqualToString:medicamentSync.productTypeId]) {
            ProductType* productType = [_entityFactory fetchEntity:NSStringFromClass([ProductType class]) usingIdentifier:medicamentSync.productTypeId withManagedObjectContext:_managedObjectContext];
            medicament.productType = productType;
            DLog(@"Set product type to: %@", productType.identifier);
        }
        // Update presentations
        for (PresentationSync* presentationSync in medicamentSync.presentations) {
            Presentation* presentation = [_entityFactory fetchEntity:NSStringFromClass([Presentation class]) usingIdentifier:presentationSync.identifier withManagedObjectContext:_managedObjectContext];
            if (presentation == nil) {
                presentation = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Presentation class]) inManagedObjectContext:_managedObjectContext];
                presentation.identifier = presentationSync.identifier;
                DLog(@"Created presentation: %@", presentationSync);
            } else {
                DLog(@"Updated presentation: %@", presentationSync);
            }
            presentation.name = presentationSync.name;
            if (![presentation.unitType.identifier isEqualToString:presentationSync.dosageUnitId]) {
                presentation.unitType = [_entityFactory fetchEntity:NSStringFromClass([UnitType class]) usingIdentifier:presentationSync.dosageUnitId withManagedObjectContext:_managedObjectContext];
                if (presentation.unitType == nil) {
                    presentation.unitType = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([UnitType class]) inManagedObjectContext:_managedObjectContext];
                    presentation.unitType.identifier = presentationSync.dosageUnitId;
                }
                DLog(@"Updated unit type: %@", presentation.unitType.identifier);
            }
            presentation.medicament = medicament;
            presentation.deleted = [NSNumber numberWithBool:NO];
        }
    }
    
}

-(void) removePresentationsInArray:(NSArray*)presentationDeleteArray {
    DLog(@"Removing %d presentations...", presentationDeleteArray.count);
    for (NSString* identifier in presentationDeleteArray) {
        Presentation* presentation = [_entityFactory fetchEntity:NSStringFromClass([Presentation class]) usingIdentifier:identifier withManagedObjectContext:_managedObjectContext];
        if (presentation != nil) {
            Medicament* medicament = presentation.medicament;
            presentation.medicament = nil;
            DLog(@"Removed presentation with id: %@", identifier);
            presentation.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:presentation];
            if (medicament.presentations.count == 0) {
                DLog(@"Removed medicament: %@", medicament.name);
                medicament.deleted = [NSNumber numberWithBool:YES];
                // [_managedObjectContext deleteObject:medicament];
            }
        }
    }
}

-(void) updateDiagnosisArray:(NSArray*)diagnosisArray {
    DLog(@"Updating %d diagnoses...", diagnosisArray.count);
    for (DiagnosisTypeSync* diagnosisSync in diagnosisArray) {
        // Check if diagnosis already exists
        DiagnosisType* fetchedDiagnosis = [_entityFactory fetchEntity:NSStringFromClass([DiagnosisType class]) usingIdentifier:diagnosisSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedDiagnosis == nil) {
            // Insert instance in managed object context
            fetchedDiagnosis = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DiagnosisType class]) inManagedObjectContext:_managedObjectContext];
            fetchedDiagnosis.identifier = diagnosisSync.identifier;
            DLog(@"Created diagnosis: %@", diagnosisSync);
        } else {
            DLog(@"Updated diagnosis: %@", diagnosisSync);
        }
        // Update diagnosis name
        fetchedDiagnosis.name = diagnosisSync.name;
        fetchedDiagnosis.deleted = [NSNumber numberWithBool:NO];
    }
}

-(void) removeDiagnosisInArray:(NSArray*)diagnosisRemoveArray {
    DLog(@"Removing %d diagnoses...", diagnosisRemoveArray.count);
    for (NSString* diagnosisId in diagnosisRemoveArray) {
        DiagnosisType* fetchedDiagnosis = [_entityFactory fetchEntity:NSStringFromClass([DiagnosisType class]) usingIdentifier:diagnosisId withManagedObjectContext:_managedObjectContext];
        if (fetchedDiagnosis != nil) {
            DLog(@"Removed diagnosis with id: %@", diagnosisId);
            fetchedDiagnosis.deleted = [NSNumber numberWithBool:YES];
         // [_managedObjectContext deleteObject:fetchedDiagnosis];
        
        }
    }
}

-(void) updateEffectsArray:(NSArray*)effectsArray {
    DLog(@"Updating %d effects...", effectsArray.count);
    for (EffectsSync* effectSync in effectsArray) {
        // Check if diagnosis already exists
        Effect* fetchedEffect = [_entityFactory fetchEntity:NSStringFromClass([Effect class]) usingIdentifier:effectSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedEffect == nil) {
            // Insert instance in managed object context
            fetchedEffect = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Effect class]) inManagedObjectContext:_managedObjectContext];
            fetchedEffect.identifier = effectSync.identifier;
            DLog(@"Created effect: %@", effectSync);
        } else {
            DLog(@"Updated effect: %@", effectSync);
        }
        // Update effect description
        fetchedEffect.name = effectSync.name;
        fetchedEffect.deleted = [NSNumber numberWithBool:NO];
    }
}

-(void) removeEffectsInArray:(NSArray*)effectsRemoveArray {
    DLog(@"Removing %d effects...", effectsRemoveArray.count);
    for (NSString* effectId in effectsRemoveArray) {
        Effect* fetchedEffect = [_entityFactory fetchEntity:NSStringFromClass([Effect class]) usingIdentifier:effectId withManagedObjectContext:_managedObjectContext];
        if (fetchedEffect != nil) {
            DLog(@"Removed effect with id: %@", effectId);
            fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetchedEffect];
        }
    }
}
//Deepak_Carpenter : Added for unity Type
//////////////////-----------------DosageUnits---------------///////////////////
-(void)updateDosageUnitsArray:(NSArray*)dosageUnitsArray {
    DLog(@"Updating %d UnitType...", dosageUnitsArray.count);
    for (UnitTypeSync* dosageUnitsSync in dosageUnitsArray) {
        // Check if UnitType already exists
        UnitType* fetchedUnitType = [_entityFactory fetchEntity:NSStringFromClass([UnitType class]) usingIdentifier:dosageUnitsSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedUnitType == nil) {
            // Insert instance in managed object context
            fetchedUnitType = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([UnitType class]) inManagedObjectContext:_managedObjectContext];
            fetchedUnitType.identifier = dosageUnitsSync.identifier;
            DLog(@"Created UnitType: %@", dosageUnitsSync);
        } else {
            DLog(@"Updated UnitType: %@", dosageUnitsSync);
        }
        // Update Unittype description
        fetchedUnitType.name = dosageUnitsSync.name;
    }
}

///
-(void) removeDosageUnitsInArray:(NSArray*)dosageUnitsRemoveArray {
    DLog(@"Removing %d UnitType...", dosageUnitsRemoveArray.count);
    for (NSString* UnitTypeId in dosageUnitsRemoveArray) {
        UnitType* fetchedUnitType = [_entityFactory fetchEntity:NSStringFromClass([UnitType class]) usingIdentifier:UnitTypeId withManagedObjectContext:_managedObjectContext];
        if (fetchedUnitType != nil) {
            DLog(@"Removed UnitType with id: %@", UnitTypeId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
             [_managedObjectContext deleteObject:fetchedUnitType];
        }
    }
}
//////////////////-----------------AgeType---------------///////////////////
-(void)updateAgeTypeArray:(NSArray*)ageTypeArray {
    DLog(@"Updating %d AgeType...", ageTypeArray.count);
    for (AgeSync* ageTypeSync in ageTypeArray) {
        // Check if AgeType already exists
        AgeType* fetchedAgeType = [_entityFactory fetchEntity:NSStringFromClass([AgeType class]) usingIdentifier:ageTypeSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedAgeType == nil) {
            // Insert instance in managed object context
            fetchedAgeType = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([AgeType class]) inManagedObjectContext:_managedObjectContext];
            fetchedAgeType.identifier = ageTypeSync.identifier;
            DLog(@"Created AgeType: %@", ageTypeSync);
        } else {
            DLog(@"Updated AgeType: %@", ageTypeSync);
        }
        // Update AgeType description
        fetchedAgeType.name = ageTypeSync.name;
    }
}

-(void) removeAgeTypeInArray:(NSArray*)ageTypeRemoveArray {
    DLog(@"Removing %d AgeType...", ageTypeRemoveArray.count);
    for (NSString* AgeTypeId in ageTypeRemoveArray) {
        AgeType* fetchedAgeType = [_entityFactory fetchEntity:NSStringFromClass([AgeType class]) usingIdentifier:AgeTypeId withManagedObjectContext:_managedObjectContext];
        if (fetchedAgeType != nil) {
            DLog(@"Removed AgeType with id: %@", AgeTypeId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetchedEffect];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------ConsultType---------------///////////////////
-(void)updateConsultTypeArray:(NSArray*)ConsultTypeArray {
    DLog(@"Updating %d ConsultType...", ConsultTypeArray.count);
    for (ConsultTypeSync* consultTypeSync in ConsultTypeArray) {
        // Check if ConsultType already exists
        ConsultType* fetchedConsultType = [_entityFactory fetchEntity:NSStringFromClass([ConsultType class]) usingIdentifier:consultTypeSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedConsultType == nil) {
            // Insert instance in managed object context
            fetchedConsultType = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([ConsultType class]) inManagedObjectContext:_managedObjectContext];
            fetchedConsultType.identifier = consultTypeSync.identifier;
            DLog(@"Created ConsultType: %@", consultTypeSync);
        } else {
            DLog(@"Updated ConsultType: %@", consultTypeSync);
        }
        // Update ConsultType description
        fetchedConsultType.name = consultTypeSync.name;
    }
}

-(void) removeConsultTypeInArray:(NSArray*)ConsultTypeRemoveArray {
    DLog(@"Removing %d ConsultType...", ConsultTypeRemoveArray.count);
    for (NSString* ConsultTypeId in ConsultTypeRemoveArray) {
        ConsultType* fetchedConsultType = [_entityFactory fetchEntity:NSStringFromClass([ConsultType class]) usingIdentifier:ConsultTypeId withManagedObjectContext:_managedObjectContext];
        if (fetchedConsultType != nil) {
            DLog(@"Removed ConsultType with id: %@", ConsultTypeId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetchedEffect];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------MedicalCentre---------------///////////////////
-(void)updateCentreListArray:(NSArray*)CentreListArray {
    DLog(@"Updating %d CentreList...", CentreListArray.count);
    for (CentreListSync* centreListSync in CentreListArray) {
        // Check if centreList already exists
        MedicalCenter* fetchedCentreList = [_entityFactory fetchEntity:NSStringFromClass([MedicalCenter class]) usingIdentifier:centreListSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedCentreList == nil) {
            // Insert instance in managed object context
            fetchedCentreList = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([MedicalCenter class]) inManagedObjectContext:_managedObjectContext];
            fetchedCentreList.identifier = centreListSync.identifier;
            DLog(@"Created centreList: %@", centreListSync);
        } else {
            DLog(@"Updated CentreList: %@", centreListSync);
        }
        // Update entreList description
        fetchedCentreList.name = centreListSync.name;
    }
}

-(void) removeCentreListInArray:(NSArray*)CentreListRemoveArray {
    DLog(@"Removing %d CentreList...", CentreListRemoveArray.count);
    for (NSString* CentreListId in CentreListRemoveArray) {
        MedicalCenter* fetchedCentreList = [_entityFactory fetchEntity:NSStringFromClass([MedicalCenter class]) usingIdentifier:CentreListId withManagedObjectContext:_managedObjectContext];
        if (fetchedCentreList != nil) {
            DLog(@"Removed entreList with id: %@", CentreListId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetchedEffect];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------DoseFrequency---------------///////////////////
-(void)updateDoseFrequencyArray:(NSArray*)doseFrequencyArray {
    DLog(@"Updating %d DoseFrequency...", doseFrequencyArray.count);
    for (FrequencySync* doseFrequencySync in doseFrequencyArray) {
        // Check if DoseFrequency already exists
        Frequency* fetchedDoseFrequency = [_entityFactory fetchEntity:NSStringFromClass([Frequency class]) usingIdentifier:doseFrequencySync.identifier withManagedObjectContext:_managedObjectContext];
               // If new, create object instance
        if (fetchedDoseFrequency == nil) {
            // Insert instance in managed object context
            fetchedDoseFrequency = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Frequency class]) inManagedObjectContext:_managedObjectContext];
            fetchedDoseFrequency.identifier = doseFrequencySync.identifier;
            
                       
            
            DLog(@"Created DoseFrequency: %@", doseFrequencySync);
        } else {
            DLog(@"Updated DoseFrequency: %@", doseFrequencySync);
        }
        // Update DoseFrequency description
        fetchedDoseFrequency.name = doseFrequencySync.name;
       
    }
}

-(void) removeDoseFrequencyInArray:(NSArray*)doseFrequencyRemoveArray {
    DLog(@"Removing %d DoseFrequency...", doseFrequencyRemoveArray.count);
    for (NSString* DoseFrequencyId in doseFrequencyRemoveArray) {
        Frequency* fetchedFrequency = [_entityFactory fetchEntity:NSStringFromClass([Frequency class]) usingIdentifier:DoseFrequencyId withManagedObjectContext:_managedObjectContext];
                if (fetchedFrequency != nil) {
            DLog(@"Removed DoseFrequency with id: %@", DoseFrequencyId);
            [_managedObjectContext deleteObject:fetchedFrequency];
                    
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------Duration---------------///////////////////
-(void) updateDurationArray:(NSArray*)otherDurationArray {
    DLog(@"Updating %d Duration...", otherDurationArray.count);
    for (DurationTypeSync* durationSync in otherDurationArray) {
        // Check if Duration already exists
        DurationType* fetchedDuration = [_entityFactory fetchEntity:NSStringFromClass([DurationType class]) usingIdentifier:durationSync.identifier withManagedObjectContext:_managedObjectContext];
               
        // If new, create object instance
        if (fetchedDuration == nil ) {
            // Insert instance in managed object context
            fetchedDuration = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DurationType class]) inManagedObjectContext:_managedObjectContext];
            fetchedDuration.identifier = durationSync.identifier;
                      
            DLog(@"Created Duration: %@", durationSync);
        } else {
            DLog(@"Updated Duration: %@", durationSync);
        }
        // Update specility description
        fetchedDuration.name = durationSync.name;
       
    }
}
-(void) removeDurationInArray:(NSArray*)durationRemoveArray {
    DLog(@"Removing %d Duration...", durationRemoveArray.count);
    for (NSString* durationId in durationRemoveArray) {
        DurationType* fetchedDuration = [_entityFactory fetchEntity:NSStringFromClass([DurationType class]) usingIdentifier:durationId withManagedObjectContext:_managedObjectContext];
               if (fetchedDuration != nil ) {
            DLog(@"Removed Duration with id: %@", durationId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            [_managedObjectContext deleteObject:fetchedDuration];
                   }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------Gender---------------///////////////////
-(void)updateGenderArray:(NSArray*)genderArray {
    DLog(@"Updating %d Gender...", genderArray.count);
    for (GenderSync* genderSync in genderArray) {
        // Check if Gender already exists
        Gender* fetchedGender = [_entityFactory fetchEntity:NSStringFromClass([Gender class]) usingIdentifier:genderSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedGender == nil) {
            // Insert instance in managed object context
            fetchedGender = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Gender class]) inManagedObjectContext:_managedObjectContext];
            fetchedGender.identifier = genderSync.identifier;
            DLog(@"Created Gender: %@", genderSync);
        } else {
            DLog(@"Updated Gender: %@", genderSync);
        }
        // Update Gender description
        fetchedGender.name = genderSync.name;
    }
}

-(void) removeGenderInArray:(NSArray*)genderRemoveArray {
    DLog(@"Removing %d Gender...", genderRemoveArray.count);
    for (NSString* GenderId in genderRemoveArray) {
        Gender* fetchedGender = [_entityFactory fetchEntity:NSStringFromClass([Gender class]) usingIdentifier:GenderId withManagedObjectContext:_managedObjectContext];
        if (fetchedGender != nil) {
            DLog(@"Removed Gender with id: %@", GenderId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetchedEffect];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------Insurance---------------///////////////////
-(void)updateInsuranceArray:(NSArray*)insuranceArray {
    DLog(@"Updating %d insurance...", insuranceArray.count);
    for (InsuranceSync* insuranceSync in insuranceArray) {
        // Check if Gender already exists
        Insurance* fetchedInsurance = [_entityFactory fetchEntity:NSStringFromClass([Insurance class]) usingIdentifier:insuranceSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedInsurance == nil) {
            // Insert instance in managed object context
            fetchedInsurance = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Insurance class]) inManagedObjectContext:_managedObjectContext];
            fetchedInsurance.identifier = insuranceSync.identifier;
            DLog(@"Created insurance: %@", insuranceSync);
        } else {
            DLog(@"Updated Gender: %@", insuranceSync);
        }
        // Update insurance description
        fetchedInsurance.name = insuranceSync.name;
    }
}

-(void) removeInsuranceInArray:(NSArray*)insuranceRemoveArray {
    DLog(@"Removing %d insurance...", insuranceRemoveArray.count);
    for (NSString* insuranceId in insuranceRemoveArray) {
        Insurance* fetchedInsurance = [_entityFactory fetchEntity:NSStringFromClass([Insurance class]) usingIdentifier:insuranceId withManagedObjectContext:_managedObjectContext];
        if (fetchedInsurance != nil) {
            DLog(@"Removed insurance with id: %@", insuranceId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetchedEffect];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------PathologyType---------------///////////////////
-(void)updatePathologyTypeArray:(NSArray*)PathologyTypeArray {
    DLog(@"Updating %d PathologyType...", PathologyTypeArray.count);
    for (PathologyTypeSync* pathologyTypeSync in PathologyTypeArray) {
        // Check if PathologyType already exists
       Pathology* fetchedPathologyType = [_entityFactory fetchEntity:NSStringFromClass([Pathology class]) usingIdentifier:pathologyTypeSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedPathologyType == nil) {
            // Insert instance in managed object context
            fetchedPathologyType = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Pathology class]) inManagedObjectContext:_managedObjectContext];
            fetchedPathologyType.identifier = pathologyTypeSync.identifier;
            DLog(@"Created PathologyType: %@", pathologyTypeSync);
        } else {
            DLog(@"Updated pathologyTypeSync: %@", pathologyTypeSync);
        }
        // Update PathologyType description
        fetchedPathologyType.name = pathologyTypeSync.name;
    }
}

-(void) removePathologyTypeInArray:(NSArray*)PathologyTypeRemoveArray {
    DLog(@"Removing %d PathologyType...", PathologyTypeRemoveArray.count);
    for (NSString* PathologyTypeId in PathologyTypeRemoveArray) {
        Pathology* fetchedPathologyType = [_entityFactory fetchEntity:NSStringFromClass([Pathology class]) usingIdentifier:PathologyTypeId withManagedObjectContext:_managedObjectContext];
        if (fetchedPathologyType != nil) {
            DLog(@"Removed fetchedPathologyType with id: %@", PathologyTypeId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetchedEffect];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------PlaceOfVisit---------------///////////////////
-(void)updatePlaceOfVisitArray:(NSArray*)PlaceOfVisitArray {
    DLog(@"Updating %d PlaceOfVisit...", PlaceOfVisitArray.count);
    for (PlaceOfVisitSync* placeOfVisitSync in PlaceOfVisitArray) {
        // Check if PlaceOfVisit already exists
        PlaceOfVisit* fetchedPlaceOfVisit = [_entityFactory fetchEntity:NSStringFromClass([PlaceOfVisit class]) usingIdentifier:placeOfVisitSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedPlaceOfVisit == nil) {
            // Insert instance in managed object context
            fetchedPlaceOfVisit = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([PlaceOfVisit class]) inManagedObjectContext:_managedObjectContext];
            fetchedPlaceOfVisit.identifier = placeOfVisitSync.identifier;
            DLog(@"Created PlaceOfVisit: %@", placeOfVisitSync);
        } else {
            DLog(@"Updated PlaceOfVisit: %@", placeOfVisitSync);
        }
        // Update PlaceOfVisit description
        fetchedPlaceOfVisit.name = placeOfVisitSync.name;
    }
}

-(void) removePlaceOfVisitInArray:(NSArray*)placeOfVisitRemoveArray {
    DLog(@"Removing %d PlaceOfVisit...", placeOfVisitRemoveArray.count);
    for (NSString* placeOfVisitId in placeOfVisitRemoveArray) {
        PlaceOfVisit* fetchedPlaceOfVisit = [_entityFactory fetchEntity:NSStringFromClass([PlaceOfVisit class]) usingIdentifier:placeOfVisitId withManagedObjectContext:_managedObjectContext];
        if (fetchedPlaceOfVisit != nil) {
            DLog(@"Removed PlaceOfVisit with id: %@", placeOfVisitId);
            [_managedObjectContext deleteObject:fetchedPlaceOfVisit];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------Province---------------///////////////////
-(void)updateProvinceArray:(NSArray*)provinceArray {
    DLog(@"Updating %d Province...", provinceArray.count);
    for (ProvinceSync* provinceSync in provinceArray) {
        // Check if Province already exists
        Province* fetchedProvince = [_entityFactory fetchEntity:NSStringFromClass([Province class]) usingIdentifier:provinceSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedProvince == nil) {
            // Insert instance in managed object context
            fetchedProvince = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Province class]) inManagedObjectContext:_managedObjectContext];
            fetchedProvince.identifier = provinceSync.identifier;
            DLog(@"Created Province: %@", provinceSync);
        } else {
            DLog(@"Updated Province: %@", provinceSync);
        }
        // Update Profession description
        fetchedProvince.name = provinceSync.name;
    }
}

-(void) removeProvinceInArray:(NSArray*)provinceRemoveArray {
    DLog(@"Removing %d Province...", provinceRemoveArray.count);
    for (NSString* ProvinceId in provinceRemoveArray) {
        Province* fetchedProvince = [_entityFactory fetchEntity:NSStringFromClass([Province class]) usingIdentifier:ProvinceId withManagedObjectContext:_managedObjectContext];
        if (fetchedProvince != nil) {
            DLog(@"Removed Province with id: %@", fetchedProvince);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            [_managedObjectContext deleteObject:fetchedProvince];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------Speciality---------------///////////////////
-(void) updateSpecialityArray:(NSArray*)specialityArray {
    DLog(@"Updating %d Speciality...", specialityArray.count);
    for (SpecialitiesSync* specialitySync in specialityArray) {
        // Check if diagnosis already exists
        Specialty* fetchedSpeciality = [_entityFactory fetchEntity:NSStringFromClass([Specialty class]) usingIdentifier:specialitySync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedSpeciality == nil) {
            // Insert instance in managed object context
            fetchedSpeciality = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Specialty class]) inManagedObjectContext:_managedObjectContext];
            fetchedSpeciality.identifier = specialitySync.identifier;
            DLog(@"Created Speciality: %@", specialitySync);
        } else {
            DLog(@"Updated Speciality: %@", specialitySync);
        }
        // Update specility description
        fetchedSpeciality.name = specialitySync.name;
        //        fetchedSpeciality.deleted = [NSNumber numberWithBool:NO];
    }
}

-(void) removeSpecialityInArray:(NSArray*)specialityRemoveArray {
    DLog(@"Removing %d effects...", specialityRemoveArray.count);
    for (NSString* specialityId in specialityRemoveArray) {
        Specialty* fetchedSpeciality = [_entityFactory fetchEntity:NSStringFromClass([Specialty class]) usingIdentifier:specialityId withManagedObjectContext:_managedObjectContext];
        if (fetchedSpeciality != nil) {
            DLog(@"Removed Speciality with id: %@", specialityId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
             [_managedObjectContext deleteObject:fetchedSpeciality];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------OtherSpeciality---------------///////////////////
-(void) updateOtherSpecialityArray:(NSArray*)otherSpecialityArray {
    DLog(@"Updating %d OtherSpeciality...", otherSpecialityArray.count);
    for (OtherSpecialitiesSync* otherSpecialitySync in otherSpecialityArray) {
        // Check if Speciality already exists
        OtherSpecialty* fetchedOtherSpeciality = [_entityFactory fetchEntity:NSStringFromClass([OtherSpecialty class]) usingIdentifier:otherSpecialitySync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedOtherSpeciality == nil) {
            // Insert instance in managed object context
            fetchedOtherSpeciality = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([OtherSpecialty class]) inManagedObjectContext:_managedObjectContext];
            fetchedOtherSpeciality.identifier = otherSpecialitySync.identifier;
            DLog(@"Created OtherSpeciality: %@", otherSpecialitySync);
        } else {
            DLog(@"Updated OtherSpeciality: %@", otherSpecialitySync);
        }
        // Update specility description
        fetchedOtherSpeciality.name = otherSpecialitySync.name;
        //        fetchedSpeciality.deleted = [NSNumber numberWithBool:NO];
    }
}

-(void) removeOtherSpecialityInArray:(NSArray*)otherSpecialityRemoveArray {
    DLog(@"Removing %d OtherSpeciality...", otherSpecialityRemoveArray.count);
    for (NSString* otherSpecialityId in otherSpecialityRemoveArray) {
        OtherSpecialty* fetchedOtherSpeciality = [_entityFactory fetchEntity:NSStringFromClass([OtherSpecialty class]) usingIdentifier:otherSpecialityId withManagedObjectContext:_managedObjectContext];
        if (fetchedOtherSpeciality != nil) {
            DLog(@"Removed OtherSpeciality with id: %@", otherSpecialityId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            [_managedObjectContext deleteObject:fetchedOtherSpeciality];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------Recurrance---------------///////////////////
-(void)updateRecurranceArray:(NSArray*)RecurranceArray {
    DLog(@"Updating %d Recurrance...", RecurranceArray.count);
    for (RecurrenceSync* recurranceSync in RecurranceArray) {
        // Check if Recurrance already exists
        Recurrence* fetchedRecurrance = [_entityFactory fetchEntity:NSStringFromClass([Recurrence class]) usingIdentifier:recurranceSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedRecurrance == nil) {
            // Insert instance in managed object context
            fetchedRecurrance = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Recurrence class]) inManagedObjectContext:_managedObjectContext];
            fetchedRecurrance.identifier = recurranceSync.identifier;
            DLog(@"Created Recurrance: %@", recurranceSync);
        } else {
            DLog(@"Updated Recurrance: %@", recurranceSync);
        }
        // Update Recurrance description
        fetchedRecurrance.name = recurranceSync.name;
    }
}

-(void) removeRecurranceInArray:(NSArray*)recurranceRemoveArray {
    DLog(@"Removing %d Recurrance...", recurranceRemoveArray.count);
    for (NSString* recurranceId in recurranceRemoveArray) {
        Recurrence* fetchedRecurrance = [_entityFactory fetchEntity:NSStringFromClass([Recurrence class]) usingIdentifier:recurranceId withManagedObjectContext:_managedObjectContext];
        if (fetchedRecurrance != nil) {
            DLog(@"Removed Recurrance with id: %@", recurranceId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetchedEffect];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------Smoker---------------///////////////////
-(void)updateSmokerArray:(NSArray*)SmokerArray {
    DLog(@"Updating %d Smoker...", SmokerArray.count);
    for (SmokerSync* SmokerSync in SmokerArray) {
        // Check if Smoker already exists
        Smoker* fetchedSmoker = [_entityFactory fetchEntity:NSStringFromClass([Smoker class]) usingIdentifier:SmokerSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedSmoker== nil) {
            // Insert instance in managed object context
            fetchedSmoker = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Smoker class]) inManagedObjectContext:_managedObjectContext];
            fetchedSmoker.identifier = SmokerSync.identifier;
            DLog(@"Created Smoker: %@", SmokerSync);
        } else {
            DLog(@"Updated Smoker: %@", SmokerSync);
        }
        // Update Recurrance description
        fetchedSmoker.name = SmokerSync.name;
    }
}

-(void) removeSmokerInArray:(NSArray*)smokerRemoveArray {
    DLog(@"Removing %d Smoker...", smokerRemoveArray.count);
    for (NSString* SmokerId in smokerRemoveArray) {
        Smoker* fetchedSmoker = [_entityFactory fetchEntity:NSStringFromClass([Smoker class]) usingIdentifier:SmokerId withManagedObjectContext:_managedObjectContext];
        if (fetchedSmoker != nil) {
            DLog(@"Removed Smoker with id: %@", SmokerId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetchedEffect];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------TherapyElection---------------///////////////////
-(void)updateTherapyElectionArray:(NSArray*)therapyElectionArray {
    DLog(@"Updating %d TherapyElection...", therapyElectionArray.count);
    for (TherapyElectionSync* therapyElectionSync in therapyElectionArray) {
        // Check if TherapyElection already exists
        TherapyElection* fetchedTherapyElection = [_entityFactory fetchEntity:NSStringFromClass([TherapyElection class]) usingIdentifier:therapyElectionSync.identifier withManagedObjectContext:_managedObjectContext];
            
        // If new, create object instance
        if (fetchedTherapyElection == nil) {
            // Insert instance in managed object context
            fetchedTherapyElection = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([TherapyElection class]) inManagedObjectContext:_managedObjectContext];
            fetchedTherapyElection.identifier = therapyElectionSync.identifier;
            
            
            DLog(@"Created TherapyElection: %@", therapyElectionSync);
        } else {
            DLog(@"Updated TherapyElection: %@", therapyElectionSync);
        }
        // Update TherapyElection description
        fetchedTherapyElection.name = therapyElectionSync.name;
    }
}

-(void) removeTherapyElectionInArray:(NSArray*)therapyElectionRemoveArray {
    DLog(@"Removing %d TherapyElection...", therapyElectionRemoveArray.count);
    for (NSString* therapyElectionId in therapyElectionRemoveArray) {
        TherapyElection* fetchedTherapyElection = [_entityFactory fetchEntity:NSStringFromClass([TherapyElection class]) usingIdentifier:therapyElectionId withManagedObjectContext:_managedObjectContext];
        
        
        if (fetchedTherapyElection != nil) {
            DLog(@"Removed TherapyType with id: %@", fetchedTherapyElection);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetche];
            // [_managedObjectContext deleteObject:fetchedTherapyType1];
        }
    }
}
//////////////////-----------------TherapyType---------------///////////////////
-(void)updateTherapyTypeArray:(NSArray*)therapyTypeArray {
    DLog(@"Updating %d TherapyType...", therapyTypeArray.count);
    for (TherapyTypeSync* therapyTypeSync in therapyTypeArray) {
        // Check if TherapyType already exists
        TherapyType* fetchedTherapyType = [_entityFactory fetchEntity:NSStringFromClass([TherapyType class]) usingIdentifier:therapyTypeSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedTherapyType == nil) {
            // Insert instance in managed object context
            fetchedTherapyType = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([TherapyType class]) inManagedObjectContext:_managedObjectContext];
            fetchedTherapyType.identifier = therapyTypeSync.identifier;
            
                     
            DLog(@"Created TherapyType: %@", therapyTypeSync);
        } else {
            DLog(@"Updated TherapyType: %@", therapyTypeSync);
        }
        // Update TherapyType description
        fetchedTherapyType.name = therapyTypeSync.name;
    }
}

-(void) removeTherapyTypeInArray:(NSArray*)therapyTypeRemoveArray {
    DLog(@"Removing %d TherapyType...", therapyTypeRemoveArray.count);
    for (NSString* therapyTypeId in therapyTypeRemoveArray) {
        TherapyType* fetchedTherapyType = [_entityFactory fetchEntity:NSStringFromClass([TherapyType class]) usingIdentifier:therapyTypeId withManagedObjectContext:_managedObjectContext];
        
        if (fetchedTherapyType != nil) {
            DLog(@"Removed TherapyType with id: %@", fetchedTherapyType);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetche];
            // [_managedObjectContext deleteObject:fetchedTherapyType1];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------Universities---------------///////////////////
-(void)updateUniversitiesArray:(NSArray*)UniversitiesArray {
    DLog(@"Updating %d Universities...", UniversitiesArray.count);
    for (UniversitiesSync* universitiesSync in UniversitiesArray) {
        // Check if Universities already exists
        University* fetchedUniversities = [_entityFactory fetchEntity:NSStringFromClass([University class]) usingIdentifier:universitiesSync.identifier withManagedObjectContext:_managedObjectContext];
        
        // If new, create object instance
        if (fetchedUniversities == nil) {
            // Insert instance in managed object context
            fetchedUniversities = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([University class]) inManagedObjectContext:_managedObjectContext];
            fetchedUniversities.identifier =universitiesSync.identifier;
            
            
            DLog(@"Created Universities: %@", universitiesSync);
        } else {
            DLog(@"Updated Universities: %@", universitiesSync);
        }
        // Update TherapyType description
        fetchedUniversities.name = universitiesSync.name;
    }
}

-(void) removeUniversitiesInArray:(NSArray*)UniversitiesRemoveArray {
    DLog(@"Removing %d Universities...", UniversitiesRemoveArray.count);
    for (NSString* UniversitiesId in UniversitiesRemoveArray) {
        University* fetchedUniversities = [_entityFactory fetchEntity:NSStringFromClass([University class]) usingIdentifier:UniversitiesId withManagedObjectContext:_managedObjectContext];
            
        if (fetchedUniversities != nil) {
            DLog(@"Removed TherapyType with id: %@", fetchedUniversities);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
             [_managedObjectContext deleteObject:fetchedUniversities];
            // [_managedObjectContext deleteObject:fetchedTherapyType1];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////



-(void) refreshUpdateDate {
    NSDate* lastUpdate = [NSDate date];
    DLog(@"Last update set to: %@", lastUpdate);
    self.metaInfo.lastUpdate = lastUpdate;
}

@end
