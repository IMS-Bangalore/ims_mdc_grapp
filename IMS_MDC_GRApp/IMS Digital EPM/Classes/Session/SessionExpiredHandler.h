//
//  SessionExpiredHandler.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 16/01/13.
//
//

#import <Foundation/Foundation.h>

@protocol SessionExpiredHandlerDelegate <NSObject>

@optional
- (void)sessionTimeExpired;

@end

@interface SessionExpiredHandler : NSObject

#pragma mark - Properties
@property (nonatomic, assign) id<SessionExpiredHandlerDelegate> delegate;

/** @brief Singleton access */
+ (SessionExpiredHandler*)sharedSessionExpiredHandler;

#pragma mark - Public methods

- (void)startSession;
- (void)refreshSession;
- (void)checkIfTimerWasFired;

@end
