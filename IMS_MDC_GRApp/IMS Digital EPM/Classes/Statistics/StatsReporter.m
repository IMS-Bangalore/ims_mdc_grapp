//
//  StatsReporter.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 18/12/12.
//
//

#import "StatsReporter.h"
#import "EntityFactory.h"
#import "StatsEvent.h"
#import "RequestFactory.h"

static NSString* const kDataBaseName = @"CoreDataEMP.sqlite";

static NSString* const kCreatePatientEventName = @"PATIENT";
static NSString* const kUpdatePatientEventName = @"UPDATE";
static NSString* const kEnterHelpEventName = @"HELP";
static NSString* const kStartSessionEventName = @"SESSIONTIME";
static NSString* const kLoginEventName = @"LOGIN";

static NSString* const kEventNameKey = @"event";
static NSString* const kEventStartDateKey = @"startDate";
static NSString* const kEventEndDateKey = @"endDate";
static NSString* const kEventCounterKey = @"counter";

@interface StatsReporter ()

@property (nonatomic, retain) NSMutableDictionary* temporaryData;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, retain) TCBaseRequest* request;

@end

@implementation StatsReporter

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize request = _request;
@synthesize userId = _userId;

#pragma mark SingletonAccess
static StatsReporter* sharedStatsReporter = nil;
+ (StatsReporter*)sharedStatsReporter {
    if (sharedStatsReporter == nil) {
        sharedStatsReporter = [[StatsReporter alloc] init];
    }
    return sharedStatsReporter;
}

#pragma mark - Init and dealloc
- (id)init {
    if (( self = [super init] )) {
        self.temporaryData = [NSMutableDictionary dictionary];
        self.managedObjectContext = [self createContext];
    }
    return self;
}

- (NSMutableDictionary*)dictForEvent:(StatsEventType)eventType {
    NSString* eventId = [self identifierForType:eventType];
    NSMutableDictionary* dict = self.temporaryData[eventId];
    if (dict == nil) {
        dict = [NSMutableDictionary dictionary];
        self.temporaryData[eventId] = dict;
    }
    
    return dict;
}

- (NSString*)identifierForType:(StatsEventType)eventType {
    NSString* identifier = nil;
    switch (eventType) {
        case kStatsEventTypeCreatePatient:
            identifier = kCreatePatientEventName;
            break;
        case kStatsEventTypeEnterHelp:
            identifier = kEnterHelpEventName;
            break;
        case kStatsEventTypeStartSession:
            identifier = kStartSessionEventName;
            break;
        case kStatsEventTypeUpdatePatient:
            identifier = kUpdatePatientEventName;
            break;
        case kStatsEventTypeLogin:
            identifier = kLoginEventName;
            break;
        default:
            DAssert(NO, @"Unhandled event type %d", eventType);
    }
    return identifier;
}


- (void)startEvent:(StatsEventType)eventType {
    NSMutableDictionary* dict = [self dictForEvent:eventType];
    [dict setObject:[NSDate date] forKey:kEventStartDateKey];
}

- (void)createAndSaveStatsEvent:(StatsEventType)eventType andWithPatiendId:(NSInteger)patientId {
    NSMutableDictionary* dict = [self dictForEvent:eventType];
    if (dict != nil) {
        [dict setObject:[NSDate date] forKey:kEventEndDateKey];
        DLog(@"%@", dict.descriptionInStringsFileFormat);
        NSString* identifier = [self identifierForType:eventType];
        StatsEvent* statsEvent = [self insertNewWithEntityName:NSStringFromClass([StatsEvent class])];
        statsEvent.startDate = [dict objectForKey:kEventStartDateKey];
        statsEvent.endDate = [NSDate date];
        statsEvent.timeElapsed = @([statsEvent.endDate timeIntervalSinceDate:statsEvent.startDate]);
        statsEvent.event = identifier;
        statsEvent.creationDate = [NSDate date];
        statsEvent.sent = [NSNumber numberWithBool:NO];
        if (patientId != 0) {
            statsEvent.patientId = @(patientId);
        }
        [self saveChanges];
    }
}

- (void)endEvent:(StatsEventType)eventType {
    [self createAndSaveStatsEvent:eventType andWithPatiendId:0];
}

- (void)endEvent:(StatsEventType)eventType withPatientId:(NSInteger)patientId {
    [self createAndSaveStatsEvent:eventType andWithPatiendId:patientId];
}

- (void)startCountingEvent:(StatsEventType)eventType {
    NSMutableDictionary* dict = [self dictForEvent:eventType];
    NSNumber* counter = @([dict[kEventCounterKey] integerValue]);
    [dict setObject:@(counter.integerValue + 1) forKey:kEventCounterKey];
    [dict setObject:[NSDate date] forKey:kEventStartDateKey];
}

- (void)endCountingEvent:(StatsEventType)eventType {
    NSMutableDictionary* dict = [self dictForEvent:eventType];
    if (dict != nil) {
        DLog(@"%@", dict.descriptionInStringsFileFormat);
        NSString* identifier = [self identifierForType:eventType];
        StatsEvent* statsEvent = [self insertNewWithEntityName:NSStringFromClass([StatsEvent class])];
        statsEvent.counter = [dict objectForKey:kEventCounterKey];
        statsEvent.event = identifier;
        statsEvent.startDate = [dict objectForKey:kEventStartDateKey];
        statsEvent.creationDate = [NSDate date];
        statsEvent.sent = [NSNumber numberWithBool:NO];
        [self saveChanges];
    }
}

#pragma mark - Core Data
// Context for saving the data in DB

- (NSManagedObjectContext*)createContext {
    NSManagedObjectContext* context = nil;
    if (self.persistentStoreCoordinator != nil) {
        context = [[[NSManagedObjectContext alloc] init] autorelease];
        context.persistentStoreCoordinator = self.persistentStoreCoordinator;
        // Merge policy gives priority to persistent store changes for other than main context
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
        
//        // Register context with the notification center
//        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
//        [nc addObserver:self
//               selector:@selector(mergeChanges:)
//                   name:NSManagedObjectContextDidSaveNotification
//                 object:context];
    }
    return context;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 */
- (NSManagedObjectModel *)managedObjectModel {
	
    if (_managedObjectModel == nil) {
        _managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:nil] retain];
    }
    return _managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
	
	NSString *storePath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent: kDataBaseName];
	NSURL *storeUrl = [NSURL fileURLWithPath:storePath];
	
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption,
                             nil];
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    
	NSError *error;
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) {
		// Update to handle the error appropriately.
		DLog(@"Unresolved error %@, %@", error, [error userInfo]);
        self.persistentStoreCoordinator = nil;
    }
	
    return _persistentStoreCoordinator;
}

/**
 Returns the path to the application's documents directory.
 */
- (NSString *)applicationDocumentsDirectory {
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

#pragma mark - Object creation

- (id)insertNewWithEntityName:(NSString*)entityName {
    DAssert([NSThread isMainThread], @"This method must be called from Main Thread");
    return [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:self.managedObjectContext];
}

#pragma mark - Saving object

/**
 Performs the save action for the application, which is to send the save:
 message to the application's managed object context.
 */
- (void)saveChanges {
	DAssert([NSThread isMainThread], @"This method must be called from Main Thread");
    
    NSError *error;
    if (![[self managedObjectContext] save:&error]) {
		// Update to handle the error appropriately.
		DLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
}

#pragma mark - Send stats

- (NSArray*)getNotSentStats {
    DAssert([NSThread isMainThread], @"This method must be called from Main Thread");
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"sent == NO"];
//    
    NSFetchRequest* fetchRequest = [[[NSFetchRequest alloc] initWithEntityName:@"StatsEvent"] autorelease];
    fetchRequest.predicate = predicate;
    
    NSError* error = nil;
    NSArray* array = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error != nil) {
        DLog(@"Error fetching StatsEvent");
    }
    
    return array;
}

- (void)sendStats {
    NSArray* notSentStats = [self getNotSentStats];
    // Array de StatsEvent. Las vamos enviando y cambiandoles el campo Sent
    // Hacer la petición, si no devuelve error poner el sent a YES;
    self.request = [[RequestFactory sharedInstance] createSendStatisticsRequestWithUserId:_userId andStats:notSentStats onComplete:^{
        for (StatsEvent* statsEvent in notSentStats) {
            statsEvent.sent = [NSNumber numberWithBool:YES];
        }
        [self saveChanges];
    } onError:^(NSError *error) {
        
    }];
    [self.request start];
    
    
}



@end
