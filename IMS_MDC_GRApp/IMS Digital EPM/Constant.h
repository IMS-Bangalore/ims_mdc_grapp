//
//  Constant.h
//  IMS Digital EPM
//
//  Created by Bukka, Ravi (Bangalore) on 21/01/14.
//
//

//For Staging


//#define kDEVBaseURL @"http://162.44.148.57/IMSService/ServiceIMS.svc/"
//#define kSTGBaseURL @"http://native-platform-stg.imshealth.com/IMSService/ServiceIMS.svc/"
#define kPRODBaseURL @"http://native-platform.imshealth.com/IMSService/ServiceIMS.svc/"

#define kBaseURL kPRODBaseURL

#define kLogin @"login?countryCode="
#define kDoctorInfo @"doctorInfo/"
#define kChangePassword @"ChangePassword?CountryCode="
#define kRemindPassword @"RemindPassword?CountryCode="
#define kPatientInfo @"PatientInfo/"
#define kDiagnosisInfo @"DiagnosisInfo/"
#define kTreatmentInfo @"TreatmentInfo/"

//Ravi Bukka: Added for checkdataversion
#define kCheckDataVersion @"CheckDataVersion?"
