//
//  LoginViewController.m
//  IMS Digital EPM
//
//  Created by Ricardo Berzal on 23/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "math.h"
#import "EntityFactory.h"
#import "RequestFactory.h"
#import "ChangeManager.h"
#import "MultipleChoiceViewController.h"
#import "LoadingDialogViewController.h"
//#import <TwinCodersLibrary/TwinCodersLibrary.h>
#include <CommonCrypto/CommonDigest.h>
#import <AdSupport/ASIdentifierManager.h>
//Deepak_Carpenter : added new reachbility Class
#import "Reachability.h"

static NSString* const kDefaultPassword = @"1234";
static const NSInteger kCollaborationDateFinishErrorCode = 11;
static const NSInteger kConnectionErrorCode = 90;
const NSInteger kDivideToObtainDays = 86400;
//const NSInteger kMaxDayAfterCollaborationEnds = 14;

//Utpal: CR#7  for Data Retention
const NSInteger kMaxDayAfterCollaborationEnds = 28;

//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
const NSInteger kMaxDayAfterExtendCollaborationEnds = 21;

#pragma Extension
@interface LoginViewController()
@property (nonatomic, retain) NSString* userName;
@property (nonatomic, retain) NSString* password;
@property (nonatomic, retain) Doctor* doctor;
//Deepak_carpenter: Added for collaboration handling
@property (nonatomic, retain) DoctorInfo* doctorInfo;

@property (nonatomic, retain) TCBaseRequest *loginRequest;
@property (nonatomic, retain) BaseRequest *activateRequest;
@property (nonatomic, retain) BaseRequest *changePasswordRequest;
@property (nonatomic, retain) BaseRequest *forgotPasswordRequest;
@property (nonatomic, retain) MultipleChoiceViewController* choiceController;
@property (nonatomic, retain) LoadingDialogViewController *loadingViewController;
@property (nonatomic, copy) NSString* appVersion;
@property (nonatomic, assign, getter = isFirstTime) BOOL firstTime;
-(void) switchActiveView;
-(void) requestPasswordChange;
-(void) setLoading:(BOOL)loading;
-(void) setLoading:(BOOL)loading withMessage:(NSString*)message;
@end

#pragma mark - 
@implementation LoginViewController
@synthesize VistaLogin;
@synthesize VistaCambiarContrasena;
@synthesize TextUsuario;
@synthesize TextContrasena;
@synthesize TextContrasenaActual;
@synthesize TextNuevaContrasena;
@synthesize TextConfirmeNuevaContrasena;
@synthesize BotonAyuda;
@synthesize BotonIrACambiarContrasena;
@synthesize BotonCambiarContrasena;
@synthesize BotonComprobarLogin;
@synthesize userName = _userName;
@synthesize password = _password;
@synthesize doctor = _doctor;
//Deepak_Carpenter:
@synthesize doctorInfo=_doctorInfo;
@synthesize loginRequest = _loginRequest;
@synthesize activateRequest = _activateRequest;
@synthesize choiceController = _choiceController;
@synthesize firstTime = _firstTime;
@synthesize changePasswordRequest = _changePasswordRequest;
@synthesize forgotPasswordRequest = _forgotPasswordRequest;
@synthesize loadingViewController = _loadingViewController;



#pragma mark- login delegate
- (void)loginResponse:(bool)resp
{
    // Hide network activity
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self setLoading:NO];
    
    if(resp){
        NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setValue:self.userName forKey:@"LAST_USER"];
        [userDefault synchronize];
//        [_delegate showVentana:3 firstTime:YES];
        [_delegate didLoginWithUser:self.doctor firstTime:self.firstTime];
    }
    
    // Enable the login button again
    BotonComprobarLogin.enabled = YES;

    _checkingLogin = NO;
}

-(Doctor*) createDoctorWithUserName:(NSString*)userName andPassword:(NSString*)password {
    Doctor* doctor = nil;
    DoctorInfo* doctorInfo = [[ChangeManager manager] createDoctorInfo];
    doctor = doctorInfo.doctor;
    doctor.userID = userName;
    doctor.passwordHash = [password md5];
    doctorInfo.identifier = userName;
    return doctor;
}

-(Doctor*) fetchDoctor {
    return [[EntityFactory sharedEntityFactory] fetchDoctorForUserID:self.userName];
}

-(Doctor*) fetchOrCreateDoctor {
    Doctor* doctor = [self fetchDoctor];
    self.firstTime = doctor == nil;
    
    if (_firstTime) {
        DLog(@"No doctor found with ID %@, creating new register", self.userName);
        doctor = [self createDoctorWithUserName:self.userName andPassword:self.password];
    }
    
    [self checkPatientsOutOfDateForDoctor:doctor];
    
    return doctor;
}

- (void)deletePatients:(NSMutableSet*)patients ifCollaborationHasEnded:(BOOL)hasEnded forBeginningDate:(NSDate*)beginningDate andEndingDate:(NSDate*)endingDate {
    if (hasEnded) {
        for (Patient* patient in patients) {
            BOOL visitDateInCollab = ([patient.visitDate compare:beginningDate] == (NSOrderedDescending | NSOrderedSame)) && ([patient.visitDate compare:endingDate] == (NSOrderedAscending | NSOrderedSame));
            if (visitDateInCollab) {
                [[EntityFactory sharedEntityFactory] deleteObject:patient];
            }
        }
        [[EntityFactory sharedEntityFactory] saveChanges];
    }
}

- (void)checkPatientsOutOfDateForDoctor:(Doctor*)doctor {
    NSMutableSet* patients = [NSMutableSet setWithSet:doctor.doctorInfo.patients];
    NSTimeInterval timeIntervalDays = [[NSDate date] timeIntervalSinceDate:doctor.doctorInfo.firstCollaborationDate];
    double days = timeIntervalDays/kDivideToObtainDays;
    BOOL firstCollabEnded = days > kMaxDayAfterCollaborationEnds;
    [self deletePatients:patients ifCollaborationHasEnded:firstCollabEnded forBeginningDate:doctor.doctorInfo.firstCollaborationDate andEndingDate:doctor.doctorInfo.firstCollaborationDateEnd];
    
    timeIntervalDays = [[NSDate date] timeIntervalSinceDate:doctor.doctorInfo.secondCollaborationDate];
    days = timeIntervalDays/kDivideToObtainDays;
    BOOL secondCollabEnded = days > kMaxDayAfterCollaborationEnds;
    [self deletePatients:patients ifCollaborationHasEnded:secondCollabEnded forBeginningDate:doctor.doctorInfo.secondCollaborationDate andEndingDate:doctor.doctorInfo.secondCollaborationDateEnd];
}

//Ravi_Bukka: Webservice SetDoctorInfo is going to be called here
-(void) activateUser:(DoctorInfo*)doctorInfo {
    [self.activateRequest cancel];
    self.activateRequest = [[RequestFactory sharedInstance] createActivateUserRequestWithDoctorInfo:doctorInfo onComplete:^{
        DLog(@"User successfuly activated");
        [self setLoading:NO];
        [self loginResponse:YES];
    } onError:^(NSError *error) {
        DLog(@"Activate user request Error: %@", error);
        [self displayAlert:error.localizedDescription];
        [self setLoading:NO];
        [self loginResponse:NO];
    }];
    [self.activateRequest start];
}

- (void)removeInformationWhenCollaborationEnds {
    DLog(@"Received collaboration date finish, deleting all the EPM info");
    Doctor* doctor = [self fetchDoctor];
    if (doctor != nil) {
        // Cancel all the doctor and patients requests
        [[ChangeManager manager] recursiveCancelActions:doctor.doctorInfo];
        // Remove all the information about the patients and diagnosis
        [doctor.doctorInfo removePatients:doctor.doctorInfo.patients];
        doctor.doctorInfo.firstCollaborationDate = nil;
        doctor.doctorInfo.firstCollaborationDateEnd = nil;
        doctor.doctorInfo.secondCollaborationDate = nil;
        doctor.doctorInfo.secondCollaborationDateEnd = nil;
        
        // Save the changes (removing patients and canceling actions)
        [[EntityFactory sharedEntityFactory] saveChanges];
    }
}

- (void)allowAccessToPatientsForDoctor:(Doctor*)doctor {
    //Deepak_Carpenter : Added to handle collaboration dates
   // switch (collaboration)
    NSLog(@"%d Collaboration",collaboration);
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"CollaborationCount"] isEqualToString:@"1"]) {
        
        
//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
        if ([(AppDelegate*)[[UIApplication sharedApplication] delegate] returnGracePeriondConfirmation]) {
            
            NSLog(@"YES Extended");
            NSDateComponents *dayComponent = [[[NSDateComponents alloc] init] autorelease];
            dayComponent.day = 15;
            
            NSCalendar *theCalendar = [NSCalendar currentCalendar];
            NSDate* today = [NSDate date];
            NSLog(@"firstCollaborationDateEnd %@",doctor.doctorInfo.firstCollaborationDateEnd);
            NSDate *firstEndDate = [theCalendar dateByAddingComponents:dayComponent toDate:doctor.doctorInfo.firstCollaborationDateEnd options:0];
            
            
            BOOL dateIsInFirstCollaborationperiod = ([today compare:doctor.doctorInfo.firstCollaborationDate] != NSOrderedAscending) && ([today compare:firstEndDate] != NSOrderedDescending);
            
            
            if (!dateIsInFirstCollaborationperiod) {
                [_delegate setIsInCollaborationPeriod:NO];
            } else {
                [_delegate setIsInCollaborationPeriod:YES];
            }
            
            
        }
        else {
            
            NSDateComponents *dayComponent = [[[NSDateComponents alloc] init] autorelease];
            dayComponent.day = 8;
            
            NSCalendar *theCalendar = [NSCalendar currentCalendar];
            NSDate* today = [NSDate date];
            NSLog(@"firstCollaborationDateEnd %@",doctor.doctorInfo.firstCollaborationDateEnd);
            NSDate *firstEndDate = [theCalendar dateByAddingComponents:dayComponent toDate:doctor.doctorInfo.firstCollaborationDateEnd options:0];
            
            
            BOOL dateIsInFirstCollaborationperiod = ([today compare:doctor.doctorInfo.firstCollaborationDate] != NSOrderedAscending) && ([today compare:firstEndDate] != NSOrderedDescending);
            
            
            if (!dateIsInFirstCollaborationperiod) {
                [_delegate setIsInCollaborationPeriod:NO];
            } else {
                [_delegate setIsInCollaborationPeriod:YES];
            }
        }
    }
        //case 2:
       else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"CollaborationCount"] isEqualToString:@"2"]) {
            NSDateComponents *dayComponent = [[[NSDateComponents alloc] init] autorelease];
            dayComponent.day = 8;
            
            NSCalendar *theCalendar = [NSCalendar currentCalendar];
            NSDate* today = [NSDate date];
            
            NSDate *firstEndDate = [theCalendar dateByAddingComponents:dayComponent toDate:doctor.doctorInfo.firstCollaborationDateEnd options:0];
            NSDate *secondEndDate = [theCalendar dateByAddingComponents:dayComponent toDate:doctor.doctorInfo.secondCollaborationDateEnd options:0];
            
            
            
            BOOL dateIsInFirstCollaborationperiod = ([today compare:doctor.doctorInfo.firstCollaborationDate] != NSOrderedAscending) && ([today compare:firstEndDate] != NSOrderedDescending);
           
            BOOL dateIsInSecondCollaborationperiod = ([today compare:doctor.doctorInfo.secondCollaborationDate] != NSOrderedAscending) && ([today compare:secondEndDate] != NSOrderedDescending);
            
            if (!dateIsInFirstCollaborationperiod && !dateIsInSecondCollaborationperiod) {
                [_delegate setIsInCollaborationPeriod:NO];
            } else {
                [_delegate setIsInCollaborationPeriod:YES];
            }

            
           // break;
        }
                    
//        default:
//            break;
   // }
    
//    NSDateComponents *dayComponent = [[[NSDateComponents alloc] init] autorelease];
//    dayComponent.day = 8;
//    
//    NSCalendar *theCalendar = [NSCalendar currentCalendar];
//    NSDate* today = [NSDate date];
//    
//    NSDate *firstEndDate = [theCalendar dateByAddingComponents:dayComponent toDate:doctor.doctorInfo.firstCollaborationDateEnd options:0];
//    NSDate *secondEndDate = [theCalendar dateByAddingComponents:dayComponent toDate:doctor.doctorInfo.secondCollaborationDateEnd options:0];
//    
//    
//    
//    BOOL dateIsInFirstCollaborationperiod = ([today compare:doctor.doctorInfo.firstCollaborationDate] != NSOrderedAscending) && ([today compare:firstEndDate] != NSOrderedDescending);
//    BOOL dateIsInSecondCollaborationperiod = ([today compare:doctor.doctorInfo.secondCollaborationDate] != NSOrderedAscending) && ([today compare:secondEndDate] != NSOrderedDescending);
//    
//    if (!dateIsInFirstCollaborationperiod && !dateIsInSecondCollaborationperiod) {
//        [_delegate setIsInCollaborationPeriod:NO];
//    } else {
//        [_delegate setIsInCollaborationPeriod:YES];
//    }

}

- (void)loginWithUserName:(NSString*)userName password:(NSString*)password removePreviousData:(BOOL)removePreviousData {
 
    //Deepak_carpenter: Added to remove alert while we have only one collaboration
    self.doctorInfo.firstCollaborationDate=nil;
    self.doctorInfo.firstCollaborationDateEnd=nil;

    //Deepak_carpenter: Added to remove alert while we have only one collaboration
    self.doctorInfo.secondCollaborationDate=nil;
    self.doctorInfo.secondCollaborationDateEnd=nil;
  
    
//Ravi_Bukka: Added below line to implement new service and remove the TwincodeLibrary dependency
  [self.loginRequest cancel];
    [self setLoading:YES withMessage:NSLocalizedString(@"LOADING_LOGIN", nil)];
    
    NSString *countryCode = @"GR";
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",kBaseURL,kLogin,countryCode];
   
    
    //Deepak Carpenter: Getting Device ID
//    UIDevice* device = [UIDevice currentDevice];;
//    NSUUID *deviceID = device.identifierForVendor;
//    NSString *deviceIDStr = deviceID.UUIDString;
//    deviceIDStr = [deviceIDStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
    //////////////////////////
    NSString *deviceadID=[[[ASIdentifierManager sharedManager]advertisingIdentifier]UUIDString];
//    NSString *deviceadID = @"42094444209444";
    
   //Deepak_Carpenter : Added iOS version into WS
    NSString *iOSVersion =[[UIDevice currentDevice]systemVersion];
    
//    NSString *ipadId=deviceadID;
    NSString *ipadId=deviceadID;
    NSString *version = _appVersion;
    NSString *responseFormat = @"JSON";
    NSString *encryptedPassword = [self sha1:password];
    
    NSLog(@"password: -- %@", encryptedPassword);

    urlString = [urlString stringByAppendingFormat:@"&username=%@&password=%@&ipadId=%@&version=%@&IpadVersion=%@&responseFormat=%@",userName,encryptedPassword,ipadId,version,iOSVersion,responseFormat];
    
    NSLog(@"urlString %@", urlString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    
//set blockDelegate since delegate_ will be gone inside blocks
//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
    __block id blockDelegate        = _delegate;
    
    __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setCompletionBlock:^{
        // Use when fetching text data
        //        NSString *responseString = [request responseString];
        NSData *responseData = [request responseData];
        
        id dictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:nil];
        
        NSLog(@"response = %@", dictionary);
        
        if ([[[dictionary valueForKey:@"Result"]stringValue] isEqualToString:@"0"]) {
            //Deepak_Carpenter : Added to resolve invalid session token issue
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"invalidSession"];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        //  [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        //Deepak_Carpenter: Added to Handle Collaboration Dates
        NSLog(@"collaboration %d", [[dictionary valueForKey:@"CollaborationDates"] count]);
            collaboration=[[dictionary valueForKey:@"CollaborationDates"] count];
            
            //Utpal : storing Collaboration Count to compare nd disply the Collaboration callender based on count
            
            NSString *colabPeriod = [NSString stringWithFormat:@"%lu",(unsigned long)[[dictionary valueForKey:@"CollaborationDates"] count]];
            NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setValue:colabPeriod forKey:@"CollaborationCount"];
            
            
            switch ([[dictionary valueForKey:@"CollaborationDates"] count]) {
                case 1:
                {
                    //Utpal:CR#4 For Demographics - Start
                    NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
                    
                    age = [[[dictionary valueForKey:@"Demographics"] objectAtIndex:0] objectForKey:@"Age"];
                    NSLog(@"Demographics Age.. %@",age);
                    
                    gender = [[[dictionary valueForKey:@"Demographics"] objectAtIndex:0] objectForKey:@"Gender"];
                    [userDefault setValue:gender forKey:@"Gender"];
                    NSLog(@"Demographics Gender.. %@",gender);
                    
                    graduationYear = [[[dictionary valueForKey:@"Demographics"] objectAtIndex:0] objectForKey:@"Year"];
                    NSLog(@"Demographics Year.. %@",graduationYear);
                    
                    specialty = [[[dictionary valueForKey:@"Demographics"] objectAtIndex:0] objectForKey:@"Speciality"];
                    [userDefault setValue:specialty forKey:@"Speciality"];
                    NSLog(@"Demographics specialty.. %@",specialty);
                    
                    
                    Otherspecialty = [[[dictionary valueForKey:@"Demographics"] objectAtIndex:0] objectForKey:@"OtherSpeciality"];
                    [userDefault setValue:Otherspecialty forKey:@"OtherSpeciality"];
                    NSLog(@"Demographics OtherSpeciality.. %@",Otherspecialty);
                    
                    province= [[[dictionary valueForKey:@"Demographics"] objectAtIndex:0] objectForKey:@"Province"];
                    [userDefault setValue:province forKey:@"Province"];
                    NSLog(@"Demographics Province.. %@",province);
                    
                    
                    university= [[[dictionary valueForKey:@"Demographics"] objectAtIndex:0] objectForKey:@"University"];
                    [userDefault setValue:university forKey:@"University"];
                    NSLog(@"Demographics University.. %@",university);
                    
                    email = [[[dictionary valueForKey:@"Demographics"] objectAtIndex:0] objectForKey:@"Email"];
                    NSLog(@"Demographics Email.. %@",email);
                    
                    weeklyActivityHrs = [[[dictionary valueForKey:@"Demographics"] objectAtIndex:0] objectForKey:@"WorkingHours"];
                    NSLog(@"Demographics weeklyActivityHrs.. %@",weeklyActivityHrs);
                    
                    
                    patientsPerWeek = [[[dictionary valueForKey:@"Demographics"] objectAtIndex:0] objectForKey:@"PatientsPerWeek"];
                    NSLog(@"Demographics PatientsPerWeek.. %@",patientsPerWeek);
                    
                    
                    comments = [[[dictionary valueForKey:@"Demographics"] objectAtIndex:0] objectForKey:@"Comments"];
                    NSLog(@"Demographics Comments.. %@",comments);
                    
//Utpal:CR#4 Demographic information for centers
                    NSArray* arrayOfCenterStrings = [[[[dictionary valueForKey:@"Demographics"] objectAtIndex:0] objectForKey:@"CentreList"] componentsSeparatedByString:@","];
                    
                    NSString *otherCentreText = [[NSString alloc] init];
                    if ([arrayOfCenterStrings containsObject:@"8"]) {
                        
                        otherCentreText = [[[dictionary valueForKey:@"Demographics"] objectAtIndex:0] objectForKey:@"OtherCentre"];
                    }
                    
                    [[NSUserDefaults standardUserDefaults]setObject:arrayOfCenterStrings forKey:@"demographicCentreArray"];
                    [[NSUserDefaults standardUserDefaults]setObject:otherCentreText forKey:@"OtherMedicalFieldText" ];
                    
                    //END
                    
                    //Deepak_carpenter: Added to remove alert while we have only one collaboration
                    self.doctorInfo.secondCollaborationDate=nil;
                    self.doctorInfo.secondCollaborationDateEnd=nil;
                    
                firstCollaborationDate = [dateFormatter dateFromString:[[[dictionary valueForKey:@"CollaborationDates"] objectAtIndex:0] objectForKey:@"StartDate"]];
                    
                firstCollaborationDateEnd = [dateFormatter dateFromString:[[[dictionary valueForKey:@"CollaborationDates"] objectAtIndex:0] objectForKey:@"EndDate"]];

//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
                    toExtendGracePeriodFirstWeek = [[[[dictionary valueForKey:@"CollaborationDates"] objectAtIndex:0] objectForKey:@"ExtendedGracePeriod"]integerValue];
                    
                    if (toExtendGracePeriodFirstWeek == 1) {
                        
                        [blockDelegate setNeedToExtendGracePeriod:YES];
                        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey: @"ExtendTheGrace"];

                    }
                    else {
                        
                        [blockDelegate setNeedToExtendGracePeriod:NO];
                        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey: @"ExtendTheGrace"];

                        
                    }
                    
                    self.doctor = [self fetchOrCreateDoctor];
                    BOOL isNecessaryToDeleteData = NO;
                    BOOL isFirstWeekData = NO;
                    
//Ravi_Bukka: CR#2 Collaboration changed
                    if (![[[[dictionary valueForKey:@"CollaborationDates"] objectAtIndex:0] objectForKey:@"StartDate"] isEqualToString:[dateFormatter stringFromDate:self.doctor.doctorInfo.firstCollaborationDate]]) {
                        
                        NSLog(@"Collaboration changes");
                        
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey: @"fireInGrace"];
                        
                    }
                    
                     NSLog(@" firstCollaborationDate %@",self.doctor.doctorInfo.firstCollaborationDate);
                    if (self.doctor.doctorInfo.firstCollaborationDate == nil) {
                        self.doctor.doctorInfo.firstCollaborationDate = firstCollaborationDate;
                        NSLog(@" firstCollaborationDate %@",self.doctor.doctorInfo.firstCollaborationDate);
                    } else if ([self.doctor.doctorInfo.firstCollaborationDate compare:firstCollaborationDate] != NSOrderedSame) {
                        self.doctor.doctorInfo.firstCollaborationDate = firstCollaborationDate;
                        isNecessaryToDeleteData = YES;
                        isFirstWeekData = YES;
                    }
                    if (self.doctor.doctorInfo.firstCollaborationDateEnd == nil) {
                        self.doctor.doctorInfo.firstCollaborationDateEnd = firstCollaborationDateEnd;
                    } else if ([self.doctor.doctorInfo.firstCollaborationDateEnd compare:firstCollaborationDateEnd] != NSOrderedSame) {
                        self.doctor.doctorInfo.firstCollaborationDateEnd = firstCollaborationDateEnd;
                    }
                    //Utpal: CR#4 For Demographics - Start
                    
                    if (self.doctor.age == [NSNumber numberWithInt:0]) {
                        self.doctor.age = [NSNumber numberWithInt:age.intValue];
                    }
                    
                    if (self.doctor.graduationYear == nil) {
                        self.doctor.graduationYear = graduationYear;
                    }
                    if (self.doctor.doctorInfo.email == nil) {
                        self.doctor.doctorInfo.email = email;
                    }
                    
                    if (self.doctor.doctorInfo.weeklyActivityHours == [NSNumber numberWithInt:0]) {
                        self.doctor.doctorInfo.weeklyActivityHours = [NSNumber numberWithInt:weeklyActivityHrs.intValue];
                    }
                    
                    if (self.doctor.doctorInfo.averageWeeklyPatients == [NSNumber numberWithInt:0]) {
                        self.doctor.doctorInfo.averageWeeklyPatients = [NSNumber numberWithInt:patientsPerWeek.intValue];
                    }
                    
                    if (self.doctor.doctorInfo.comments == nil) {
                        self.doctor.doctorInfo.comments = comments;
                    }
                    //End
                    

                                       
                    if (isNecessaryToDeleteData) {
                        NSSet* patients = [NSSet setWithSet:self.doctor.doctorInfo.patients];
                        for (Patient* patient in patients) {
                            BOOL dateIsEqualOrBiggerThanFirstCollaborationDate = [patient.visitDate compare:self.doctor.doctorInfo.firstCollaborationDate] == (NSOrderedDescending | NSOrderedSame);
                            BOOL dateIsEqualOrSmallerThanFirstCollaborationDateEnd = [patient.visitDate compare:self.doctor.doctorInfo.firstCollaborationDateEnd] == (NSOrderedAscending | NSOrderedSame);
                            BOOL patientIsInFirstWeek = (dateIsEqualOrBiggerThanFirstCollaborationDate && dateIsEqualOrSmallerThanFirstCollaborationDateEnd);
                            
                            if (isFirstWeekData && patientIsInFirstWeek) {
                                [[EntityFactory sharedEntityFactory] deleteObject:patient];
                            } else if (!patientIsInFirstWeek) {
                                [[EntityFactory sharedEntityFactory] deleteObject:patient];
                            }
                        }
                    }
                    break;
                }
                case 2:{
                  firstCollaborationDate = [dateFormatter dateFromString:[[[dictionary valueForKey:@"CollaborationDates"] objectAtIndex:0] objectForKey:@"StartDate"]];
                    
                  firstCollaborationDateEnd = [dateFormatter dateFromString:[[[dictionary valueForKey:@"CollaborationDates"] objectAtIndex:0] objectForKey:@"EndDate"]];
                    
                    secondCollaborationDate = [dateFormatter dateFromString:[[[dictionary valueForKey:@"CollaborationDates"] objectAtIndex:1] objectForKey:@"StartDate"]];
                    
                    secondCollaborationDateEnd = [dateFormatter dateFromString:[[[dictionary valueForKey:@"CollaborationDates"] objectAtIndex:1] objectForKey:@"EndDate"]];
                    self.doctor = [self fetchOrCreateDoctor];
                    BOOL isNecessaryToDeleteData = NO;
                    BOOL isFirstWeekData = NO;
                    BOOL isSecondWeekData = NO;
                    if (self.doctor.doctorInfo.firstCollaborationDate == nil) {
                        self.doctor.doctorInfo.firstCollaborationDate = firstCollaborationDate;
                    } else if ([self.doctor.doctorInfo.firstCollaborationDate compare:firstCollaborationDate] != NSOrderedSame) {
                        self.doctor.doctorInfo.firstCollaborationDate = firstCollaborationDate;
                        isNecessaryToDeleteData = YES;
                        isFirstWeekData = YES;
                    }
                    if (self.doctor.doctorInfo.firstCollaborationDateEnd == nil) {
                        self.doctor.doctorInfo.firstCollaborationDateEnd = firstCollaborationDateEnd;
                    } else if ([self.doctor.doctorInfo.firstCollaborationDateEnd compare:firstCollaborationDateEnd] != NSOrderedSame) {
                        self.doctor.doctorInfo.firstCollaborationDateEnd = firstCollaborationDateEnd;
                    }
                    if (self.doctor.doctorInfo.secondCollaborationDate == nil) {
                        self.doctor.doctorInfo.secondCollaborationDate = secondCollaborationDate;
                    } else if ([self.doctor.doctorInfo.secondCollaborationDate compare:secondCollaborationDate] != NSOrderedSame) {
                        self.doctor.doctorInfo.secondCollaborationDate = secondCollaborationDate;
                        isNecessaryToDeleteData = YES;
                        isSecondWeekData = YES;
                    }
                    if (self.doctor.doctorInfo.secondCollaborationDateEnd == nil) {
                        self.doctor.doctorInfo.secondCollaborationDateEnd = secondCollaborationDateEnd;
                    } else if ([self.doctor.doctorInfo.secondCollaborationDateEnd compare:secondCollaborationDateEnd] != NSOrderedSame) {
                        self.doctor.doctorInfo.secondCollaborationDateEnd = secondCollaborationDateEnd;
                    }
                    
                    if (isNecessaryToDeleteData) {
                        NSSet* patients = [NSSet setWithSet:self.doctor.doctorInfo.patients];
                        for (Patient* patient in patients) {
                            BOOL dateIsEqualOrBiggerThanFirstCollaborationDate = [patient.visitDate compare:self.doctor.doctorInfo.firstCollaborationDate] == (NSOrderedDescending | NSOrderedSame);
                            BOOL dateIsEqualOrSmallerThanFirstCollaborationDateEnd = [patient.visitDate compare:self.doctor.doctorInfo.firstCollaborationDateEnd] == (NSOrderedAscending | NSOrderedSame);
                            BOOL patientIsInFirstWeek = (dateIsEqualOrBiggerThanFirstCollaborationDate && dateIsEqualOrSmallerThanFirstCollaborationDateEnd);
                            BOOL dateIsEqualOrBiggerThanSecondCollaborationDate = [patient.visitDate compare:self.doctor.doctorInfo.secondCollaborationDate] == (NSOrderedDescending | NSOrderedSame);
                            BOOL dateIsEqualOrSmallerThanSecondCollaborationDateEnd = [patient.visitDate compare:self.doctor.doctorInfo.secondCollaborationDateEnd] == (NSOrderedAscending | NSOrderedSame);
                            BOOL patientIsInSecondWeek = (dateIsEqualOrBiggerThanSecondCollaborationDate && dateIsEqualOrSmallerThanSecondCollaborationDateEnd);
                            if ((isFirstWeekData && patientIsInFirstWeek) || (isSecondWeekData && patientIsInSecondWeek)) {
                                [[EntityFactory sharedEntityFactory] deleteObject:patient];
                            } else if (!patientIsInFirstWeek && !patientIsInSecondWeek) {
                                [[EntityFactory sharedEntityFactory] deleteObject:patient];
                            }
                        }
                    }

                    
                    break;
                }
               
                default:
                    break;
            }
        

        
        NSString* sessionToken = [dictionary valueForKey:@"SessionToken"];
        NSLog(@"sessionToken: -- %@", sessionToken);
       [[NSUserDefaults standardUserDefaults]
             setObject:sessionToken forKey:@"SessionToken"];
        

        
                [[EntityFactory sharedEntityFactory] saveChanges];
        
        [self allowAccessToPatientsForDoctor:self.doctor];
        
        // Update password hash (this will ensure doctor will be saved with current password and avoid issues with offline login)
        self.doctor.passwordHash = [self.password md5];
 
        [self setLoading:NO];
        [self loginResponse:YES];
        self.loginRequest = nil;
            
        }
        
     else {
         if ([[dictionary valueForKey:@"Result"]stringValue]==NULL) {
             [self displayAlert:NSLocalizedString(@"SERVER_RESP", nil)];
             [self loginResponse:NO];
         }
         else{
         [self displayAlert:NSLocalizedString([[dictionary valueForKey:@"Result"] stringValue], nil)];
         [self loginResponse:NO];
         }
     }
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        
        NSLog(@"Login Error %@", error);
        
        NSLog(@"Login Error code %d", [error code]);
        
        [self setLoading:NO];
        
        // Service returned error
        if ([[error domain] isEqualToString:kBaseRequestErrorDomain]) {
            NSInteger errorCode = [error code];
            if (errorCode == kCollaborationDateFinishErrorCode) {
                [self removeInformationWhenCollaborationEnds];
                [self displayAlert:error.localizedDescription];
                NSLog(@"error 1");
            } else if (errorCode == kConnectionErrorCode) {
                // Network error with online login, try offline login
                                NSLog(@"error 2");
                [self checkLoginOffline];
            } else {
                [self displayAlert:error.localizedDescription];
            }
            [self loginResponse:NO];
        }
        else {
            // Network error with online login, try offline login
                            NSLog(@"error 3");
            Doctor* doctor = [self fetchDoctor];
            if (doctor != nil) {
                BOOL loginOk = [doctor.passwordHash isEqualToString:[password md5]];
                if (loginOk) {
                    [self allowAccessToPatientsForDoctor:doctor];
                    self.doctor = doctor;
                    [self checkPatientsOutOfDateForDoctor:self.doctor];
                } else {
                    [self displayAlert:NSLocalizedString(@"LOGIN_USER_FAILMSG", nil)];
                }
                [self loginResponse:loginOk];
            }
            else {
                [self displayAlert:error.localizedDescription];
                [self loginResponse:NO];
            }
        }
        self.loginRequest = nil;
    }];
    [request startAsynchronous];

    
}

//Ravi_Bukka: Added Method to encrypt the password to SHA 1

- (NSString *)sha1:(NSString *)str {
    const char *cStr = [str UTF8String];
    unsigned char result[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(cStr, strlen(cStr), result);
    NSString *s = [NSString  stringWithFormat:
                   @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                   result[0], result[1], result[2], result[3], result[4],
                   result[5], result[6], result[7],
                   result[8], result[9], result[10], result[11], result[12],
                   result[13], result[14], result[15],
                   result[16], result[17], result[18], result[19]
                   ];
  
    
    return s;
}

- (void)checkLogin {
    // If is the first login in application, remove previous data stored in server
    BOOL removePreviousData = NO; //[self fetchDoctor] == nil;
    [self loginWithUserName:self.userName password:self.password removePreviousData:removePreviousData];
}

- (void)checkLoginOffline {
    Doctor* doctor = [self fetchDoctor];
    self.firstTime = doctor == nil;
    
#ifdef __OFFLINE_VERSION__
    if ([self isFirstTime]) {
        NSDate *now = [NSDate date];
        // set up date components
        NSDateComponents *components = [[[NSDateComponents alloc] init] autorelease];
        [components setDay:6];
        NSDate *firstDateEnd = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:now options:0];
        [components setDay:7];
        NSDate *secondDate = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:now options:0];
        [components setDay:13];
        NSDate *secondDateEnd = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:now options:0];
        
        doctor = [self createDoctorWithUserName:self.userName andPassword:kDefaultPassword];
        doctor.doctorInfo.firstCollaborationDate = [NSDate date];
        doctor.doctorInfo.firstCollaborationDateEnd = firstDateEnd;
        doctor.doctorInfo.secondCollaborationDate = secondDate;
        doctor.doctorInfo.secondCollaborationDateEnd = secondDateEnd;
    }
#endif
    
    NSString* passwordHash = [self.password md5];
    if ([passwordHash isEqualToString:doctor.passwordHash]) {
        self.doctor = doctor;
        [self allowAccessToPatientsForDoctor:self.doctor];
        [self loginResponse:YES];
    }
    else {
        [self displayAlert:NSLocalizedString(@"LOGIN_USER_FAILMSG",nil)];
        [self loginResponse:NO];
    }
}

- (void)hideKeyboard {
    [TextContrasena resignFirstResponder];
    [TextUsuario resignFirstResponder];
    [TextConfirmeNuevaContrasena resignFirstResponder];
    [TextContrasenaActual resignFirstResponder];
    [TextNuevaContrasena resignFirstResponder];
}

-(void) switchActiveView {
    [self hideKeyboard];
    
    //Paso de la ventana de login a la de cambiar contraseña y viceversa
    VistaCambiarContrasena.alpha=1;
    VistaLogin.alpha=1;
    
    [UIView transitionFromView:((_EstadoVentana==LOGIN) ? VistaLogin : VistaCambiarContrasena)
                        toView:((_EstadoVentana==LOGIN) ? VistaCambiarContrasena : VistaLogin)
                      duration:0.5
                       options:((_EstadoVentana==LOGIN) ? UIViewAnimationOptionTransitionFlipFromRight :
                                UIViewAnimationOptionTransitionFlipFromLeft)
                    completion:^(BOOL finished){
                        if (_EstadoVentana==LOGIN) {
                            VistaLogin.alpha=0;
                            DLog(@"VistaLogin alpha 0");
                            //[TextContrasenaActual becomeFirstResponder];
                            _EstadoVentana=CAMBIOCONTRASENA;
                        }    
                        else {
                            VistaCambiarContrasena.alpha=0;
                            DLog(@"VistaCambiarContrasena alpha 0");
                            _EstadoVentana=LOGIN;
                        }
                    }];
}

- (IBAction)BotonIrACambiarContrasenaTouchUpInside:(id)sender {
    [self switchActiveView];
}

-(void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event
{
    //Si está editando la contraseña y pulsa en el fondo vuelvo a login
    UITouch *cTouch = [touches anyObject];
    CGPoint position = [cTouch locationInView:self.view];  
    UIView *Vista1 = [self.view hitTest:position withEvent:nil];//Miro el primero obj con el que toca
    if (Vista1==self.view)
  	  if (_EstadoVentana==CAMBIOCONTRASENA)//Esta Cambiando la contraseña
          // Si el teclado está visible no puedo llamar directamente al IBAction porque en el resign me ejecuta dos veces lo mismo
          if (_TecladoVisible) {
             [TextContrasenaActual becomeFirstResponder]; 
             [TextContrasenaActual resignFirstResponder];   
          }
//          else [self BotonIrACambiarContrasenaTouchUpInside:nil];
}
/**********************************
 *UITextField
 **********************************/

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    //LOGIN--------------------------------------------------------------
    if (textField == TextContrasena) {
        [textField resignFirstResponder];
        [self loginButtonClicked];
    } 
    else if (textField==TextUsuario) {
        [TextContrasena becomeFirstResponder];
    }
    //CAMBIO CONTRASEÑA--------------------------------------------------
    else if (textField==TextContrasenaActual) {
        [TextNuevaContrasena becomeFirstResponder];
    }
    else if (textField==TextNuevaContrasena) {
        [TextConfirmeNuevaContrasena becomeFirstResponder];
    }
    else if (textField==TextConfirmeNuevaContrasena) {
        [self requestPasswordChange];
    }
    return YES;
}

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString* str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    textField.text = str;
    if (textField==TextUsuario) {
        BotonIrACambiarContrasena.enabled = str.length > 0;
    }
    
    BotonComprobarLogin.enabled = TextContrasena.text.length > 0 && TextUsuario.text.length > 0;
    
    // Already assigned the text change
    return NO;
}




/**********************************
 *Ocultar/Mostrar Teclado 
 **********************************/

-(void) keyboardWillShow:(NSNotification*)aNotification
{
    if (!_TecladoVisible) {
        _TecladoVisible=YES;
        //Coloco el cuadro para que se vean los botones
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        // iv is a UIImageView whose image is Mars.png
        CGRect frame=VistaLogin.frame;
        frame.origin.y=frame.origin.y-31;
        [VistaLogin setFrame:frame];
        frame=VistaCambiarContrasena.frame;
        frame.origin.y=frame.origin.y-31;
        [VistaCambiarContrasena setFrame:frame];
        [UIView commitAnimations];
    }
}

-(void) keyboardWillHide:(NSNotification*)aNotification
{
    if (_TecladoVisible) {
        _TecladoVisible=NO;
        //Coloco el cuadro para que se vean los botones
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        // iv is a UIImageView whose image is Mars.png
        CGRect frame=VistaLogin.frame;
        frame.origin.y=frame.origin.y+31;
        [VistaLogin setFrame:frame];
        frame=VistaCambiarContrasena.frame;
        frame.origin.y=frame.origin.y+31;
        [VistaCambiarContrasena setFrame:frame];
        [UIView commitAnimations];
    }
}

-(IBAction) hidePasswordChange
{
    _TecladoVisible=NO;
    //Si está editando la contraseña y oculta el teclado vuelvo a Login
    if (_EstadoVentana==CAMBIOCONTRASENA)//Esta Cambiando la contraseña
      [self BotonIrACambiarContrasenaTouchUpInside:nil];
    
    //Devuelvo el cuadro a su sitio
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    // iv is a UIImageView whose image is Mars.png
    [UIView commitAnimations];
}

//Ravi_Bukka: webservice1 need to be integrated at loginButtonClicked
- (IBAction)loginButtonClicked
{
    
//Ravi_Bukka: Added to get country code.
/* this will give country language code based on region format setting in internation section of simulator settings */
//    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
//    NSLocale *currentLocale = [NSLocale currentLocale];
//    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
//    NSString *countryLanguageCode = [currentLocale objectForKey:NSLocaleLanguageCode];
//    
//    NSLog(@"Selected country  code %@", countryCode);
//    NSLog(@"Selected Language code %@", countryLanguageCode);
    
    //NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    
     NSLog(@"Selected Language last code %@", language);
    
//    NSLocale *locale = [NSLocale autoupdatingCurrentLocale];
//    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
//    NSString *countryName = [locale displayNameForKey: NSLocaleCountryCode value: countryCode];
//    NSLog(@"countryName %@", countryName);
    
    // To avoid duplicate requests
    if (!_checkingLogin) {
        _checkingLogin = YES;
        
        // Disable login button while loading
        BotonComprobarLogin.enabled = NO;
        
        // Show network activity
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        // Ocultamos el teclado
        [TextUsuario resignFirstResponder];
        [TextContrasena resignFirstResponder];
        
        self.userName = TextUsuario.text;
        self.password = TextContrasena.text;
        
        if (_userName.length > 0) {
#ifdef __OFFLINE_VERSION__
            [self checkLoginOffline];
#else
            [self checkLogin];
#endif
        }
        else {
            [self displayAlert:NSLocalizedString(@"LOGIN_USER_MISSING",nil)];
            _checkingLogin = NO;
        }
    }
}


- (IBAction)forgotPasswordButtonClicked:(id)sender {
    
//Ravi_Bukka: Commented below line to implement new service and remove the TwincodeLibrary dependency
/*    if (self.TextUsuario.text.length > 0) {
        self.BotonAyuda.enabled = NO;
        [self.forgotPasswordRequest cancel];
        self.forgotPasswordRequest = nil;
        self.forgotPasswordRequest = [[RequestFactory sharedInstance] createRemindPasswordRequestWithDoctorId:TextUsuario.text onComplete:^{
            self.BotonAyuda.enabled = YES;
            [self displayAlert:NSLocalizedString(@"LOGIN_REMEMBER_PASSWORD_MSG", @"")];
        } onError:^(NSError *error) {
            self.BotonAyuda.enabled = YES;
            [self displayAlert:error.localizedDescription];
        }];
        [self.forgotPasswordRequest start];
    } else {
        [self displayAlert:NSLocalizedString(@"LOGIN_REMEMBER_PASSWORD_MSG_NECESSARY", @"")];
    } */

    
//Ravi_Bukka: Added below line to implement new service and remove the TwincodeLibrary dependency
    
    if (self.TextUsuario.text.length > 0) {
        self.BotonAyuda.enabled = NO;
        [self.forgotPasswordRequest cancel];
        self.forgotPasswordRequest = nil;
        
        NSString *countryCode = @"GR";
        NSString *responseFormat = @"JSON";
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@%@",kBaseURL,kRemindPassword,countryCode];
        urlString = [urlString stringByAppendingFormat:@"&username=%@&responseFormat=%@",TextUsuario.text,responseFormat];
        
    
        NSURL *url = [NSURL URLWithString:urlString];
        __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setCompletionBlock:^{
            
            NSData *responseData = [request responseData];
            
            id dictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:nil];
            
            if ([[[dictionary valueForKey:@"Result"]stringValue] isEqualToString:@"0"]) {
            
            self.BotonAyuda.enabled = YES;
            
            [self displayAlert:NSLocalizedString(@"LOGIN_REMEMBER_PASSWORD_MSG", @"")];
            
            }
            else {
                [self displayAlert:NSLocalizedString([[dictionary valueForKey:@"Result"] stringValue], nil)];
                //         [self setLoading:NO];
                //         [self loginResponse:YES];
                //
                //          self.loginRequest = nil;
                [self loginResponse:NO];
            }
            
        }];
        [request setFailedBlock:^{
            NSError *error = [request error];
            
            NSLog(@"Login Error %@", error);
            
            NSLog(@"Login Error code %d", [error code]);

            self.BotonAyuda.enabled = YES;
            [self displayAlert:error.localizedDescription];
        }];
            [request startAsynchronous];
        [self.forgotPasswordRequest start];
    } else {
        
        [self displayAlert:NSLocalizedString(@"LOGIN_REMEMBER_PASSWORD_MSG_NECESSARY", @"")];
    }  

}

-(void) displayAlert:(NSString*)message {
    [self.TextUsuario resignFirstResponder];
    [self.TextContrasena resignFirstResponder];
    if (self.choiceController != nil) {
        [_delegate dismissModalController:self.choiceController];
    }
    self.choiceController = [[MultipleChoiceViewController new] autorelease];

  // Ravi_Bukka: Added for localizing error messages
    
    _choiceController.message = NSLocalizedString(message,nil);
    _choiceController.messageFont = [UIFont boldSystemFontOfSize:32];
    _choiceController.acceptButtonTitle = nil;
    _choiceController.cancelButtonTitle = NSLocalizedString(@"CHANGE_PASSWORD_ERROR_BUTTON", @"");
    _choiceController.otherButtonTitle = nil;
    _choiceController.eventBlock = ^(MultipleChoiceButton button){        
        // Dismiss and release the controller
        [_delegate dismissModalController:self.choiceController];
        self.choiceController = nil;
    };
    
    // Present modal controller
    [_delegate presentModalController:self.choiceController];
}

-(void) setLoading:(BOOL)loading {
    //Deepak Carpenter : Added Cause Crashing issue
     if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"]) {
    [self setLoading:loading withMessage:@"Φορτώνει..."];
     } else if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {
         [self setLoading:loading withMessage:@"Ładowanie..."];
     }
     else if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"en"]) {
         [self setLoading:loading withMessage:@"Loading..."];
     }
   
    else
        
        
        [self setLoading:loading withMessage:NSLocalizedString(@"LOADING_MESSAGE", nil)];
    
  
}
-(void) setLoading:(BOOL)loading withMessage:(NSString*)message {
    if (loading) {
        if (_loadingViewController == nil) {
            _loadingViewController = [[LoadingDialogViewController alloc] init];
        }
        _loadingViewController.text = message;
        // Present modal controller
        [_delegate presentModalController:self.loadingViewController];
    } else {
        // Hide modal controller
        [_delegate dismissModalController:self.loadingViewController];
    }
}

-(void) requestPasswordChange {
    [self hideKeyboard];
    
#ifdef __OFFLINE_VERSION__
    return;
#endif
    
    
    //Ravi_Bukka: Added below line to implement new service and remove the TwincodeLibrary dependency
    // Check passwords
    NSString* userName = TextUsuario.text;
    NSString* prevPassword = [self sha1:TextContrasenaActual.text];
    NSString* password = TextNuevaContrasena.text;
    NSString* repeatPassword = TextConfirmeNuevaContrasena.text;
    // Check if password is empty
    if (prevPassword.length > 0 && password.length > 0 && repeatPassword.length > 0) {
        // Check repeated password
        if ([password isEqualToString:repeatPassword]) {
            [self setLoading:YES withMessage:NSLocalizedString(@"LOADING_PASSWORD_CHANGE", nil)];
            
            NSString *countryCode = @"GR";
            NSString *responseFormat = @"JSON";
            
            NSString *urlString = [NSString stringWithFormat:@"%@%@%@",kBaseURL,kChangePassword,countryCode];
            urlString = [urlString stringByAppendingFormat:@"&username=%@&OldPassword=%@&NewPassword=%@&responseFormat=%@",userName,prevPassword,password,responseFormat];

            
            NSURL *url = [NSURL URLWithString:urlString];
            __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
            [request setCompletionBlock:^{
                
                NSData *responseData = [request responseData];
                
                id dictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:nil];
                
                if ([[[dictionary valueForKey:@"Result"]stringValue] isEqualToString:@"0"]) {

                
                [self setLoading:NO];
                TextContrasenaActual.text = @"";
                TextNuevaContrasena.text = @"";
                TextConfirmeNuevaContrasena.text = @"";
                Doctor* doctor = [self fetchDoctor];
                doctor.passwordHash = [password md5];
                [[EntityFactory sharedEntityFactory] saveChanges];
                DLog(@"Password successfuly changed");
                [self switchActiveView];
                [self displayAlert:NSLocalizedString(@"CHANGE_PASSWORD_SUCCESS_MESSAGE", nil)];
                }
                
                else {
                    
                    [self displayAlert:NSLocalizedString([[dictionary valueForKey:@"Result"] stringValue], nil)];
                    //         [self setLoading:NO];
                    //         [self loginResponse:YES];
                    //
                    //          self.loginRequest = nil;
                    [self loginResponse:NO];
                    
                }
            }];
            [request setFailedBlock:^{
                [self setLoading:NO];
                NSError *error = [request error];
                
                NSLog(@"Login Error %@", error);
                
                NSLog(@"Login Error code %d", [error code]);
                [self displayAlert:error.localizedDescription];
            }];
            [request startAsynchronous];
            [self.changePasswordRequest start];
        } else {
            [self displayAlert:NSLocalizedString(@"CHANGE_PASSWORD_REPEAT_PASSWORD_ERROR_MESSAGE", nil)];
        }
    }
    else {
        [self displayAlert:NSLocalizedString(@"CHANGE_PASSWORD_EMPTY_PASSWORD_ERROR_MESSAGE", nil)];
    }
    
    //Ravi_Bukka: Commented below line to implement new service and remove the TwincodeLibrary dependency
    // Check passwords
    /*    NSString* userName = TextUsuario.text;
     NSString* prevPassword = TextContrasenaActual.text;
     NSString* password = TextNuevaContrasena.text;
     NSString* repeatPassword = TextConfirmeNuevaContrasena.text;
     // Check if password is empty
     if (prevPassword.length > 0 && password.length > 0 && repeatPassword.length > 0) {
     // Check repeated password
     if ([password isEqualToString:repeatPassword]) {
     [self setLoading:YES withMessage:NSLocalizedString(@"LOADING_PASSWORD_CHANGE", nil)];
     self.changePasswordRequest = [[RequestFactory sharedInstance] createChangePasswordRequestWithUserName:userName prevPassword:prevPassword password:password onComplete:^{
     [self setLoading:NO];
     TextContrasenaActual.text = @"";
     TextNuevaContrasena.text = @"";
     TextConfirmeNuevaContrasena.text = @"";
     Doctor* doctor = [self fetchDoctor];
     doctor.passwordHash = [password md5];
     [[EntityFactory sharedEntityFactory] saveChanges];
     DLog(@"Password successfuly changed");
     [self switchActiveView];
     [self displayAlert:NSLocalizedString(@"CHANGE_PASSWORD_SUCCESS_MESSAGE", nil)];
     } onError:^(NSError *error) {
     [self setLoading:NO];
     [self displayAlert:error.localizedDescription];
     }];
     [self.changePasswordRequest start];
     } else {
     [self displayAlert:NSLocalizedString(@"CHANGE_PASSWORD_REPEAT_PASSWORD_ERROR_MESSAGE", nil)];
     }
     }
     else {
     [self displayAlert:NSLocalizedString(@"CHANGE_PASSWORD_EMPTY_PASSWORD_ERROR_MESSAGE", nil)];
     } */
}
//Ravi_Bukka: webservice3 need to be integrated at loginButtonClicked
- (IBAction)changePasswordButtonClicked:(id)sender {
    [self requestPasswordChange];
}


/**********************************
 *Métodos de la clase 
 **********************************/

- (id)initWithDelegate:(id<LoginViewControllerDelegate>)delegate
{
    self = [super initWithNibName:@"LoginViewController" bundle:nil];
    if (self) {
        _delegate = delegate;
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewWillAppear:(BOOL)animated {
    
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    // Clean password TextField
    TextContrasena.text = @"";
//    [TextContrasena setReturnKeyType:UIReturnKeyNext];
    BotonComprobarLogin.enabled = NO;
    
    
    
//Ravi_Bukka: Added below lines for localizing the application
    

    
    TextUsuario.placeholder = NSLocalizedString(@"LOGIN_USER_FIELD", nil);
    TextContrasena.placeholder = NSLocalizedString(@"LOGIN_USER_PWD", nil);
    TextContrasenaActual.placeholder = NSLocalizedString(@"LOGIN_USER_CURRENT_PASSWORD", nil);
    TextNuevaContrasena.placeholder = NSLocalizedString(@"LOGIN_USER_NEW_PASSWORD", nil);
    TextConfirmeNuevaContrasena.placeholder = NSLocalizedString(@"LOGIN_USER_CONFIRM_PASSWORD", nil);
    
    [BotonAyuda setTitle:NSLocalizedString(@"LOGIN_USER_FORGET_PASSWORD", nil) forState:UIControlStateNormal];
    [BotonAyuda setTitle:NSLocalizedString(@"LOGIN_USER_FORGET_PASSWORD", nil) forState:UIControlStateHighlighted];
    
    [BotonIrACambiarContrasena setTitle:NSLocalizedString(@"LOGIN_USER_CHANGE_PASSWORD", nil) forState:UIControlStateNormal];
    [BotonIrACambiarContrasena setTitle:NSLocalizedString(@"LOGIN_USER_CHANGE_PASSWORD", nil) forState:UIControlStateHighlighted];
    [BotonIrACambiarContrasena setTitle:NSLocalizedString(@"LOGIN_USER_CHANGE_PASSWORD", nil) forState:UIControlStateDisabled];
    
    
    [BotonCambiarContrasena setTitle:NSLocalizedString(@"LOGIN_USER_CHANGE_PASSWORD", nil) forState:UIControlStateNormal];
    [BotonCambiarContrasena setTitle:NSLocalizedString(@"LOGIN_USER_CHANGE_PASSWORD", nil) forState:UIControlStateHighlighted];
    
    [BotonComprobarLogin setTitle:NSLocalizedString(@"LOGIN_USER", nil) forState:UIControlStateNormal];
    [BotonComprobarLogin setTitle:NSLocalizedString(@"LOGIN_USER", nil) forState:UIControlStateHighlighted];
    
    
    
    TextUsuario.adjustsFontSizeToFitWidth = YES;
    TextContrasena.adjustsFontSizeToFitWidth = YES;
    TextContrasenaActual.adjustsFontSizeToFitWidth = YES;
    TextNuevaContrasena.adjustsFontSizeToFitWidth = YES;
    TextConfirmeNuevaContrasena.adjustsFontSizeToFitWidth = YES;
    
    BotonAyuda.titleLabel.adjustsFontSizeToFitWidth = YES;
    BotonIrACambiarContrasena.titleLabel.adjustsFontSizeToFitWidth = YES;
    BotonCambiarContrasena.titleLabel.adjustsFontSizeToFitWidth = YES;
    BotonComprobarLogin.titleLabel.adjustsFontSizeToFitWidth = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}



- (void)viewDidLoad
{
    [super viewDidLoad];
   
    

    
    TextUsuario.delegate=self;
    [TextUsuario setReturnKeyType:UIReturnKeyNext];
    NSString* lastUser = [[NSUserDefaults standardUserDefaults] valueForKey:@"LAST_USER"];
    if (lastUser!=nil) TextUsuario.text = lastUser;
    if ([TextUsuario.text length]>0)//Hay algo en el usuario, dejo cambiar la contraseña
        BotonIrACambiarContrasena.enabled=YES;
    else BotonIrACambiarContrasena.enabled=NO;
    
    TextContrasena.delegate=self;
//    [TextContrasena setReturnKeyType:UIReturnKeyNext];

    BotonComprobarLogin.enabled = NO;
    
    TextContrasenaActual.delegate=self;
    TextNuevaContrasena.delegate=self;
    TextConfirmeNuevaContrasena.delegate=self;
    
    _checkingLogin = NO;
   
    //Preparo las vistas
    VistaCambiarContrasena.alpha=0;
    [VistaCambiarContrasena setBackgroundColor:[UIColor clearColor]];
    CGRect frame=VistaCambiarContrasena.frame;
    frame.origin.x=(self.view.frame.size.width-frame.size.width) / 2;
    [VistaCambiarContrasena setFrame:frame];
   
    VistaLogin.alpha=1;
    [VistaLogin setBackgroundColor:[UIColor clearColor]];
    frame=VistaLogin.frame;
    frame.origin.x=(self.view.frame.size.width-frame.size.width) / 2;
    [VistaLogin setFrame:frame];
    _EstadoVentana=LOGIN;//VentanaLogin
    
    self.appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey];
    
#if defined(STAGING)
    self.appVersionLabel.text = [NSString stringWithFormat:@"v%@ STG", _appVersion];
#else
    self.appVersionLabel.text = [NSString stringWithFormat:@"v%@", _appVersion];
#endif
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft||
    toInterfaceOrientation==UIInterfaceOrientationLandscapeRight;
}

- (void)dealloc {
    _delegate=nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    [VistaLogin release];
    [TextUsuario release];
    [TextContrasena release];
    [TextContrasenaActual release];
    [BotonAyuda release];
    [BotonCambiarContrasena release];
    
    [VistaCambiarContrasena release];
    [TextContrasenaActual release];
    [TextNuevaContrasena release];
    [TextConfirmeNuevaContrasena release];
    [BotonCambiarContrasena release];
    
    [_userName release];
    [_password release];
    [_doctor release];
    [_choiceController release];
    [_loadingViewController release];
    
    [_loginRequest cancel];
    [_loginRequest release];
    [_activateRequest cancel];
    [_activateRequest release];
    [_changePasswordRequest cancel];
    [_changePasswordRequest release];
    [_appVersionLabel release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setAppVersionLabel:nil];
    [super viewDidUnload];
}
@end
