//
//  main.m
//  IMS Deactivated
//
//  Created by Guillermo Gutiérrez on 22/03/13.
//
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
