//
//  AppDelegate.h
//  IMS Deactivated
//
//  Created by Guillermo Gutiérrez on 22/03/13.
//
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
